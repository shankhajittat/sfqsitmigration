<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JP</fullName>
        <description>JP 上長に申請メール</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_To_Manager_2</template>
    </alerts>
    <fieldUpdates>
        <fullName>ActionFieldUpdate</fullName>
        <field>Stages__c</field>
        <literalValue>Delivered Quote</literalValue>
        <name>進捗状況を進捗中にする。</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ActionFieldUpdate_d_2</fullName>
        <field>Stages__c</field>
        <literalValue>Ordered</literalValue>
        <name>進捗状況を購入決定にする</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>JPTestQuotesApprovalChangeStatus</fullName>
        <description>☆☆☆</description>
        <field>Stages__c</field>
        <literalValue>Ordered</literalValue>
        <name>JP Test Quotes Approval Change Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
