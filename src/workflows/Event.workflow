<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ActionFieldUpdate</fullName>
        <field>Subject</field>
        <name>件名を主要活動とリンク</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ActivityTypetoCon</fullName>
        <field>Subject</field>
        <name>Activity Type to Con</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ConsignmentManagement</fullName>
        <field>Subject</field>
        <name>Consignment Management</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Dealer</fullName>
        <field>Activity_With__c</field>
        <literalValue>Dealer</literalValue>
        <name>Dealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Doctor</fullName>
        <field>Activity_With__c</field>
        <literalValue>Customer</literalValue>
        <name>Doctor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EventSubjectDoctor</fullName>
        <description>Japan /Automaticary copy Event Subject &quot;Doctor&quot; to &quot;Subject&quot;</description>
        <field>Activity_With__c</field>
        <literalValue>Customer</literalValue>
        <name>Event Subject Doctor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HospitalStaff</fullName>
        <field>Activity_With__c</field>
        <literalValue>Hospital Staff</literalValue>
        <name>Hospital Staff</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ManagersComment2Copy</fullName>
        <field>Manager_s_Comment_Sub2__c</field>
        <formula>Manager_s_Comment_2__c</formula>
        <name>Manager&apos;s Comment2 Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ManagersCommentCopy</fullName>
        <field>Manager_s_Comment_Sub__c</field>
        <formula>Future_Plans__c</formula>
        <name>Manager&apos;s Comment Copy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Synthes</fullName>
        <field>Activity_With__c</field>
        <literalValue>Synthes</literalValue>
        <name>Synthes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JP Event Reply to Manager Comment</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Event.Meeting_Cancelled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>JP Manager%27s%E3%80%80Comment%E3%80%80Mail</fullName>
        <actions>
            <name>ActionTask_d_2</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <formula>AND(  ISPICKVAL( $User.Country_Region__c, &quot;Japan&quot;), OR(Manager_s_Comment_Sub__c  &lt;&gt;   Future_Plans__c, Manager_s_Comment_Sub2__c &lt;&gt; Manager_s_Comment_2__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>JP チェックをしたら上長にメール</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Event.Meeting_Cancelled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manager%27s Comment Copy</fullName>
        <actions>
            <name>ManagersComment2Copy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ManagersCommentCopy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>User.Country_Region__c</field>
            <operation>equals</operation>
            <value>Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Country_Region__c</field>
            <operation>equals</operation>
            <value>Malaysia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Country_Region__c</field>
            <operation>equals</operation>
            <value>Taiwan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TW %26 Malaysia MGR Comment Mail</fullName>
        <actions>
            <name>ManageraddCommentsinCRM</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Send notification when Manager add Comments on Activity in CRM</description>
        <formula>AND( OR(  ISPICKVAL( $User.Country_Region__c, &quot;Taiwan&quot;), ISPICKVAL( $User.Country_Region__c , &quot;Malaysia&quot;)) , OR(Manager_s_Comment_Sub__c  &lt;&gt;   Future_Plans__c, Manager_s_Comment_Sub2__c &lt;&gt; Manager_s_Comment_2__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>ActionTask</fullName>
        <assignedToType>owner</assignedToType>
        <description>日報の上長コメント欄が編集されました。
Salesforceにログインして内容を確認してください。</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>上長コメントが編集されました。</subject>
    </tasks>
    <tasks>
        <fullName>ActionTask_d_2</fullName>
        <assignedToType>owner</assignedToType>
        <description>日報の上長コメント欄が編集されました。
Salesforceにログインして内容を確認してください。
https://login.salesforce.com/


******** 下のリンクからは上長コメントは確認できません。 *********</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Event.StartDateTime</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>上長コメント欄が編集されました</subject>
    </tasks>
    <tasks>
        <fullName>ManageraddCommentsinCRM</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Manager entered comments in CRM Activity.
Please log in and check the comments.
https://login.salesforce.com/


***** Sorry, you can&apos;t check Manager&apos;s Comments from the link below. *****</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Event.StartDateTime</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Manager add Comments in CRM</subject>
    </tasks>
</Workflow>
