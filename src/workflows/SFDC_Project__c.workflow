<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Project_7_Day_Notification</fullName>
        <description>Project 7 Day Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Project_Past_7_Day_Notification</template>
    </alerts>
    <alerts>
        <fullName>Project_Past_Due</fullName>
        <description>Project Past Due</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Project_Past_Due</template>
    </alerts>
    <rules>
        <fullName>Project Past Due</fullName>
        <actions>
            <name>Project_Past_Due</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SFDC_Project__c.SFDC_Project_End_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
