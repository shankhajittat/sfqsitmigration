<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>JPContactFieldChangeToSalesEmail</fullName>
        <description>JP Contact Field Change To Sales E-mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>CMF</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Spine</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Trauma</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_Contact_Field_Change_To_Sales_E_Mail</template>
    </alerts>
    <rules>
        <fullName>JP Contact Field Change To Sales E-mail</fullName>
        <actions>
            <name>JPContactFieldChangeToSalesEmail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>5 AND 6 AND (1 OR 2 OR 3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Contact.Department__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.JP_Medical_School__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Birthdate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Hospital_Role__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Country__c</field>
            <operation>equals</operation>
            <value>Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Email</field>
            <operation>equals</operation>
            <value>Tsunomori.Naomi@Synthes.com,Nomura.Mayuka@Synthes.com,Okuya.Maki@Synthes.com,Hosaka.Miyuki@Synthes.com,Shimotamari.Tomoko@Synthes.com,Fujii.Nobuo@Synthes.com</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
