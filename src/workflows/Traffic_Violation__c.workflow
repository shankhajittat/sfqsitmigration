<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>IfthereisareportofthetrafficviolationemailtoManagerandGA</fullName>
        <description>If there is a report of the traffic violation e-mail to Manager and GA.</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPTrafficViolationNotification</template>
    </alerts>
    <alerts>
        <fullName>ManagerDenied</fullName>
        <description>申請者に承認連絡メール（Manager Denied）</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_Traffic_Violation3_Manager_Denied</template>
    </alerts>
    <alerts>
        <fullName>Managerconfirmed</fullName>
        <description>申請者に承認連絡メール（Manager confirmed）</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPTrafficViolationManagerreceived</template>
    </alerts>
    <alerts>
        <fullName>emailtoGA</fullName>
        <description>申請を人事総務部に知らせるメール（e-mail to GA）</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPTrafficViolationNotification</template>
    </alerts>
    <rules>
        <fullName>JP Traffic Violation e-mail Notification</fullName>
        <actions>
            <name>IfthereisareportofthetrafficviolationemailtoManagerandGA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.Country_Region__c</field>
            <operation>equals</operation>
            <value>Japan</value>
        </criteriaItems>
        <description>If there is a report of the traffic violation e-mail to Manager and GA.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
