<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RegistrarNotificationEmail</fullName>
        <description>Registrar Notification Email</description>
        <protected>false</protected>
        <recipients>
            <type>campaignMemberDerivedOwner</type>
        </recipients>
        <recipients>
            <recipient>contellis.steve@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rdesilva@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>swordsw1@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/RegistrarNotification</template>
    </alerts>
    <rules>
        <fullName>Registrar Hospital Notification</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Registrar_Hospital__c.End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Registrar_Hospital__c.Currently_At__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>RegistrarNotificationEmail</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Registrar_Hospital__c.End_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
