<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SFQConsignmentSetBuild_HandOff_Notification_to_ERP_AU</fullName>
        <ccEmails>AU-NSW-SetsCreation@its.jnj.com</ccEmails>
        <description>SFQConsignmentSetBuild HandOff Notification to ERP AU</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>dlopez16@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQConsignSetBuildERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQConsignmentSetBuild_HandOff_Notification_to_ERP_NZ</fullName>
        <ccEmails>dl-setcreation-NZ@its.jnj.com</ccEmails>
        <description>SFQConsignmentSetBuild HandOff Notification to ERP NZ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQConsignSetBuildERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQQuoteApprovedNotification</fullName>
        <description>SFQQuoteApprovedNotification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQQuoteApprovedNotificationV1</template>
    </alerts>
    <alerts>
        <fullName>SFQQuoteRejectedNotification</fullName>
        <description>SFQQuoteRejectedNotification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQQuoteRejectedNotificationV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_CBC_HandOff_Notification_to_ERP_AU</fullName>
        <ccEmails>supplychain_au@its.jnj.com</ccEmails>
        <description>SFQ CBC HandOff Notification to ERP AU</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thankachan.thomas@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQCBCERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_CBC_HandOff_Notification_to_ERP_NZ</fullName>
        <ccEmails>supplychain_au@its.jnj.com</ccEmails>
        <description>SFQ CBC HandOff Notification to ERP NZ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thankachan.thomas@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQCBCERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_Changed_SP_HandOff_to_ERP_AU</fullName>
        <description>SFQ Changed SP HandOff to ERP AU</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmcgreev@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQChangedSPERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_Changed_SP_HandOff_to_ERP_NZ</fullName>
        <description>SFQ Changed SP HandOff to ERP NZ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rblaza1@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQChangedSPERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_Customer_Quote_HandOff_Notification_to_ERP_AU</fullName>
        <ccEmails>depuysynthescsau@its.jnj.com</ccEmails>
        <description>SFQ Customer Quote HandOff Notification to ERP AU</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQCustomerQuoteERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_Customer_Quote_HandOff_Notification_to_ERP_NZ</fullName>
        <ccEmails>DePuySynthesCSNZ@its.jnj.com</ccEmails>
        <description>SFQ Customer Quote HandOff Notification to ERP NZ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQCustomerQuoteERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_Incomplete_CBC</fullName>
        <description>SFQ - Incomplete CBC</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQCBCIncompleteTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_Incomplete_Customer_Quote</fullName>
        <description>SFQ - Incomplete Customer Quote</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQCustomerQuoteIncompleteTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_New_SP_HandOff_Notification_to_ERP_AU</fullName>
        <description>SFQ New SP HandOff Notification to ERP AU</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmcgreev@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQNewSPERPHandOffTemplateV1</template>
    </alerts>
    <alerts>
        <fullName>SFQ_New_SP_HandOff_Notification_to_ERP_NZ</fullName>
        <description>SFQ New SP HandOff Notification to ERP NZ</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>baillie.stuart@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>humphreys.john@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rblaza1@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>totalview.ap.assist@its.jnj.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SFQ_Email_Templates/SFQNewSPERPHandOffTemplateV1</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_As_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update As Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_As_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Quote As Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_As_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Quote Status As Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_As_R2C</fullName>
        <description>Update Quote Status As Release To Customer</description>
        <field>Status</field>
        <literalValue>Released to Customer</literalValue>
        <name>Update Quote Status As R2C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_As_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Quote Status As Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_Price_Confirmed</fullName>
        <field>Status</field>
        <literalValue>Price Confirmed</literalValue>
        <name>Update Quote Status Price Confirmed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_to_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Quote Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_to_Draft</fullName>
        <field>Status</field>
        <literalValue>Draft</literalValue>
        <name>Update Quote Status As Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_to_Pending</fullName>
        <field>Status</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Quote Status to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SFQ CBC HandOff to ERP AU</fullName>
        <actions>
            <name>SFQ_CBC_HandOff_Notification_to_ERP_AU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>LEFT(Opportunity.Account.OrganizationCode__c, 2)   = &quot;AU&quot; 
 &amp;&amp;  ISPICKVAL(QuoteType__c, &quot;CBC&quot;) 
&amp;&amp;  ISPICKVAL(Status, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ CBC HandOff to ERP NZ</fullName>
        <actions>
            <name>SFQ_CBC_HandOff_Notification_to_ERP_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>LEFT(Opportunity.Account.OrganizationCode__c, 2)   = &quot;NZ&quot; 
 &amp;&amp;  ISPICKVAL(QuoteType__c, &quot;CBC&quot;) 
&amp;&amp;  ISPICKVAL(Status, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ Changed Surgeon Preference HandOff to ERP AU</fullName>
        <actions>
            <name>SFQ_Changed_SP_HandOff_to_ERP_AU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(QuoteType__c, &apos;SurgeonPreference&apos;) 
&amp;&amp;   ISPICKVAL( Status , &apos;Approved&apos;) 
&amp;&amp;  NOT( ISBLANK( Surgeon_Preference__c ))
&amp;&amp; LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;AU&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ Changed Surgeon Preference HandOff to ERP NZ</fullName>
        <actions>
            <name>SFQ_Changed_SP_HandOff_to_ERP_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(QuoteType__c, &apos;SurgeonPreference&apos;) 
  &amp;&amp;   ISPICKVAL( Status , &apos;Approved&apos;) 
  &amp;&amp;  NOT( ISBLANK( Surgeon_Preference__c ))
  &amp;&amp; LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;NZ&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ Customer Quote HandOff to ERP AU</fullName>
        <actions>
            <name>SFQ_Customer_Quote_HandOff_Notification_to_ERP_AU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;AU&quot; 
&amp;&amp; ISPICKVAL(QuoteType__c, &quot;CustomerQuote&quot;) 
&amp;&amp; ISPICKVAL(Status, &quot;Completed - Won&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ Customer Quote HandOff to ERP NZ</fullName>
        <actions>
            <name>SFQ_Customer_Quote_HandOff_Notification_to_ERP_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;NZ&quot; 
&amp;&amp; ISPICKVAL(QuoteType__c, &quot;CustomerQuote&quot;) 
&amp;&amp; ISPICKVAL(Status, &quot;Completed - Won&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ Incomplete CBC</fullName>
        <actions>
            <name>SFQ_Incomplete_CBC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Incomplete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.QuoteType__c</field>
            <operation>equals</operation>
            <value>CBC</value>
        </criteriaItems>
        <description>Fire email to notify the owner that a CBC quote has an Incomplete status. The user may not always notice the error that caused this.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ Incomplete Customer Quote</fullName>
        <actions>
            <name>SFQ_Incomplete_Customer_Quote</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Status</field>
            <operation>equals</operation>
            <value>Incomplete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.QuoteType__c</field>
            <operation>equals</operation>
            <value>CustomerQuote</value>
        </criteriaItems>
        <description>Fire email to notify the owner that a Customer quote has an Incomplete status. The user may not always notice the error that caused this.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ New Surgeon Preference HandOff to ERP AU</fullName>
        <actions>
            <name>SFQ_New_SP_HandOff_Notification_to_ERP_AU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(QuoteType__c, &apos;SurgeonPreference&apos;) 
&amp;&amp;   ISPICKVAL( Status , &apos;Approved&apos;) 
&amp;&amp;  ISBLANK( Surgeon_Preference__c )
&amp;&amp; LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;AU&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQ New Surgeon Preference HandOff to ERP NZ</fullName>
        <actions>
            <name>SFQ_New_SP_HandOff_Notification_to_ERP_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(QuoteType__c, &apos;SurgeonPreference&apos;) 
&amp;&amp;   ISPICKVAL( Status , &apos;Approved&apos;) 
&amp;&amp;  ISBLANK( Surgeon_Preference__c )
&amp;&amp; LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;NZ&quot;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQConsignmentSetBuild HandOff to ERP AU</fullName>
        <actions>
            <name>SFQConsignmentSetBuild_HandOff_Notification_to_ERP_AU</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;AU&quot; 
&amp;&amp; ISPICKVAL(QuoteType__c, &quot;SetRequest&quot;) 
&amp;&amp; ISPICKVAL(Status, &quot;Released to Customer&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SFQConsignmentSetBuild HandOff to ERP NZ</fullName>
        <actions>
            <name>SFQConsignmentSetBuild_HandOff_Notification_to_ERP_NZ</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>LEFT(Opportunity.Account.OrganizationCode__c, 2) = &quot;NZ&quot; 
&amp;&amp; ISPICKVAL(QuoteType__c, &quot;SetRequest&quot;) 
&amp;&amp; ISPICKVAL(Status, &quot;Released to Customer&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Approval_Rejection</fullName>
        <assignedToType>owner</assignedToType>
        <description>Approval Rejected by Manager</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Approval Rejection</subject>
    </tasks>
    <tasks>
        <fullName>Approved_by_Manager</fullName>
        <assignedToType>owner</assignedToType>
        <description>Approved by Manager</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Approved by Manager</subject>
    </tasks>
</Workflow>
