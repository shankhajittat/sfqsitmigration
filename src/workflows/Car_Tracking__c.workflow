<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>x3MonthEmailMaintanceNotification</fullName>
        <description>3 Month E-mail Maintance Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarTrackingThreeMonthCarCheck</template>
    </alerts>
    <alerts>
        <fullName>x3MonthEmailMaintenance</fullName>
        <description>3 Month E-mail Maintenance</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarTrackingThreeMonthCarCheck</template>
    </alerts>
    <alerts>
        <fullName>x6MNotificationforCarCheck</fullName>
        <description>6 M Notification for Car Check</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarTrackingSixMonthCarCheck</template>
    </alerts>
    <rules>
        <fullName>JP 12 M Maintenance Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Car_Tracking__c.Lease_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Japan Car Maintenance Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Car_Tracking__c.Lease_Start_Date__c</offsetFromField>
            <timeLength>345</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JP 3 M Maintenance Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Car_Tracking__c.Lease_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Japan Car Maintenance Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>x3MonthEmailMaintenance</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Car_Tracking__c.Lease_Start_Date__c</offsetFromField>
            <timeLength>75</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JP 6 M Maintenance Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Car_Tracking__c.Lease_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Japan Car Maintenance Notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>x6MNotificationforCarCheck</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Car_Tracking__c.Lease_Start_Date__c</offsetFromField>
            <timeLength>165</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>JP Car Owner Copy to User</fullName>
        <active>false</active>
        <formula>ISNULL( OwnerId) =FALSE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>x3</fullName>
        <assignedToType>owner</assignedToType>
        <description>3ヶ月点検が近づいてきました。
詳細はリース会社から送付されるハガキを確認の上点検に行ってください。</description>
        <dueDateOffset>75</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Car_Tracking__c.Lease_Start_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>New Not Started</status>
        <subject>3ヶ月点検</subject>
    </tasks>
</Workflow>
