<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ActionEmail</fullName>
        <description>承認したら人事にメール</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>oka.misato@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarAccidentNotification</template>
    </alerts>
    <alerts>
        <fullName>ActionEmail_d_2</fullName>
        <description>申請者に返信</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarAccidentNotification</template>
    </alerts>
    <alerts>
        <fullName>Itisapplicationstartmailtoanapplicant</fullName>
        <description>申請者に申請開始メール（It is application start mail to an applicant.)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_Monthly_Car_Report_1_Creator_New</template>
    </alerts>
    <alerts>
        <fullName>JPDenegationEmailtoapplicant</fullName>
        <description>申請者に否認メール（JP Denegation E-mail to applicant</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPMonthlyCarReport_3</template>
    </alerts>
    <alerts>
        <fullName>JPMonthlyCarReportStatusCheck</fullName>
        <description>JP Monthly Car Report Status Check</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_Monthly_Car_Report_0_1_Creator_Alarm</template>
    </alerts>
    <alerts>
        <fullName>JPMonthlyCarReport_0CreatorAlarm</fullName>
        <description>JP Monthly Car Report_0-Creator Alarm</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_Monthly_Car_Report_0_Creator_Alarm</template>
    </alerts>
    <fieldUpdates>
        <fullName>StatuschangetoCreateOnly</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Create Only</literalValue>
        <name>承認状況を作成のみに(Status change to Create Only)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Statuschangetoapproved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approval</literalValue>
        <name>承認状況を承認に(Status change to approved)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Statuschangetodenegation</fullName>
        <field>Approval_Status__c</field>
        <literalValue>denegation</literalValue>
        <name>承認状況を否認に(Status change to denegation)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Statuschangetounapproved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>nonapproved</literalValue>
        <name>承認状況を申請中にする(Status change to unapproved)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>JP Monthly Car Report Save Mail To Creater</fullName>
        <actions>
            <name>JPMonthlyCarReport_0CreatorAlarm</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>StatuschangetoCreateOnly</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>Japan</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>JP Monthly Car Report Status Check</fullName>
        <actions>
            <name>JPMonthlyCarReportStatusCheck</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Monthly_Car_Report__c.Approval_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Monthly_Car_Report__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Create Only</value>
        </criteriaItems>
        <description>JP Monthly Car Report Status Check</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Monthly_Car_Report__c.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
