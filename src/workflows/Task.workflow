<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Task Status Change</fullName>
        <actions>
            <name>Task_Updated</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Task.Task_Acceptance__c</field>
            <operation>equals</operation>
            <value>Reassign,Decline,Accept</value>
        </criteriaItems>
        <description>Task Acceptance Change</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Task_Updated</fullName>
        <assignedTo>thankachan.thomas@synthes.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Waiting on someone else</status>
        <subject>Task Updated</subject>
    </tasks>
    <tasks>
        <fullName>ToDo</fullName>
        <assignedToType>owner</assignedToType>
        <description>依頼したToDoが完了になりました。
Salesforceにログインし確認してください。 

https://login.salesforce.com/?locale=jp</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>ToDo完了連絡</subject>
    </tasks>
</Workflow>
