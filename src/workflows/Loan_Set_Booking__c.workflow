<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Consignment_Usage</fullName>
        <ccEmails>Assist.AU@synthes.com</ccEmails>
        <description>Consignment Usage</description>
        <protected>false</protected>
        <recipients>
            <recipient>contellis.steve@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rmcgreev@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Consignment_Usage</template>
    </alerts>
    <alerts>
        <fullName>Loan_Set_Booking_Booked_in_GP</fullName>
        <description>Loan Set Booking - Booked in GP</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Loan_Set_Booked_in_GP</template>
    </alerts>
    <alerts>
        <fullName>Loan_Set_Request_Submitted_to_CSLS</fullName>
        <ccEmails>assist.au@synthes.com</ccEmails>
        <description>Loan Set Request Submitted to CSLS</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Account_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Loan_Set_Request_submitted_to_CSLS</template>
    </alerts>
    <rules>
        <fullName>CONSIGNMENT USAGE</fullName>
        <actions>
            <name>Consignment_Usage</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Status__c</field>
            <operation>equals</operation>
            <value>CONSIGNMENT USAGE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Account_Name__c</field>
            <operation>contains</operation>
            <value>3ALFR001,2PRIN001</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Set_Type__c</field>
            <operation>equals</operation>
            <value>Consignment</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Event</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Status__c</field>
            <operation>equals</operation>
            <value>SUBMIT TO CSLS</value>
        </criteriaItems>
        <description>Create an event when a LSB is submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Loan Set Booking - Booked in GP</fullName>
        <actions>
            <name>Loan_Set_Booking_Booked_in_GP</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Status__c</field>
            <operation>equals</operation>
            <value>Booked</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Loan Set Request Submitted to CSLS</fullName>
        <actions>
            <name>Loan_Set_Request_Submitted_to_CSLS</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Status__c</field>
            <operation>contains</operation>
            <value>SUBMIT TO CSLS,CANCELLED,AMENDMENT TO ORIGINAL REQUEST,DUPLICATE,NO SET REQUIRED</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Set_Type__c</field>
            <operation>equals</operation>
            <value>Loan</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Loan Set Request Submitted to CSLS - QLD</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Status__c</field>
            <operation>contains</operation>
            <value>SUBMIT TO CSLS,AMENDMENT TO ORIGINAL REQUEST</value>
        </criteriaItems>
        <criteriaItems>
            <field>Loan_Set_Booking__c.Region__c</field>
            <operation>equals</operation>
            <value>QLD</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
