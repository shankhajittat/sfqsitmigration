<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>DocnotificationforAdmin</fullName>
        <description>Doc notification for Admin</description>
        <protected>false</protected>
        <recipients>
            <recipient>thankachan.thomas@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <rules>
        <fullName>CRM Admin Doc Notification</fullName>
        <actions>
            <name>DocnotificationforAdmin</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Documents__c.Name</field>
            <operation>contains</operation>
            <value>CRM,Admin</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
