<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Product_Event_Reporting_Template</fullName>
        <ccEmails>complaintanz@medau.jnj.com</ccEmails>
        <description>Product Event Reporting Template</description>
        <protected>false</protected>
        <recipients>
            <recipient>avladimi@its.jnj.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>ellis.jane@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>johnson.fiona@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>natalia.keren@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shaw.kellie@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Business_Unit_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Product_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Reported_by_Synthes_employee__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Sales_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Salesperson__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Product_Event_Reporting_Template</template>
    </alerts>
    <rules>
        <fullName>Device Report Notification</fullName>
        <actions>
            <name>Product_Event_Reporting_Template</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Product_Event_Reporting__c.Status__c</field>
            <operation>equals</operation>
            <value>Modified,Submit to QA,Further Info Required</value>
        </criteriaItems>
        <description>Device Notification Template</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
