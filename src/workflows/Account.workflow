<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Desktop_Integration_Request</fullName>
        <description>Desktop Integration Request</description>
        <protected>false</protected>
        <recipients>
            <recipient>thankachan.thomas@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Desktop_Integration_Request</template>
    </alerts>
    <rules>
        <fullName>Desktop Integration Change</fullName>
        <actions>
            <name>Desktop_Integration_Request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Connect_Offline__c</field>
            <operation>contains</operation>
            <value>No,Currently in Use,Would like</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Connect_for_Outlook__c</field>
            <operation>contains</operation>
            <value>No,Currently in Use,Would like</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SF_via_Mobile__c</field>
            <operation>contains</operation>
            <value>No,Would like,Currently in Use</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
