<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Maintain_Unique_Active</fullName>
        <description>Populate the name into the Unique Active field when a SetClass is active. Otherwise use the ID which is always unique.
This ensures only one active SetClass with a unique name</description>
        <field>Unique_Active__c</field>
        <formula>IF( Active__c,
   Name,
   Id
)</formula>
        <name>Maintain Unique Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Active or Name Change</fullName>
        <actions>
            <name>Maintain_Unique_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Set_Class__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Should fire for every save</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
