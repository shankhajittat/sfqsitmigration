<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ActionEmail</fullName>
        <description>製品クレーム申請　関係者にメール</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPComplaintsApprovalEmail</template>
    </alerts>
    <alerts>
        <fullName>NZFeedbackNotification</fullName>
        <description>NZ Feedback Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>mockford.tony@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nielsen.mark@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>wilson.louise@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/Feedback_Created</template>
    </alerts>
    <rules>
        <fullName>JP Complaints %3AApproval E-mail</fullName>
        <actions>
            <name>ActionEmail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserRoleId</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
