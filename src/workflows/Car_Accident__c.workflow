<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ManagerDenied</fullName>
        <description>申請者に否認連絡メール（Manager Denied）</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JP_Car_Accident3_Report_Manager_Denied</template>
    </alerts>
    <alerts>
        <fullName>Managerconfirmed</fullName>
        <description>申請者に承認連絡メール（Manager confirmed）</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarAccidentReportManagerreceived</template>
    </alerts>
    <alerts>
        <fullName>emailtoGA</fullName>
        <description>人事総務部に連絡メール（e-mail to GA）</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Japan_Temp/JPCarAccidentNotification</template>
    </alerts>
    <rules>
        <fullName>JP Car Accident E-mail Notification</fullName>
        <actions>
            <name>emailtoGA</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Car_Accident__c.Accsident_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
