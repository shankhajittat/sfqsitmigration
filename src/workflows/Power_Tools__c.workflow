<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AU_Power_Tools_Warranty_Expiration_Notice_60_Days</fullName>
        <description>AU Power Tools Warranty Expiration Notice - 60 Days</description>
        <protected>false</protected>
        <recipients>
            <recipient>ellis.jane@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>johnson.fiona@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>louw.janice@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>staras.nick@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>SynthesEmailTemplates/PTWarrantyExp</template>
    </alerts>
    <alerts>
        <fullName>Notification_60_days_prior_to_warranty_expitration</fullName>
        <description>Notification 60 days prior to warranty expitration</description>
        <protected>false</protected>
        <recipients>
            <recipient>ellis.jane@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>johnson.fiona@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>louw.janice@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>staras.nick@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/PTWarrantyExp</template>
    </alerts>
    <alerts>
        <fullName>PT_Warranty_E_mail_Notification</fullName>
        <description>PT Warranty E-mail Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>douglas.tracy@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thankachan.thomas@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/PTWarrantyExp</template>
    </alerts>
    <alerts>
        <fullName>SendPTWarrantyduenotification</fullName>
        <description>Send PT Warranty due notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>thankachan.thomas@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SynthesEmailTemplates/PTWarrantyExp</template>
    </alerts>
    <rules>
        <fullName>AU PT Warranty Expiration - 60 Days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Power_Tools__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>AU Power Tools Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Power_Tools__c.Expiration_Notification__c</field>
            <operation>equals</operation>
            <value>60 days</value>
        </criteriaItems>
        <description>E-mail notification 60 days prior to warranty expiration</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notification_60_days_prior_to_warranty_expitration</name>
                <type>Alert</type>
            </actions>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>NZ PT Warranty Expiration - 60 Days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Power_Tools__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>NZ Power Tool Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Power_Tools__c.Expiration_Notification__c</field>
            <operation>equals</operation>
            <value>60 days</value>
        </criteriaItems>
        <description>E-mail notification 60 days prior to warranty expiration</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PT_Warranty_E_mail_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Power_Tools__c.Warranty_Exp_x__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PT Warranty Expiration - 30 Days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Power_Tools__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>NZ Power Tool Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Power_Tools__c.Expiration_Notification__c</field>
            <operation>equals</operation>
            <value>30 days</value>
        </criteriaItems>
        <description>E-mail notification 30 days prior to warranty expiration</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PT_Warranty_E_mail_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Power_Tools__c.Warranty_Exp_x__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PT Warranty Expiration - 30 Days Notification</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Power_Tools__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>NZ Power Tool Record Type,AU Power Tools Record Type</value>
        </criteriaItems>
        <criteriaItems>
            <field>Power_Tools__c.Expiration_Notification__c</field>
            <operation>equals</operation>
            <value>30 days</value>
        </criteriaItems>
        <description>E-mail notification 30 days prior to warranty expiration</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>PT_Warranty_E_mail_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Power_Tools__c.Warranty_Exp_x__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
