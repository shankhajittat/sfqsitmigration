<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Ed_Materials_Changed</fullName>
        <ccEmails>Course.Logistics@synthes.com</ccEmails>
        <description>Ed. Materials Changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>christinat.roger@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lam.leon@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Education_Materials_Templates/Ed_Materials_List_Changed</template>
    </alerts>
    <alerts>
        <fullName>Ed_Materials_Marketing_Notification</fullName>
        <description>Ed. Materials Marketing Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>hincapie.cristina@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mclennan.janine@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>squires.sheridan@synthes.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Education_Materials_Templates/Ed_Materials_Mktg_Not</template>
    </alerts>
    <rules>
        <fullName>Ed%2E Mat%2E Marketing Notification</fullName>
        <actions>
            <name>Ed_Materials_Marketing_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Ed_Materials_List__c.Category__c</field>
            <operation>contains</operation>
            <value>Marketing,Bone Models</value>
        </criteriaItems>
        <criteriaItems>
            <field>Education_Materials__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>If Approved and List contains Marketing Materials then Marketing will be notified</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ed%2E Materials List Change</fullName>
        <actions>
            <name>Ed_Materials_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Quantity__c ) &amp;&amp;   Ed_Materials_Request__r.CreatedDate  &lt;&gt; LastModifiedDate</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Ed%2E Materials Marketing Shipping Reminder</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Education_Materials__c.Projected_O_Shipping_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Ed_Materials_List__c.Category__c</field>
            <operation>contains</operation>
            <value>Marketing</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
