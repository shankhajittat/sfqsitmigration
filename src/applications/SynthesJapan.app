<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>DePuy Synthes Japan</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Training_Events__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Case</tab>
    <tab>Learner_Portal</tab>
</CustomApplication>
