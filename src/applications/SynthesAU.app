<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>DePuy Synthes AU</label>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Contract</tab>
    <tab>standard-Opportunity</tab>
    <tab>Training_Events__c</tab>
    <tab>standard-Case</tab>
    <tab>standard-Document</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-report</tab>
    <tab>standard-Campaign</tab>
    <tab>Education_Materials__c</tab>
    <tab>Ed_Materials_Products__c</tab>
    <tab>Travel_Request__c</tab>
    <tab>Loan_Set_Booking__c</tab>
    <tab>Customer_Feedback__c</tab>
    <tab>Product_Event_Reporting__c</tab>
    <tab>PE_Request_form__c</tab>
    <tab>Learner_Portal</tab>
</CustomApplication>
