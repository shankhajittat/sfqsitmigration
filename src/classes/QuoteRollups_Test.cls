@isTest(seeAllData=true)
private class QuoteRollups_Test {

    static testMethod void testrollupQuoteLineGroup_Insert() {
        Quote_Line_Group__c lineGroup = getTestQuoteLineGroup();
        
        //Start Test
        Test.startTest();
        
 	    QuoteRollups rollupQuote = new QuoteRollups(new List<Quote_Line_Group__c>{lineGroup});
        
        rollupQuote.buildUniqueQuoteIds();
        System.debug('>>>>rollupQuote.buildUniqueQuoteIds<<<<' + rollupQuote.quoteIds);
		System.assert(rollupQuote.quoteIds != null && rollupQuote.quoteIds.isEmpty() == false,
			'An error occured while execuing buildUniqueQuoteIds method');
		
		rollupQuote.buildQuotes();
		System.assert(rollupQuote.quotes != null && rollupQuote.quotes.isEmpty() == false,
			'An error occured while execuing buildQuotes method');
		
		rollupQuote.buildAggregateRollups();
		System.assert(rollupQuote.rollupQuotes != null && rollupQuote.rollupQuotes.isEmpty() == false,
			'An error occured while execuing buildAggregateRollups method');
						
		rollupQuote.buildQuotesToUpdate();
		System.assert(rollupQuote.quotesToUpdate != null && rollupQuote.quotesToUpdate.isEmpty() == false,
			'An error occured while execuing buildQuotesToUpdate method');
		
		boolean isUpdated = rollupQuote.updateRollups();
		System.assert(isUpdated,'An error occured while execuing updateRollups method');
        
        Test.stopTest();
        
        //Validate Results
        
        Quote quoteTestResult = getQuote(lineGroup.QuoteId__c );
		
		System.debug('<<<<<After Insert>>>>>' + quoteTestResult);
		
		
        System.assertEquals(18.00, quoteTestResult.Tax
			,'Expected the quoteTestResult.Tax field to be updated based on the rollup of QuoteLineItem.LineGroup_Total_Tax__c');
			
		 System.assertEquals(GlobalConstants.QS_STATUS_PRICE_CONFIRMED, quoteTestResult.Status
			,'Expected the quoteTestResult.Status field to be updated based on the rollup of QuoteLineItem.LineGroup_Status__c');	
    }
    
    static testMethod void testrollupQuoteLineGroup_SingleMethod_Insert() {
        Quote_Line_Group__c lineGroup = getTestQuoteLineGroup();
        
        //Start Test
        Test.startTest();
        
        QuoteRollups rollupQuote = new QuoteRollups(new List<Quote_Line_Group__c>{lineGroup});
		
		boolean isUpdated = rollupQuote.rollupQuoteLineGroups();
		System.assert(isUpdated,'An error occured while execuing rollupQuoteLineGroups method');
        
        Test.stopTest();
        
        //Validate Results
       	Quote quoteTestResult = getQuote(lineGroup.QuoteId__c );
		
		System.debug('<<<<<After Insert>>>>>' + quoteTestResult);
		
		
        System.assertEquals(18.00, quoteTestResult.Tax
			,'Expected the quoteTestResult.Tax field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Price__c');
		
		 System.assertEquals(GlobalConstants.QS_STATUS_PRICE_CONFIRMED, quoteTestResult.Status
			,'Expected the quoteTestResult.Status field to be updated based on the rollup of QuoteLineItem.LineGroup_Status__c');		
    }
    
    static testMethod void testrollupQuoteLineGroup_Update() {
        Quote_Line_Group__c lineGroup = getTestQuoteLineGroup();
        
        lineGroup.LineGroup_Total_Tax__c = 20;
        update lineGroup;
        
        
        //Start Test
        Test.startTest();
        QuoteRollups rollupQuote = new QuoteRollups(new List<Quote_Line_Group__c>{lineGroup});
		
		boolean isUpdated = rollupQuote.rollupQuoteLineGroups();
		System.assert(isUpdated,'An error occured while execuing rollupQuoteLineGroups method');
        
        Test.stopTest();
        
        //Validate Results
        Quote quoteTestResult = getQuote(lineGroup.QuoteId__c );
		
		System.debug('<<<<<After Insert>>>>>' + quoteTestResult);
		
		
        System.assertEquals(20.00, quoteTestResult.Tax
			,'Expected the quoteTestResult.Tax field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Price__c');
        
         System.assertEquals(GlobalConstants.QS_STATUS_PRICE_CONFIRMED, quoteTestResult.Status
			,'Expected the quoteTestResult.Status field to be updated based on the rollup of QuoteLineItem.LineGroup_Status__c');			
    }
    
    static testMethod void testrollupQuoteLineGroup_Delete() {
    	
    	Quote_Line_Group__c lineGroup = getTestQuoteLineGroup();
        delete lineGroup;
        
        
        //Start Test
        Test.startTest();
        QuoteRollups rollupQuote = new QuoteRollups(new List<Quote_Line_Group__c>{lineGroup});
		
		boolean isUpdated = rollupQuote.rollupQuoteLineGroups();
		System.assert(isUpdated,'An error occured while execuing rollupQuoteLineGroups method');
        
        Test.stopTest();
        
        //Validate Results
        Quote quoteTestResult = getQuote(lineGroup.QuoteId__c );
		
		System.debug('<<<<<After Insert>>>>>' + quoteTestResult);
		
		
        System.assertEquals(0.00, quoteTestResult.Tax
			,'Expected the quoteTestResult.Tax field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Price__c');
			
         System.assertEquals(GlobalConstants.QS_STATUS_DRAFT, quoteTestResult.Status
			,'Expected the quoteTestResult.Status field to be updated based on the rollup of QuoteLineItem.LineGroup_Status__c');
    }
    
    private static Quote getQuote(Id quoteId)
    {
    	Quote quote = [SELECT 	Id,
								Tax,
								Status
				FROM Quote WHERE Id =: quoteId];
				
		return quote;
    }
    
    private static Quote_Line_Group__c getTestQuoteLineGroup(){
    	
    	Boolean createInDatabase = true;
        
      	String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(createInDatabase);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);

        
        //String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
		//String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();

        Opportunity o = new OpportunityBuilder()
        					.account(vinnies)
        					.priceBookId(customPricebook.Id).build(createInDatabase);
        System.assert(o.Id <> null,'An error occured while creating Opportunity.');

        Product2 product = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001')
        		.build(createInDatabase);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
         Product2 bomProduct = new ProductBuilder()
        		.name('BOM-BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001BOM')
        		.build(createInDatabase);
        System.assert(bomProduct.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(Test.getStandardPriceBookId()).build(createInDatabase);
	        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
	        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(customPricebook.Id).build(createInDatabase);
	        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
	        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');

        
        //Create Set Class
        Set_Class__c setClass = new SetClassBuilder()
        								.equipmentNumber('TEST SC101').productCode('03.161.001BOM').build(createInDatabase);
        System.assert(setClass.Id <> null,'An error occured while creating SetClass.');
        
       
        //Create SetClass Items
        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
        								.header(setClass)
        								.product(product)
        								.quantity(2).build(createInDatabase);
        
        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(createInDatabase);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        */
        
        //Create Quote
        Quote quote = new QuoteBuilder()
        		.opportunity(o)
        		.priceBookId(customPricebook.Id)
        		.quoteType(GlobalConstants.QuoteTypes.CustomerQuote)
        		.build(createInDatabase);
        System.assert(quote.Id <> null,'An error occured while creating Quote.');
        
        //sync Opportunity and Quote lines.
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
        
       
        Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
        		.name('TEST SC101')
        		.quote(quote)
        		.setClass(setClass)
        		.groupType('Kit')
        		.groupStatus('Validated')
        		.build(createInDatabase);
        System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup.');

		
		QuoteLineItem quoteLineItem = new QuoteLineItem(
						QuoteId=quote.Id,
						PriceBookEntryId=pbCustomEntry.Id,
						UnitPrice=100.00,
						Quantity= 2,
						Discount = 10.00,
						Set_Class__c = setClass.Id,
						Set_Class_Item__c = setClassItem1.Id,
						Quote_Line_Group__c = quoteLineGroup.Id,
						Tax_Rate__c = 10.00,
						Standard_Cost__c = 30.00,
						Landed_Cost__c = 20.00,
						Sequence_Number__c = 10,
						Price_Status__c = 'Validated'
						);
		insert quoteLineItem;
		
		quoteLineGroup = [SELECT Id, QuoteId__c, LineGroup_Total_Tax__c FROM Quote_Line_Group__c WHERE Id =: quoteLineGroup.Id];
								
		return quoteLineGroup;
        
    }
    
}