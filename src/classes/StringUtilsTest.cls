@IsTest
private class StringUtilsTest {
    
	private static testmethod void testLeftPadThreeArgs(){
        //test: global static String leftPad(String str, Integer size, String padStr) {
        assertLeftPadThreeArgs(null,     -1, null,     null);
        assertLeftPadThreeArgs('',         3,     'z',     'zzz'); 
        assertLeftPadThreeArgs('bat',     3,     'yz',     'bat');
        assertLeftPadThreeArgs('bat',     5,     'yz',     'yzbat');
        assertLeftPadThreeArgs('bat',     8,     'yz',     'yzyzybat');
        assertLeftPadThreeArgs('bat',     1,     'yz',     'bat');
        assertLeftPadThreeArgs('bat',     -1, 'yz',     'bat');
        assertLeftPadThreeArgs('bat',     5,     null,     '  bat');
        assertLeftPadThreeArgs('bat',     5,     '',     '  bat');
    }
    
    private static void assertLeftPadThreeArgs(String str, Integer size, String padStr, String expected){
        String actual = StringUtils.leftPad(str,size,padStr);
        System.assert(actual==expected, 'StringUtils.leftPad(\'' + str + '\',' 
            + size + ',\'' + padStr + '\') returned \'' + actual + '\'; expected \'' + expected + '\'');
    }    
    
    private static testmethod void testIsEmpty(){
        assertIsEmpty(null,     true);
        assertIsEmpty('',        true);
        assertIsEmpty(' ',        false);
        assertIsEmpty('bob',    false);
        assertIsEmpty('  bob  ',false);
    }    
    
    private static void assertIsEmpty(String str, Boolean expected){
        Boolean actual = StringUtils.IsEmpty(str);
        System.assert(actual==expected, 'StringUtils.IsEmpty(\'' + str + '\') returned ' + actual);
    }
    
    private static testMethod void test1ValidateId() {
    	String validId = StringUtils.validateId('ABC');
    	System.AssertEquals(null, validId, 'Expecting null to be returned instead of a valid Id since ABC is not a valid Id');
    }
    
     private static testMethod void test2ValidateId() {
    	String validId = StringUtils.validateId('a009000000rkKarAAE');
    	System.AssertNotEquals(null, validId, 'Expecting a valid id to be returned');
    }
    
    private static testMethod void isNullOrEmpty1(){
        Boolean result = StringUtils.isNullOrEmpty(null);
        System.AssertEquals(true, result, 'Expecting a true to be returned');
    }
    
    private static testMethod void isNullOrEmpty2(){
        Boolean result = StringUtils.isNullOrEmpty('dfadsfads');
        System.AssertNotEquals(true, result, 'Expecting a false to be returned');
    }
    
    private static testMethod void isNullOrWhiteSpace1(){
        Boolean result = StringUtils.isNullOrWhiteSpace('');
        System.AssertEquals(true, result, 'Expecting a true to be returned');
    }
    
    private static testMethod void isNullOrWhiteSpace2(){
        Boolean result = StringUtils.isNullOrWhiteSpace('dfadsfads');
        System.AssertNotEquals(true, result, 'Expecting a false to be returned');
    }
    
    private static testMethod void joinArray1(){
    	String joinedString = 'One|Two';
    	List<String> arrayString = joinedString.Split('|');
        String result = StringUtils.joinArray(arrayString);
        System.AssertEquals(joinedString, result, 'Expecting Same String back since no seperator specified.');
    }
    
    private static testMethod void joinArray2(){
    	String joinedString = 'One|Two';
    	List<String> arrayString = joinedString.Split('|');
        String result = StringUtils.joinArray(arrayString,'|');
        System.AssertNotEquals(joinedString, result, 'Not Expecting Same String back since seperator specified.');
    }
    
    private static testMethod void joinArray3(){
    	String joinedString = 'One|Two';
    	List<String> arrayString = joinedString.Split('|');
        String result = StringUtils.joinArray(null);
        System.AssertNotEquals(joinedString, result, 'Not Expecting Same String back since null passed.');
    }
    
    private static testMethod void joinArray4(){
    	String joinedString = 'One|Two|Three|Four';
    	List<String> arrayString = joinedString.Split('|');
        String result = StringUtils.joinArray(arrayString,'|',2,3);
        System.AssertNotEquals(joinedString, result, 'Not Expecting Same String back since index passed.');
    }
    
    private static testMethod void joinArray5(){
    	String joinedString = 'One|Two|Three|Four';
    	List<String> arrayString = joinedString.Split('|');
        String result = StringUtils.joinArray(arrayString,'|',-2,4);
        System.AssertNotEquals(joinedString, result, 'Not Expecting Same String back since index passed.');
    }
    
    private static testMethod void joinArray6(){
    	String joinedString = 'One|Two|Three|Four';
    	List<String> arrayString = joinedString.Split('|');
        String result = StringUtils.joinArray(null,'|',0,0);
        System.AssertNotEquals(joinedString, result, 'Not Expecting Same String back since null passed.');
    }
}