@isTest
private class PreferenceToolsTest {

    static testMethod void testPreferenceCRUD() {
    	
    	String defaultSetbuySortDir = PreferenceTools.getPreference(
    		PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_SORT_DIR_ASC);
 		System.assertEquals('false', defaultSetbuySortDir,'default sort dir when no pref save is asc');   	
    	String defaultSetbuySortField = PreferenceTools.getPreference(
    		PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBuy_SORT_FIELD);
 		System.assertEquals('ProductCode', defaultSetbuySortField,'default sort field when no pref save is ProductCode');   	
    	
    	PreferenceTools.setPreference(
    		PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_SORT_DIR_ASC,'true');

    	String savedSetbuySortDir = PreferenceTools.getPreference(
    		PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_SORT_DIR_ASC);
 		System.assertEquals('true', savedSetbuySortDir,'sort dir was saved');   	
    		
    	Map<String, String> gridGroup = PreferenceTools.getGroup(PreferenceTools.Groups.QUOTE_GRIDS);
 		System.assertEquals('true', gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_SORT_DIR_ASC)),
 			'saved sort dir is in group');   	
 		System.assertEquals('ProductCode', gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_SORT_FIELD)),
 			'default sort field is in group');   	

    	PreferenceTools.setPreference(
    		PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_SORT_FIELD,'Quantity');
    		
    	gridGroup = PreferenceTools.getGroup(PreferenceTools.Groups.QUOTE_GRIDS);
 		System.assertEquals('Quantity', gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_SORT_FIELD)),
 			'saved sort field is in group');  
 			    	
    }
}