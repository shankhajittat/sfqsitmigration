public with sharing class OpportunityBuilder {

	private static Integer sequence = 100;

	private String opportunityName;

	//Mandatory fields with defaults
	private Double amount = 500.00;
	private Date closeDate = Date.today();
	private String stage = GlobalConstants.OPP_STAGENAME_QUALIFICAITON;
	private Account account;
	private Id priceBookId;

	/*
	public OpportunityBuilder name(String opportunityName)
	{
		this.opportunityName = opportunityName;
		return this;
	}
	*/
	
	public OpportunityBuilder account(Account account)
	{
		this.account = account;
		return this;
	}
	
	public OpportunityBuilder priceBookId(Id priceBookId)
	{
		this.priceBookId = priceBookId;
		return this;
	}
	
	public Opportunity build(Boolean createInDatabase)
	{
		//System.debug('>>>>>><<<<<<' + this.priceBook);
		
		Opportunity opportunity = new Opportunity(
			Name = 'OPP-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			CloseDate = this.closeDate,
			Amount = this.amount,
			StageName = this.stage,
			AccountId = this.account.Id,
			Pricebook2Id = this.priceBookId,
			Type = GlobalConstants.OPP_TYPE
		);
		
		if (createInDatabase){
			insert opportunity;
			Id oppId = opportunity.Id;
			opportunity = [Select Id,Name,CloseDate,Amount,StageName,AccountId,Pricebook2Id,Type From Opportunity Where Id = :oppId];
		}
		return opportunity;
	}
	
	

}