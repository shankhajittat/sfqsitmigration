public without sharing class QuoteLineGroupRollups {
	
	private List<QuoteLineItem> quoteLines = null;
	
	public Set<Id> quoteLineGroupIds = null;
	public Map<Id, Quote_Line_Group__c> quoteLineGroups = null;
	public Map<Id, Quote_Line_Group__c> rollupLineGroups = null;
	public List<Quote_Line_Group__c> quoteGroupsToUpdate = null;
	private Map<Id,Map<String,InsImpTotal>> quoteGroupInsImpOtherTotal = null;
	
	private Set<Id> quoteIds = null;
	
	public QuoteLineGroupRollups(List<QuoteLineItem> quoteLines)
	{
		this.quoteLines = quoteLines;
	}
	
	public void buildUniqueQuoteLineGroupIds()
	{
		if (quoteLines == null || quoteLines.isEmpty())
			return;
		
		//Get unique QuoteLineGroup Ids
		quoteLineGroupIds = new Set<Id>();
		quoteIds = new Set<Id>();
		for(QuoteLineItem lineItem : quoteLines){
			quoteLineGroupIds.add(lineItem.Quote_Line_Group__c);
			quoteIds.add(lineItem.QuoteId);
		}
	}
	
	public void buildQuoteLineGroups()
	{
		if (quoteLineGroupIds == null || quoteLineGroupIds.isEmpty())
			return;
			
		//Get Map of QuoteLineGroups
		quoteLineGroups = new Map<Id, Quote_Line_Group__c>();
				
		for(List<Quote_Line_Group__c> lineGroups: [SELECT 	Id,
															LineGroup_Total_Discount_Amount__c,
															LineGroup_Total_Extended_UnitPrice__c,
															LineGroup_Total_GrossMargin_Amount__c,
															LineGroup_Total_NetMargin_Amount__c,
															LineGroup_Total_Landed_Cost__c,
															LineGroup_Total_Standard_Cost__c,
															LineGroup_TotalPrice_ExTax__c,
															LineGroup_TotalPrice_IncTax__c,
															LineGroup_Total_Quantity__c,
															LineGroup_Total_ProposedQuantity__c,
															LineGroup_Total_Tax__c,
															LineGroup_Status__c,
															LineGroup_TotalPrice_ImpMaterialGroup__c,
															LineGroup_TotalPrice_InsMaterialGroup__c,
															LineGroup_TotalPrice_OtherMaterialGroup__c,
															NumberOfBOMsRequired__c,
															LineGroup_TotalStdCost_ImpMaterialGroup__c,
					        								LineGroup_TotalStdCost_InsMaterialGroup__c,
					        								LineGroup_TotalStdCost_OtherMaterialGrou__c  
				FROM Quote_Line_Group__c WHERE Id IN :quoteLineGroupIds ]){
			for(Quote_Line_Group__c lineGroup : lineGroups){
				quoteLineGroups.put(lineGroup.Id,lineGroup);
			}
		}
	}
	
	public void buildAggregateRollups()
	{
		if (quoteLineGroups == null || quoteLineGroups.isEmpty())
			return;
		
		//Aggregate Rollups
		rollupLineGroups = new Map<Id, Quote_Line_Group__c>();
		
		for(AggregateResult[] aggreResults : [SELECT Quote_Line_Group__c, 
											SUM(Extended_Unit_Discount__c) RollupTotalDiscountAmount,
											SUM(Extended_Unit_Price__c) RollupExtendedUnitPrice,
											SUM(Gross_Margin_Amount__c) RollupTotalGrossMarginAmt,
											SUM(Net_Margin_Amount__c) RollupTotalNetMarginAmt,
											SUM(Total_Landed_Cost__c) RollupTotalLandedCost,
											SUM(Total_Standard_Cost__c) RollupTotalStandardCost,
											SUM(Total_Price_ExTax__c) RollupTotalPriceExTax,
											SUM(Total_Price_IncTax__c) RollupTotalPriceIncTax,
											SUM(Quantity) RollupTotalQuantity,
											SUM(Proposed_Quantity__c) RollupTotalProposedQty,
											SUM(Total_Tax__c) RollupTotalTax,
											MIN(Price_Status__c) RollupStatus
										FROM QuoteLineItem WHERE Quote_Line_Group__c IN :quoteLineGroupIds 
										GROUP BY Quote_Line_Group__c]){
			for(AggregateResult aggreResult : aggreResults)
			{
				Id quoteLineGroupId = (Id) aggreResult.get('Quote_Line_Group__c');
				Quote_Line_Group__c lineGroup = new Quote_Line_Group__c();
				
				//System.debug('RollupTotalPriceIncTax>>>>>>' + (double)aggreResult.get('RollupTotalPriceIncTax') );
				
				lineGroup.LineGroup_Total_Discount_Amount__c = (double)aggreResult.get('RollupTotalDiscountAmount');
				lineGroup.LineGroup_Total_Extended_UnitPrice__c = (double)aggreResult.get('RollupExtendedUnitPrice');
				lineGroup.LineGroup_Total_GrossMargin_Amount__c = (double)aggreResult.get('RollupTotalGrossMarginAmt');
				lineGroup.LineGroup_Total_NetMargin_Amount__c = (double)aggreResult.get('RollupTotalNetMarginAmt');
				lineGroup.LineGroup_Total_Landed_Cost__c = (double)aggreResult.get('RollupTotalLandedCost');
				lineGroup.LineGroup_Total_Standard_Cost__c = (double)aggreResult.get('RollupTotalStandardCost');
				lineGroup.LineGroup_TotalPrice_ExTax__c = (double)aggreResult.get('RollupTotalPriceExTax');
				lineGroup.LineGroup_TotalPrice_IncTax__c = (double)aggreResult.get('RollupTotalPriceIncTax');
				lineGroup.LineGroup_Total_Quantity__c = (double)aggreResult.get('RollupTotalQuantity');
				lineGroup.LineGroup_Total_ProposedQuantity__c = (double)aggreResult.get('RollupTotalProposedQty');
				lineGroup.LineGroup_Total_Tax__c = (double)aggreResult.get('RollupTotalTax');
				lineGroup.LineGroup_Status__c = (String)aggreResult.get('RollupStatus');
				
				/*
				lineGroup.Group_Total_Discount_Percentage__c = 	 (lineGroup.Group_Total_Extended_Unit_Price__c != null && lineGroup.Group_Total_Discount_Amount__c != null)
					? (double)((lineGroup.Group_Total_Discount_Amount__c / lineGroup.Group_Total_Extended_Unit_Price__c) * 100.00)
					: (double)(0.00);
				*/
				
				rollupLineGroups.put(quoteLineGroupId,lineGroup);
			}
		}
		
		
		//Instrument/Implant Group Totals
		quoteGroupInsImpOtherTotal = new Map<Id,Map<String,InsImpTotal>>();
				
		for(AggregateResult[] aggreResults : [SELECT 
			                q.Quote_Line_Group__c, 
			                q.PricebookEntry.MaterialGroup1__c,
			                SUM(q.Total_Price_ExTax__c) RollupTotalPriceExTax,
			                SUM(q.Total_Standard_Cost__c) RollupTotalStdCost //it must be always from QuoteLine since if it is SET Product then the Cost gets calculated from the SetItems.
			            FROM QuoteLineItem q WHERE Quote_Line_Group__c IN :quoteLineGroupIds 
			            GROUP BY q.Quote_Line_Group__c,q.PricebookEntry.MaterialGroup1__c]){

			for(AggregateResult aggreResult : aggreResults){
	        	//system.debug('result=>' + aggreResult);
	    		//system.debug('Id=>' + (Id)aggreResult.get('Quote_Line_Group__c') + 'IMP/INs:' + (String)aggreResult.get('MaterialGroup1__c') +  ' Price: ' + (double)aggreResult.get('RollupTotalPriceExTax'));

	    		Id quoteLineGroupId = (Id) aggreResult.get('Quote_Line_Group__c');
	    		String materialGroup = (String)aggreResult.get('MaterialGroup1__c');
	    		//system.debug('materialGroup=>' + materialGroup );
	    		String materialGroupKey = '';
	    		if (materialGroup == 'INS' || materialGroup == 'IMP')
	    			materialGroupKey = materialGroup;
	    		else
	    			materialGroupKey = 'Other';
	    		
	    		//system.debug('materialGroupKey=>' + materialGroupKey );
	    		
	    		Map<String,InsImpTotal> mapInsImpGroupTotal= null;
	    		InsImpTotal otherExistingValue = null;
	    		if (quoteGroupInsImpOtherTotal.containsKey(quoteLineGroupId)){
	    			mapInsImpGroupTotal = quoteGroupInsImpOtherTotal.get(quoteLineGroupId);

					//if the Group value is other than INS/IMP and if more than once then need to sum everything into Other group.
					//otherwise it control won't come here since grouping with q.Quote_Line_Group__c and q.PricebookEntry.MaterialGroup1__c 			
	    			if (mapInsImpGroupTotal != null && mapInsImpGroupTotal.containsKey(materialGroupKey)){
	    				otherExistingValue = mapInsImpGroupTotal.get(materialGroupKey);
	    			}
	    		}
	    		else{
	    			mapInsImpGroupTotal = new Map<String,InsImpTotal>();
	    		}
	    		
	    		double totalPrice = (double)aggreResult.get('RollupTotalPriceExTax');
	    		double totalStandardCost = (double)aggreResult.get('RollupTotalStdCost');
	    		
	    		totalPrice = totalPrice == null ? 0 : totalPrice;
	    		totalStandardCost = totalStandardCost == null ? 0 : totalStandardCost;
	    		if (otherExistingValue == null) otherExistingValue = new InsImpTotal();
	    			    			
	    		system.debug('totalPrice=>' + totalPrice + ' totalStandardCost=>' + totalStandardCost);
	    		system.debug('otherExistingValue=>' + otherExistingValue );
	    		
	    		InsImpTotal tempInsImpTotal = new InsImpTotal();
	    		tempInsImpTotal.totalPrice = totalPrice; 
	    		tempInsImpTotal.totalStandardCost = totalStandardCost; 
	    			
	    		if (materialGroupKey == 'INS' || materialGroupKey == 'IMP'){
	    			mapInsImpGroupTotal.put(materialGroupKey,tempInsImpTotal);
	    		}
	    		else if (materialGroupKey == 'Other'){
	    			//if it is other than INS and IMP
	    			otherExistingValue.totalPrice += totalPrice; 
	    			otherExistingValue.totalStandardCost += totalStandardCost; 
	    			mapInsImpGroupTotal.put(materialGroupKey,otherExistingValue);
	    		}
	    		quoteGroupInsImpOtherTotal.put(quoteLineGroupId,mapInsImpGroupTotal);
	    	}                                          
		}                                     
		
	}
	
	public void buildQuoteGroupsToUpdate()
	{
		//if no more QuoteLineItems then buildAggregateRollups method won't generate any AggregateRollups, 
		//still it needs to update with 0s.

		quoteGroupsToUpdate = new List<Quote_Line_Group__c>();
		
		for(Id quoteLineGroupId : quoteLineGroups.keySet()){
			Quote_Line_Group__c lineGroup = quoteLineGroups.get(quoteLineGroupId);
			
			Quote_Line_Group__c rollupLineGroup = null;
			if (rollupLineGroups.containsKey(quoteLineGroupId))
			{
				rollupLineGroup = rollupLineGroups.get(quoteLineGroupId);
			}
			
			double impMaterialGroupTotalPrice = 0;
			double insMaterialGroupTotalPrice = 0;
			double otherMaterialGroupTotalPrice = 0;
			double impMaterialGroupTotalStandardCost = 0;
			double insMaterialGroupTotalStandardCost = 0;
			double otherMaterialGroupTotalStandardCost = 0;
			
			double numberOfBOMsRequired = lineGroup.NumberOfBOMsRequired__c != null && lineGroup.NumberOfBOMsRequired__c > 0 
										? lineGroup.NumberOfBOMsRequired__c : 1;
			
			if (quoteGroupInsImpOtherTotal != null && quoteGroupInsImpOtherTotal.containsKey(quoteLineGroupId)){
	    		Map<String,InsImpTotal> mapInsImpGroupTotal = quoteGroupInsImpOtherTotal.get(quoteLineGroupId);
	    		
	    		InsImpTotal tempInsTotal = null;
	    		InsImpTotal tempImpTotal = null;
	    		InsImpTotal tempOtherTotal = null;
	    		
	    		if (mapInsImpGroupTotal.containsKey('INS'))
	    			tempInsTotal = mapInsImpGroupTotal.get('INS');
	    		if (mapInsImpGroupTotal.containsKey('IMP'))
	    			tempImpTotal = mapInsImpGroupTotal.get('IMP');
	    		if (mapInsImpGroupTotal.containsKey('Other'))
	    			tempOtherTotal = mapInsImpGroupTotal.get('Other');
	    			
	    		if (tempInsTotal != null){
	    			insMaterialGroupTotalPrice = tempInsTotal.totalPrice;
	    			insMaterialGroupTotalStandardCost = tempInsTotal.totalStandardCost;
	    		}
	    		if (tempImpTotal != null){
	    			impMaterialGroupTotalPrice = tempImpTotal.totalPrice;
	    			impMaterialGroupTotalStandardCost = tempImpTotal.totalStandardCost;
	    		}
	    		if (tempOtherTotal != null){
	    			otherMaterialGroupTotalPrice = tempOtherTotal.totalPrice;
	    			otherMaterialGroupTotalStandardCost = tempOtherTotal.totalStandardCost;
	    		}
			}
			
			if (rollupLineGroup != null)
			{
				lineGroup.LineGroup_Total_Discount_Amount__c = rollupLineGroup.LineGroup_Total_Discount_Amount__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_Extended_UnitPrice__c = rollupLineGroup.LineGroup_Total_Extended_UnitPrice__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_GrossMargin_Amount__c = rollupLineGroup.LineGroup_Total_GrossMargin_Amount__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_NetMargin_Amount__c = rollupLineGroup.LineGroup_Total_NetMargin_Amount__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_Landed_Cost__c = rollupLineGroup.LineGroup_Total_Landed_Cost__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_Standard_Cost__c = rollupLineGroup.LineGroup_Total_Standard_Cost__c * numberOfBOMsRequired;
				lineGroup.LineGroup_TotalPrice_ExTax__c = rollupLineGroup.LineGroup_TotalPrice_ExTax__c * numberOfBOMsRequired;
				lineGroup.LineGroup_TotalPrice_IncTax__c = rollupLineGroup.LineGroup_TotalPrice_IncTax__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_Quantity__c = rollupLineGroup.LineGroup_Total_Quantity__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Total_ProposedQuantity__c = rollupLineGroup.LineGroup_Total_ProposedQuantity__c; //only for CBC & SP
				lineGroup.LineGroup_Total_Tax__c = rollupLineGroup.LineGroup_Total_Tax__c * numberOfBOMsRequired;
				lineGroup.LineGroup_Status__c = rollupLineGroup.LineGroup_Status__c;

				lineGroup.LineGroup_TotalPrice_ImpMaterialGroup__c = impMaterialGroupTotalPrice * numberOfBOMsRequired;
				lineGroup.LineGroup_TotalPrice_InsMaterialGroup__c = insMaterialGroupTotalPrice * numberOfBOMsRequired;
				lineGroup.LineGroup_TotalPrice_OtherMaterialGroup__c = otherMaterialGroupTotalPrice * numberOfBOMsRequired;
				lineGroup.LineGroup_TotalStdCost_ImpMaterialGroup__c = impMaterialGroupTotalStandardCost * numberOfBOMsRequired;
		        lineGroup.LineGroup_TotalStdCost_InsMaterialGroup__c = insMaterialGroupTotalStandardCost * numberOfBOMsRequired;
		        lineGroup.LineGroup_TotalStdCost_OtherMaterialGrou__c = otherMaterialGroupTotalStandardCost * numberOfBOMsRequired;
				quoteGroupsToUpdate.add(lineGroup);
			}
			else
			{
				//default with 0. no more QuoteLineItems
				lineGroup.LineGroup_Total_Discount_Amount__c = 0.00;
				lineGroup.LineGroup_Total_Extended_UnitPrice__c = 0.00;
				lineGroup.LineGroup_Total_GrossMargin_Amount__c = 0.00;
				lineGroup.LineGroup_Total_NetMargin_Amount__c = 0.00;
				lineGroup.LineGroup_Total_Landed_Cost__c = 0.00;
				lineGroup.LineGroup_Total_Standard_Cost__c = 0.00;
				lineGroup.LineGroup_TotalPrice_ExTax__c = 0.00;
				lineGroup.LineGroup_TotalPrice_IncTax__c = 0.00;
				lineGroup.LineGroup_Total_Quantity__c = 0;
				lineGroup.LineGroup_Total_ProposedQuantity__c = 0;
				lineGroup.LineGroup_Total_Tax__c = 0.00;
				lineGroup.LineGroup_Status__c = 'New';
				lineGroup.LineGroup_TotalPrice_ImpMaterialGroup__c = 0.00;
				lineGroup.LineGroup_TotalPrice_InsMaterialGroup__c = 0.00;
				lineGroup.LineGroup_TotalPrice_OtherMaterialGroup__c = 0.00;
				lineGroup.LineGroup_TotalStdCost_ImpMaterialGroup__c= 0.00;
		        lineGroup.LineGroup_TotalStdCost_InsMaterialGroup__c= 0.00;
		        lineGroup.LineGroup_TotalStdCost_OtherMaterialGrou__c = 0.00;
				quoteGroupsToUpdate.add(lineGroup);
			}
			
		}
	}
	
	public boolean updateRollups()
	{
		
		//Update QuoteLineGroups
		if (quoteGroupsToUpdate != null && quoteGroupsToUpdate.isEmpty() == false)
			update quoteGroupsToUpdate;
		
		return true;
	}
	
	//Update Quote Rollup Fields - DiscountedLineItemCount
	private void updateQuoteRollupFields(){
		if (quoteIds != null && quoteIds.size() > 0){
			Map<Id,Quote> mapQuotesToUpdate = new Map<Id,Quote>();
			for(Id quoteId : quoteIds){
				Quote updQuote = new Quote();
		    	updQuote.Id = quoteId;
		    	updQuote.DiscountedLineItemCount__c = 0;
		    	mapQuotesToUpdate.put(quoteId,updQuote);
			}
			
			for(AggregateResult[] aggreResults : [SELECT 
			                q.QuoteId, 
			                Count(q.Id) DiscountedLineItemCount
			            FROM QuoteLineItem q WHERE (q.QuoteId IN :quoteIds)  And (q.Discount > 0)
			            GROUP BY q.QuoteId]){

				for(AggregateResult aggreResult : aggreResults){
		    		Id quoteId = (Id) aggreResult.get('QuoteId');
		    		double discountedLineItemCount = (double)aggreResult.get('DiscountedLineItemCount');
		    		
		    		Quote updQuote = mapQuotesToUpdate.get(quoteId);
		    		updQuote.DiscountedLineItemCount__c = discountedLineItemCount;
		    		mapQuotesToUpdate.put(quoteId,updQuote);
		    	}                                          
			} 
			if (mapQuotesToUpdate.values().size() > 0)
				update mapQuotesToUpdate.values();
		}
	}
	
	public boolean rollupQuoteLines()
	{
		buildUniqueQuoteLineGroupIds();
		//System.assert(quoteLineGroupIds != null && quoteLineGroupIds.isEmpty() == false,'An error occured while execuing buildUniqueQuoteLineGroupIds method');
		buildQuoteLineGroups();
		//System.assert(quoteLineGroups != null && quoteLineGroups.isEmpty() == false,'An error occured while execuing buildQuoteLineGroups method');
		buildAggregateRollups(); // if no more QuoteLineItems then this method won't generate any AggregateRollups, still it needs to update with 0s.
		//System.assert(rollupLineGroups != null && rollupLineGroups.isEmpty() == false,'An error occured while execuing buildAggregateRollups method');
		buildQuoteGroupsToUpdate();
		//System.assert(quoteGroupsToUpdate != null && quoteGroupsToUpdate.isEmpty() == false,'An error occured while execuing buildQuoteGroupsToUpdate method');
		Boolean result = updateRollups();
		
		//Update Quote Rollup field - DiscountedLineItemCount
		updateQuoteRollupFields();
		
		return result;	
	}
	
	private class InsImpTotal{
		double totalPrice;
		double totalStandardCost;
		
		InsImpTotal(){
			totalPrice = 0;
			totalStandardCost = 0;
		}
	}
}