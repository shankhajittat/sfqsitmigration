@IsTest(seealldata=true)
public with sharing class AutocompleteSetClassPluginTest {

	@IsTest
	//This first test checks a quote that has no sets on it already
	public static void testAutocompleteSetClassPlugin() {
		
		
		AutoCompleteSetClassPlugin theTestSetClassPlugin = new AutoCompleteSetClassPlugin();
		
		String organizationCode = 'AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').setErpAccountId('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel And Division__c =: division LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel + division)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.division(division)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel + division);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
        String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity o = new OpportunityBuilder().name(nextOpportunityDocumentNumber).account(vinnies).priceBook(customPricebook).build(true);
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        Quote quote = new QuoteBuilder().name('My Quote 101').opportunity(o).priceBook(customPricebook).quoteTransactionType(quoteTransactionType).build(true);
       
        //Set up context with Quote id
        Map<String,String> context = new Map<String,String>();
		context.put('id2',quote.Id);
		theTestSetClassPlugin.init(context);
		
		// Test where set class doesn't exist on quote
		List<SObject> unfilteredSearchList = theTestSetClassPlugin.find('VV');
		System.assertEquals(0, unfilteredSearchList.size(), 'Search should return empty'); 
	}
	
	@IsTest
	//This first test checks a quote that has sets on it already
	public static void test2AutocopleteSetClassPlugin(){
		
		String organizationCode = 'AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').setErpAccountId('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel And Division__c =: division LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel + division)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.division(division)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel + division);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
        
	    // Taken from QuotationControllerExtensionTest.cls to create a quote and add the first set class to it
       	SetClassParser parser = new SetClassParser(TestingTools.getResourceBodyAsString('SetClass_JH230'));
 		parser.run();
 		Set_Class__c setClass = parser.newSetClass;
 		  	
        PageReference pageRef = Page.Quotation;
    	pageRef.getParameters().put('accId', vinnies.Id);
        Test.setCurrentPage(pageRef);
        
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote());
    	QuotationControllerExtension ext = new QuotationControllerExtension(stdController);
    	
    	System.assert(ext.account.Id != null, 'constructor should load the account sobject');
    	System.assert(ext.account.Name != null, 'constructor should load the account name for display');
		System.assert(ext.getPricebook().Id != null, 'constructor should load the standard pricebook');
    	System.assertEquals(ext.getGroupsWithRollups().size(), 0, 'No groups in account mode');
    	
    	ext.setClassId = setClass.Id;
    	PageReference urlAfterFirstSetClass = ext.chooseSet();
    	System.assertNotEquals(null, ext.extensionQuote, 'a new quote is generated');
    	
    	AutoCompleteSetClassPlugin theTestSetClassPlugin2 = new AutoCompleteSetClassPlugin();
		
    	 //Set up context with Quote id
        Map<String,String> context = new Map<String,String>();
		context.put('id2',ext.extensionQuote.Id);
		theTestSetClassPlugin2.init(context);
    	
    	// Test where set class exists on quote - SOSL results do not get returned in TESTS - see web docs as to why the next three lines are here
    	Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = ext.setClassId;
        Test.setFixedSearchResults(fixedSearchResults);
      
		List<SObject> unfilteredSearchList = theTestSetClassPlugin2.find('jh');
		System.assert(unfilteredSearchList <> null, 'Search returned empty'); 
	    system.assert(unfilteredSearchList.size() > 0, 'Search should not return empty'); 
       
       
		boolean filter = theTestSetClassPlugin2.filter(setClass);
		system.assertEquals(false, filter, 'Set Class should be filtered out because it exists o the quote already');
		
		// Check that styling has been set up
		string cssStyling = theTestSetClassPlugin2.css();
		system.assert(cssStyling.length() > 0, 'cssStyling should not be empty');
		
		// Check that render returns a value
		string jsRender = theTestSetClassPlugin2.jsRender();
		system.assert(jsRender.length() > 0, 'jsRender should not be empty');
		
		//jsHandleSelect mimics the user selecting an account
		string jsHandleSelect = theTestSetClassPlugin2.jsHandleSelect(new String[] {null, null},
																	new String[] {null, null});
		system.assert(jsHandleSelect.length() > 0, 'jsHandleSelect should not be empty');
	}
}