/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
@isTest
private class NetExamWebServiceAPIHelper71Test{

    public class MissingAccountInformationException extends Exception{}
    public class NetExamWebserviceException extends Exception{}
    
    //NetExam API Credentials    
    private static string ne_apiKey = 'APIKey';
    private static string ne_apiUser = 'APIUsername';
    private static string ne_apiPass = 'APIPassword';
     
   
     static testMethod void BulkUpsertUsers_Test() {    
       string result = 'FAIL';
       try
       {        
            webservicesNetexamComUserservice71.ArrayOfUser Users = new webservicesNetexamComUserservice71.ArrayOfUser();
            Users.User_x =  new webservicesNetexamComUserservice71.User_x[1];
                                  
                 
            webservicesNetexamComUserservice71.UserUserGroup acct = new webservicesNetexamComUserservice71.UserUserGroup();
            acct.ExternalId = '3837263728918376AAG';
            acct.Name = 'TestUsers_USA';
            acct.Country = 'USA';       
               
            webservicesNetexamComUserservice71.User_x user = new webservicesNetexamComUserservice71.User_x();
            user.FirstName = 'Carlito';
            user.LastName = 'Hlazo';
            user.UserName = 'carlito@email.com';
            user.Email = 'carlito@email.com';
            user.AlternateEmail = '';
            user.City = 'Dallas';
            user.Country = 'USA';
            user.State = 'TX';
            user.PostalCode = '75226';
            user.Language = 'English';
            user.BusinessPhone = '';
            user.PersonalPhone = '';
            user.PersonalEmail = '';
            user.JobCategory = '';
            user.JobTitle = '';
            user.TimeZone = '9';
            user.ExternalID = '786398483728379AAA';
            user.UserRole = 'Student';
            user.UserTypeName = 'Channel - US';
            user.ManagerExternalID = '';
            user.Status = '1';
            user.userGroup = acct;
            Users.User_x[0] = user;      
                   
           
            NetExamWebServiceAPIHelper71.BulkUpsertUsers(ne_apiKey, ne_apiUser, ne_apiPass, Users);
            
            result = 'SUCCESS';            
            system.assert(result == 'SUCCESS');        
        
        }
        catch(Exception ex)
        {                       
            system.assert(result == 'SUCCESS'); 
        }        
        
    }  

   static testMethod void SendContactToNetExam_TestMethod()
    {
    
        Account testAccount = new Account(Name='Test Account NE');
        insert testAccount;
 
        List<Account> objAccount = [Select ID FROM Account WHERE Name = 'Test Account NE' LIMIT 1];        
        
        String testAccountID = (String)objAccount[0].ID;
        system.assert(testAccountID.length() > 0);      
        
        //Missing Info
        string result = NetExamWebServiceAPIHelper71.ValidateAccountData('', '', '');
        system.assert(result != 'Success');
        
        result = NetExamWebServiceAPIHelper71.ValidateContactData('', '', '', '', '', '', '', '');
        system.assert(result != 'Success');
        
        //Missing Account Info
        result = NetExamWebServiceAPIHelper71.SendContactToNetExam('test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', '', '', '');
        system.assert(result != 'Success');
        
        //Missing Contact Info
        result = NetExamWebServiceAPIHelper71.SendContactToNetExam('', '', '', '', '', 'test', 'test', 'test', 'test', testAccountID, 'test');
        //system.assert(result == 'Success');
        
        //Valid Info
        result = NetExamWebServiceAPIHelper71.ValidateAccountData('1234', '1234', '1234');
        system.assert(result == 'Success');
        
        result = NetExamWebServiceAPIHelper71.ValidateContactData('test', 'test', 'test', 'test', 'test', 'test', 'test', 'test');
        system.assert(result == 'Success');
        
        result = NetExamWebServiceAPIHelper71.SendContactToNetExam('test', 'test', 'test', 'test', '', 'test', 'test', 'test', 'test', testAccountID, 'test');
        //system.assert(result == 'Success');  
        
        delete testAccount;
        
         
    }     

    static testMethod void SendContactFromTrigger_TestMethod()
    {
    
        Account testAccount = new Account(Name='Test Account NE');
        insert testAccount;
 
        List<Account> objAccount = [Select ID FROM Account WHERE Name = 'Test Account NE' LIMIT 1];        
        
        String testAccountID = (String)objAccount[0].ID;
        system.assert(testAccountID.length() > 0);
    
        try
        {
            //Missing Contact Info
            NetExamWebServiceAPIHelper71.SendContactFromTrigger('', '', '', '', 'test');
        }
        catch(MissingAccountInformationException e)
        {
            system.assert(e.getMessage() == 'Account Could Not Be Found');
        }        
        
        delete testAccount;
           
    }



    static testMethod void AddEditUserGroup_TestMethod()
    {
                
        // This always fails because it includes Callouts
        string result = NetExamWebServiceAPIHelper71.AddEditUserGroup('1234', '1234', '1234', '1234', '1234', '1234');
        //system.assert(result == 'Success');
    }
    
    
    static testMethod void UpsertUserWithUserGroups_TestMethod()
    {    
        // This always fails because it includes Callouts
        webservicesNetexamComUserservice71.ArrayOfUserUserGroup Groups = new webservicesNetexamComUserservice71.ArrayOfUserUserGroup();
        webservicesNetexamComUserservice71.ArrayOfCustomField CustomFields = new webservicesNetexamComUserservice71.ArrayOfCustomField();
        string result = NetExamWebServiceAPIHelper71.UpsertUserWithUserGroups('1234', '1234', '1234', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', Groups ,CustomFields,'USA',true,'test' );
        //system.assert(result == 'Success');
                                                          
    } 
}