public with sharing class QuoteShadowTools {

	public List<Quote__c> createShadowRecords(List<Quote> quotes) {
		List<Quote__c> shadows = new List<Quote__c>();
		
		for (List<Quote> quotesWithAccounts : [
			select Id, Name, Opportunity.Account.Id
			from Quote
			where Id in :quotes]) {		
				
			for (Quote q : quotesWithAccounts) {
				Quote__c shadow = new Quote__c(
					Quote__c = q.Id, 
					Name = q.Name,
					Opportunity__c = q.Opportunity.Id,
					Account__c = q.Opportunity.AccountId);
				shadows.add(shadow);
			}
		}
		insert shadows;
		
		return shadows;
	}

}