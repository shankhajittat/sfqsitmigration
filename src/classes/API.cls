global class API {

	global class Status {
		webservice Id id {get;set;}
		webservice boolean isSuccess {get;set;}
		webservice List<Error> errors {get;set;}
	}

	global class Error {
		webservice Integer code {get;set;}
		webservice String message {get;set;}
	}

	// the super class for all Records sent/received by APIs
	// null status means success for any write operation	
	global virtual class Record {		
		webservice Status status{get;set;}
		webservice Id id{get;set;}
	}

}