public with sharing class QuoteLineItemTools {
	
	/*
	public static List<APIv001.QuoteLineItemDetail> getQuoteLineItemsDetail(List<Id> quoteLineIds)
	{
		List<APIv001.QuoteLineItemDetail> result = new List<APIv001.QuoteLineItemDetail>();
		
		for(List<QuoteLineItem> quoteItems : [	Select qi.Quote.QuoteNumber, qi.Quote.Opportunity.Account.ERP_Account_ID__c, qi.QuoteId,
													qi.Quantity, qi.PricebookEntry.ProductCode, qi.Id From QuoteLineItem qi
												Where qi.Id IN : quoteLineIds ])
		{
			
			for(QuoteLineItem qi : quoteItems)
			{
				APIv001.QuoteLineItemDetail itemDetail = mapQuoteLineItemToQuoteLineItemDetail(qi);
				result.add(itemDetail);
			}
			
		}
		
		return result;
			
	}
	
	public static List<APIV001.QuoteLineItemDetail> getPendingQuoteLineItemsDetailToGetPrice(){
		List<APIv001.QuoteLineItemDetail> result = new List<APIv001.QuoteLineItemDetail>();
		
		for(List<QuoteLineItem> quoteItems : [	Select qi.Quote.QuoteNumber, qi.Quote.Opportunity.Account.ERP_Account_ID__c, qi.QuoteId,
													qi.Quantity, qi.PricebookEntry.ProductCode, qi.Id From QuoteLineItem qi
												Where qi.Price_Status__c IN ('New','Revalidate') ])
		{
			
			for(QuoteLineItem qi : quoteItems)
			{
				APIv001.QuoteLineItemDetail itemDetail = mapQuoteLineItemToQuoteLineItemDetail(qi);
				result.add(itemDetail);
			}
			
		}
		
		return result;
	}
	
	
	private static APIv001.QuoteLineItemDetail mapQuoteLineItemToQuoteLineItemDetail(QuoteLineItem qi)
	{
		APIv001.QuoteLineItemDetail itemDetail = new APIv001.QuoteLineItemDetail();
		itemDetail.id = qi.Id;
		itemDetail.quoteId = qi.QuoteId;
		itemDetail.companyCode = 'SYNAU';
		itemDetail.quoteNumber = qi.Quote.QuoteNumber;
		itemDetail.customerNumber = qi.Quote.Opportunity.Account.ERP_Account_ID__c;
		itemDetail.quantity = qi.Quantity;
		itemDetail.productCode = qi.PricebookEntry.ProductCode;
		
		return itemDetail;
	}
	
	
	public static List<APIV001.QuoteLineItemId> getPendingQuoteLineItemIdsToGetPrice(){
		List<APIV001.QuoteLineItemId> result = new List<APIV001.QuoteLineItemId>();
		
		for(List<QuoteLineItem> quoteItems : [	Select qi.Id From QuoteLineItem qi
												Where qi.Price_Status__c IN ('New','Revalidate') ])
		{
			
			for(QuoteLineItem qi : quoteItems)
			{
				APIV001.QuoteLineItemId itemId = new APIV001.QuoteLineItemId();
				itemId.id = qi.Id;
				result.add(itemId);
			}
			
		}
		
		return result;
		
	}
	
	*/
	
 
}