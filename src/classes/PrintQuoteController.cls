public with sharing class PrintQuoteController 
{
	/*
	public String configParamName {get;set;}
	public Id objectId{get;set;}
	public CongaConfig config {get;set;}
	public String sessionId {get;set;}
	public String serverUrl {get;set;}
	public class CongaConfig {
		public Id targetObjectId;
		public String targetObjectApiName;
		public String name;
		public String paramString;
		public Set<String> fieldList ;
		public SObject targetObject {get;set;}
	}*/
	
	public PrintQuoteController()
	{
		/*
		configParamName = ApexPages.currentPage().getParameters().get('configParamName');
		objectId = ApexPages.currentPage().getParameters().get('objectId');
		sessionId = ApexPages.currentPage().getParameters().get('sessionId');
		serverUrl = ApexPages.currentPage().getParameters().get('serverUrl');
		if(configParamName == null || configParamName == '' || sessionId == null || sessionId == '' || serverUrl == null || serverUrl == '' || objectId == null)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid input parameters.Please contact system administrator.'));
		}
		Conga_Doc_Gen_Setting__c cdgs = [select Name,TargetObjectApiName__c,paramString__c from Conga_Doc_Gen_Setting__c where name = :configParamName];
		if(cdgs == null)
		{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid Conga Document Generation Settings.Please contact system administrator.'));
		}
		else
		{
			config = new CongaConfig();
			config.Name = cdgs.Name;
			config.targetObjectApiName = cdgs.TargetObjectApiName__c;
			config.paramString = cdgs.paramString__c;
			config.fieldList = new Set<String>();
			if(config.paramString<> null && config.paramString <> '')
			{
				Pattern p = Pattern.compile('\\{\\!(\\w|\\.)+\\}');
				Matcher m = p.matcher(config.paramString);
				while (m.find()) {
					if(m.group()<> null)
					{
						config.fieldList.add(m.group().replaceAll('\\{\\!'+config.targetObjectApiName+'\\.','').replaceAll('\\}',''));
					}
				}
			}

			if(!config.fieldList.isEmpty())
			{
				String soqlString = 'select ';
				for(String fieldname : config.fieldList)
					soqlString += (fieldname + ',');
				soqlString = soqlString.substring(0,soqlString.length()-1) + ' from '+config.targetObjectApiName+' where Id= : objectId limit 1';
				try
				{
				config.targetObject = Database.query(soqlString);
				}
				catch(DMLException ex)
				{
					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error while populating target object information.Please contact system administrator.'));
				}
			}
			
			
		}
		*/
		
	}
	public PageReference printQuote()
	{
		
		Pagereference pageref = null;
		/*
		if(config.fieldList<> null && !config.fieldList.isEmpty())
		{
			for(String fieldName : config.fieldList)
			{
				String key = '\\{\\!'+config.targetObjectApiName+'\\.'+fieldName+'\\}';
				String value = (String) config.targetObject.get(fieldName);
				config.paramString = config.paramString.replaceAll(key,value);
			}
		}
		pageref = new PageReference('https://composer.congamerge.com?sessionId='+sessionId+'&serverUrl='+serverUrl+'&'+config.paramString);
		pageref.setRedirect(true);
		*/
		
		return pageref;
	}
}