@isTest
private class EditableTableQuoteControllerExtTest {

	@IsTest
	public static void testEditableTableQuoteControllerExtension() { 
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
		
			String organizationCode = 'UT-AU10';
        	String distributionChannel = '01';
        	String division = '01'; 	
		
			// Get set buy quote that was created in testing tools
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Test constructor
	    	ApexPages.StandardController stdController = new ApexPages.StandardController(theTestQuote);
			EditableTableQuoteControllerExtension theEditableTableQuoteControllerExt = new EditableTableQuoteControllerExtension(stdController);
			System.assert(theEditableTableQuoteControllerExt.extQuote.Id == theTestQuote.Id, 'Constructor did not set up extQuote - it should be the same as theTestQuote');
			System.assertNotEquals('{}', theEditableTableQuoteControllerExt.getExtQuoteJS(), 'The JSON object should contain the quote - it should not be empty');
			System.assertEquals(GlobalConstants.QuoteTypes.CustomerQuote, theEditableTableQuoteControllerExt.quoteType, 'Expected the quote type to be set buy' );
			System.assertEquals(true, theEditableTableQuoteControllerExt.showForSetBuy, 'Expected showForSetBuy to be true');
			System.assertEquals(false, theEditableTableQuoteControllerExt.showForCBC, 'Expected showForCBC to be false');
			
			// Test saveQuoteHeaderChanges method
			theEditableTableQuoteControllerExt.extQuote.Customer_Purchase_Order_Number__c = 'PO Test 101';
			PageReference thePageRefer = theEditableTableQuoteControllerExt.saveQuoteHeaderChanges();
			List<Quote> testQuotes = [SELECT Id, Customer_Purchase_Order_Number__c, QuoteType__c From Quote Where Id = :sourceQuoteId LIMIT 1];
			System.assertNotEquals(true, testQuotes.isEmpty(), 'Quote not found');
			System.assert(testQuotes[0].Customer_Purchase_Order_Number__c == 'PO Test 101', 'The quote should have been updated but has not');
			
			// Test getter for whether or not to show approval history
			Boolean showApprovalHistory = theEditableTableQuoteControllerExt.getShowApprovalHistory();
			System.assertEquals(false, showApprovalHistory, 'This quote is a setbuy at draft status - the approval history should not be shown - false should have been returned.');
			
			// Test viewState method
			PageReference thePR = theEditableTableQuoteControllerExt.updateViewStateHeader();
			System.assertEquals(sourceQuoteId, theEditableTableQuoteControllerExt.extQuote.Id, 'The ViewState should not have changed - The quote created in testing tools and the one found here are not the same but should be');
			
			// Test User preference
			PageReference thePageReference = theEditableTableQuoteControllerExt.navigateToUserPreferences();
			System.assert(thePageReference.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page should be redirected to the quote page');
			
								        
	        Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
			
			// Create another product
			Product2 anotherproduct = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL LARGE')
	        		.code('03.162.002')
	        		.build(true);
	        System.assert(anotherproduct.Id <> null,'An error occured while creating Product 03.162.002.');
	        Product2 theNewProduct = [SELECT Id, Name, ProductCode From Product2 Where ProductCode = '03.162.002'];
	        system.assertNotEquals(null, theNewProduct, 'A new product should have been created');
	        system.assertEquals('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL LARGE', theNewProduct.Name, 'A problem has occurred creating the second test product');
	      
	      	Product2 bomProduct = new ProductBuilder()
	        		.name('BOM - BENDING TMPLTE F/OCCIPIT PLATE MEDIAL LARGE')
	        		.code('03.162.002BOM')
	        		.build(true);
	        System.assert(bomProduct.Id <> null,'An error occured while creating Product 03.162.002BOM.');
	        
	     
	        
	        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
	        									.productId(anotherproduct.Id)
	        									.pricebookId(Test.getStandardPricebookId()).build(true);
	        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
	        									.productId(anotherproduct.Id)
	        									.pricebookId(customPricebook.Id).build(true);
	        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');

		       
	        
			// Create Set Class
	        Set_Class__c setClass = new SetClassBuilder()
	        								.equipmentNumber('TEST SC202').productCode('03.162.002BOM').build(true);
	        System.assert(setClass.Id <> null,'An error occured while creating SetClass TEST SC202.');
	        
	        //Create SetClass Items
	        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
	        								.header(setClass)
	        								.product(anotherproduct)
	        								.quantity(4).build(true);
	        
	        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
	        
	        
			Map<String, Object> returnObj = EditableTableQuoteControllerExtension.addSet((String)sourceQuoteId, (String)setClass.Id);
			EditableTableGroup addedSet = (EditableTableGroup)returnObj.get('group');  
			System.assert(addedSet.records.size() > 0, 'At least one item should have been returned');
			Map<String,String> theTestGroup = addedSet.attributes;
			Boolean contains = theTestGroup.containsKey('Id');
			System.assertEquals(contains, True, 'The group attributes do not contain a group Id field');
			String oneGroupId = theTestGroup.get('Id');
			System.assertNotEquals(null, oneGroupId, 'Group data should have been returned');
			
			
			// Test bomIndicator method which explodes the BOM into items
			
			List<Set_Class_Items__c> setClassItems = EditableTableQuoteControllerExtension.bomIndicator('03.162.002', null, 1);
			system.assertEquals(0, setClassItems.size(), 'This product is not a bom so no results should have been returned');
			
			
			// Now test addset but pass a quote id of null to test that group returned is null and doesn't get created.
			addedSet = null;
			try {
				returnObj = EditableTableQuoteControllerExtension.addSet(null, (String)setClass.Id); 
				Integer i = 1;
				System.assertEquals(5, i, 'This line should never have executed - a custom exception should have been thrown from the addSet method since quote id is null') ;
			} catch (EditableTableQuoteControllerExtension.ValueException ve) {
				// this is the expected behaviour
				System.assertEquals('Adding BOM failed', ve.getMessage(), 'Expected a custom error message');
			} 
			System.assertEquals(null, addedSet, 'No items should have been returned because an invalid quoteid was passed in');
			
			// Test submitQuoteForApproval method - first set expiry date to null to test error condition, then set to a value to test valid process
			//theEditableTableQuoteControllerExt.extQuote.ExpirationDate = null;
			//theEditableTableQuoteControllerExt.updateQuote();
			//theEditableTableQuoteControllerExt.submitQuoteForApproval();
			theEditableTableQuoteControllerExt.extQuote.ExpirationDate = System.Now().Date();
			theEditableTableQuoteControllerExt.updateQuote();
			//theEditableTableQuoteControllerExt.submitQuoteForApproval();
			//System.assertEquals(true,theEditableTableQuoteControllerExt.isQuoteUpdatedWithApprovers,'A problem occurred submitting quote for approval - Populating quote with approvers failed');
//TODO find out how to create an approval process in the test so that the approval process is found when the submit for approval code is tested

			//Create UnitTest Prerequisite Data.
	        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
        			
			//need to impliment external webservice mock
			Test.startTest() ;
			
    	 	// This causes a fake response to be generated
        	Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        	// Call the method that invokes a callout
        	PageReference p = theEditableTableQuoteControllerExt.submitQuoteWithAllQuoteLinesForPricing();

			//Cannot test two interfaces together, since it has data update, get the this error message : You have uncommitted work pending. Please commit or rollback before calling out
			
			// Test wonQuote method - can only update an approved quote as won
			//Test.setMock(WebServiceMock.class, new WebMethodsQuotationSoapAPIv1MockImpl());
			//theEditableTableQuoteControllerExt.wonQuote();
			
			// Test wonQuote - first set customer po to null to test error condition, then set it to a valid value to test valid process
			theEditableTableQuoteControllerExt.extQuote.Customer_Purchase_Order_Number__c = null;
			theEditableTableQuoteControllerExt.updateQuote();
			PageReference page = theEditableTableQuoteControllerExt.wonQuote();
			System.assertEquals('Customer Purchase Order number must be entered before sending this quote to ERP', theEditableTableQuoteControllerExt.errorMessage, 'An error should have been thrown since customer purchase order number is null');
			theEditableTableQuoteControllerExt.extQuote.Customer_Purchase_Order_Number__c = 'PO12345';
			theEditableTableQuoteControllerExt.updateQuote();
			page = theEditableTableQuoteControllerExt.wonQuote();
			
			Test.stopTest();
			
			Quote updatedQuote = [SELECT Id, QuoteNumber,Status From Quote Where Id = :sourceQuoteId];
			system.debug('updatedQuote=>' + updatedQuote);
			//System.assertEquals(GlobalConstants.QS_STATUS_SUBMITTED_FOR_PRICING, updatedQuote.Status, 'Failed to update the quote to Submitted for Pricing.');
		
			//Test.stopTest();
			
			// Test lostQuote method - can only update an approved quote as won
			//theEditableTableQuoteControllerExt.lostQuote();
			//System.assertEquals('Lost', theEditableTableQuoteControllerExt.extQuote.Customer_Approval__c, 'Failed to update the quote to lost');
		}
	}

	@IsTest
	public static void testEditableTableQuoteControllerExtension2() { 
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			
		
			Quote theTestQuote = [SELECT Id, QuoteType__c,Opportunity.AccountId From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Test constructor
	    	ApexPages.StandardController stdController = new ApexPages.StandardController(theTestQuote);
			EditableTableQuoteControllerExtension theEditableTableQuoteControllerExt = new EditableTableQuoteControllerExtension(stdController);
			System.assert(theEditableTableQuoteControllerExt.extQuote.Id == theTestQuote.Id, 'Constructor did not set up extQuote - it should be the same as theTestQuote');
			System.assertNotEquals('{}', theEditableTableQuoteControllerExt.getExtQuoteJS(), 'The JSON object should contain the quote - it should not be empty');
			
			// Test copyQuote
			Pagereference page = theEditableTableQuoteControllerExt.copyQuote();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page for copy quote should be redirected to the quote page');
			System.assert(page.getUrl().contains('/apex/CopyQuote?id='), 'The pagereference url does not contain /apex/CopyQuote?id= ');
			
			// Set the quote to won to test the getter for disabling approval button
			//theEditableTableQuoteControllerExt.extQuote.Customer_Approval__c = 'Won';
			theEditableTableQuoteControllerExt.extQuote.Status = GlobalConstants.QS_STATUS_COMPLETED_WON;
			theEditableTableQuoteControllerExt.updateQuote();
			Boolean isDisabled = theEditableTableQuoteControllerExt.getSaveButtonDisabledStatus();
			System.assertEquals(true, isDisabled, 'The save button should be disabled if the quote has been won because the user should no longer be able to change the po number if the quote is won. ');
			
			// Test lostQuote
			PageReference pageRef = theEditableTableQuoteControllerExt.lostQuote();
			//System.assertEquals('Lost', theEditableTableQuoteControllerExt.extQuote.Customer_Approval__c, 'The customer approval should have been set to Lost');
			System.assertEquals(GlobalConstants.QS_STATUS_COMPLETED_LOST,theEditableTableQuoteControllerExt.extQuote.Status, 'The quote should have been updated to Completed when it was Lost');
			
			// Test getter for savebuttondisabled - completed condition
			isDisabled = theEditableTableQuoteControllerExt.getSaveButtonDisabledStatus();
			System.assertEquals(true, isDisabled, 'The save button should be disabled if the quote has been won because the user should no longer be able to change the po number if the quote is won. ');
			
			// Test getter for revisebuttondisabled - setbuy and completed condition
			isDisabled = theEditableTableQuoteControllerExt.getReviseButtonDisabled();
			//System.assertEquals(false, isDisabled, 'The revise button should be enabled if the quote has been lost');
			
			
			// Test reviseQuote
			page = theEditableTableQuoteControllerExt.reviseQuote();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page for revise quote should be redirected to the quote page');
			System.assert(page.getUrl().contains('/apex/ReviseQuote?id='), 'The pagereference url does not contain /apex/ReviseQuote?id= ');
			
			//Test for Quote Deletion if the status is Draft
			theEditableTableQuoteControllerExt.extQuote.Status = GlobalConstants.QS_STATUS_DRAFT;
			page = theEditableTableQuoteControllerExt.deleteQuote();
			List<Quote> quoteList= [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assert(page.getUrl().contains('/'+theTestQuote.Opportunity.AccountId),'The page should be redirected to the account page as no return url is present.');
			System.assert(quoteList.IsEmpty(),'The quote should be deleted if it is in Draft Status');
		}
	}
	
	@IsTest
	public static void testEditableTableQuoteControllerExtensionCBCQuote() { 
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getCBCandSPTestQuote(GlobalConstants.QuoteTypes.CBC);
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid CBC testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			
		
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Test constructor
	    	ApexPages.StandardController stdController = new ApexPages.StandardController(theTestQuote);
			EditableTableQuoteControllerExtension theEditableTableQuoteControllerExt = new EditableTableQuoteControllerExtension(stdController);
			System.assert(theEditableTableQuoteControllerExt.extQuote.Id == theTestQuote.Id, 'Constructor did not set up extQuote - it should be the same as theTestQuote');
			System.assertNotEquals('{}', theEditableTableQuoteControllerExt.getExtQuoteJS(), 'The JSON object should contain the quote - it should not be empty');
			System.assertEquals(GlobalConstants.QuoteTypes.CBC, theEditableTableQuoteControllerExt.quoteType, 'Expected the quote type to be CBC' );
			System.assertEquals(false, theEditableTableQuoteControllerExt.showForSetBuy, 'Expected showForSetBuy to be false');
			System.assertEquals(true, theEditableTableQuoteControllerExt.showForCBC, 'Expected showForCBC to be true');
			
			// Test getter for whether or not to show approval history
			Boolean showApprovalHistory = theEditableTableQuoteControllerExt.getShowApprovalHistory();
			System.assertEquals(true, showApprovalHistory, 'This quote is a cbc - the approval history should always be shown - true should have been returned.');
			
			// Test saveInstructions
			theEditableTableQuoteControllerExt.extQuote.Instruction_Comments__c = 'This is a test comment';
			PageReference savePage = theEditableTableQuoteControllerExt.saveInstructions();
			Quote checkInstructionsQuote = [SELECT Id, Instruction_Comments__c FROM Quote WHERE Id = :theTestQuote.Id];
			System.assertEquals('This is a test comment', checkInstructionsQuote.Instruction_Comments__c, 'The quote failed to be updated by the saveInstructions method');
			
			// Test getter for savebuttondisabled - cbc and draft condition
			theEditableTableQuoteControllerExt.extQuote.Status = GlobalConstants.QS_STATUS_APPROVED;
			theEditableTableQuoteControllerExt.updateQuote();
			Boolean isDisabled = theEditableTableQuoteControllerExt.getSaveButtonDisabledStatus();
			System.assertEquals(true, isDisabled, 'The save button should be disabled if the quote has been won because the user should no longer be able to change the po number if the quote is won. ');
			
			// Test getter for revisebuttondisabled
			isDisabled = theEditableTableQuoteControllerExt.getReviseButtonDisabled();
			System.assertEquals(true, isDisabled, 'The revise button should be disabled for CBC quote');
			
			// Test copyQuote
			Pagereference page = theEditableTableQuoteControllerExt.copyQuote();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page for copy quote should be redirected to the quote page');
			System.assert(page.getUrl().contains('/apex/CopyQuote?id='), 'The pagereference url does not contain /apex/CopyQuote?id= ');
			
			//need to impliment external webservice mock
			Test.startTest() ;
			
    	 	// This causes a fake response to be generated
        	Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());

			//Cannot test two interfaces together, since it has data update, get the this error message : You have uncommitted work pending. Please commit or rollback before calling out
			
			// Test wonQuote
			PageReference pageRef = theEditableTableQuoteControllerExt.cbcSendToERP();
			
			Test.stopTest();
			
			// Test reviseQuote
			page = theEditableTableQuoteControllerExt.reviseQuote();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page for revise quote should be redirected to the quote page');
			System.assert(page.getUrl().contains('/apex/ReviseQuote?id='), 'The pagereference url does not contain /apex/ReviseQuote?id= ');
			
			// Test selectApprovers
			page = theEditableTableQuoteControllerExt.selectApprovers();
			System.assertEquals(null,page,'The expiration date (agreement to date) was not set so the page reference should be null');
			theEditableTableQuoteControllerExt.extQuote.ExpirationDate = System.now().date();
			page = theEditableTableQuoteControllerExt.selectApprovers();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page should be redirected to the approval picker page with the quote id as a parameter - quote id is missing');
			System.assert(page.getUrl().contains('/apex/Approval_Picker?id='), 'The pagereference url does not contain /apex/Approval_Picker?id= ');
			
			// Test catch block of refresh function
			theEditableTableQuoteControllerExt.extQuote.id = null;
			theEditableTableQuoteControllerExt.refresh();
			System.assert(theEditableTableQuoteControllerExt.errorMessage.contains('An error occurred reading the quote. Contact support with the following error : '),
						  'Expected the catch block to fire since the quote id should have been null and therefore invalid' );
		}
	}
	
	@IsTest
	public static void testEditableTableQuoteControllerExtensionSPQuote() { 
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getCBCandSPTestQuote(GlobalConstants.QuoteTypes.SurgeonPreference);
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid SP testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			
		
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Test constructor
	    	ApexPages.StandardController stdController = new ApexPages.StandardController(theTestQuote);
			EditableTableQuoteControllerExtension theEditableTableQuoteControllerExt = new EditableTableQuoteControllerExtension(stdController);
			System.assert(theEditableTableQuoteControllerExt.extQuote.Id == theTestQuote.Id, 'Constructor did not set up extQuote - it should be the same as theTestQuote');
			System.assertNotEquals('{}', theEditableTableQuoteControllerExt.getExtQuoteJS(), 'The JSON object should contain the quote - it should not be empty');
			System.assertEquals(GlobalConstants.QuoteTypes.SurgeonPreference, theEditableTableQuoteControllerExt.quoteType, 'Expected the quote type to be CBC' );
			System.assertEquals(false, theEditableTableQuoteControllerExt.showForSetBuy, 'Expected showForSetBuy to be false');
			System.assertEquals(false, theEditableTableQuoteControllerExt.showForCBC, 'Expected showForCBC to be false');
			
			// Test copyQuote
			Pagereference page = theEditableTableQuoteControllerExt.copyQuote();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page for copy quote should be redirected to the quote page');
			System.assert(page.getUrl().contains('/apex/CopyQuote?id='), 'The pagereference url does not contain /apex/CopyQuote?id= ');
			
			// Test selectApprovers
			page = theEditableTableQuoteControllerExt.selectApprovers();
			System.assertEquals(null,page,'The expiration date (valid to date) was not set so the page reference should be null');
			theEditableTableQuoteControllerExt.extQuote.ExpirationDate = System.now().date();
			page = theEditableTableQuoteControllerExt.selectApprovers();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page should be redirected to the approval picker page with the quote id as a parameter - quote id is missing');
			System.assert(page.getUrl().contains('/apex/Approval_Picker?id='), 'The pagereference url does not contain /apex/Approval_Picker?id= ');
			
			
			//need to impliment external webservice mock
			Test.startTest() ;
			
    	 	// This causes a fake response to be generated
        	Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());

			//Cannot test two interfaces together, since it has data update, get the this error message : You have uncommitted work pending. Please commit or rollback before calling out
			
			// Test wonQuote
			PageReference pageRef = theEditableTableQuoteControllerExt.spSendToERP();
			
			Test.stopTest();
			
			// Test reviseQuote
			page = theEditableTableQuoteControllerExt.reviseQuote();
			System.assert(page.getUrl().contains(theEditableTableQuoteControllerExt.extQuote.Id), 'The page for revise quote should be redirected to the quote page');
			System.assert(page.getUrl().contains('/apex/ReviseQuote?id='), 'The pagereference url does not contain /apex/ReviseQuote?id= ');
			
		}
	}
	
	
	@IsTest
	public static void testEditableTableQuoteContrExtCBCAddBOM2BOM() { 
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid CBC testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			String organizationCode = 'UT-AU10';
        	String distributionChannel = '01';
        	String division = '01'; 
		
			// Get set buy quote that was created in testing tools
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Create another set and set classitems to add the set onto the quote using the populator. First pricebook and product and pbe must be created
			Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
			
			// Create another product
			Product2 anotherproduct = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL LARGE')
	        		.code('03.162.002')
	        		.build(true);
	        System.assert(anotherproduct.Id <> null,'An error occured while creating Product 03.162.002.');
	        Product2 theNewProduct = [SELECT Id, Name, ProductCode From Product2 Where ProductCode = '03.162.002'];
	        system.assertNotEquals(null, theNewProduct, 'A new product should have been created');
	        system.assertEquals('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL LARGE', theNewProduct.Name, 'A problem has occurred creating the second test product');
	        system.debug('>>>>>>>>>>>>>>>>>>Second prodcut : ' + anotherproduct);
	        
	        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
	        									.productId(anotherproduct.Id)
	        									.pricebookId(Test.getStandardPricebookId()).build(true);
	        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
	        									.productId(anotherproduct.Id)
	        									.pricebookId(customPricebook.Id).build(true);
	        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');

		       
			
			// Create Set Class
	        Set_Class__c setClass = new SetClassBuilder()
	        								.equipmentNumber('TEST SC202').productCode('03.162.002').build(true);
	        System.assert(setClass.Id <> null,'An error occured while creating SetClass TEST SC202.');
	        
	        //Create SetClass Items
	        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
	        								.header(setClass)
	        								.product(anotherproduct)
	        								.quantity(4).build(true);
	        
	        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
	        
			// Test constructor
	    	ApexPages.StandardController stdController = new ApexPages.StandardController(theTestQuote);
			EditableTableQuoteControllerExtension theEditableTableQuoteControllerExt = new EditableTableQuoteControllerExtension(stdController);
			System.assert(theEditableTableQuoteControllerExt.extQuote.Id == theTestQuote.Id, 'Constructor did not set up extQuote - it should be the same as theTestQuote');
			System.assertNotEquals('{}', theEditableTableQuoteControllerExt.getExtQuoteJS(), 'The JSON object should contain the quote - it should not be empty');
			System.assertEquals(GlobalConstants.QuoteTypes.CustomerQuote, theEditableTableQuoteControllerExt.quoteType, 'Expected the quote type to be set buy' );
			System.assertEquals(true, theEditableTableQuoteControllerExt.showForSetBuy, 'Expected showForSetBuy to be true');
			System.assertEquals(false, theEditableTableQuoteControllerExt.showForCBC, 'Expected showForCBC to be false');
			
	        
			Map<String, Object> returnObj = EditableTableQuoteControllerExtension.addSet((String)sourceQuoteId, (String)setClass.Id);
			EditableTableGroup addedSet = (EditableTableGroup)returnObj.get('group');  
			System.assert(addedSet.records.size() > 0, 'At least one item should have been returned');
			Map<String,String> theTestGroup = addedSet.attributes;
			Boolean contains = theTestGroup.containsKey('Id');
			System.assertEquals(contains, True, 'The group attributes do not contain a group Id field');
			String oneGroupId = theTestGroup.get('Id');
			System.assertNotEquals(null, oneGroupId, 'Group data should have been returned');
			
			// Get the group created
			Quote_Line_Group__c newGroup = [SELECT Id, SetClassId__c from Quote_Line_Group__c Where QuoteId__c = :theTestQuote.Id AND SetClassId__c = :setClass.Id];
			System.assertNotEquals(null, newGroup, 'A group should have been created for this quote and set class');
			System.assertNotEquals(null, newGroup.Id, 'The group Id should not be null');
			System.assertEquals(setClass.Id, newGroup.SetClassId__c, 'The group should contain the set class that it was templated from');
			
			// Create another product
			Product2 yetAnotherProduct = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE VERY SMALL')
	        		.code('03.162.333')
	        		.build(true);
	        System.assert(yetAnotherProduct.Id <> null,'An error occured while creating Product 03.162.333.');
	        Product2 theNewProd = [SELECT Id, Name, ProductCode From Product2 Where ProductCode = '03.162.333'];
	        system.assertNotEquals(null, theNewProd, 'A new product should have been created');
	        system.assertEquals('BENDING TMPLTE F/OCCIPIT PLATE VERY SMALL', theNewProd.Name, 'A problem has occurred creating the test product');
	        
	        
	        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry2 = new PricebookEntryBuilder()
	        									.productId(yetAnotherProduct.Id)
	        									.pricebookId(Test.getStandardPricebookId()).build(true);
	        System.assert(pbStandardEntry2.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry2 = new PricebookEntryBuilder()
	        									.productId(yetAnotherProduct.Id)
	        									.pricebookId(customPricebook.Id).build(true);
	        System.assert(pbCustomEntry2.Id <> null,'An error occured while creating PricebookEntry.');

		       
			//Create an additional SetClass Item
	        Set_Class_Items__c setClassItem2 = new SetClassItemBuilder()
	        								.header(setClass)
	        								.product(yetAnotherProduct)
	        								.quantity(9).build(true);
	        								
			returnObj = EditableTableQuoteControllerExtension.addSetToSet(theTestQuote.Id, setClass.Id, newGroup.Id);
			System.assertNotEquals(null, returnObj, 'A group should have been returned');
			EditableTableGroup theGroup = (EditableTableGroup)returnObj.get('group');
			System.assert(theGroup.attributes != null && theGroup.attributes.size() != 0,'The test group should have been returned');
			System.assertEquals(theGroup.attributes.get('id'),newGroup.Id, 'The test group should be in the map');
			System.assert(theGroup.records != null && theGroup.records.isEmpty() == false,'Items should have been returned');
			
			// Get the items that should have been created
			List<QuoteLineItem> theItems = [SELECT Id, Set_Class__c, Set_Class_Item__c, QuoteId, Quantity, Quote_Line_Group__c From QuoteLineItem 
											Where QuoteId = :theTestQuote.Id AND Quote_Line_Group__c = :newGroup.Id AND Set_Class__c = :setClass.Id]; 
			System.assertNotEquals(true,theItems.isEmpty(),'There should be items for this quote, group and set class');
			
			Boolean foundFirstTestItem = false;
			Boolean foundSecondTestItem = false;
			for (QuoteLineItem item : theItems) {
				if (item.Set_Class_Item__c == setClassItem1.Id) {
					foundFirstTestItem = true;
					System.assertEquals(8, item.Quantity, 'The quantity for the first item should have doubled when the same BOM was added to the group');
				}
				if (item.Set_Class_Item__c == setClassItem2.Id) {
					foundSecondTestItem = true;
					System.assertEquals(9, item.Quantity, 'The quantity for the second item should have been the same as the set class item since that item did not already exist on the group');
				}
			}
			System.assertEquals(true, foundFirstTestItem, 'Expected an item to have been created for setClassItem1');
			System.assertEquals(true, foundSecondTestItem, 'Expected an item to have been created for setClassItem2');
		}
	}
	@IsTest
	public static void testDefaultUserPreferenceSelectionAndPopulationInQuote()
	{
		User runAsUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(runAsUser) {
		//Populate BU List Test Data
		List<Default_BU_List__c> defBUoptions = new List<Default_BU_List__c>();
		defBUoptions.add( new Default_BU_List__c(Name='CODMAN'));
		defBUoptions.add( new Default_BU_List__c(Name='TRAUMA'));
		insert defBUoptions;
		//save user preference data
		QuoteUserPreferenceController controller = new QuoteUserPreferenceController();
		Component.Apex.SelectList selList = controller.getAllDefaultBUSelectOptions();
		System.assertEquals(2,selList.ChildComponents.size(),'Expected to populate 2 BU Options SelectOptions as child component.');
		controller.selectedDefaultBUSelectOptions = defBUoptions.get(0).Name;
		boolean success = controller.saveDefaultBUPreference();
		ObjectFieldsUserPreference objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    	ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.DefaultBUSelection);
		List<SelectOption> selectedDefaultBUAsSelectOptionsList = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
		System.assertEquals(1,selectedDefaultBUAsSelectOptionsList.size(),'Expected BU Preference save to be successful.');
		System.assertEquals(defBUoptions.get(0).Name,selectedDefaultBUAsSelectOptionsList[0].getValue(),'Expected BU Preference value to be same after save.');
		//Create A new Quote with default BU
		Id sourceQuoteId = null;
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			break;
		}
		

		Quote theTestQuote = [SELECT Id, QuoteType__c,Business_Unit__c From Quote Where Id = :sourceQuoteId];
		//Open the Quote Editor for the created Quote and check BU population
		ApexPages.StandardController stdController = new ApexPages.StandardController(theTestQuote);
		EditableTableQuoteControllerExtension theEditableTableQuoteControllerExt = new EditableTableQuoteControllerExtension(stdController);
		List<SelectOption> selOptionList = theEditableTableQuoteControllerExt.getAllDefaultBUSelectOptions();
		System.assertEquals(2,selOptionList.size(),'Expected to populate 2 BU Options SelecOptions in Quote Editor dropdown.');
					
		}
	}
	 
}