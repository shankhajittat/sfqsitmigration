public class ItemGroupWrapper implements Comparable {
	
	public Integer compareTo(Object objectToCompareTo) {
		return setClassId.compareTo(((ItemGroupWrapper)objectToCompareTo).setClassId);
	}

	public Boolean selected{get;set;} // only used to maintain state of UI. not used for Apex logic
	public String setClassId{get;set;}
	public String groupName{get;set;}
	public double salesTotal{get;set;}
	public double discountAmountTotal{get;set;}
	public double discountPercentageTotal{get;set;}
	
	public List<ItemWrapper> items {
		get {
			if (items == null) {
				items = new List<ItemWrapper>(); 
			}
			return items;
		}
		set;
	}
	
	public ItemGroupWrapper(String setClassId, String name, double salesTotal, double discountAmountTotal, double discountPercentageTotal) {
		this.setClassId = setClassId;
		this.groupName = name;
		this.salesTotal = salesTotal;
		this.discountAmountTotal = discountAmountTotal;
		this.discountPercentageTotal = discountPercentageTotal;		
	}	

}