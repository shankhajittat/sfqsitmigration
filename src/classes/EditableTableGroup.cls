public class EditableTableGroup implements Comparable{

	public List<Sobject> records{
		get {
			if (records == null) {
				records = new List<Sobject>(); 
			}
			return records;
		}
		set;}
	public Map<String, String> attributes{
		get {
			if (attributes == null) {
				attributes = new Map<String, String>(); 
			}
			return attributes;
		}
		set;} 
	public Integer compareTo(Object objectToCompareTo)
	{
		EditableTableGroup objToCompare = (EditableTableGroup) objectToCompareTo;
		if(this.attributes<> null && objToCompare.attributes<> null && this.attributes.get('CreatedDateGMT')<> null && objToCompare.attributes.get('CreatedDateGMT')<> null)
		{
			DateTime dtThis = Datetime.valueOfGMT(this.attributes.get('CreatedDateGMT'));
			DateTime dtCompareTo = Datetime.valueOfGMT(objToCompare.attributes.get('CreatedDateGMT'));
			if( dtThis > dtCompareTo)
				return 1;
			else
				return -1;
		}
		else 
			return 0;
	}
}