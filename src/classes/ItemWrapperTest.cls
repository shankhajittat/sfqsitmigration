@isTest(seeAllData=true)
private class ItemWrapperTest {

	@IsTest
    static void myItemWrapperTest() {
        		
        
        QuoteLineItem newQuoteLineItem = new QuoteLineItem();
        newQuoteLineItem.Quantity = 5.00;
        newQuoteLineItem.UnitPrice = 100.00;
        newQuoteLineItem.Discount = 10;
        
    	System.assert(newQuoteLineItem <> null, 'newQuoteLineItem is null');
    	
    	
    	ItemWrapper iw = new ItemWrapper(newQuoteLineItem, false);
    	
    	System.assertEquals(iw.getQuantity(),'5','Quantity is not same');
    	
    	System.assertEquals(iw.getDiscountP(),'10%','DiscountP is not same');
    	//System.debug(iw.Item.Discount_Amount__c);
    	//System.debug(iw.getDiscountA());
    	System.assertEquals(iw.getDiscountA(),null,'DiscountA is not same');
    	
    	
    	iw.selected = true;
    	System.assertEquals(iw.selected,true,'selected property is not true');
    	
        
        
    }
}