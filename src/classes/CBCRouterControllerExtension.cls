public with sharing class CBCRouterControllerExtension {
	public final Id cbcId;
	public Id cbcAccountId {get; set;}
	public String retURL;
	
	public CBCRouterControllerExtension(ApexPages.StandardController stdController) {
        this.cbcId = stdController.getRecord().Id;
	}
	
	// use the retURL which is always passed by standard buttons to resolve the account id
	public Id getMasterRecordId() {
		try {
			retURL = ApexPages.currentPage().getParameters().get('retURL');
			if (retURL == null) {
				return null;
			} else {
				// If this page has been initiated from the custom CBC tab then the url will not have a valid account so the account picker needs to be enabled
				String idInURL = retURL.substring(1);
				String validatedId = StringUtils.validateId(idInURL);
				return validatedId;
			}
		} catch (Exception e) {
	 		ApexPages.addMessages(e);
	 		return null;
		}
	}

	public PageReference route() {
		Boolean isNewCBC = this.cbcId == null;
		
		if (isNewCBC) {
			String masterId = getMasterRecordId();
			Boolean masterIsAccount = masterId != null && masterId.startsWith('001');
			
			if (masterIsAccount) {
				PageReference pr = new PageReference('/apex/QuoteSBPartner?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.CBC.ordinal() +
					'&retURL=' + retURL);
				pr.setRedirect(true);
				return pr;
			} else {
				PageReference pr = new PageReference('/apex/Quote_Account_Picker?quoteType=' + GlobalConstants.QuoteTypes.CBC.ordinal());
				pr.setRedirect(true);
				return pr;
			}
		}else {
			// Redirect to a VF page that just runs an action to create quote and related records because we don't need any user input in this case
			Consignment_Business_Case__c cbc = [select Id, Account__c, ShippingLocation__c from Consignment_Business_Case__c where Id = :this.cbcId];
		
			PageReference target = Page.QuoteCreate;
			target.getParameters().put('accId',cbc.Account__c);
			target.getParameters().put('quoteType', String.valueof(GlobalConstants.QuoteTypes.CBC.ordinal()));
			target.getParameters().put('SBPartner', cbc.ShippingLocation__c);
			target.setRedirect(true);
			return target;
		}
	}
}