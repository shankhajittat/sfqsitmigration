@isTest
public class JSONPreferenceToolsTest {

	@isTest
    static void testJSONReads() {
		JSONPreferenceTools.getPreferences('QUOTE_GRIDS');
		JSONPreferenceTools.getPreferences('QUOTE_COLLAPSIBLE');
    }
    
	@isTest
    static void testCollapsiblePreferences() {

		Map<String, Map<String, String>> c = JSONPreferenceTools.readCollapsiblePreferences();
    	System.assertEquals('open', c.get('summary').get('state'), 'default summary state should be open');
    	System.assertEquals('closed', c.get('address').get('state'), 'default address state should be closed');
    	System.assertEquals('closed', c.get('instruction').get('state'), 'default user state should be closed');
    	System.assertEquals('closed', c.get('user').get('state'), 'default user state should be closed');

		String saveSample = '{"section":"user","state":"open"}';
    	QuoteEditorControllerExtension.setPreferences('QUOTE_COLLAPSIBLE', saveSample);
		
    	c = JSONPreferenceTools.readCollapsiblePreferences();
    	System.assertEquals('open', c.get('user').get('state'), 'user state is open after save');

    }
    
	@isTest
    static void testGridPreferences() {
    	JSONPreferenceTools.GridPrefs gp = JSONPreferenceTools.readGridPreferences();
    	System.assertEquals(false, gp.SETBUY_SORT_DIR_ASC, 'translated default sort dir should be ascending');
    	System.assertEquals(true, gp.CBC_SORT_DIR_ASC, 'translated default sort dir should be ascending');
    	System.assertEquals(false, gp.SP_SORT_DIR_ASC, 'translated default sort dir should be ascending');
    	System.assertEquals(false, gp.CHANGESET_SORT_DIR_ASC, 'translated default sort dir should be ascending');
    	System.assertEquals(false, gp.LOANSETREQUEST_SORT_DIR_ASC, 'translated default sort dir should be ascending');
    	
    	System.assertEquals('ProductCode', gp.SETBUY_SORT_FIELD, 'translated default sort field should be ProductCode');
    	System.assertEquals('ProductCode', gp.CBC_SORT_FIELD, 'translated default sort field should be ProductCode');
    	System.assertEquals('ProductCode', gp.SP_SORT_FIELD, 'translated default sort field should be ProductCode');
    	System.assertEquals('ProductCode', gp.CHANGESET_SORT_FIELD, 'translated default sort field should be ProductCode');
    	System.assertEquals('ProductCode', gp.LOANSETREQUEST_SORT_FIELD, 'translated default sort field should be ProductCode');
    	
    	System.assertEquals(8, gp.SETBUY_COLUMNS_DISPLAYED.size(), '8 default fields are displayed');
    	System.assertEquals(4, gp.CBC_COLUMNS_DISPLAYED.size(), '4 default fields are displayed');
    	System.assertEquals(4, gp.SP_COLUMNS_DISPLAYED.size(), '4 default fields are displayed');
    	System.assertEquals(4, gp.CHANGESET_COLUMNS_DISPLAYED.size(), '4 default fields are displayed');
    	System.assertEquals(4, gp.LOANSETREQUEST_COLUMNS_DISPLAYED.size(), '4 default fields are displayed');
    	
    	System.assertEquals('Actions', gp.SETBUY_COLUMNS_DISPLAYED.get(0), 'default first field displayed should be Actions');
    	System.assertEquals('Actions', gp.CBC_COLUMNS_DISPLAYED.get(0), 'default first field displayed should be Actions');
    	System.assertEquals('Actions', gp.SP_COLUMNS_DISPLAYED.get(0), 'default first field displayed should be Actions');
    	System.assertEquals('ProductCode', gp.SETBUY_COLUMNS_DISPLAYED.get(1), 'default second field displayed should be Actions');
    	System.assertEquals('ProductCode', gp.CBC_COLUMNS_DISPLAYED.get(1), 'default second field displayed should be Actions');
    	System.assertEquals('ProductCode', gp.SP_COLUMNS_DISPLAYED.get(1), 'default second field displayed should be Actions');   
    	System.assertEquals('ProductCode', gp.CHANGESET_COLUMNS_DISPLAYED.get(1), 'default second field displayed should be Actions');   
    	System.assertEquals('ProductCode', gp.LOANSETREQUEST_COLUMNS_DISPLAYED.get(1), 'default second field displayed should be Actions');   
    	 
    	System.assertEquals(7, gp.SETBUY_COLUMN_SIZES.size(), '7 default field sizes are provided');
    	System.assertEquals(4, gp.CBC_COLUMN_SIZES.size(), '4 default field sizes are provided');
    	System.assertEquals(4, gp.SP_COLUMN_SIZES.size(), '4 default field sizes are provided');    		
		System.assertEquals(4, gp.CHANGESET_COLUMN_SIZES.size(), '4 default field sizes are provided'); 
		System.assertEquals(4, gp.LOANSETREQUEST_COLUMN_SIZES.size(), '4 default field sizes are provided'); 
		
		System.debug(JSON.serialize(gp));
		
		// note that sort field and direction have changed from the default, quantity field is removed and product code is 350 instead of 250
		String saveSample = '{"SETBUY_SORT_FIELD":"Quantity","SETBUY_SORT_DIR_ASC":true,"SETBUY_COLUMNS_DISPLAYED":["Actions","ProductCode","UnitPrice","Discount_Amount__c","Discount","TotalPrice","DelCol"],"CONSETBUILD_SORT_FIELD":"Quantity","CONSETBUILD_SORT_DIR_ASC":true,"CONSETBUILD_COLUMNS_DISPLAYED":["Actions","ProductCode","UnitPrice","TotalPrice","DelCol"],"CBC_COLUMNS_DISPLAYED":["Actions","ProductCode"],"SP_COLUMNS_DISPLAYED":["Actions","ProductCode"],"SETBUY_COLUMN_SIZES":{"UnitPrice":100,"Discount":75,"Actions":125,"Discount_Amount__c":100,"TotalPrice":125,"ProductCode":350},"CONSETBUILD_COLUMN_SIZES":{"UnitPrice":100,"Actions":125,"TotalPrice":125,"ProductCode":350},"CBC_COLUMN_SIZES":{"Actions":125,"ProductCode":350},"SP_COLUMN_SIZES":{"Actions":125,"ProductCode":350}}';
    	QuoteEditorControllerExtension.setPreferences('QUOTE_GRIDS', saveSample);
    	
    	gp = JSONPreferenceTools.readGridPreferences();
    	System.assertEquals(true, gp.SETBUY_SORT_DIR_ASC, 'sort dir was updated from the JSON');
    	System.assertEquals('Quantity', gp.SETBUY_SORT_FIELD, 'sort field was updated from the JSON');
    	Set<String> displayed = new Set<String>();
    	displayed.addAll(gp.SETBUY_COLUMNS_DISPLAYED);
    	System.assert(!displayed.contains('Quantity'), 'Quantity column was removed from the JSON');
    	System.assertEquals(350, gp.SETBUY_COLUMN_SIZES.get('ProductCode'), 'ProductCode column size was increased by the JSON');
    	
    	displayed = new Set<String>();
    	displayed.addAll(gp.CONSETBUILD_COLUMNS_DISPLAYED);
    	System.assert(!displayed.contains('Quantity'), 'Quantity column was removed from the JSON');
    	System.assertEquals(350, gp.SETBUY_COLUMN_SIZES.get('ProductCode'), 'ProductCode column size was increased by the JSON');
    	
    	displayed = new Set<String>();
    	displayed.addAll(gp.CBC_COLUMNS_DISPLAYED);
    	System.assert(!displayed.contains('ERP_Record_Action_Mode__c'), 'ERP_Record_Action_Mode__c column was removed from the JSON');
    	System.assertEquals(350, gp.CBC_COLUMN_SIZES.get('ProductCode'), 'ProductCode column size was increased by the JSON');
    	
    	displayed = new Set<String>();
    	displayed.addAll(gp.SP_COLUMNS_DISPLAYED);
    	System.assert(!displayed.contains('ERP_Record_Action_Mode__c'), 'ERP_Record_Action_Mode__c column was removed from the JSON');
    	System.assertEquals(350, gp.SP_COLUMN_SIZES.get('ProductCode'), 'ProductCode column size was increased by the JSON');
    	
    }
    
}