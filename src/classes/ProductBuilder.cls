public with sharing class ProductBuilder {

	private static Integer sequence = 100;

	private String productName;
	private String productCode;
	private Product_Level__c level;

	//Mandatory fields with defaults
	private Boolean isActive = true;


	public ProductBuilder level(Product_Level__c level)
	{
		this.level = level;
		return this;
	}
	public ProductBuilder name(String productName)
	{
		this.productName = productName;
		return this;
	}
	
	public ProductBuilder code(String productCode)
	{
		this.productCode = productCode;
		return this;
	}
	
	
	
	
	
	public Product2 build(Boolean createInDatabase)
	{
		Product2 product = new Product2(
			Name = this.productName,
			ProductCode = this.productCode,
			IsActive = this.isActive,
			Description = this.productName
		);
		if (level != null) {
			product.Product_Level__c = level.Id;
		}
		
		if (createInDatabase)
		{
			insert product;
			
			/*
			List<PricebookEntry> priceBookEntries = new List<PricebookEntry>();
	
			if (this.priceBook2s.size() > 0)
			{
				for(Pricebook2 entry : this.priceBook2s)
				{
					priceBookEntries.add(new PricebookEntry(
						Product2Id = product.Id,
						Pricebook2Id = entry.Id,
						UnitPrice = 100.00,
						IsActive = true,
						UseStandardPrice = false
						));
				}
				insert priceBookEntries;
			}
			*/
			
		}
		
		return product;
	}
	
}