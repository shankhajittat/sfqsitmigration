public with sharing class QuoteLineGroupBuilder {

	private Quote quoteLineGroup_Quote;
	private Set_Class__c quoteLineGroup_SetClass;
	private String quoteLineGroup_groupName;
	private String quoteLineGroup_groupType;
	private String quoteLineGroup_Status;
	private Consignment_Business_Case__c cbc;
	private Surgeon_Preference__c surgeonPref;
	private ActualBillOfMaterial__c actualBOM;
	
	public QuoteLineGroupBuilder name(String groupName)
	{
		this.quoteLineGroup_groupName = groupName;
		return this;
	}
	
	public QuoteLineGroupBuilder quote(Quote quote)
	{
		this.quoteLineGroup_Quote = quote;
		return this;
	}

	public QuoteLineGroupBuilder setClass(Set_Class__c setClass)
	{
		this.quoteLineGroup_SetClass = setClass;
		return this;
	}
	
	public QuoteLineGroupBuilder groupType(String groupType)
	{
		this.quoteLineGroup_groupType = groupType;
		return this;
	}
	
	public QuoteLineGroupBuilder groupStatus(String status)
	{
		this.quoteLineGroup_Status = status;
		return this;
	}
	
	public QuoteLineGroupBuilder consignmentBusinessCase(Consignment_Business_Case__c cbc)
	{
		this.cbc = cbc;
		return this;
	}
	
	public QuoteLineGroupBuilder surgeonPreference(Surgeon_Preference__c surgeonPref)
	{
		this.surgeonPref = surgeonPref;
		return this;
	}
	
	public QuoteLineGroupBuilder actualBOM(ActualBillOfMaterial__c actualBOM)
	{
		this.actualBOM = actualBOM;
		return this;
	}
	
	public Quote_Line_Group__c build(Boolean createInDatabase)
	{
		if (quoteLineGroup_Status == null || quoteLineGroup_Status == '')
			quoteLineGroup_Status = 'New';
		
		Quote_Line_Group__c lineGroup = new Quote_Line_Group__c(
			Name = this.quoteLineGroup_groupName,
			QuoteId__c  = this.quoteLineGroup_Quote.Id,
			SetClassId__c = this.quoteLineGroup_SetClass.Id,
			GroupType__c = this.quoteLineGroup_groupType,
			LineGroup_Status__c = this.quoteLineGroup_Status,
			NumberOfBOMsRequired__c = 1
		);
		
		if (createInDatabase)
			insert lineGroup;
			
		return lineGroup;
	}
	
	public Quote_Line_Group__c buildCBCGroup(Boolean createInDatabase)
	{
		if (quoteLineGroup_Status == null || quoteLineGroup_Status == '')
			quoteLineGroup_Status = 'New';
		
		Quote_Line_Group__c lineGroup = new Quote_Line_Group__c(
			Name = this.quoteLineGroup_groupName,
			QuoteId__c  = this.quoteLineGroup_Quote.Id,
			GroupType__c = this.quoteLineGroup_groupType,
			LineGroup_Status__c = this.quoteLineGroup_Status,
			NumberOfBOMsRequired__c = 1,
			Consignment_Business_Case__c = this.cbc.Id
		);
		
		if (createInDatabase)
			insert lineGroup;
			
		return lineGroup;
	}
	
	public Quote_Line_Group__c buildSPGroup(Boolean createInDatabase)
	{
		if (quoteLineGroup_Status == null || quoteLineGroup_Status == '')
			quoteLineGroup_Status = 'New';
		
		Quote_Line_Group__c lineGroup = new Quote_Line_Group__c(
			Name = this.quoteLineGroup_groupName,
			QuoteId__c  = this.quoteLineGroup_Quote.Id,
			GroupType__c = this.quoteLineGroup_groupType,
			LineGroup_Status__c = this.quoteLineGroup_Status,
			NumberOfBOMsRequired__c = 1,
			Surgeon_Preference__c = this.surgeonPref.Id
		);
		
		if (createInDatabase)
			insert lineGroup;
			
		return lineGroup;
	}
	
	public Quote_Line_Group__c buildSetRequestGroup(Boolean createInDatabase)
	{
		if (quoteLineGroup_Status == null || quoteLineGroup_Status == '')
			quoteLineGroup_Status = 'New';
		
		Quote_Line_Group__c lineGroup = new Quote_Line_Group__c(
			Name = this.quoteLineGroup_groupName,
			QuoteId__c  = this.quoteLineGroup_Quote.Id,
			GroupType__c = this.quoteLineGroup_groupType,
			LineGroup_Status__c = this.quoteLineGroup_Status,
			NumberOfBOMsRequired__c = 1
		);
		
		if (createInDatabase)
			insert lineGroup;
			
		return lineGroup;
	}
	
	public Quote_Line_Group__c buildChangeSetGroup(Boolean createInDatabase)
	{
		if (quoteLineGroup_Status == null || quoteLineGroup_Status == '')
			quoteLineGroup_Status = 'New';
		
		Quote_Line_Group__c lineGroup = new Quote_Line_Group__c(
			Name = this.quoteLineGroup_groupName,
			QuoteId__c  = this.quoteLineGroup_Quote.Id,
			GroupType__c = this.quoteLineGroup_groupType,
			LineGroup_Status__c = this.quoteLineGroup_Status,
			NumberOfBOMsRequired__c = 1,
			ActualBillOfMaterial__c = this.actualBOM.Id
		);
		
		if (createInDatabase)
			insert lineGroup;
			
		return lineGroup;
	}
	
}