public with sharing class QuoteTransactionTypeBuilder {
	/*
	private String transactionTypeName;
	private String level2_Approver1_RoleName;
	private String level3_Approver1_RoleName;
	private String destinationTransactionDocumentTypeId;
	private String destinationTransactionDocumentLocation;
	
	
	public QuoteTransactionTypeBuilder name(String transactionTypeName)
	{
		this.transactionTypeName = transactionTypeName;
		return this;
	}
	
	public QuoteTransactionTypeBuilder setDestinationTransactionDocumentTypeId(String destinationTransactionDocumentTypeId){
		this.destinationTransactionDocumentTypeId = destinationTransactionDocumentTypeId;
		return this;
	}
	
	public QuoteTransactionTypeBuilder setDestinationTransactionDocumentLocation(String destinationTransactionDocumentLocation){
		this.destinationTransactionDocumentLocation = destinationTransactionDocumentLocation;
		return this;
	}
	
	public QuoteTransactionTypeBuilder setLevel2_Approver1_RoleName(String level2_Approver1_RoleName)
	{
		this.level2_Approver1_RoleName = level2_Approver1_RoleName;
		return this;
	}
	
	public QuoteTransactionTypeBuilder setLevel3_Approver1_RoleName(String level3_Approver1_RoleName)
	{
		this.level3_Approver1_RoleName = level3_Approver1_RoleName;
		return this;
	}
	
	public Quote_Transaction_Type__c build(Boolean createInDatabase)
	{
		Quote_Transaction_Type__c quoteTranType = new Quote_Transaction_Type__c(
			Name = this.transactionTypeName
		);
		
		if (level2_Approver1_RoleName != null && level2_Approver1_RoleName != '')
			quoteTranType.Level2_Approver1_Role__c = level2_Approver1_RoleName;
			
		if (level3_Approver1_RoleName != null && level3_Approver1_RoleName != '')
			quoteTranType.Level3_Approver1_Role__c = level3_Approver1_RoleName;
		
		if (destinationTransactionDocumentTypeId != null && destinationTransactionDocumentTypeId != '')
			quoteTranType.Destination_Transaction_DocumentType_Id__c = destinationTransactionDocumentTypeId;
		
		if (destinationTransactionDocumentLocation != null && destinationTransactionDocumentLocation != '')
			quoteTranType.Destination_Transaction_DocumentLocation__c = destinationTransactionDocumentLocation;
		
		
		if (createInDatabase)
			insert quoteTranType;
		
		return quoteTranType;
	}
	*/
	
}