@isTest
// For this to be tested properly we a quote already in the system thats more than 4 days old (createdDate < 4 days from today)
// You then need to check the logs to see that it gets deleted. A debug message should show you this
global class NightlyBatchJobsTest {

	@isTest
	// Test where quote does not fit deletion criteria and does not get deleted
	global static void testSchedule()
	    {
	        Test.startTest();
	        
	        String organizationCode = 'UT-AU10';
	        String distributionChannel = '01';
	        String division = '01'; 
	        
	        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
				.organizationCode(organizationCode)
				.distributionChannel(distributionChannel)
				.division(division)
		        .build(true);
	        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');	        
		        
	        Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
        	//String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        	Opportunity o = new OpportunityBuilder().account(vinnies).priceBookId(customPricebook.Id).build(true);
        	/*
         	Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        											
        	System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        	*/
        	//String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
        	Quote quote = new QuoteBuilder().opportunity(o).priceBookId(customPricebook.Id).quoteType(GlobalConstants.QuoteTypes.CustomerQuote).build(true);
       		System.assert(quote.Id <> null,'An error occured while creating quote.');
       		
	        // Update custom settings for Quote deletion criteria to make the number of days 4 so that this quote just created does NOT qualify for deletion
	        // This test wont run in salesforce without the following wrapper
	        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];  
			System.runAs ( thisUser ) {  
				
				// This record might exist because we have seeAllData=true. If it exists, just update it, otherwise create it for the test
				try {
					Delete_Quote_Criteria__c deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
					
					if (deleteQuoteCriteria == null) {
						system.debug ('>>>>>>>>>deletequotecriteria was null as expected');
						Delete_Quote_Criteria__c cs = new Delete_Quote_Criteria__c();
						cs.Name = 'Delete_Quote_Rules';
						cs.No_of_Days_Old__c = 4;
						cs.Quote_Status__c = 'Draft';
						Database.SaveResult DR_Dels = Database.insert(cs);
						System.assertEquals(true,DR_Dels.isSuccess(),'>>>>>Custom setting for Delete Quote Rules failed to insert');
				
					} else {
						deleteQuoteCriteria.No_of_Days_Old__c = 4;
						Database.SaveResult DR_Dels = Database.update(deleteQuoteCriteria);
						System.assertEquals(true,DR_Dels.isSuccess(),'>>>>>Custom setting for Delete Quote Rules failed to update');
					}
				} catch (DMLException e){
					system.debug('>>>>>>>Got an exception reading the custom setting for Delete_Quote_Criteria__c' + e);
				}
			
				
			}
			
	        NightlyBatchJobs sched = new NightlyBatchJobs();
	        Id job_id = System.schedule('test', '0 0 0 30 12 ? 2099', sched);
	        System.assertNotEquals(null, job_id);
	        
	     	
	        Test.stopTest();
	        
	        List<Quote> theQuote = [SELECT id, QuoteNumber from Quote WHERE Id=: quote.Id LIMIT 1];
	        System.assertEquals(1,theQuote.size(),'The quote should not have been deleted because it is not more than 4 days old');
	        
	    }
	    
	@isTest
	// Test where quote does fit deletion criteria and gets deleted
	global static void testScheduleDelete()
	    {
	        Test.startTest();
	        
	      	String organizationCode = 'UT-AU10';
	        String distributionChannel = '01';
	        String division = '01'; 
	        
	        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
				.organizationCode(organizationCode)
				.distributionChannel(distributionChannel)
				.division(division)
		        .build(true);
	        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
	        		        
	        Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
        	
        	Opportunity o = new OpportunityBuilder().account(vinnies).priceBookId(customPricebook.Id).build(true);
        	/*
         	Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        	System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        	*/
        	Quote quote = new QuoteBuilder().opportunity(o).priceBookId(customPricebook.Id).quoteType(GlobalConstants.QuoteTypes.CustomerQuote).build(true);
       		System.assert(quote.Id <> null,'An error occured while creating quote.');
       		
	        // Update custom settings for Quote deletion criteria to make the number of days 0 so that this quote just created qualifies for deletion
	        // This test wont run in salesforce without the following wrapper
	        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];  
			System.runAs ( thisUser ) {  
				try {
					Delete_Quote_Criteria__c deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
					
					if (deleteQuoteCriteria == null) {
						system.debug ('>>>>>>>>>deletequotecriteria was null as expected');
						Delete_Quote_Criteria__c cs = new Delete_Quote_Criteria__c();
						cs.Name = 'Delete_Quote_Rules';
						cs.No_of_Days_Old__c = 0;
						cs.Quote_Status__c = 'Draft';
						Database.SaveResult DR_Dels = Database.insert(cs);
						System.assertEquals(true,DR_Dels.isSuccess(),'>>>>>Custom setting for Delete Quote Rules failed to insert');
				
					} else {
						deleteQuoteCriteria.No_of_Days_Old__c = 0;
						Database.SaveResult DR_Dels = Database.update(deleteQuoteCriteria);
						System.assertEquals(true,DR_Dels.isSuccess(),'>>>>>Custom setting for Delete Quote Rules failed to update');
					}
				} catch (DMLException e){
					system.debug('>>>>>>>Got an exception reading the custom setting for Delete_Quote_Criteria__c' + e);
				}
			}
	        
	        NightlyBatchJobs sched = new NightlyBatchJobs();
	        Id job_id = System.schedule('test', '0 0 0 30 12 ? 2099', sched);
	        System.assertNotEquals(null, job_id);
	        
	     	
	        Test.stopTest();
	        
	        List<Quote> theQuote = [SELECT id, QuoteNumber from Quote WHERE Name='My Quote 102' LIMIT 1];
	        System.assertEquals(0,theQuote.size(),'The quote should have been deleted');
	        
	    }

}