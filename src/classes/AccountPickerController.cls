public class AccountPickerController {
	public String accountId {get;set;}
	
	public PageReference route() {
		PageReference pr = new PageReference('/apex/Quote_Account_Picker?accId='+accountId);
		pr.setRedirect(true);
		return pr;
	}
}