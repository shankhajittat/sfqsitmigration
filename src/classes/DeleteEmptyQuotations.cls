public class DeleteEmptyQuotations {
	
	public DeleteEmptyQuotations(){
		
		datetime dt = System.now();
		
		//Get custom settings for delete criteria for quotes
		Delete_Quote_Criteria__c deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
		String quoteStatus = deleteQuoteCriteria.Quote_Status__c;
		Integer noOfDaysOld = (Integer)deleteQuoteCriteria.No_of_Days_Old__c;
		
		//Calculate date from today using custom settings - a quote must be older than this calculated date in order to qualify for deletion
		datetime latestDeleteDate = dt.addDays(-noOfDaysOld);
		
		List<Quote> QuotesWithoutGroups = [SELECT Id, QuoteNumber, CreatedDate 
										   FROM Quote 
										   WHERE Id NOT IN (SELECT QuoteId__c FROM Quote_Line_Group__c)
													AND Id NOT IN(SELECT QuoteId FROM QuoteLineItem)
													AND Status = :quoteStatus
													AND CreatedDate <= :latestDeleteDate];
													
		system.debug('>>>>>>>>>>>>>>>>>> Quotation Records with no groups : ' + QuotesWithoutGroups);
		
		for (Quote QuoteToDelete : QuotesWithoutGroups){
			string quoteNumber = QuoteToDelete.QuoteNumber;
			
			try {
				Database.DeleteResult DR_Dels = Database.delete(QuoteToDelete);
				
				if (DR_Dels.isSuccess()){
					System.debug('>>>>>>>>>>>>>>>>>>> Quotation deleted, QuoteNumber : ' + quoteNumber);
				} else{
					System.debug('>>>>>>>>>>>>>>>>>>> Delete failed for Quote : ' + quoteNumber);
				}
			} catch (DMLException e){
				System.debug('>>>>>>>>>>>> Delete for quotation failed, QuoteNumber : ' + quoteNumber);
				//QuoteToDelete.addError('There was a problem deleting this quote in the nightly batch delete job - check the logs - QuoteNumber: ' + quoteNumber);
     		}
				
		}
		
	}

}