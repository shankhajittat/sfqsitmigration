public class MaintainConfigurationData {
	
	// This class will be used to seed data.
	// To run it, use the EXECUTE ANONYMOUS window and type the following:
	// MaintainConfigurationData.run();
	/*
	public static Map<String, String> defaultPushTopics = new Map<String, String>
		{'LineItemsWithValidPrice' => 'Select Id, UnitPrice , Price_Status__c, Group_Status__c, Total_Standard_Cost__c, Total_Price_IncTax__c, Total_Price_ExTax__c, Total_Landed_Cost__c, Unit_Discount_Amount__c, Unit_Net_Price__c, ListPrice, Subtotal, QuoteId, Discount, Discount_Amount__c, TotalPrice, Gross_Margin_Amount__c, Gross_Margin_Percent__c, Extended_Unit_Discount__c, Net_Margin_Percent__c,Net_Margin_Amount__c, Landed_Cost__c, Tax_Rate__c, Standard_Cost__c, Unit_Tax__c, Total_Tax__c, Contract_PriceCode__c, Unit_ContractPrice__c, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'Validated\',\'Revalidate\',\'Invalid\')',
		 'GetQuoteErpPricing' => 'Select Id from Quote where Status = \'Submitted for Pricing\'',
		 'RefreshQuoteWhenCompleted' => 'Select Id, Status from Quote where Status in (\'Completed - Won\',\'Incomplete\')',
		 'CreateSalesOrder' => 'Select Id from Quote where Status = \'Submitted to ERP\' '};
		
	public static void updatePushTopics(String pushTopicName, String queryStatement){
		List<PushTopic> thePushTopics = [Select  p.Query,  p.Name, p.Id 
										From PushTopic p
										Where p.Name = :pushTopicName LIMIT 1];
										
		if (thePushTopics.isEmpty()) {
			System.debug('>>>>> Push topic not found : ' + pushTopicName);
			return;
		}
		
		PushTopic thePushTopic = thePushTopics[0];
		
		thePushTopic.Query = queryStatement;
			
		update thePushTopic;
		
	}
	
	// If the push topic doesn't exist then create it
	public static void createPushTopic(String pushTopicName, String queryStatement){
		List<PushTopic> thePushTopics = [Select  p.Query,  p.Name, p.Id 
										From PushTopic p
										Where p.Name = :pushTopicName LIMIT 1];
										
		if (thePushTopics.isEmpty()) {
			System.debug('>>>>> Push topic not found....Creating push topic : ' + pushTopicName);
			PushTopic onePushTopic = new PushTopic();
			onePushTopic.Name = pushTopicName;
			onePushTopic.Query = queryStatement;
			onePushTopic.ApiVersion = 29.0;       // This needs to get the apiversion from custom settings
			
			insert onePushTopic;
			
		}
		
		return;
	}
	
	// The run method can be changed to run other seeding methods including this one. If you only want to seed the push topics 
	// then call this method directly. You can either set the push topics to the default values, or pass in the query to update the push topic.
	public static void seedPushTopics(Boolean useDefaults, String pushTopicName, String queryStatement) {
		if (useDefaults) {
			
			for (String topicName : defaultPushTopics.keySet()) {
			    String theQuery = defaultPushTopics.get(topicName);
			    createPushTopic(topicName,theQuery);
			    updatePushTopics(topicName,theQuery);
			}
			
		} else {
			
			// If you want to send in your own query 
		}
	}
	
	// If you want to create a transaction type only, you can run this method directly. Otherwise the run method seeds all necessary data
	public static String createQuoteTransactionTypes(String transactionTypeName) {
		
		String quoteTypeId;
		List<Quote_Transaction_Type__c> transactionTypes = [Select t.Id 
															From Quote_Transaction_Type__c t
															Where t.Name = :transactionTypeName LIMIT 1];
											
		if (transactionTypes.isEmpty()) {
			System.debug('>>>>> Transaction type ....Creating transaction type  : ' + transactionTypeName);
			Quote_Transaction_Type__c oneTransactionType = new Quote_Transaction_Type__c();
			oneTransactionType.Name = transactionTypeName;
			
			insert oneTransactionType;
			quoteTypeId = oneTransactionType.Id;
		} else{
			quoteTypeId = transactionTypes[0].Id;
		}
		return quoteTypeId;
		
		return null;
	}
	
	public static void seedQuoteTransactionTypes() {
		Id quoteTypeId;
		
		String defaultTransType = 'Set Buy';
		quoteTypeId = createQuoteTransactionTypes(defaultTransType);
		seedCustomSettingQuoteTypes(quoteTypeId, defaultTransType, 0);  // Set Buy must be index 0 because the global enum QuoteTypes has Set buy as index 0
		
		defaultTransType = 'CBC';
		quoteTypeId = createQuoteTransactionTypes(defaultTransType);
		seedCustomSettingQuoteTypes(quoteTypeId, defaultTransType, 1);  // CBC must be index 0 because the global enum QuoteTypes has CBC as index 1
		
		defaultTransType = 'Surgeon Preference';
		quoteTypeId = createQuoteTransactionTypes(defaultTransType);
		seedCustomSettingQuoteTypes(quoteTypeId, defaultTransType, 2);  // Surgeon Preference must be index 0 because the global enum QuoteTypes has Surgeon Pref as index 2
		
		return;
	}
	
	public static void seedCustomSettingQuoteTypes(Id pQuoteTypeId, String pQuoteTypeDescription, Integer pQuoteTypeEnumIndex) {
		try {
			Quote_Types__c quoteTypesCS = Quote_Types__c.getValues(pQuoteTypeDescription);
			
			if (quoteTypesCS == null) {
				system.debug ('>>>>>>>>>Quote_Types__c was null for name: ' + pQuoteTypeDescription);
				Quote_Types__c cs = new Quote_Types__c();
				cs.Name = pQuoteTypeDescription;
				cs.Quote_Transaction_Type_Id__c = pQuoteTypeId;
				cs.Quote_Type_Enum_Index__c = pQuoteTypeEnumIndex;
				Database.SaveResult DR_QuoteTypeCS = Database.insert(cs);
		
			} else {
				quoteTypesCS.Quote_Transaction_Type_Id__c = pQuoteTypeId;
				quoteTypesCS.Quote_Type_Enum_Index__c = pQuoteTypeEnumIndex;
				Database.SaveResult DR_QuoteTypeCS = Database.update(quoteTypesCS);
			}
		} catch (DMLException e){
			system.debug('>>>>>>>Got an exception upserting the custom setting for QuoteTypes__c : ' + e);
			system.debug('>>>>>>>Quote Type : ' + pQuoteTypeId + ',Name : ' + pQuoteTypeDescription + ', Index : ' + pQuoteTypeEnumIndex);
		}
	}
	
	public static void seedDelete_Quote_Criteria_CustomSetting() {
		try {
			Delete_Quote_Criteria__c deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
			
			if (deleteQuoteCriteria == null) {
				system.debug ('>>>>>>>>>deletequotecriteria was null as expected');
				Delete_Quote_Criteria__c cs = new Delete_Quote_Criteria__c();
				cs.Name = 'Delete_Quote_Rules';
				cs.No_of_Days_Old__c = 30;
				cs.Quote_Status__c = 'Draft';
				Database.SaveResult DR_Dels = Database.insert(cs);
				System.assertEquals(true,DR_Dels.isSuccess(),'>>>>>Custom setting for Delete Quote Rules failed to insert');
		
			} else {
				deleteQuoteCriteria.No_of_Days_Old__c = 30;
				Database.SaveResult DR_Dels = Database.update(deleteQuoteCriteria);
				System.assertEquals(true,DR_Dels.isSuccess(),'>>>>>Custom setting for Delete Quote Rules failed to update');
			}
		} catch (DMLException e){
			system.debug('>>>>>>>Got an exception reading the custom setting for Delete_Quote_Criteria__c' + e);
		}
	}
	
	public static void run() {
		seedPushTopics(true, null, null);  // See push topics with the default queries
		seedDelete_Quote_Criteria_CustomSetting();
	}
	*/
}