public class AutocompleteItemPlugin implements AutocompleteController.AutoCompletePlugin {
	
	public Set<String> itemsAlreadyOnQuote = new Set<String>();
	public String setClassId;
	public String quoteId;
	
	public void init(Map<String,String> context){
		setClassId=context.get('id1');
		quoteId=context.get('id2');
	}
	
	
	public List<SObject> find(String searchTerm){
		String searchTermWild = String.escapeSingleQuotes(searchTerm) + '*';
		
		List<List<SObject>> searchList = [FIND :searchTermWild IN ALL FIELDS
												RETURNING Product2 (Id, ProductCode, Name WHERE IsActive = true)];
												
		Set<Id> productIds = new Set<Id>();
		for (SObject p : searchList.get(0)) {
			productIds.add(p.Id);
		}  												
									
		List<Product2> matchesWithLevels = new List<Product2>();
		// batch for loop used for max performance 										
		for (List<Product2> products : 
				[Select p.ProductCode,p.Name,
	  			 p.Product_Level__r.Name, 
	  			 p.Product_Level__r.Parent_Level__r.Name, 
	  			 p.Product_Level__r.Parent_Level__r.Parent_Level__r.Name, 
	  			 p.Product_Level__r.Parent_Level__r.Parent_Level__r.Parent_Level__r.Name, 
	  			 p.Product_Level__r.Parent_Level__r.Parent_Level__r.Parent_Level__r.Parent_Level__r.Name
	  			 From Product2 p
	  			 Where p.Id in :productIds]) {
	  		matchesWithLevels.addAll(products);
		}												
												
		// Build a list of items already on this quote and set class, to be used for filtering
		for(List<QuoteLineItem> productCodes : [SELECT q.PriceBookEntry.ProductCode
												FROM QuoteLineItem q
												WHERE q.QuoteId = :quoteId 
												AND q.Set_Class__c = :setClassId]) {
										
			for (QuoteLineItem productCode : productCodes) {
	    		itemsAlreadyOnQuote.add(productCode.PriceBookEntry.ProductCode);
	    	}	
		}
    							  
		return matchesWithLevels;
	}
	
	public boolean filter(SObject oneSObject) {
		Product2 item = (Product2)oneSObject;
		boolean isAlreadyOnQuote = itemsAlreadyOnQuote.contains(item.ProductCode);
		return !isAlreadyOnQuote;
	}
	
	public String css(){
		return 	'.ui-menu-item .description { font-size: 0.8em; font-style: italic; }' +
				'.ui-menu-item .label { font-weight: bold; }' +
				'.ui-menu-item .level { font-size: 0.8em; }';
	}
	
	public String jsRender(){
		return
			'return $172( "<li></li>" )'+
			'.data( "item.autocomplete", item )' +
			'.append( "<a><span class=\'label\'>" + item.ProductCode + "</span>'+
			'<br><span class=\'description\'>" + ((item.Name==item.ProductCode)?"Description unavailable":item.Name) + "</span></a>" )' +
			'.appendTo( ul )';
	}
	
	public String jsHandleSelect(List<String> domIds, List<String> values) {
		return 
		'document.getElementById("'+domIds.get(0)+'").value=id;'+ // put the selected item id in the hidden selectedProductId field
		'document.getElementById("'+domIds.get(1)+'").value="'+values.get(0)+'";'+ // put the setClassId in the hidden groupClassId field
		'processAddItem();';
	}

}