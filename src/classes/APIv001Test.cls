@isTest(SeeAllData=true)
private class APIv001Test {

    static testMethod void testGetAPIQuote()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			APIv001.Quote apiQuote = APIv001.getQuote(sourceQuoteId);
			System.debug('apiQuote>>>>>:' + apiQuote);
			System.assert(apiQuote != null, 'Was expecting a valid API Quote');
		}
	}
	
	
	static testMethod void testGetQuotePriceRequest() {
    
    	User runAsUser = null;
		Id sourceQuoteId = null;
		
    	Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
    	
    	System.runAs(runAsUser){ 
    		
    		Quote updateQuoteStatus = new Quote();
			updateQuoteStatus.id = sourceQuoteId;
			updateQuoteStatus.Status = 'Submitted for Pricing';
			update updateQuoteStatus;
			 	
	    	//Start Test
	        Test.startTest();
	          		       
	        APIv001.QuotePriceRequest quotePriceRequest = APIv001.getQuotePriceRequest(sourceQuoteId);
	        System.assert(quotePriceRequest!= null,
				'An error occured while execuing getQuoteLineItemsDetail method');
	        
	        Test.stopTest();
	                
	        System.assert(quotePriceRequest.quoteNumber != null && quotePriceRequest.quoteNumber != ''
				,'Expected a value for QuoteNumber');
				
	        System.debug('APIv001.quotePriceRequest>>>>: ' + quotePriceRequest); 
	    }      
    }
    
    
    static testMethod void testUpdateQuotePriceDetail_Validated(){
    	
    	User runAsUser = null;
		Id sourceQuoteId = null;
		
    	Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
    	
    	System.runAs(runAsUser){ 
    		
    		//Start Test
	        Test.startTest();
	        
    		Quote existingQuote = [Select q.Id,q.Name,q.QuoteNumber,q.Opportunity.AccountId From Quote q Where Id =: sourceQuoteId];
    		
    		APIv001.QuotePriceRequest updateQuotePriceRequest = new APIv001.QuotePriceRequest();
    		updateQuotePriceRequest.validationResult = new APIv001.ValidationResult();
	    	updateQuotePriceRequest.validationResult.isValid = true;
	    	updateQuotePriceRequest.id = sourceQuoteId;
	    	updateQuotePriceRequest.quoteNumber = existingQuote.QuoteNumber;
	    	updateQuotePriceRequest.customerNumber = existingQuote.Opportunity.AccountId;
	    	updateQuotePriceRequest.Lines = new List<APIv001.QuoteLineItemPriceRequest>();
	    	
    		List<QuoteLineItem> quoteLines = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: sourceQuoteId];
			for(QuoteLineItem ql : quoteLines){
				APIv001.QuoteLineItemPriceRequest linePriceRequest = new APIv001.QuoteLineItemPriceRequest();
				linePriceRequest.validationResult = new APIv001.ValidationResult();
	    		linePriceRequest.validationResult.isValid = true;
				linePriceRequest.id = ql.id;
				linePriceRequest.unitPrice = 250.00;
				linePriceRequest.taxRate = 10.00;
				linePriceRequest.unitStandardCost = 60.00;
				linePriceRequest.unitLandedCost = 40.00;
				updateQuotePriceRequest.Lines.add(linePriceRequest);
			}
			
			APIv001.ReturnResult rrQuotePriceDetail = APIv001.updateQuotePriceDetail(updateQuotePriceRequest);
			System.debug('rrQuotePriceDetail>>>>>>>>>' + rrQuotePriceDetail);
	      
	        
	        Test.stopTest();
	        
	    }  
	    
    }
    
    
    static testMethod void testUpdateQuoteErpDetail(){
    	
    	User runAsUser = null;
		Id sourceQuoteId = null;
		
    	Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
    	
    	System.runAs(runAsUser){ 
    		
    		//Start Test
	        Test.startTest();
	        
    		Quote existingQuote = [Select q.Id,q.Name,q.QuoteNumber,q.Opportunity.AccountId From Quote q Where Id =: sourceQuoteId];
    		
    		APIv001.QuotePriceRequest updateQuotePriceRequest = new APIv001.QuotePriceRequest();
    		updateQuotePriceRequest.validationResult = new APIv001.ValidationResult();
	    	updateQuotePriceRequest.validationResult.isValid = true;
	    	updateQuotePriceRequest.id = sourceQuoteId;
	    	updateQuotePriceRequest.quoteNumber = existingQuote.QuoteNumber;
	    	updateQuotePriceRequest.customerNumber = existingQuote.Opportunity.AccountId;
	    	updateQuotePriceRequest.Lines = new List<APIv001.QuoteLineItemPriceRequest>();
	    	
    		List<QuoteLineItem> quoteLines = [SELECT Id FROM QuoteLineItem WHERE QuoteId =: sourceQuoteId];
			for(QuoteLineItem ql : quoteLines){
				APIv001.QuoteLineItemPriceRequest linePriceRequest = new APIv001.QuoteLineItemPriceRequest();
				linePriceRequest.id = ql.id;
				linePriceRequest.unitPrice = 250.00;
				linePriceRequest.taxRate = 10.00;
				//linePriceRequest.taxScheduleId = 'AUS+GST10';
				linePriceRequest.unitStandardCost = 60.00;
				linePriceRequest.unitLandedCost = 40.00;
				updateQuotePriceRequest.Lines.add(linePriceRequest);
			}
			
			APIv001.ReturnResult rrQuotePriceDetail = APIv001.updateQuotePriceDetail(updateQuotePriceRequest);
			System.debug('rrQuotePriceDetail>>>>>>>>>' + rrQuotePriceDetail);
	        	
	    	APIv001.Quote updateQuote = QuoteTools.getQuote(sourceQuoteId);
	    	updateQuote.validationResult = new APIv001.ValidationResult();
	    	updateQuote.validationResult.isValid = true;
			updateQuote.erpDocumentNumber = 'ORDSCON000000';
	    	APIv001.ReturnResult rr = APIv001.updateQuoteErpDetail(updateQuote);
	    	System.debug('ReturnResult>>>>>>>>>' + rr);
	        
	        Test.stopTest();
	        
	    }  
	    
    }
    
    static testMethod void transformSaveResultErrorsIntoListOfApiLogs_Test() {
		//trying to save pb without specifying required fields.
		Pricebook2 insertPB = new Pricebook2();
		insertPB.Name = 'pbtofail';
		Database.SaveResult pbSR = Database.insert(insertPB,false);
						
		List<APIv001.Log> logs = APIv001Tools.transformSaveResultErrorsIntoListOfApiLogs(pbSR.getErrors());
		
		system.debug('Logs: ' + logs);
		system.assert(logs != null && logs != null && logs.size() >= 1, 'Was expecting one log record');
	}
	
	
}