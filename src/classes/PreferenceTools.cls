public with sharing class PreferenceTools {

	public enum Groups {QUOTE_GRIDS, QUOTE_COLLAPSIBLE}

	public enum Keys {
		SETBUY_SORT_DIR_ASC,CONSETBUILD_SORT_DIR_ASC, CBC_SORT_DIR_ASC, SP_SORT_DIR_ASC,CHANGESET_SORT_DIR_ASC,LOANSETREQUEST_SORT_DIR_ASC,
		SETBUY_SORT_FIELD, CONSETBUILD_SORT_FIELD , CBC_SORT_FIELD, SP_SORT_FIELD,CHANGESET_SORT_FIELD,LOANSETREQUEST_SORT_FIELD,
		SETBUY_COLUMN_SIZES , CONSETBUILD_COLUMN_SIZES, CBC_COLUMN_SIZES, SP_COLUMN_SIZES,CHANGESET_COLUMN_SIZES,LOANSETREQUEST_COLUMN_SIZES,
		SETBUY_COLUMNS_DISPLAYED , CONSETBUILD_COLUMNS_DISPLAYED, CBC_COLUMNS_DISPLAYED, SP_COLUMNS_DISPLAYED,CHANGESET_COLUMNS_DISPLAYED,LOANSETREQUEST_COLUMNS_DISPLAYED, 
		FILTERS, // for QUOTE_GRIDS
		SUMMARY, ADDRESS, INSTRUCTION, PROCESSLOG, USER // for QUOTE_COLLAPSIBLE
	}
	
	private static Map<Groups, Set<Keys>> GROUP_KEYS = new Map<Groups, Set<Keys>>(); 
	private static final Map<Groups,Map<Keys, String>> DEFAULTS = new Map<Groups,Map<Keys, String>>();
	static {
		
		GROUP_KEYS.put(Groups.QUOTE_GRIDS, new Set<Keys>(
			new Keys[] {Keys.SETBUY_SORT_DIR_ASC, Keys.CONSETBUILD_SORT_DIR_ASC, Keys.CBC_SORT_DIR_ASC, Keys.SP_SORT_DIR_ASC, Keys.CHANGESET_SORT_DIR_ASC, Keys.LOANSETREQUEST_SORT_DIR_ASC,
						Keys.SETBUY_SORT_FIELD, Keys.CONSETBUILD_SORT_FIELD, Keys.CBC_SORT_FIELD, Keys.SP_SORT_FIELD, Keys.CHANGESET_SORT_FIELD, Keys.LOANSETREQUEST_SORT_FIELD,
						Keys.SETBUY_COLUMN_SIZES,Keys.CONSETBUILD_COLUMN_SIZES, Keys.CBC_COLUMN_SIZES, Keys.SP_COLUMN_SIZES, Keys.CHANGESET_COLUMN_SIZES, Keys.LOANSETREQUEST_COLUMN_SIZES,
						Keys.SETBUY_COLUMNS_DISPLAYED,Keys.CONSETBUILD_COLUMNS_DISPLAYED, Keys.CBC_COLUMNS_DISPLAYED, Keys.SP_COLUMNS_DISPLAYED, Keys.CHANGESET_COLUMNS_DISPLAYED, Keys.LOANSETREQUEST_COLUMNS_DISPLAYED}));
		
		GROUP_KEYS.put(Groups.QUOTE_COLLAPSIBLE, new Set<Keys>(
			new Keys[] {Keys.SUMMARY, Keys.ADDRESS, Keys.INSTRUCTION, Keys.PROCESSLOG, Keys.USER}));
		
		DEFAULTS.put(Groups.QUOTE_GRIDS, new Map<Keys, String>());
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SETBUY_SORT_DIR_ASC,'false');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CONSETBUILD_SORT_DIR_ASC,'false');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CBC_SORT_DIR_ASC,'true');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SP_SORT_DIR_ASC,'false');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CHANGESET_SORT_DIR_ASC,'false');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.LOANSETREQUEST_SORT_DIR_ASC,'false');
		
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SETBUY_SORT_FIELD,'ProductCode');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CONSETBUILD_SORT_FIELD,'ProductCode');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CBC_SORT_FIELD,'ProductCode');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SP_SORT_FIELD,'ProductCode');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CHANGESET_SORT_FIELD,'ProductCode');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.LOANSETREQUEST_SORT_FIELD,'ProductCode');
		
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SETBUY_COLUMN_SIZES,'{"Actions":50, "ProductCode":350, "Quantity":100, "UnitPrice":100, "Discount_Amount__c":100, "Discount":75, "TotalPrice":125}');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CONSETBUILD_COLUMN_SIZES,'{"Actions":50, "ProductCode":350, "Quantity":100, "UnitPrice":100, "TotalPrice":125}');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CBC_COLUMN_SIZES,'{"Actions":50,  "ProductCode":350, "ERP_Record_Action_Mode__c":50, "Quantity":100}');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SP_COLUMN_SIZES,'{"Actions":50, "ProductCode":350, "ERP_Record_Action_Mode__c":50, "Quantity":100}');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CHANGESET_COLUMN_SIZES,'{"Actions":50, "ProductCode":350, "ERP_Record_Action_Mode__c":50, "Quantity":100}');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.LOANSETREQUEST_COLUMN_SIZES,'{"Actions":50, "ProductCode":350, "ERP_Record_Action_Mode__c":50, "Quantity":100}');
		
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SETBUY_COLUMNS_DISPLAYED,'["Actions", "ProductCode", "Quantity", "UnitPrice", "Discount_Amount__c", "Discount", "TotalPrice", "DelCol"]');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CONSETBUILD_COLUMNS_DISPLAYED,'["Actions", "ProductCode", "Quantity", "UnitPrice", "TotalPrice", "DelCol"]');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CBC_COLUMNS_DISPLAYED,'["Actions", "ProductCode", "ERP_Record_Action_Mode__c", "Quantity"]');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.SP_COLUMNS_DISPLAYED,'["Actions",  "ProductCode", "ERP_Record_Action_Mode__c", "Quantity"]');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.CHANGESET_COLUMNS_DISPLAYED,'["Actions",  "ProductCode", "ERP_Record_Action_Mode__c", "Quantity"]');
		DEFAULTS.get(Groups.QUOTE_GRIDS).put(Keys.LOANSETREQUEST_COLUMNS_DISPLAYED,'["Actions",  "ProductCode", "ERP_Record_Action_Mode__c", "Quantity"]');
		
		DEFAULTS.put(Groups.QUOTE_COLLAPSIBLE, new Map<Keys, String>());
		DEFAULTS.get(Groups.QUOTE_COLLAPSIBLE).put(Keys.SUMMARY,'open');
		DEFAULTS.get(Groups.QUOTE_COLLAPSIBLE).put(Keys.ADDRESS,'closed');
		DEFAULTS.get(Groups.QUOTE_COLLAPSIBLE).put(Keys.INSTRUCTION,'closed');
		DEFAULTS.get(Groups.QUOTE_COLLAPSIBLE).put(Keys.PROCESSLOG,'closed');
		DEFAULTS.get(Groups.QUOTE_COLLAPSIBLE).put(Keys.USER,'closed');
	} 
	
	public class PreferenceException extends Exception {}
	
	public static Map<String, String> getGroup(Groups grp) {
		Map<String, String> vals = getGroupDefaults(grp);
		//system.debug('getGroupDefaults1=>' + vals);
		for (Preference__c p : [SELECT Group__c, Name, Value__c FROM Preference__c
							    WHERE OwnerId = :UserInfo.getUserId()
							    AND Group__c = :String.valueOf(grp)]) {
			if (p.Value__c != null){
				vals.put(p.Name, p.Value__c);
			}
		}
		//system.debug('getGroupDefaults2=>' + vals);
		return vals;
	}

	public static String getPreference(Groups grp, Keys key) {
		Preference__c match = getPreferenceRecord(grp, key);
		if (match == null) {
			return getDefault(grp, key);
		} else {
			return match.Value__c;
		}
	}
	
	public static Preference__c getPreferenceRecord(Groups grp, Keys key) {
		List<Preference__c> matches = 
			[SELECT Value__c FROM Preference__c
			 WHERE OwnerId = :UserInfo.getUserId()
			 AND Group__c = :String.valueOf(grp)
			 AND Name = :String.valueOf(key)];
		if (matches.size() == 0) {
			return null;
		} else if (matches.size() == 1) {
			return matches.get(0);
		} else {
			throw new PreferenceException('Too many preferences found: '+grp+':'+key);
		}
	}
	
	public static Preference__c setPreference(Groups grp, Keys key, String value) {
		Preference__c match = getPreferenceRecord(grp, key);
		if (match == null) {
			match = new Preference__c(
				Group__c = String.valueOf(grp), 
				Name = String.valueOf(key), 
				Value__c = value);
			insert match;
		} else {
			match.Value__c = value;
			update match;
		}
		return match;
	}
	
	public static Map<String,String> getGroupDefaults(Groups grp) {
		Map<String,String> result = new Map<String,String>();
		for (Keys k : GROUP_KEYS.get(grp)) {
			result.put(
				String.valueOf(k),
				String.valueOf(getDefault(grp, k)));
		}
		return result;
	}
		
	public static String getDefault(Groups grp, Keys key) {
		if (DEFAULTS.containsKey(grp)) {
			return DEFAULTS.get(grp).get(key);
		} else {
			return null;
		}
	}

}