public class SetClassImportControllerExtension {

    public Blob csvFile{get;set;}
    public String csv;
    
	public String standardPricebookId{
		get {
			if (standardPricebookId == null) {
				List<Pricebook2> matches = [select Id from Pricebook2 where IsStandard = true];
				if (matches.size() == 1) {
					standardPricebookId = matches.get(0).Id;
				}
			}
			return standardPricebookId;
		}
		set;
	}
    
    public SetClassImportControllerExtension(ApexPages.StandardController stdController) {}
    
    public PageReference doImport() {
    	if (csvFile != null) {
    		csv = csvFile.toString();
    	}
    	if (csv == null || csv.length() == 0) {
    		throw new ImportException('csv file must be chosen before import');
    	}
        SetClassParser parser = new SetClassParser(standardPricebookId, csv);
        parser.run();
        
    	ApexPages.StandardController stdController = new ApexPages.StandardController(parser.newSetClass);        
        PageReference pr = stdController.view();
        pr.setRedirect(true);
        return pr;
    }

	public class ImportException extends Exception{}

}