@IsTest
public with sharing class QuoteRouterControllerExtensionTest {

	private static QuoteRouterControllerExtension initQuoteAndExtension() {
		QuoteLineItem qli = TestingTools.getTestQuoteLineItem();
		Quote__c shadow = [select Id from Quote__c where Quote__c = :qli.QuoteId];
    	ApexPages.StandardController stdController = new ApexPages.StandardController(shadow);
    	return new QuoteRouterControllerExtension(stdController);
	}
	
	@isTest
    static void testRoutingForExistingQuote() {
    	QuoteRouterControllerExtension ext = initQuoteAndExtension();
    	PageReference pr = ext.route();
    	System.assert(pr.getUrl().startsWithIgnoreCase('/apex/quoteeditor'));
    }

	@isTest
    static void testRoutingForNewQuoteFromAccount() {
		Account vinnies = new AccountBuilder().name('St Vincents').build(true);
    	ApexPages.currentPage().getParameters().put('retURL', '/'+vinnies.Id);    	
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote__c());
    	QuoteRouterControllerExtension ext = new QuoteRouterControllerExtension(stdController);
    	PageReference pr = ext.route();
    	System.assertEquals('/apex/QuoteCreate?accId=', pr.getUrl().substring(0,24),
    		'routing for new quotes from the account page should redirect to Quote Create selection page');
    }

	@isTest
    static void testRoutingForNewQuoteFromOpp() {
			String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity opp = new OpportunityBuilder().account(vinnies).priceBookId(customPricebook.Id).build(true);
        
    	ApexPages.currentPage().getParameters().put('retURL', '/'+opp.Id);    	
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote__c());
    	QuoteRouterControllerExtension ext = new QuoteRouterControllerExtension(stdController);
    	PageReference pr = ext.route();
    	System.assertEquals('/apex/QuoteType?accId=', pr.getUrl().substring(0,22),
    		'routing for new quotes from the opportunity page shoud redirect to Quote Type selection page');
    }
	
	@isTest
    static void testRoutingForNewQuoteFromQuoteTab() {
           	
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote__c());
    	QuoteRouterControllerExtension ext = new QuoteRouterControllerExtension(stdController);
    	PageReference pr = ext.route();
    	System.assertEquals('/apex/Quote_Account_Picker', pr.getUrl().substring(0,26),
    		'routing for new quotes from the custom Quote tab page shoud redirect to account picker where you can choose an account');
    }

}