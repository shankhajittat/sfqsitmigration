/*

Custom Settings - Text field store only upto 255 characters. When the value gets encrypted it can go beyond the 255 charactor limit.
Implimented QuoteSettings custom SObject to overcome this limitation.

CustomSettingsTools only can can be used if the the value doesn't go more than 255 charactors.
To store more than 255 charactors, please use QuoteSettingTools.
*/

public with sharing class CustomSettingsTools {
	
	public static String upsertSetting(String key, String value, boolean encrypt){
		String valueToUpsert = null;
		SecuredSettings__c qs = null;
		boolean quoteSettingsNotFound = false;
		
		try{

			qs = SecuredSettings__c.getValues(key);
			
		}
		catch(Exception e){
			quoteSettingsNotFound = true;
		}
		
		if (encrypt){
			Blob crypToKey = null;
			try{
				crypToKey = getCryptToKey();
				if (crypToKey == null){
					throw new ApplicationException('Could not find CrypToKey to encrypt the data.');
				}
			}
			catch(Exception e){
				throw e;
			}
			
			Blob valueToEncrypt = Blob.valueOf(value);
			Blob valueEncryptedInBlob = Crypto.encrypt('AES128',crypToKey,crypToKey,valueToEncrypt);
			String valueEncrypted = EncodingUtil.base64Encode(valueEncryptedInBlob);
			
			valueToUpsert = valueEncrypted;
		}
		else{
			valueToUpsert = value;
		}
		
		
		if (quoteSettingsNotFound || qs == null){
			SecuredSettings__c qsInsert = new SecuredSettings__c();
			qsInsert.Name = key;
			qsInsert.SecuredValue__c = valueToUpsert;
			qsInsert.IsEncrypted__c = encrypt;
			insert qsInsert;
		}
		else{
			qs.SecuredValue__c = valueToUpsert;
			qs.IsEncrypted__c = encrypt;
			update qs;
		}
		
		return valueToUpsert;
	}
	
	public static void createTestCryptToKey(){
		Blob crypToKey = null;
		try{
			crypToKey = getCryptToKey();
		}
		catch(Exception e){
			//insert crypToKey
			SecuredSettings__c qsInsert = new SecuredSettings__c();
			qsInsert.Name = GlobalConstants.SFDC_CRYPTOKEY_NAME;
			qsInsert.SecuredValue__c = '1234567890abcdef';
			qsInsert.IsEncrypted__c = false;
			insert qsInsert;
		}
	}
	
	public static String getSecuredKeyValue(String key){
		String result = null;
		SecuredSettings__c qs = null;
		
		try{
			qs = getValue(key);
			if (qs == null){
				throw new ApplicationException('Could not find any record for the specified key : ' + key);
			}
			
			if (qs.IsEncrypted__c == true){
				Blob crypToKey = null;
				try{
					crypToKey = getCryptToKey();
					if (crypToKey == null){
						throw new ApplicationException('Could not find CrypToKey to encrypt the data.');
					}
				}
				catch(Exception e){
					throw e;
				}
				
				String value = qs.SecuredValue__c;
				Blob encryptedValueInBlob = EncodingUtil.base64Decode(value);
				Blob decryptedValue = Crypto.decrypt('AES128',crypToKey,crypToKey,encryptedValueInBlob);
				result = decryptedValue.toString();
			}
			else{
				result = qs.SecuredValue__c;
			}
		}
		catch(Exception e){
			throw e;
		}
		return result;
	}

	
	private static SecuredSettings__c getValue(String key){
		SecuredSettings__c qs = null;
		boolean quoteSettingsNotFound = false;
		
		try{
			qs = SecuredSettings__c.getValues(key);
		}
		catch(Exception e){
			throw new ApplicationException('Could not find any record for the specified key : ' + key);
		}
		
		return qs;
	}
	
	public static Blob getCryptToKey(){
		SecuredSettings__c ss = null;
		String crypToKey = null;
		try{
			ss = SecuredSettings__c.getValues(GlobalConstants.SFDC_CRYPTOKEY_NAME);	
			crypToKey = ss.SecuredValue__c;
		}
		catch(Exception e){
			throw e;
		}
		system.debug('Blob Key: ' + Blob.valueOf(crypToKey));
		return Blob.valueOf(crypToKey);
	}
	
	
	/*
		Generate a unique Quotation Number for Quote from the Custom Settings.
	
	public static String getNextQuotationNumber(){
		return determineQuotationNumber('StandardQuoteNextDocumentNumberKey', 'QU-{0}');
	}
	*/
	
	/*
		Generate a unique Quotation Number for CBC Quote from the Custom Settings.

	public static String getNextCBCQuotationNumber(){
		return determineQuotationNumber('CBCQuoteNextDocumentNumberKey', 'CBC-{0}');	
	}
	*/
	
	
	/*
		Generate a unique Quotation Number for Surgeon Preference Quote from the Custom Settings.

	public static String getNextSPQuotationNumber(){
		return determineQuotationNumber('SurgeonPrefQuoteNextDocumentNumberKey', 'SP-{0}');
	}
	*/
		
	public static String getQuotePrefix(String quoteType){
		String quotePrefix = '';
		String defaultPrefix ='QU';
		
		if (quoteType == GlobalConstants.QuoteTypes.CustomerQuote.name()){
			defaultPrefix = 'QU';
		}
		else if (quoteType == GlobalConstants.QuoteTypes.CBC.name()){
			defaultPrefix = 'CBC';
		}
		else if (quoteType == GlobalConstants.QuoteTypes.SurgeonPreference.name()){
			defaultPrefix = 'SP';
		}
		else if (quoteType == GlobalConstants.QuoteTypes.SetRequest.name()){
			defaultPrefix = 'SR';
		}
		else if (quoteType == GlobalConstants.QuoteTypes.ChangeSet.name()){
			defaultPrefix = 'CS';
		}
		else if (quoteType == GlobalConstants.QuoteTypes.LoanSetRequest.name()){
			defaultPrefix = 'LSR';
		}

		
		QuotePrefixSetting__c quotePrefixSetting = null;
		boolean errorWhileGettingCustomSettings = false;
		try{
			quotePrefixSetting = [Select QuotePrefix__c From QuotePrefixSetting__c Where Name = :quoteType];
		}
		catch(Exception e) {
			errorWhileGettingCustomSettings = true;
		}
		if (errorWhileGettingCustomSettings == true || quotePrefixSetting == null){
			QuotePrefixSetting__c createQuotePrefixSetting = new QuotePrefixSetting__c();
			createQuotePrefixSetting.Name = quoteType;
			createQuotePrefixSetting.QuotePrefix__c = defaultPrefix;
			insert createQuotePrefixSetting;
			
			quotePrefixSetting = [Select Id, QuotePrefix__c From QuotePrefixSetting__c Where Name = :quoteType];
		}
		
		quotePrefix = quotePrefixSetting != null && quotePrefixSetting.QuotePrefix__c != null ? quotePrefixSetting.QuotePrefix__c : '';
		return quotePrefix;
	}
	
	private static List<String> getAllAccountAutoCompleteAccountGroups(){
		List<String> result = new List<String>();
		for(List<AccountAutoCompleteAccountGroup__c> accountAutoCompleteAccountGroupSettigns : [Select Id, AccountGroupCode__c From AccountAutoCompleteAccountGroup__c 
																								Where AccountGroupCode__c != null And AccountGroupCode__c != ''
																								LIMIT 25]){
																									
			for(AccountAutoCompleteAccountGroup__c ag : accountAutoCompleteAccountGroupSettigns){
				result.add(ag.AccountGroupCode__c);
			}																						
		}
		
		return result;
	}
	
	public static List<String> getAllAccountAutoCompleteAccountGroupCodes(){
		
		List<String> result = getAllAccountAutoCompleteAccountGroups();

		if (result == null || result.size() == 0){
			List<AccountAutoCompleteAccountGroup__c> insertAccountAutoCompleteAccountGroup = new List<AccountAutoCompleteAccountGroup__c>();
			//Sold To Customer
			AccountAutoCompleteAccountGroup__c newAG1 = new AccountAutoCompleteAccountGroup__c();
			newAG1.Name = '0001';
			newAG1.AccountGroupCode__c = '0001';
			insertAccountAutoCompleteAccountGroup.add(newAG1);
			//Surgeon
			AccountAutoCompleteAccountGroup__c newAG2 = new AccountAutoCompleteAccountGroup__c();
			newAG2.Name = 'ZSUR';
			newAG2.AccountGroupCode__c = 'ZSUR';
			insertAccountAutoCompleteAccountGroup.add(newAG2);
			
			insert insertAccountAutoCompleteAccountGroup;
			
			result = getAllAccountAutoCompleteAccountGroups();
		}
		return result;
	}
	
	//If you call this method from Controller ini() method you get the following error.
	//DML currently not allowed 
	//An unexpected error has occurred. Your development organization has been notified.
	public static String getKeyValue(String key, String defaultValue){
		QuoteSettingKeyValue__c keyValue = null;
		boolean notFound = false;
		
		try{

			keyValue = [Select Id, Value__c From QuoteSettingKeyValue__c Where (Name = :key) LIMIT 1];
		}
		catch(Exception e){
			notFound = true;
		}
		
		if ((notFound || keyValue == null) && (!StringUtils.isNullOrWhiteSpace(defaultValue))){
			try{
				QuoteSettingKeyValue__c keyValueInsert = new QuoteSettingKeyValue__c();
				keyValueInsert.Name = key;
				keyValueInsert.Value__c = defaultValue;
				insert keyValueInsert;
				
				keyValue = [Select Id, Value__c From QuoteSettingKeyValue__c Where (Name = :key) LIMIT 1];
			}
			catch(Exception e){
				system.debug('getKeyValue Exception' + e.getMessage());
			}
		}
		
		return (keyValue != null && keyValue.Value__c != null) ? keyValue.Value__c : '';
	}
	
	public static String getKeyValue(String key){
		QuoteSettingKeyValue__c keyValue = null;
		boolean notFound = false;
		
		try{

			keyValue = [Select Id, Value__c From QuoteSettingKeyValue__c Where (Name = :key) LIMIT 1];
		}
		catch(Exception e){
			notFound = true;
		}
		
		return (keyValue != null && keyValue.Value__c != null) ? keyValue.Value__c : '';
	}
	
	
	/*
		Determine Quotation Number for a Quote.
	
	public static String determineQuotationNumber(String customSettingName, String CustomSettingFormat){
		String nextDocumentNumber = '';
		
		Quotation_Document_Number_Setting__c quoteNextDocumentNumberSettings = null;
		boolean errorWhileGettingCustomSettings = false;
		
		try{
			quoteNextDocumentNumberSettings = [Select Id, Document_Number_Format__c,Next_Document_Number__c 
												From Quotation_Document_Number_Setting__c 
												Where Name = :customSettingName FOR UPDATE];
		}
		catch(Exception e) {
			errorWhileGettingCustomSettings = true;
		}
		
		System.debug('quoteNextDocumentNumberSettings - 1>>>>:' + quoteNextDocumentNumberSettings);
		
		if (errorWhileGettingCustomSettings == true || quoteNextDocumentNumberSettings == null)
		{
			Quotation_Document_Number_Setting__c createSetting = new Quotation_Document_Number_Setting__c();
			createSetting.Name = customSettingName;
			createSetting.Document_Number_Format__c = customSettingFormat;
			createSetting.Next_Document_Number__c = 1;
			insert createSetting;
			
			quoteNextDocumentNumberSettings = [Select Id, Document_Number_Format__c,Next_Document_Number__c 
												From Quotation_Document_Number_Setting__c 
												Where Name = :customSettingName  FOR UPDATE];
												
			System.debug('quoteNextDocumentNumberSettings - 2>>>>:' + quoteNextDocumentNumberSettings);
		}
		
		Integer nextNumber = 1;
		String numberFormat = quoteNextDocumentNumberSettings.Document_Number_Format__c;
		double tryCount = 0;
		
		do{
			
			nextNumber = (Integer)quoteNextDocumentNumberSettings.Next_Document_Number__c;
			String nextNumberSufixWithZeros = StringUtils.leftPad(String.valueOf(nextNumber),4,'0');
			
			String tempNextDocumentNumber = String.format(numberFormat, new String[]{ nextNumberSufixWithZeros });
			System.debug('Next Document Number - 1>>>>:' + tempNextDocumentNumber);
			boolean foundNextDocumentNumber = false;
				
			if (tryCount > 100)
			{
				throw new ApplicationException('Limit exceeded while generating next Quote Document Number');
				break;
			}
			
			try{
				
				Quote existingQuote = [Select Id, Name From Quote Where Name =: tempNextDocumentNumber LIMIT 1];
				if (existingQuote == null)
					foundNextDocumentNumber = true;
					
			}
			catch(Exception e) {
				foundNextDocumentNumber = true;
			}

			nextNumber++;
			quoteNextDocumentNumberSettings.Next_Document_Number__c = nextNumber;
			update quoteNextDocumentNumberSettings;
			
			System.debug('Update quoteNextDocumentNumberSettings >>>>:' + quoteNextDocumentNumberSettings);
			
			if (foundNextDocumentNumber)
			{
				nextDocumentNumber = tempNextDocumentNumber;
				break;
			}
			
			tryCount++;
		}
		while(nextDocumentNumber == '');
		//while(nextDocumentNumber != '');
		
		System.debug('Next Document Number - 2>>>>:' + nextDocumentNumber);
	
		return nextDocumentNumber;	
	}
	*/
	
	
	
	/*
		Generate a unique Opportunity Number for Quote from the Custom Settings.

	public static String GetNextOpportunityNumber(){
		String nextDocumentNumber = '';
		
		
		Opportunity_Document_Number_Setting__c nextDocumentNumberSettings = null;
		boolean errorWhileGettingCustomSettings = false;
		
		try{
			nextDocumentNumberSettings = [Select Id, Document_Number_Format__c,Next_Document_Number__c 
												From Opportunity_Document_Number_Setting__c 
												Where Name = 'OpportunityNextDocumentNumberKey' FOR UPDATE];
		}
		catch(Exception e) {
			errorWhileGettingCustomSettings = true;
		}
		
		System.debug('nextDocumentNumberSettings - 1>>>>:' + nextDocumentNumberSettings);
		
		if (errorWhileGettingCustomSettings == true || nextDocumentNumberSettings == null)
		{
			Opportunity_Document_Number_Setting__c createSetting = new Opportunity_Document_Number_Setting__c();
			createSetting.Name = 'OpportunityNextDocumentNumberKey';
			createSetting.Document_Number_Format__c = 'OPP-{0}';
			createSetting.Next_Document_Number__c = 1;
			insert createSetting;
			
			nextDocumentNumberSettings = [Select Id, Document_Number_Format__c,Next_Document_Number__c 
												From Opportunity_Document_Number_Setting__c 
												Where Name = 'OpportunityNextDocumentNumberKey'  FOR UPDATE];
												
			System.debug('nextDocumentNumberSettings - 2>>>>:' + nextDocumentNumberSettings);
		}
		
		Integer nextNumber = 1;
		String numberFormat = nextDocumentNumberSettings.Document_Number_Format__c;
		double tryCount = 0;
		
		do{
			
			nextNumber = (Integer)nextDocumentNumberSettings.Next_Document_Number__c;
			String nextNumberSufixWithZeros = StringUtils.leftPad(String.valueOf(nextNumber),4,'0');
			
			String tempNextDocumentNumber = String.format(numberFormat, new String[]{ nextNumberSufixWithZeros });
			System.debug('Next Document Number - 1>>>>:' + tempNextDocumentNumber);
			boolean foundNextDocumentNumber = false;
				
			if (tryCount > 100)
			{
				throw new ApplicationException('Limit exceeded while generating next Opportunity Document Number');
				break;
			}
			
			try{
				
				Opportunity existingOpportunity = [Select Id, Name From Opportunity Where Name =: tempNextDocumentNumber LIMIT 1];
				if (existingOpportunity == null)
					foundNextDocumentNumber = true;
					
			}
			catch(Exception e) {
				foundNextDocumentNumber = true;
			}

			nextNumber++;
			nextDocumentNumberSettings.Next_Document_Number__c = nextNumber;
			update nextDocumentNumberSettings;
			
			System.debug('Update nextDocumentNumberSettings >>>>:' + nextDocumentNumberSettings);
			
			if (foundNextDocumentNumber)
			{
				nextDocumentNumber = tempNextDocumentNumber;
				break;
			}
			
			tryCount++;
		}
		while(nextDocumentNumber == '');
		//while(nextDocumentNumber != '');
		
		System.debug('Next Document Number - 2>>>>:' + nextDocumentNumber);
		
	
		return nextDocumentNumber;	
	}
		*/
	
}