public class EditableTableQuotePlugin implements EditableTableController.EditableTablePlugin {

	public class ValueException extends Exception{}

	public Boolean getItemPricing(String quoteId, List<String> itemsForPricing) {
		List<Id> itemsIdsToPrice = new List<Id>();
		for(String oneItem : itemsForPricing) {
			itemsIdsToPrice.Add((Id)oneItem);
		}
		Boolean isSuccessFul = QuoteTools.submitQuoteWithSelectedItemsForPricing(quoteId, itemsIdsToPrice);
		return isSuccessful;
	} 
	
	public Boolean getGroupPricing(String quoteId, String groupForPricing) {
		Boolean isSuccessFul = QuoteTools.submitQuoteWithSelectedGroupForPricing(quoteId, (Id)groupForPricing);
		return isSuccessful;
	} 
	
	public Boolean distributeDiscountAmountToTheQuote(String quoteId, String discountAmount) {
		Boolean isSuccessFul = false;
		try{
			if (StringUtils.isNullOrWhiteSpace(discountAmount)) discountAmount = '0.00';
			Double discAmt = Double.valueOf(discountAmount);
			QuoteTools.distributeDiscountAmountToTheQuote((Id)quoteId, discAmt);
			isSuccessful = true;
		}
		catch(Exception e){
			isSuccessful = false;
			throw e;
		}
		return isSuccessful;
	} 
	
	public String deleteItems(List<String> itemsToDelete) {
		List<Id> uniqueItemsToBeDeleted = new List<Id>();
		for(String oneItem : itemsToDelete) {
			uniqueItemsToBeDeleted.Add((Id)oneItem);
		}
		
		Map<Id,  String> itemsFailedToDelete = new Map<Id,  String>();
		if (uniqueItemsToBeDeleted.size() > 0) {
	    	Database.DeleteResult[] dr = database.delete(uniqueItemsToBeDeleted, false);
	    	// Any deletes that failed must be returned
	    	for (Database.DeleteResult oneDR : dr) {
	    		if (!oneDR.isSuccess()) {
	    			String errorMessage;
	    			for(Database.Error err : oneDR.getErrors()) {
	    				errorMessage = err.getMessage();
			            System.debug('>>>>>>>> The following error has occurred.' + errorMessage);                    
			        }
	    			itemsFailedToDelete.put(oneDR.getId() ,errorMessage);
	    		}
	    	}
		}
		
    	String jsonString = JSON.serialize(itemsFailedToDelete);
    	system.debug('>>>>>>>>>>>>>>>>>About to show serialised version of error');
    	system.debug(jsonString);	
    	return jsonString;
	} 
	
	public String deleteGroups(List<String> groupsToDelete) {
		List<Id> uniqueGroupsToBeDeleted = new List<Id>();
		for(String oneGroup : groupsToDelete) {
			uniqueGroupsToBeDeleted.Add((Id)oneGroup);
		}
		
		Map<Id,  String> groupsFailedToDelete = new Map<Id,  String>();
		if (uniqueGroupsToBeDeleted.size() > 0) {
	    	Database.DeleteResult[] dr = database.delete(uniqueGroupsToBeDeleted, false);
	    	// Any deletes that failed must be returned
	    	for (Database.DeleteResult oneDR : dr) {
	    		if (!oneDR.isSuccess()) {
	    			String errorMessage;
	    			for(Database.Error err : oneDR.getErrors()) {
	    				errorMessage = err.getMessage() + ' ';
			        }
	    			groupsFailedToDelete.put(oneDR.getId() ,errorMessage);
	    		}
	    	}
		}
		
    	String jsonString = JSON.serialize(groupsFailedToDelete);
    	system.debug('>>>>>>>>>>>>>>>>>>>>About to show serialised version of group errors');
    	system.debug(jsonString);	
    	return jsonString;
	} 
	
	public SObject setValue(String rowId, String field, String value) {
		QuoteLineItem item = prepareItem(rowId, field, value);		
		update item;		
		return getLineItems(new Id[] {rowId})[0];
	}
	
	private QuoteLineItem prepareItem(String rowId, String field, String value) {
		System.debug('Preparing: '+rowId+' '+field+' -> '+value);
		QuoteLineItem item = new QuoteLineItem(Id=rowId);		
		if (field == 'Quantity') {		
			item.Quantity = Double.valueOf(value);
		} else if (field == 'Discount') {
			item.Discount = Double.valueOf(value);
		} else if (field == 'Proposed_Quantity__c') {
			item.Proposed_Quantity__c = Double.valueOf(value);
		} else if (field == 'ERP_Record_Action_Mode__c') {
			item.ERP_Record_Action_Mode__c = value;
		} else {
			throw new ValueException('Cannot update field: '+field);
		}
		return item;		
	}
	
	public List<SObject> setValues(Map<Id, String> rowToValue, String field) {	
		List<QuoteLineItem> items = new List<QuoteLineItem>();
		for (Id id: rowToValue.keyset()) {
			items.add(prepareItem(id, field, rowToValue.get(id)));
		}		
		update items;
		List<Id> ids = new List<Id>();
		ids.addAll(rowToValue.keyset());
		return getLineItems(ids);
	}
	
	// returns lineItems with a standard set of fields available for the UI
	public static List<QuoteLineItem> getLineItems(List<Id> itemIds) {
		List<QuoteLineItem> items = new List<QuoteLineItem>();
		for (List<QuoteLineItem> batch : [select Quantity, Discount, Discount_Amount__c, UnitPrice, TotalPrice, Gross_Margin_Amount__c,Gross_Margin_Percent__c, 
				Extended_Unit_Discount__c, Agreed_Quantity__c, Actual_Quantity__c,Proposed_Quantity__c, ERP_Record_Action_Mode__c,
				Net_Margin_Percent__c,Net_Margin_Amount__c,Landed_Cost__c,Tax_Rate__c,Standard_Cost__c,Material_StatusCode__c,Material_StatusCode_Description__c,Unit_Tax__c,Total_Tax__c,Extended_Unit_Price__c, 
				Total_Standard_Cost__c,Total_Price_IncTax__c,Total_Price_ExTax__c,Total_Landed_Cost__c,Unit_Discount_Amount__c, Unit_Net_Price__c, 
				SystemModstamp, ServiceDate, CreatedDate, SortOrder, Price_Status__c, LastModifiedDate, LineNumber,  Id, Description, ListPrice,UnitListPrice__c, 
				Sequence_Number__c, Subtotal,ValidationMessage__c,
				Set_Class__r.Name, Contract_PriceCode__c, Unit_ContractPrice__c, PricebookEntry.Product2.ProductCode, PricebookEntry.Product2.Id, 
				PricebookEntry.MaterialGroup1__c,
		  		PricebookEntry.MaterialGroup2__c,
		  		PricebookEntry.MaterialGroup3__c,
		  		PricebookEntry.MaterialGroup4__c,
		  		PricebookEntry.MaterialGroup5__c,
		  		PricebookEntry.MyMedisetItemCategory__c,
		  		PricebookEntry.GenItemCategoryGroup__c,
				PricebookEntry.LocalPH1__r.Name,
				PricebookEntry.LocalPH2__r.Name,
				PricebookEntry.LocalPH2__r.Description__c,
				Product2.ProductTypeCode__c, Set_Class__c,
				Quote_Line_Group__r.LineGroup_Status__c, Quote_Line_Group__r.LineGroup_Total_Tax__c, Quote_Line_Group__r.LineGroup_Total_Standard_Cost__c, 
        		Quote_Line_Group__r.LineGroup_Total_Quantity__c, Quote_Line_Group__r.NumberOfBOMsRequired__c,Quote_Line_Group__r.LineGroup_Total_ProposedQuantity__c,
        		Quote_Line_Group__r.LineGroup_Total_NetMargin_Amount__c,
        		Quote_Line_Group__r.LineGroup_NetMargin_Percent__c,Quote_Line_Group__r.LineGroup_Total_Landed_Cost__c, 
        		Quote_Line_Group__r.LineGroup_Total_GrossMargin_Amount__c, Quote_Line_Group__r.LineGroup_GrossMargin_Percent__c,
        		Quote_Line_Group__r.LineGroup_Total_Extended_UnitPrice__c, Quote_Line_Group__r.LineGroup_Total_Discount_Amount__c,  
        		Quote_Line_Group__r.LineGroup_Discount_Percent__c,Quote_Line_Group__r.LineGroup_TotalPrice_IncTax__c, Quote_Line_Group__r.LineGroup_TotalPrice_ExTax__c ,
        		Quote_Line_Group__r.SystemModstamp, Quote_Line_Group__r.Name, Quote_Line_Group__r.LastModifiedDate, Quote_Line_Group__r.LastModifiedById, 
        		Quote_Line_Group__r.GroupType__c, Quote_Line_Group__r.CreatedDate, Quote_Line_Group__r.CreatedById, Quote_Line_Group__r.Consignment_Business_Case__c
				from QuoteLineItem where (Id in :itemIds) order by PricebookEntry.Product2.ProductCode]) {
					items.addAll(batch);
				}
		return items;
	}

	public List<EditableTableGroup> getGroups(String parentId) {
		
		BuildGroupData buildGrpData = new BuildGroupData();
		List<EditableTableGroup> groups = buildGrpData.getAllGroupsAndItems(parentId);
		groups.sort();
		return groups;
		 
	}
	
	public Boolean updateSetGroupProperty(Id recordId, String fieldName , String fieldValue)
	{
		Quote_Line_Group__c qlg = new Quote_Line_Group__c(Id=recordId);
		if(Quote_Line_Group__c.sObjectType.getDescribe().isUpdateable())
		{
			Map<String,Schema.SObjectField> fieldsMap = Schema.sObjectType.Quote_Line_Group__c.fields.getMap();
			Schema.SObjectField field = fieldsMap.get(fieldName.toLowerCase());
			if(field == null)
				throw new ValueException(Label.CM_INSUFFICIENT_PRIVILEGES);
			Schema.DescribeFieldResult dfr = field.getDescribe();
			Schema.DisplayType fieldType = dfr.getType();
			Object fieldValueObj = fieldValue;
	        if(dfr.isUpdateable())
	        {
	        	if((fieldType == Schema.DisplayType.Double) || (fieldType == Schema.DisplayType.Currency) || (fieldType == Schema.DisplayType.Percent) )
	        	{
	        		fieldValueObj =  Decimal.valueOf(fieldValue);
	        	}
	        	else if(fieldType == Schema.DisplayType.Boolean)
	        	{
	        		fieldValueObj = Boolean.valueOf(fieldValue);
	        	}
	        	else if(fieldType == Schema.DisplayType.Date)
	        	{
	        		fieldValueObj =  Date.parse(fieldValue);
	        	}
	        	else if(fieldType == Schema.DisplayType.DateTime)
	        	{
	        		fieldValueObj = DateTime.parse(fieldValue);
	        	}
	        	
	        	qlg.put(fieldName,fieldValueObj);
	        }
	        else 
	        	throw new ValueException(Label.CM_INSUFFICIENT_PRIVILEGES);
	        upsert qlg;
	        return true;
		}
        else 
            throw new ValueException(Label.CM_INSUFFICIENT_PRIVILEGES);
	}
	
}