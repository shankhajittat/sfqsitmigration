@isTest
global class WebMethodsQuotationSoapAPIv1MockImpl implements WebServiceMock {

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           	
       	WebMethodsQuotationSoapAPIv1.receiveQuotationRequestResponse respElement = 
           new WebMethodsQuotationSoapAPIv1.receiveQuotationRequestResponse();
       
       	WebMethodsQuotationSoapAPIv1.quotationResponse resp = new WebMethodsQuotationSoapAPIv1.quotationResponse();
		resp.isSuccessful = 'true';
 
       
       respElement.quotationResponse = resp;
       response.put('response_x', respElement);
   }
	
}