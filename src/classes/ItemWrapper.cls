public class ItemWrapper {

	public QuoteLineItem item{get;set;}
	public Boolean selected{get;set;}
	
	public ItemWrapper(QuoteLineItem item, Boolean selected) {
		this.item = item;
		this.selected = selected;
	}
	
	// custom getters to display fields with UI friendly formatting
	
	// when this returns false, the UI will display a throbber
	public Boolean getIsValidated() {
		return item.Price_Status__c == 'Validated';
	}
	
	public String getQuantity() {
		return String.valueOf(item.Quantity.intValue());
	}

	public String getDiscountP() {
		if (item.Discount == null || item.Discount == 0) {
			return null;
		} else {
			return String.valueOf(item.Discount.intValue())+'%';
		}
	}

	public String getDiscountA() {
		if (item.Discount_Amount__c == null || item.Discount_Amount__c == 0) {
			return null;
		} else {
			return '$'+String.valueOf((item.Discount*item.UnitPrice/100)
				.setScale(2, RoundingMode.HALF_UP));
		}
	}
	
}