public with sharing class QuoteUserPreferenceController {

	public SelectOption[] selectedHeaderTooltipFields {get; set;}
	public SelectOption[] allHeaderTooltipFieldsExcludingSelectedFields {get; set;}
	
	public SelectOption[] selectedGroupTooltipFields {get; set;}
	public SelectOption[] allGroupTooltipFieldsExcludingSelectedFields {get; set;}
	
	public SelectOption[] selectedItemTooltipFields {get; set;}
	public SelectOption[] allItemTooltipFieldsExcludingSelectedFields {get; set;}
	
	public SelectOption[] selectedItemGridFields {get; set;}
	public SelectOption[] allItemGridFieldsExcludingSelectedFields {get; set;}
	
	
	/*
	public String selectedObjectSection {get; set;}
		
	public Boolean getExpandHeaderTooltipPanelItem()
	{
		if (selectedObjectSection == null) return false;
		
		return selectedObjectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip.name() ? true : false;
	}
	
	public Boolean getExpandGroupTooltipPanelItem()
	{
		if (selectedObjectSection == null) return false;
		return selectedObjectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip.name() ? true : false;
	}
	
	public Boolean getExpandItemTooltipPanelItem()
	{
		if (selectedObjectSection == null) return false;
		return selectedObjectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip.name() ? true : false;
	}
	
	public String getPanelBarValueToSelectPanelBarItem()
	{
		if (selectedObjectSection == null || selectedObjectSection == ''
			|| selectedObjectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip.name())
		{
			return 'headerTooltip';
		}
		else if (selectedObjectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip.name())
		{
			return 'groupTooltip';
		}
		else if (selectedObjectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip.name())
		{
			return 'itemTooltip';
		}
		
		return 'headerTooltip'; 
	}
	*/
	
	
	public QuoteUserPreferenceController(){
		
		/*
			Header Tooltip - begin
		*/
		selectedHeaderTooltipFields = new List<SelectOption>();
		allHeaderTooltipFieldsExcludingSelectedFields= new List<SelectOption>();
		
		//get existing (previously selected) user preference for Quote - Header1Tooltip
		ObjectFieldsUserPreference objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip);
    	
    	selectedHeaderTooltipFields = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
    	if (selectedHeaderTooltipFields == null)
    		selectedHeaderTooltipFields = new List<SelectOption>();
		
		//get all metadata fields for Quote.
		allHeaderTooltipFieldsExcludingSelectedFields = buildSObjectFieldListExcludingSelectedFields('Quote',
																	ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip, 
																	selectedHeaderTooltipFields);
		
		/*
			Group Tooltip
		*/
		selectedGroupTooltipFields = new List<SelectOption>();
		allGroupTooltipFieldsExcludingSelectedFields = new List<SelectOption>();
		//get existing (previously selected) user preference for Quote - Group1Tooltip
		objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip);
    	
    	selectedGroupTooltipFields = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
    	if (selectedGroupTooltipFields == null)
    		selectedGroupTooltipFields = new List<SelectOption>();
		
		//get all metadata fields for Quote.
		allGroupTooltipFieldsExcludingSelectedFields = buildSObjectFieldListExcludingSelectedFields('Quote_Line_Group__c',
																ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
																selectedGroupTooltipFields);
		
		
		/*
			Item Tooltip
		*/
		selectedItemTooltipFields = new List<SelectOption>();
		allItemTooltipFieldsExcludingSelectedFields = new List<SelectOption>();
		//get existing (previously selected) user preference for Quote - Item1Tooltip
		objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip);
    	
    	selectedItemTooltipFields = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
    	if (selectedItemTooltipFields == null)
    		selectedItemTooltipFields = new List<SelectOption>();
		
		//get all metadata fields for Quote.
		allItemTooltipFieldsExcludingSelectedFields = buildSObjectFieldListExcludingSelectedFields('QuoteLineItem',
															ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip,
															selectedItemTooltipFields);
		
		
		/*
			Item Grid
		*/
		selectedItemGridFields = new List<SelectOption>();
		allItemGridFieldsExcludingSelectedFields = new List<SelectOption>();
		//get existing (previously selected) user preference for Quote - Item1Header
		objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Header);
    	
    	selectedItemGridFields = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
    	if (selectedItemGridFields == null)
    		selectedItemGridFields = new List<SelectOption>();
		
		//get all metadata fields for Quote.
		allItemGridFieldsExcludingSelectedFields = buildSObjectFieldListExcludingSelectedFields('QuoteLineItem',
															ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Header,
															selectedItemGridFields);
		/*
		Section For My Default BU
		*/	
		objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.DefaultBUSelection);
		selectedDefaultBUAsSelectOptionsList = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
		if(!selectedDefaultBUAsSelectOptionsList.IsEmpty())	
		{
			selectedDefaultBUSelectOptions = selectedDefaultBUAsSelectOptionsList[0].getValue();
		}				
	}
	
	public PageReference saveAllAndClose(){
		
		saveSelectedHeaderTooltipFields();
		saveSelectedGroupTooltipFields();
		saveSelectedItemTooltipFields();
		saveSelectedItemGridFields();
		saveDefaultBUPreference();
		pagereference backPage= new PageReference('/apex/'+ApexPages.currentPage().getParameters().get('Name')); 
        backPage.setRedirect(true); 
        return backPage; 

	}
	
	public PageReference cancel(){
		
		pagereference backPage= new PageReference('/apex/'+ApexPages.currentPage().getParameters().get('Name')); 
        backPage.setRedirect(true); 
        return backPage; 

	}
	
	public boolean saveSelectedHeaderTooltipFields() {
		
		List<SelectOption> selectedOptions = new List<SelectOption>();
		
        for ( SelectOption so : selectedHeaderTooltipFields ) {
            selectedOptions.add(new SelectOption(so.getValue(), so.getLabel()));
        }
        
        //selectedObjectSection = ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip.name();
        
        ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip,
    					selectedOptions);
    	
    	return objectFieldsUP.upsertUserPreference();
    	
    }
    
    public boolean saveSelectedGroupTooltipFields() {
		
		List<SelectOption> selectedOptions = new List<SelectOption>();
		
        for ( SelectOption so : selectedGroupTooltipFields ) {
            selectedOptions.add(new SelectOption(so.getValue(), so.getLabel()));
        }
        
        //selectedObjectSection = ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip.name();
        
        ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					selectedOptions);
    	return objectFieldsUP.upsertUserPreference();
    	
    }
    
    public boolean saveSelectedItemTooltipFields() {
		
		List<SelectOption> selectedOptions = new List<SelectOption>();
		
        for ( SelectOption so : selectedItemTooltipFields ) {
            selectedOptions.add(new SelectOption(so.getValue(), so.getLabel()));
        }
        
        //selectedObjectSection = ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip.name();
        
        ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip,
    					selectedOptions);
    	return objectFieldsUP.upsertUserPreference();
    	
    }
    
     public boolean saveSelectedItemGridFields() {
		
		List<SelectOption> selectedOptions = new List<SelectOption>();
		
        for ( SelectOption so : selectedItemGridFields ) {
            selectedOptions.add(new SelectOption(so.getValue(), so.getLabel()));
        }
        
        //selectedObjectSection = ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip.name();
        
        ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Header,
    					selectedOptions);
    	return objectFieldsUP.upsertUserPreference();
    	
    }
    
    private static List<SelectOption> buildSObjectFieldListExcludingSelectedFields(String sObjectName, ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections objectSection, 
    	List<SelectOption> fieldCollectionToExclude)
	{
		List<SelectOption> result = new List<SelectOption>();
		
		Map<String,String> apiFieldsToExclude = new Map<String,String>();
		
		if (sObjectName == 'Quote' && objectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip)
			apiFieldsToExclude = getQuoteMasterFieldsListToExclude();
		else if (sObjectName == 'Quote_Line_Group__c' && objectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip)
			apiFieldsToExclude = getQuoteGroupMasterFieldsListToExclude();
		else if (sObjectName == 'QuoteLineItem' && objectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip)
			apiFieldsToExclude = getQuoteLineItemsMasterFieldsListToExclude_Tooltip();
		else if (sObjectName == 'QuoteLineItem' && objectSection == ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Header)
			apiFieldsToExclude = getQuoteLineItemsMasterFieldsListToExclude_Grid();
			
				
		List<SelectOption> allSObjectFields = MetadataTools.getSObjectFieldsLabelAndApiName(sObjectName, apiFieldsToExclude);
		for(SelectOption qf : allSObjectFields){
			boolean isExist = false;
			for(SelectOption s : fieldCollectionToExclude){
				if (s.getValue() == qf.getValue()){
					isExist = true;
					break;
				}
			}
			if (isExist == false){
				result.add(qf);
			}
		}
		
		return result;
	}
	
	
	private static Map<String,String> getQuoteMasterFieldsListToExclude()
	{
		Map<String,String> excludeList = new Map<String,String>();
		
		excludeList.put('AdditionalCity','AdditionalCity');
		excludeList.put('AdditionalCountry','AdditionalCountry');
		excludeList.put('AdditionalLatitude','AdditionalLatitude');
		excludeList.put('AdditionalLongitude','AdditionalLongitude');
		excludeList.put('AdditionalName','AdditionalName');
		excludeList.put('AdditionalState','AdditionalState');
		excludeList.put('AdditionalStreet','AdditionalStreet');
		excludeList.put('AdditionalPostalCode','AdditionalPostalCode');
		excludeList.put('BillingCity','BillingCity');
		excludeList.put('BillingCountry','BillingCountry');
		excludeList.put('BillingLatitude','BillingLatitude');
		excludeList.put('BillingLongitude','BillingLongitude');
		excludeList.put('BillingName','BillingName');
		excludeList.put('BillingState','BillingState');
		excludeList.put('BillingStreet','BillingStreet');
		excludeList.put('BillingPostalCode','BillingPostalCode');
		excludeList.put('BillingAddress','BillingAddress');
		excludeList.put('CreatedDate','CreatedDate');
		excludeList.put('Email','Email');
		excludeList.put('Fax','Fax');
		excludeList.put('Id','Id');
		excludeList.put('LineItemCount','LineItemCount');
		excludeList.put('LastModifiedDate','LastModifiedDate');
		excludeList.put('LastReferencedDate','LastReferencedDate');
		excludeList.put('LastViewedDate','LastViewedDate');
		excludeList.put('Phone','Phone');
		excludeList.put('QuoteToCity','QuoteToCity');
		excludeList.put('QuoteToCountry','QuoteToCountry');
		excludeList.put('QuoteToLatitude','QuoteToLatitude');
		excludeList.put('QuoteToLongitude','QuoteToLongitude');
		excludeList.put('QuoteToName','QuoteToName');
		excludeList.put('QuoteToState','QuoteToState');
		excludeList.put('QuoteToStreet','QuoteToStreet');
		excludeList.put('QuoteToPostalCode','QuoteToPostalCode');
		excludeList.put('ShippingCity','ShippingCity');
		excludeList.put('ShippingCountry','ShippingCountry');
		excludeList.put('ShippingLatitude','ShippingLatitude');
		excludeList.put('ShippingLongitude','ShippingLongitude');
		excludeList.put('ShippingName','ShippingName');
		excludeList.put('ShippingState','ShippingState');
		excludeList.put('ShippingStreet','ShippingStreet');
		excludeList.put('ShippingPostalCode','ShippingPostalCode');
		excludeList.put('ShippingHandling','ShippingHandling');
		excludeList.put('SystemModstamp','SystemModstamp');
		
		
		return excludeList;
	}
	
	private static Map<String,String> getQuoteGroupMasterFieldsListToExclude()
	{
		Map<String,String> excludeList = new Map<String,String>();
		
		excludeList.put('CreatedDate','CreatedDate');
		excludeList.put('LastModifiedDate','LastModifiedDate');
		excludeList.put('linegroup_status__c','linegroup_status__c');
		excludeList.put('Id','Id');
		excludeList.put('GroupType__c','GroupType__c');
		
		excludeList.put('SystemModstamp','SystemModstamp');
			
		
		return excludeList;
	}
	
	private static Map<String,String> getQuoteLineItemsMasterFieldsListToExclude_Tooltip()
	{
		Map<String,String> excludeList = new Map<String,String>();
		
		excludeList.put('CreatedDate','CreatedDate');
		excludeList.put('LastModifiedDate','LastModifiedDate');
		excludeList.put('ServiceDate','ServiceDate');
		excludeList.put('Id','Id');
		excludeList.put('LineNumber','LineNumber');
		excludeList.put('SortOrder','SortOrder');
		excludeList.put('Total_Landed_Cost__c','Total_Landed_Cost__c');
		excludeList.put('SystemModstamp','SystemModstamp');
			
		
		return excludeList;
	}
	
	private static Map<String,String> getQuoteLineItemsMasterFieldsListToExclude_Grid()
	{
		Map<String,String> excludeList = new Map<String,String>();
		
		excludeList.put('Quantity','Quantity');
		excludeList.put('UnitPrice','UnitPrice');
		excludeList.put('discount_amount__c','discount_amount__c');
		excludeList.put('TotalPrice','TotalPrice');
		excludeList.put('Discount','Discount');
		excludeList.put('CreatedDate','CreatedDate');
		excludeList.put('LastModifiedDate','LastModifiedDate');
		excludeList.put('ServiceDate','ServiceDate');
		excludeList.put('Id','Id');
		excludeList.put('LineNumber','LineNumber');
		excludeList.put('SortOrder','SortOrder');
		excludeList.put('Total_Landed_Cost__c','Total_Landed_Cost__c');
		excludeList.put('SystemModstamp','SystemModstamp');
			
		
		return excludeList;
	}
	/**
	Below method populate the default BU dropdown value in user preference
	*/
	public String selectedDefaultBUSelectOptions {get;set;}
    private SelectOption[] selectedDefaultBUAsSelectOptionsList;
	public Component.Apex.SelectList getAllDefaultBUSelectOptions()
	{
        Component.Apex.SelectList retComponent = new Component.Apex.SelectList(id='defBUSelection',multiselect=false,size=1);
        retComponent.expressions.value = '{!selectedDefaultBUSelectOptions}';
        retComponent.expressions.label = '{!$Label.USER_PREF_TAB_DFLT_BU}';
		List<Component.Apex.SelectOption> retList = new List<Component.Apex.SelectOption>();
		Set<String> allowedDefaultBUOptions = Default_BU_List__c.getAll().keySet();
		for(String buName : allowedDefaultBUOptions)
		{
            Component.Apex.SelectOption selOption = new Component.Apex.SelectOption();
            selOption.itemlabel = buName;
            selOption.itemvalue = buName;
			retComponent.childComponents.add(selOption);
		}
		return retComponent;
	}
	public boolean saveDefaultBUPreference()
	{
		if(selectedDefaultBUSelectOptions <> null)
		{
			ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
	    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.DefaultBUSelection,
	    					new List<SelectOption>{new SelectOption(selectedDefaultBUSelectOptions,selectedDefaultBUSelectOptions)});
	    	return objectFieldsUP.upsertUserPreference();
		}
		else
		return true;
	}
	
}