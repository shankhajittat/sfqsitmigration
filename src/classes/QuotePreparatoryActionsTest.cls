@isTest
private class QuotePreparatoryActionsTest {

	static testMethod void testQuotePreparatoryActions() {
		
		// Create and manipulate testdata
		List<Quote> oldTriggerQuotes = new List<Quote>();
		List<Quote> newTriggerQuotes = new List<Quote>();
        Quote oneQuote = buildTestData();
        Quote afterInsertQuote = [SELECT Id, ExpirationDate, Document_Date__c, QuoteType__c FROM Quote WHERE Id =: oneQuote.Id];
        System.assert(afterInsertQuote.ExpirationDate == afterInsertQuote.Document_Date__c.addDays(30), 'Expiry date should have been calculated');
        
        oldTriggerQuotes.Add(oneQuote);
        oneQuote.Document_Date__c =  oneQuote.Document_Date__c.addDays(2);
        
        newTriggerQuotes.Add(oneQuote);
        
        //Start Test
        Test.startTest();
        
        // Test the constructor with no data
        QuotePreparatoryActions theQuotePreparatoryActionsEmpty = new QuotePreparatoryActions( null, null);
        System.assertEquals(false, theQuotePreparatoryActionsEmpty.terminateProcessing,'An exception occured while execuing quote trigger with no data');
        
        // Tests the constructor with data
        QuotePreparatoryActions theQuotePreparatoryActions = new QuotePreparatoryActions( newTriggerQuotes, oldTriggerQuotes);
		
		// Test the retrieveExpiryDateCustomSetting and calculateQuoteExpiryDate method ERROR conditions by setting 
		// newquotes to null and making sure processing in the method is not done
		theQuotePreparatoryActions.newQuotes = null;
		theQuotePreparatoryActions.retrieveExpiryDateCustomSetting();
		System.assertEquals(null, theQuotePreparatoryActions.expiryDateRules, 'Custom settings object should have been null');
		theQuotePreparatoryActions.calculateQuoteExpiryDate();
		for(Quote oneNewQuote : newTriggerQuotes) {
			System.assertEquals(null,oneNewQuote.ExpirationDate, 'The expiration date should not have been calculated because we set newquotes to null');
		}
		
		// Tests the retrieveExpiryDateCustomSetting method valid conditions by setting newQuotes back to the correct values
		theQuotePreparatoryActions.newQuotes = newTriggerQuotes;
		theQuotePreparatoryActions.retrieveExpiryDateCustomSetting();
		system.assertNotEquals(null, theQuotePreparatoryActions.expiryDateRules, 'Custom settings should not be null');
		
		
		// Tests the autopopulate method
		theQuotePreparatoryActions.autoPopulate();
		System.assertEquals(false, theQuotePreparatoryActions.terminateProcessing,'An exception occured while execuing quote trigger');
		
		update oneQuote;
		Quote afterUpdateQuote = [SELECT Id, ExpirationDate, Document_Date__c, QuoteType__c FROM Quote WHERE Id =: oneQuote.Id];
		System.assert(afterUpdateQuote.ExpirationDate == afterUpdateQuote.Document_Date__c.addDays(30), 'Expiry date should have been recalculated and changed after document date changed');
        
        
        //Test the handleErrors method
		theQuotePreparatoryActions.handleErrors('Test Error');
		for(ApexPages.Message message : ApexPages.getMessages()) {
        	System.debug('>>>>>>>>>>' + message.getComponentLabel() + ' : ' + message.getSummary());
        	System.assertEquals('Test Error', message.getSummary(), 'The apex page should have "Test Error" added to it');
		}
		
        Test.stopTest();
        	
    }
    
   
     private static Quote buildTestData(){
    	
    	Boolean createInDatabase = true;
        
       	String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(createInDatabase);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);

        
        //String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
		//String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();

        Opportunity o = new OpportunityBuilder()
        					.account(vinnies)
        					.priceBookId(customPricebook.Id).build(createInDatabase);
        System.assert(o.Id <> null,'An error occured while creating Opportunity.');
        
       	/*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(createInDatabase);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        
        
        // You need a custom setting for the transaction type to state what type of quote it is. Create custom setting mapping the above transaction type to the 
        // globalconstants enum QuoteTypes.
        Quote_Types__c cs = new Quote_Types__c();
		cs.Name = 'SetBuy Test';  
		cs.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
		cs.Quote_Type_Enum_Index__c = 0;
		Database.SaveResult DR_QuoteTypeCS = Database.insert(cs);
        */
        
        //Create Quote
        Quote quote = new QuoteBuilder()
        		.opportunity(o)
        		.priceBookId(customPricebook.Id)
        		.quoteType(GlobalConstants.QuoteTypes.CustomerQuote)
        		.build(createInDatabase);
        System.assert(quote.Id <> null,'An error occured while creating Quote.');
        
        //sync Opportunity and Quote lines.
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
        Quote oneQuote = [SELECT Id, Document_Date__c FROM Quote WHERE Id =: quote.Id];
        			
		return oneQuote;
        
    }
}