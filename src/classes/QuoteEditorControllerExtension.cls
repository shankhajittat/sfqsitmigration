public class QuoteEditorControllerExtension {
		
	public class ValueException extends Exception{}

	// using a distinct property name to ensure view binds to view state from this extension
    // instead the quote property of the standard controller
    public Quote quoteFromStdController{get;set;}
        
	public QuoteEditorControllerExtension(ApexPages.StandardController stdController) {
        this.quoteFromStdController = (Quote)stdController.getRecord();
	}
	
	
		
	@RemoteAction
    public static QuoteLineItem addItem(String quoteId, String groupId, String productId) {
        if (StringUtils.isEmpty(quoteId)) {
            return null;
        }
        if (productId == null) {
            return null;
        }

        Quote quotePriceBook = [SELECT q.Id,q.Pricebook2Id FROM Quote q WHERE q.Id =: quoteId LIMIT 1];
      	PricebookEntry pbe;
	    List <PricebookEntry> pbes = [SELECT pbe.Product2Id, pbe.UnitPrice, pbe.Product2.Description,pbe.Pricebook2Id, pbe.StandardCost__c, pbe.MyMedisetItemCategory__c,
	    								pbe.Material_StatusCode__c,pbe.Material_StatusCode_Description__c
		        					  FROM PricebookEntry pbe
		        					  WHERE pbe.Product2Id = :productId And pbe.Pricebook2Id =: quotePriceBook.Pricebook2Id And pbe.AvailableForSale__c = true];
        if (pbes == null || pbes.size() == 0) {
        	throw new ValueException('Could not find pricebookentry for product : ' + productId);
        } else {
        	pbe = pbes[0];
        }
        					  
        		  
        Quote_Line_Group__c quoteLineGroup = null;					  
        List<Quote_Line_Group__c> quoteLineGroups = [Select g.Id, g.Name, g.QuoteId__c, g.SetClassId__c, g.GroupType__c 
													From Quote_Line_Group__c g 
													Where g.QuoteId__c =:quoteId And g.Id = :groupId];
		if (quoteLineGroups == null || quoteLineGroups.size() == 0) {
			throw new ValueException('Could not find any groups for this quote that have a group id of : ' + groupId);
		}
		
		if (quoteLineGroups.size() == 1)	
			quoteLineGroup = quoteLineGroups[0];
			
		Double	sequenceNumber = 10;
		// Determine next sequence number to use for the items
		QuoteLineItem theLastItem;
		for (List<QuoteLineItem> existingItems : [SELECT Id, Sequence_Number__c FROM QuoteLineItem
													WHERE QuoteId = :quoteId
													order by Sequence_Number__c desc]) {
			if (existingItems.isEmpty() == false ) {
				theLastItem = existingItems[0];
				if (theLastItem.Sequence_Number__c == null) {
					sequenceNumber = 10;	
				} else {
					sequenceNumber = theLastItem.Sequence_Number__c + 10;
				}
			}
		}
		
		double totalStandardCost = pbe.StandardCost__c;
		//If Material MyMedisetItemCategory__c is ZSET, then calculate StandardCost from BOM Items (Components). BOM Component Qty * Material PriceBookEntry StandardCost
		if (pbe.MyMedisetItemCategory__c != null && pbe.MyMedisetItemCategory__c == 'ZSET'){
			totalStandardCost = 0;
			List<Set_Class_Items__c> setClassItems = new List<Set_Class_Items__c>();
			Set<Id> setProductIds = new Set<Id>();
			for(List<Set_Class_Items__c> items: [Select s.Quantity__c, s.Product__c, s.Product__r.ProductCode, s.Set_Class__c,
									s.Set_Class__r.BomReferenceProduct__c 
									From Set_Class_Items__c s
									WHERE s.Set_Class__r.BomReferenceProduct__c = :pbe.Product2Id]) {
				for(Set_Class_Items__c item :items){
					setClassItems.add(item);
					setProductIds.add(item.Product__c);
				}
			}
			
			//Now get StandardCost from PricebookEntry for all the BOM Item Materials (Components)
			Map<Id,double> mapComponentsStdCost = new Map<Id,double>(); //<ProductId,StandardCost>
			for(List<PricebookEntry> entries: [Select p.StandardCost__c, p.Product2Id From PricebookEntry p
													WHERE p.Pricebook2Id = :quotePriceBook.Pricebook2Id 
														And p.Product2Id IN :setProductIds]) {
				for(PricebookEntry entry :entries) {
					mapComponentsStdCost.put(entry.Product2Id,entry.StandardCost__c);
				}				
			}
			setProductIds.clear();
			setProductIds = null;
			
			for(Set_Class_Items__c bomItem : setClassItems){
				double stdCost = 0;
				if (mapComponentsStdCost.containsKey(bomItem.Product__c)){
					stdCost = mapComponentsStdCost.get(bomItem.Product__c);
				}
				double lineTotalStdCost = bomItem.Quantity__c * stdCost;
				
				totalStandardCost += lineTotalStdCost;
			}
			
			mapComponentsStdCost.clear();
			setClassItems.clear();
			mapComponentsStdCost = null;
			setClassItems = null;
			
		}
					
					
        QuoteLineItem newQuoteLine = new QuoteLineItem(
						QuoteId=quoteId,
						PriceBookEntryId=pbe.Id,
						UnitPrice=pbe.UnitPrice,
						Standard_Cost__c= totalStandardCost,
						Agreed_Quantity__c=0,
						Actual_Quantity__c=0,
						Quantity=1,  // Salesforce forces you to put a non zero quantity when creating an item
						Proposed_Quantity__c = 1,
						Discount = 0,
						Description = pbe.Product2.Description,
						Set_Class__c = quoteLineGroup.SetClassId__c,
						ERP_Record_Action_Mode__c = 'Create',
						Sequence_Number__c = sequenceNumber,
						Material_StatusCode__c = pbe.Material_StatusCode__c,
	    				Material_StatusCode_Description__c = pbe.Material_StatusCode_Description__c,
						Quote_Line_Group__c = quoteLineGroup.Id);
		insert newQuoteLine;
        return EditableTableQuotePlugin.getLineItems(new Id[] {newQuoteLine.Id})[0];    
    }	
    
    /*public static string getProduct(Id productId) {
		String productDescription = '';
		
		List<Product2> items = [SELECT Id, Description From Product2 WHERE Id = :productId LIMIT 1];
		
		if (items.isEmpty() ) {
			return productDescription;
		} else {
			return productDescription = items[0].Description;
		}
	}*/
        
    public String getGridPreferences() {
    	return JSONPreferenceTools.getPreferences('QUOTE_GRIDS');
    }

    public String getCollapsiblePreferences() {
    	return JSONPreferenceTools.getPreferences('QUOTE_COLLAPSIBLE');
    }
    
	@RemoteAction
    public static void setPreferences(String prefGroup, String prefsJSON) {
    	JSONPreferenceTools.setPreferences(prefGroup, prefsJSON);
    }
    	
    public boolean getQuoteIsRevisable() {
    	Quote q = [select Status from Quote where Id = :quoteFromStdController.Id];
    	return q.Status == GlobalConstants.QS_STATUS_DRAFT || q.Status == GlobalConstants.QS_STATUS_PRICE_CONFIRMED;
    }
    
    public String getProcessLogURL() {
		String processLogURL = '';
		List<Report> processLogreport = [SELECT Id FROM Report WHERE Name = 'ProcessLog' LIMIT 1];
		if (processLogReport.isEmpty() == false) {
			processLogURL = '/' + processLogreport[0].Id + '?pv0=' + quoteFromStdController.Id;
		} else {
			processLogURL='';
		}
		return processLogURL;
		
	}	

    // The following 4 methods are purely for tooltips on quote/group and item level
	public string getQuoteUserHeaderSettings(){
		// Get user settings for quote
    	ObjectFieldsUserPreference quoteTooltipUserSettings = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip);
    	String userSettings = retrieveUserSettings(quoteTooltipUserSettings);
    	return userSettings;
    }
    
    public string getQuoteUserGroupSettings(){
		// Get user settings for group
    	ObjectFieldsUserPreference groupTooltipUserSettings = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip);
    	String userSettings = retrieveUserSettings(groupTooltipUserSettings);
    	return userSettings;
    }
    
    public string getQuoteUserItemSettings(){
		// Get user settings for item
    	ObjectFieldsUserPreference itemTooltipUserSettings = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Item1Tooltip);
    	String userSettings = retrieveUserSettings(itemTooltipUserSettings);
    	return userSettings;
    }
   
    private string retrieveUserSettings(ObjectFieldsUserPreference pTooltipUserSettings){
    	List<Map<String,String>> UserSettingFields = pTooltipUserSettings.retrieveUserPreferenceDetailsAsListOfMap();
    	String jsonString = JSON.serialize(UserSettingFields);	
    	system.debug('>>>>>>>>>>> JSON string of usersettings : ' + jsonString);
    	return jsonString;    	
    }
    /**
    Quote Document Generation By VisualForce
    */
    public PageReference generateQuoteDocAsAttachment()
    {
		try
		{
	    	Quote quoteTemp = [select Id,QuoteNumber,QuoteType__c,Status from Quote where Id = :quoteFromStdController.Id];  
            quoteTemp.Status = GlobalConstants.QS_STATUS_RELEASED_TO_CUSTOMER;
            upsert quoteTemp;
            //System.debug('quoteTemp=>'+quoteTemp);
            //System.debug('quoteTemp.Quote_Transaction_Type__r.Name'+ quoteTemp.Quote_Transaction_Type__r.Name);
	    	String autoGenPdfFileName = ((quoteTemp == null) ? 'QUOTENUMBER' : ( (quoteTemp.QuoteNumber == null) ? 'QUOTENUMBER' : quoteTemp.QuoteNumber))+'_'+Datetime.now().format('yyyy_MM_dd_hh_mm_ss')+'.pdf';
            Attachment att = new Attachment();
	    	att.Name=autoGenPdfFileName;
	    	att.parentId = quoteTemp.Id;
	    	if(quoteTemp.QuoteType__c == GlobalConstants.QuoteTypes.CustomerQuote.name()
	    		|| quoteTemp.QuoteType__c == GlobalConstants.QuoteTypes.SetRequest.name()
	    		|| quoteTemp.QuoteType__c == GlobalConstants.QuoteTypes.ChangeSet.name() || quoteTemp.QuoteType__c == GlobalConstants.QuoteTypes.LoanSetRequest.name())
	    	{
                //System.debug('Creating attachment');

		     	PageReference reportPage = Page.GPQSetBuyReportPage;
		     	reportPage.getParameters().put('id',quoteTemp.Id);
                if(!Test.isRunningTest())
                {
		     		att.body = reportPage.getContent(); 
                }
                else
                {
                    att.body = Blob.toPdf('Test attachment content');
                }
                
	    	}
            if(att.Body<> null)
            	insert att;
		}	
		catch(Exception ex)
		{
			ApexPages.addMessages(ex);
            //System.debug('Error:'+ex);
		}
     	return null ;
    }
	
}