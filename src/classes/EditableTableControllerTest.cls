@isTest(seeAllData=true) // required for tests that use pricebooks
private class EditableTableControllerTest {
	
	
	@IsTest
	public static void testEditableTableController() { 
		EditableTableController theEditableTableController = new EditableTableController();
		
		// Create test data
		QuoteLineItem testQuoteLine = TestingTools.getTestQuoteLineItem();    	
    	System.assertEquals(2, testQuoteLine.Quantity, 'Initial Quantity should be 2');
    	System.assertEquals(10, testQuoteLine.Discount, 'Initial Discount Percentage should be 10%');
    	
    	
		//Get Groups
		String quoteId = testQuoteLine.QuoteId;
		List<EditableTableGroup> groups = EditableTableController.getGroups('EditableTableQuotePlugin', quoteId);
		system.assert(groups.size() <> 0, 'A group should have been returned');
		
		//Set Value
		QuoteLineItem updatedItem = (QuoteLineItem)EditableTableController.setValue('EditableTableQuotePlugin', testQuoteLine.Id, 'Quantity', '20');    	
    	System.assertEquals(20, updatedItem.Quantity, 'Quantity in db should now be updated');
		
		//Delete Item
		List<String> itemsToDelete = new List<String>();
		itemsToDelete.Add((String)testQuoteLine.Id);
		String returnResult = EditableTableController.deleteItems('EditableTableQuotePlugin', itemsToDelete);   
		System.assertEquals('{}', ReturnResult, 'Delete of one quoteline item has failed but should have been successful'); 
		
		//Delete Group
		List<String> groupsToDelete = new List<String>();
		
		EditableTableGroup oneQLG = groups[0]; 
		Map<String,String> theTestGroup = oneQLG.attributes;
		Boolean contains = theTestGroup.containsKey('Id');
		System.assertEquals(contains, True, 'The group attributes do not contain a group Id field');
		String oneGroupId = theTestGroup.get('Id');
		System.assertNotEquals(null, oneGroupId, 'At least one Group should have been found - the testing tool should have created one for the quote.');
		
		groupsToDelete.Add(oneGroupId);
		returnResult = EditableTableController.deleteGroups('EditableTableQuotePlugin', groupsToDelete);   
		System.assertEquals('{}', ReturnResult, 'Delete of one group has failed but should have been successful'); 
	}
	
	
	//Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
	//need to impliment external webservice mock
	@IsTest
	public static void testEditableTableController2() { 
		EditableTableController theEditableTableController = new EditableTableController();
		
		// Create test data
		QuoteLineItem testQuoteLine = TestingTools.getTestQuoteLineItem();    	
    	System.assertEquals(2, testQuoteLine.Quantity, 'Initial Quantity should be 2');
    	System.assertEquals(10, testQuoteLine.Discount, 'Initial Discount Percentage should be 10%');
		
		//Create UnitTest Prerequisite Data.
        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
    	
    	Test.startTest() ;
    	 // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        
        // Call the method that invokes a callout
    	Boolean isSuccessful = EditableTableController.getGroupPricing('EditableTableQuotePlugin', testQuoteLine.QuoteId, testQuoteLine.Quote_Line_Group__c);
		System.assertEquals( true, isSuccessful, 'Expected pricing to return true but returned false');
		Test.stopTest();
	}
	
	
	@IsTest
	public static void testEditableTableController3() { 
		EditableTableController theEditableTableController = new EditableTableController();
		
		// Create test data
		QuoteLineItem testQuoteLine = TestingTools.getTestQuoteLineItem();    	
    	System.assertEquals(2, testQuoteLine.Quantity, 'Initial Quantity should be 2');
    	System.assertEquals(10, testQuoteLine.Discount, 'Initial Discount Percentage should be 10%');

    	List<String> itemsForPricing = new List<String>();
		itemsForPricing.Add((String)testQuoteLine.Id);    

		//Create UnitTest Prerequisite Data.
        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
		
    	Test.startTest() ;

    	 // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        
        // Call the method that invokes a callout
    	Boolean isSuccessful = EditableTableController.getItemPricing('EditableTableQuotePlugin', testQuoteLine.QuoteId, itemsForPricing);
		System.assertEquals( true, isSuccessful, 'Expected pricing to return true but returned false');
		Test.stopTest();
	}
	
	
}