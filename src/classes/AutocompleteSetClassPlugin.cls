public class AutocompleteSetClassPlugin implements AutocompleteController.AutoCompletePlugin {
	
	public Set<String> itemsAlreadyOnQuote = new Set<String>();
	public String quoteId;
	
	public void init(Map<String,String> context){
		quoteId=context.get('id2');
	}
	
	public List<SObject> find(String searchTerm) {
		String searchTermWild = String.escapeSingleQuotes(searchTerm) + '*';
		// using SOSL only is approx 2x faster than using SOQL. Compare this to AccountPlugin performance.
    	List<List<SObject>> searchList = [FIND :searchTermWild IN ALL FIELDS 
    								  	  RETURNING Set_Class__c (Id, Name, Item_Count__c WHERE Active__c = true)];  
    			  	  
        // Build a list of sets already on this quote, to be used for filtering
		for(List<QuoteLineItem> items : [Select q.Set_Class__c
												From QuoteLineItem q
												WHERE q.QuoteId = :quoteId]) {
										
			for (QuoteLineItem item : items) {
	    		itemsAlreadyOnQuote.add(item.Set_Class__c);
	    		system.debug('adding set class id to set: ' + item.Set_Class__c);
	    	}	
		}
    							   	
    	return searchList.get(0);
    	
	}
	
	
	public boolean filter(SObject oneSObject) {
		for(string one : itemsAlreadyOnQuote){
			system.debug('set classes already in set : ' + one);
		}
		Set_Class__c setClass = (Set_Class__c)oneSObject;
		boolean isAlreadyOnQuote = itemsAlreadyOnQuote.contains(setClass.Id);
		return !isAlreadyOnQuote;
	}
	
	public String css() {
		return '.ui-menu-item .count { font-size: 0.8em; font-style: italic;}'+						
			   '.ui-menu-item .label { font-weight: bold; }';
	}

	public String jsRender() {		
		return
			//'console.log(item.Name);'+
			'return $172( "<li></li>" )'+						
			'.data( "item.autocomplete", item )'+
		 	'.append( "<a><span class=\'label\'>" + item.Name + "</span>'+
		 	'<br><span class=\'count\'>" + item.Item_Count__c + " Item(s)</span></a>")'+
			'.appendTo( ul )';
	}

	public String jsHandleSelect(List<String> domIds, List<String> values) {
		return 'document.getElementById("'+domIds.get(0)+'").value=id;'+						
			   'processChooseSet();';
	}
	
	
	
}