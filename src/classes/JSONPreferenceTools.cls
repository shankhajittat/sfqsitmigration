public with sharing class JSONPreferenceTools {

    // this class is used to structure the grid preferences correctly for JS
    // when stored in the database, all pref values are stored as strings
    // so methods exist below to translate in and out of the database
    public class GridPrefs {
    	public Boolean SETBUY_SORT_DIR_ASC{get;set;}
    	public Boolean CONSETBUILD_SORT_DIR_ASC{get;set;}
    	public Boolean CBC_SORT_DIR_ASC{get;set;}
    	public Boolean SP_SORT_DIR_ASC{get;set;}
    	public Boolean CHANGESET_SORT_DIR_ASC{get;set;}
    	public Boolean LOANSETREQUEST_SORT_DIR_ASC{get;set;}
    	public String SETBUY_SORT_FIELD{get;set;}
    	public String CONSETBUILD_SORT_FIELD{get;set;}
    	public String CBC_SORT_FIELD{get;set;}
    	public String CHANGESET_SORT_FIELD{get;set;}
    	public String LOANSETREQUEST_SORT_FIELD{get;set;}
    	public String SP_SORT_FIELD{get;set;}
    	public Map<String, Integer> SETBUY_COLUMN_SIZES{get;set;}
    	public Map<String, Integer> CONSETBUILD_COLUMN_SIZES{get;set;}
    	public Map<String, Integer> CBC_COLUMN_SIZES{get;set;}
    	public Map<String, Integer> SP_COLUMN_SIZES{get;set;}
    	public Map<String, Integer> CHANGESET_COLUMN_SIZES{get;set;}
    	public Map<String, Integer> LOANSETREQUEST_COLUMN_SIZES{get;set;}
    	public List<String> SETBUY_COLUMNS_DISPLAYED{get;set;}
    	public List<String> CONSETBUILD_COLUMNS_DISPLAYED{get;set;} 
    	public List<String> CBC_COLUMNS_DISPLAYED{get;set;} 
    	public List<String> SP_COLUMNS_DISPLAYED{get;set;}
    	public List<String> CHANGESET_COLUMNS_DISPLAYED{get;set;} 
    	public List<String> LOANSETREQUEST_COLUMNS_DISPLAYED{get;set;} 
    }
    
    public class Collapsible {
    	public String state{get;set;}
    	public String section{get;set;}
    }
    
    // read and translate the various String values for Grid prefs into correct types for the JS code
    public static GridPrefs readGridPreferences() {
    	Map<String, String> gridGroup = PreferenceTools.getGroup(PreferenceTools.Groups.QUOTE_GRIDS);
    	GridPrefs gp = new GridPrefs();
    	gp.SETBUY_SORT_FIELD = gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_SORT_FIELD));
    	gp.CONSETBUILD_SORT_FIELD = gridGroup.get(String.valueOf(PreferenceTools.Keys.CONSETBUILD_SORT_FIELD));
    	gp.CBC_SORT_FIELD = gridGroup.get(String.valueOf(PreferenceTools.Keys.CBC_SORT_FIELD));
    	gp.SP_SORT_FIELD = gridGroup.get(String.valueOf(PreferenceTools.Keys.SP_SORT_FIELD));
    	gp.CHANGESET_SORT_FIELD = gridGroup.get(String.valueOf(PreferenceTools.Keys.CHANGESET_SORT_FIELD));
    	gp.LOANSETREQUEST_SORT_FIELD = gridGroup.get(String.valueOf(PreferenceTools.Keys.LOANSETREQUEST_SORT_FIELD));
    	
    	gp.SETBUY_SORT_DIR_ASC = gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_SORT_DIR_ASC)) == 'true';
    	gp.CONSETBUILD_SORT_DIR_ASC = gridGroup.get(String.valueOf(PreferenceTools.Keys.CONSETBUILD_SORT_DIR_ASC)) == 'true';
    	gp.CBC_SORT_DIR_ASC = gridGroup.get(String.valueOf(PreferenceTools.Keys.CBC_SORT_DIR_ASC)) == 'true';
    	gp.SP_SORT_DIR_ASC = gridGroup.get(String.valueOf(PreferenceTools.Keys.SP_SORT_DIR_ASC)) == 'true';
    	gp.CHANGESET_SORT_DIR_ASC = gridGroup.get(String.valueOf(PreferenceTools.Keys.CHANGESET_SORT_DIR_ASC)) == 'true';
    	gp.LOANSETREQUEST_SORT_DIR_ASC = gridGroup.get(String.valueOf(PreferenceTools.Keys.LOANSETREQUEST_SORT_DIR_ASC)) == 'true';
    	
    	Map<String, Object> sizesSetBuy = (Map<String, Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_COLUMN_SIZES)));
    	gp.SETBUY_COLUMN_SIZES = new Map<String, Integer>();
    	if (sizesSetBuy != null){
	    	for (String fieldName : sizesSetBuy.keySet()) {
	    		gp.SETBUY_COLUMN_SIZES.put(fieldName, (Integer) sizesSetBuy.get(fieldName));
	    	}
    	}
    	Map<String, Object> sizesConSetBuild = (Map<String, Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.CONSETBUILD_COLUMN_SIZES)));
    	gp.CONSETBUILD_COLUMN_SIZES = new Map<String, Integer>();
    	if (sizesConSetBuild != null){
	    	for (String fieldName : sizesConSetBuild.keySet()) {
	    		gp.CONSETBUILD_COLUMN_SIZES.put(fieldName, (Integer) sizesConSetBuild.get(fieldName));
	    	}
    	}
    	Map<String, Object> sizesCBC = (Map<String, Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.CBC_COLUMN_SIZES)));
    	gp.CBC_COLUMN_SIZES = new Map<String, Integer>();
    	if (sizesCBC != null){
	    	for (String fieldName : sizesCBC.keySet()) {
	    		gp.CBC_COLUMN_SIZES.put(fieldName, (Integer) sizesCBC.get(fieldName));
	    	}
    	}
    	Map<String, Object> sizesSP = (Map<String, Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.SP_COLUMN_SIZES)));
    	gp.SP_COLUMN_SIZES = new Map<String, Integer>();
    	if (sizesSP != null){
	    	for (String fieldName : sizesSP.keySet()) {
	    		gp.SP_COLUMN_SIZES.put(fieldName, (Integer) sizesSP.get(fieldName));
	    	}
    	}
    	Map<String, Object> sizesCHANGESET = (Map<String, Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.CHANGESET_COLUMN_SIZES)));
    	gp.CHANGESET_COLUMN_SIZES = new Map<String, Integer>();
    	if (sizesCHANGESET != null){
	    	for (String fieldName : sizesCHANGESET.keySet()) {
	    		gp.CHANGESET_COLUMN_SIZES.put(fieldName, (Integer) sizesCHANGESET.get(fieldName));
	    	}
    	}
    	Map<String, Object> sizesLOANSETREQUEST = (Map<String, Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.LOANSETREQUEST_COLUMN_SIZES)));
    	gp.LOANSETREQUEST_COLUMN_SIZES = new Map<String, Integer>();
    	if (sizesLOANSETREQUEST != null){
	    	for (String fieldName : sizesLOANSETREQUEST.keySet()) {
	    		gp.LOANSETREQUEST_COLUMN_SIZES.put(fieldName, (Integer) sizesLOANSETREQUEST.get(fieldName));
	    	}
    	}
    	
    	List<Object> colsSetBuy = (List<Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.SETBUY_COLUMNS_DISPLAYED)));
    	gp.SETBUY_COLUMNS_DISPLAYED = new List<String>();
    	if (colsSetBuy != null){
	    	for (Object c : colsSetBuy) {
	    		gp.SETBUY_COLUMNS_DISPLAYED.add((String)c);
	    	}
    	}
    	List<Object> colsConSetBuild = (List<Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.CONSETBUILD_COLUMNS_DISPLAYED)));
    	gp.CONSETBUILD_COLUMNS_DISPLAYED = new List<String>();
    	if (colsConSetBuild != null){
	    	for (Object c : colsConSetBuild) {
	    		gp.CONSETBUILD_COLUMNS_DISPLAYED.add((String)c);
	    	}
    	}
    	List<Object> colsCBC = (List<Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.CBC_COLUMNS_DISPLAYED)));
    	gp.CBC_COLUMNS_DISPLAYED = new List<String>();
    	if (colsCBC != null){
	    	for (Object c : colsCBC) {
	    		gp.CBC_COLUMNS_DISPLAYED.add((String)c);
	    	}
    	}
    	List<Object> colsSP = (List<Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.SP_COLUMNS_DISPLAYED)));
    	gp.SP_COLUMNS_DISPLAYED = new List<String>();
    	if (colsSP != null){
	    	for (Object c : colsSP) {
	    		gp.SP_COLUMNS_DISPLAYED.add((String)c);
	    	}
    	}
    	List<Object> colsCHANGESET = (List<Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.CHANGESET_COLUMNS_DISPLAYED)));
    	gp.CHANGESET_COLUMNS_DISPLAYED = new List<String>();
    	if (colsCHANGESET != null){
	    	for (Object c : colsCHANGESET) {
	    		gp.CHANGESET_COLUMNS_DISPLAYED.add((String)c);
	    	}
    	}
    	List<Object> colsLOANSETREQUEST = (List<Object>) JSON.deserializeUntyped(gridGroup.get(String.valueOf(PreferenceTools.Keys.LOANSETREQUEST_COLUMNS_DISPLAYED)));
    	gp.LOANSETREQUEST_COLUMNS_DISPLAYED = new List<String>();
    	if (colsLOANSETREQUEST != null){
	    	for (Object c : colsLOANSETREQUEST) {
	    		gp.LOANSETREQUEST_COLUMNS_DISPLAYED.add((String)c);
	    	}
    	}
    	return gp;
    }
    
    // read and translate the various String values for Collapsible prefs into correct shape for the JS code
    public static Map<String, Map<String, String>> readCollapsiblePreferences() {
    	Map<String, String> collapsibleGroup = PreferenceTools.getGroup(PreferenceTools.Groups.QUOTE_COLLAPSIBLE);
    	Map<String, Map<String, String>> prefs = new Map<String, Map<String, String>>();
    	for (String k : collapsibleGroup.keyset()) {
    		Map<String, String> v = new Map<String, String>();
    		v.put('state', collapsibleGroup.get(k)); 
    		prefs.put(k.toLowerCase(), v);
    	}
    	return prefs;
    }    
    
    public static String getPreferences(String prefGroup) {
		if (prefGroup == 'QUOTE_GRIDS') {
	    	return JSON.serialize(readGridPreferences());
		} else if (prefGroup == 'QUOTE_COLLAPSIBLE') {
	    	return JSON.serialize(readCollapsiblePreferences());
    	} else {
			throw new PreferenceTools.PreferenceException('Unimplemented prefs: '+prefGroup);
    	}
    }
    
    public static void writeGridPreferences(GridPrefs prefs) {
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_SORT_DIR_ASC, String.valueOf(prefs.SETBUY_SORT_DIR_ASC));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CONSETBUILD_SORT_DIR_ASC, String.valueOf(prefs.CONSETBUILD_SORT_DIR_ASC));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CBC_SORT_DIR_ASC, String.valueOf(prefs.CBC_SORT_DIR_ASC));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SP_SORT_DIR_ASC, String.valueOf(prefs.SP_SORT_DIR_ASC));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CHANGESET_SORT_DIR_ASC, String.valueOf(prefs.CHANGESET_SORT_DIR_ASC));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.LOANSETREQUEST_SORT_DIR_ASC, String.valueOf(prefs.LOANSETREQUEST_SORT_DIR_ASC));
    	
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_SORT_FIELD, prefs.SETBUY_SORT_FIELD);
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CONSETBUILD_SORT_FIELD, prefs.CONSETBUILD_SORT_FIELD);
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CBC_SORT_FIELD, prefs.CBC_SORT_FIELD);
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SP_SORT_FIELD, prefs.SP_SORT_FIELD);
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CHANGESET_SORT_FIELD, prefs.CHANGESET_SORT_FIELD);
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.LOANSETREQUEST_SORT_FIELD, prefs.LOANSETREQUEST_SORT_FIELD);
    	
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_COLUMN_SIZES, JSON.serialize(prefs.SETBUY_COLUMN_SIZES));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CONSETBUILD_COLUMN_SIZES, JSON.serialize(prefs.CONSETBUILD_COLUMN_SIZES));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CBC_COLUMN_SIZES, JSON.serialize(prefs.CBC_COLUMN_SIZES));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SP_COLUMN_SIZES, JSON.serialize(prefs.SP_COLUMN_SIZES));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CHANGESET_COLUMN_SIZES, JSON.serialize(prefs.CHANGESET_COLUMN_SIZES));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.LOANSETREQUEST_COLUMN_SIZES, JSON.serialize(prefs.LOANSETREQUEST_COLUMN_SIZES));
    	
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SETBUY_COLUMNS_DISPLAYED, JSON.serialize(prefs.SETBUY_COLUMNS_DISPLAYED));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CONSETBUILD_COLUMNS_DISPLAYED, JSON.serialize(prefs.CONSETBUILD_COLUMNS_DISPLAYED));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CBC_COLUMNS_DISPLAYED, JSON.serialize(prefs.CBC_COLUMNS_DISPLAYED));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.SP_COLUMNS_DISPLAYED, JSON.serialize(prefs.SP_COLUMNS_DISPLAYED));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.CHANGESET_COLUMNS_DISPLAYED, JSON.serialize(prefs.CHANGESET_COLUMNS_DISPLAYED));
    	PreferenceTools.setPreference(PreferenceTools.Groups.QUOTE_GRIDS, PreferenceTools.Keys.LOANSETREQUEST_COLUMNS_DISPLAYED, JSON.serialize(prefs.LOANSETREQUEST_COLUMNS_DISPLAYED));
    }
    
    private static PreferenceTools.Keys parseKey(String key) {
    	for (PreferenceTools.Keys k : PreferenceTools.Keys.values()) {
    		if (k.name() == key.toUpperCase()) {
    			return k;
    		}
    	}
    	return null;
    }

    public static void setPreferences(String prefGroup, String prefsJSON) {
		JSONParser parser = JSON.createParser(prefsJSON);
		if (prefGroup == 'QUOTE_GRIDS') {
			GridPrefs prefs = (GridPrefs)parser.readValueAs(GridPrefs.class);
			writeGridPreferences(prefs);
		} else if (prefGroup == 'QUOTE_COLLAPSIBLE') {
			Collapsible prefs = (Collapsible)parser.readValueAs(Collapsible.class);
    		PreferenceTools.setPreference(
    			PreferenceTools.Groups.QUOTE_COLLAPSIBLE, 
    			parseKey(prefs.section), 
    			prefs.state);
		} else {
			throw new PreferenceTools.PreferenceException('Unimplemented prefs: '+prefGroup);
		}
    }

}