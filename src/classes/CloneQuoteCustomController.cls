public with sharing class CloneQuoteCustomController {
	
	public Id sourceQuoteId {get; set;}
	public Quote sourceQuote{get;set;}
	public Id newQuoteId {get; set;}
	public String copyToAccountName {get; set;}
	public String accountId {get;set;}
	public String accountName {get; set;}
	public Boolean isQuoteTypeChangeRequired {get;set;}
	public CloneQuoteCustomController(){
		isQuoteTypeChangeRequired = false;

		sourceQuoteId = ApexPages.currentPage().getParameters().get('id');
		
		List<Quote> quotes = [SELECT Id, Name, Version__c, ERP_Document_Number__c, Document_Date__c, ExpirationDate, Opportunity.AccountId , 
									 Status , Customer_Purchase_Order_Number__c , Opportunity.Account.Name , QuoteType__c
								From Quote 
								WHERE Id = :sourceQuoteId LIMIT 1];
		if (quotes.isEmpty() ) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Could not find source QuoteId to continue CopyQuote action.'));
		} else {
			sourceQuote = quotes[0];
			copyToAccountName = sourceQuote.Opportunity.Account.Name;
			accountId = sourceQuote.Opportunity.AccountId;   // Default account to copy to 
		}
	}
	
	public PageReference chosenAccount() {
		try {
			
			if (StringUtils.isNullOrWhiteSpace(this.sourceQuoteId)){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Could not find source QuoteId to continue CopyQuote action.'));
				return null;
			}
			
			QuoteTools.QuotationCloneTypes cloneType = QuoteTools.QuotationCloneTypes.CopyQuote;
			
	        String selectedQuoteType = (sourceQuote<>null) ? sourceQuote.QuoteType__c : null;
	        newQuoteId = QuoteTools.cloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, accountId,selectedQuoteType);
	        
	        if (newQuoteId == null){
	        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An unexpected error occured while creating a copy of the selected Quote.')); return null;
	        }
	        
	        //Redirect to the QuoteEditor with the new QuoteId
	        PageReference pr = Page.QuoteEditor;
	        pr.getParameters().put('id',newQuoteId);
	        pr.setRedirect(true);
	        return pr;
			
		} catch (Exception e) {ApexPages.addMessages(e); return null;}
	}
	
	public PageReference cancel(){
		try {
			pagereference backPage= new PageReference('/apex/quoteeditor?id=' + sourceQuoteId);
	        backPage.setRedirect(true); 
	        return backPage; 
	        
		} catch (Exception e) {ApexPages.addMessages(e); return null;}
	}
	
	public PageReference reviseQuote(){
		
		try{
			if (StringUtils.isNullOrWhiteSpace(this.sourceQuoteId)){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Could not find source QuoteId to continue ReviseQuote action.'));
				return null;
			}
			
			QuoteTools.QuotationCloneTypes cloneType = QuoteTools.QuotationCloneTypes.ReviseQuote;
			
	        try{
	        	newQuoteId = QuoteTools.cloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, null,null);
	        }
	        catch (Exception e) {ApexPages.addMessages(e); return null;}
	        
	        if (newQuoteId == null){
	        	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An unexpected error occured while creating a revised copy of the selected Quote.'));
	        	return null;
	        }
	        
	        //Redirect to the QuoteEditor with the new QuoteId
	        PageReference pr = Page.QuoteEditor;
	        pr.getParameters().put('id',newQuoteId);
	        pr.setRedirect(true);
	        return pr;
		}
	    catch (Exception e) {ApexPages.addMessages(e); return null;}
	}
    public List<SelectOption> getChangeTypeList()
    {
        List<SelectOption> retList = new List<SelectOption>();
        retList.add(new SelectOption(GlobalConstants.QuoteTypes.CustomerQuote.name(),GlobalConstants.QuoteTypes.CustomerQuote.name()));
        retList.add(new SelectOption(GlobalConstants.QuoteTypes.SetRequest.name(),GlobalConstants.QuoteTypes.SetRequest.name()));
        return retList;
    }
	
}