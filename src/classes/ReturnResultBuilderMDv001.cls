public with sharing class ReturnResultBuilderMDv001 {

	
	private MasterDataAPIv001.ReturnResult rr;
	
	public ReturnResultBuilderMDv001(MasterDataAPIv001.ReturnResult rr){
		if (rr == null)
		{
			rr = new MasterDataAPIv001.ReturnResult();
			rr.isSuccessful = true;
		}	
		this.rr = rr;
	}
	
	
	public MasterDataAPIv001.ReturnResult addInformation(String recordId, String message)
	{
		MasterDataAPIv001.Log log = new MasterDataAPIv001.Log();
		log.message = message;
		log.severity = MasterDataAPIv001.SeverityType.Information;
				
		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
		processLog.logs = new List<MasterDataAPIv001.Log>{ log };
		if (!StringUtils.isEmpty(recordId))
			processLog.recordId = recordId;

		if (rr.processLogs == null)
			rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	
	public MasterDataAPIv001.ReturnResult addWarning(String recordId, String message)
	{
		MasterDataAPIv001.Log log = new MasterDataAPIv001.Log();
		log.message = message;
		log.severity = MasterDataAPIv001.SeverityType.Warning;
				
		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
		processLog.logs = new List<MasterDataAPIv001.Log>{ log };
		if (!StringUtils.isEmpty(recordId))
			processLog.recordId = recordId;
			
		if (rr.processLogs == null)
			rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public MasterDataAPIv001.ReturnResult addNullOrEmptyError(String recordId, String objectNameOrAttribute)
	{
		MasterDataAPIv001.Log log = new MasterDataAPIv001.Log();
		log.message = 'value of ' + objectNameOrAttribute + ' is null or empty.';
		log.severity = MasterDataAPIv001.SeverityType.ValidationError;
				
		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
		processLog.logs = new List<MasterDataAPIv001.Log>{ log };
		if (!StringUtils.isEmpty(recordId))
			processLog.recordId = recordId;
			
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public MasterDataAPIv001.ReturnResult addApplicationError(String recordId, string message){
		
		MasterDataAPIv001.Log log = new MasterDataAPIv001.Log();
		log.message = message;
		log.severity = MasterDataAPIv001.SeverityType.ApplicationError;
				
		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
		processLog.logs = new List<MasterDataAPIv001.Log>{ log };
		if (!StringUtils.isEmpty(recordId))
			processLog.recordId = recordId;
		
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public MasterDataAPIv001.ReturnResult addSystemError(String recordId, string message){
		
		MasterDataAPIv001.Log log = new MasterDataAPIv001.Log();
		log.message = message;
		log.severity = MasterDataAPIv001.SeverityType.SystemError;
				
		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
		processLog.logs = new List<MasterDataAPIv001.Log>{ log };
		if (!StringUtils.isEmpty(recordId))
			processLog.recordId = recordId;
		
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public MasterDataAPIv001.ReturnResult addValidationError(String recordId, string message){
		
		MasterDataAPIv001.Log log = new MasterDataAPIv001.Log();
		log.message = message;
		log.severity = MasterDataAPIv001.SeverityType.ValidationError;
				
		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
		processLog.logs = new List<MasterDataAPIv001.Log>{ log };
		if (!StringUtils.isEmpty(recordId))
			processLog.recordId = recordId;
		
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public MasterDataAPIv001.ReturnResult addException(String recordId, Exception e){

		MasterDataAPIv001.ProcessLog processLog  = null;
		
		if (e != null){
			processLog = new MasterDataAPIv001.ProcessLog();
			processLog.logs = new List<MasterDataAPIv001.Log>();
			if (!StringUtils.isEmpty(recordId))
				processLog.recordId = recordId;
				
			MasterDataAPIv001.Log exceptionLog = new MasterDataAPIv001.Log();
			exceptionLog.message = 'Line Number: ' + e.getLineNumber() + ' Error Message: ' + e.getMessage();
			exceptionLog.severity = MasterDataAPIv001.SeverityType.SystemError;
			processLog.logs.add(exceptionLog);
			
			MasterDataAPIv001.Log stackTraceLog = new MasterDataAPIv001.Log();
			stackTraceLog.message = 'Exception TypeName: ' + e.getTypeName() + ' StackTraceString: ' + e.getStackTraceString();
			stackTraceLog.severity = MasterDataAPIv001.SeverityType.SystemError;
			processLog.logs.add(stackTraceLog);
			
		}	

		rr.isSuccessful = false;
		
		if (processLog != null){
			if (rr.processLogs == null)
				rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
		
			rr.processLogs.add(processLog);
		}
		return this.rr;
		
	}
	
	public MasterDataAPIv001.ReturnResult addSaveResults(List<Database.Saveresult> saveResults)
	{
		boolean isSuccessful = true;
		
		List<MasterDataAPIv001.ProcessLog> processLogs = new List<MasterDataAPIv001.ProcessLog>();
		for(Database.Saveresult r :saveResults)
        {
        	if (r.isSuccess() == false)
        	{
        		isSuccessful = false;
        		
        		MasterDataAPIv001.ProcessLog processLog = new MasterDataAPIv001.ProcessLog();
        		processLog.recordId = r.getId();
        		processLog.logs = MasterDataAPIv001Tools.transformSaveResultErrorsIntoListOfApiLogs(r.getErrors());
        		processLogs.add(processLog);
        	}
        }
		
		if (rr.isSuccessful && isSuccessful == false)
			rr.isSuccessful = isSuccessful;	/// need to look into collection
			
		if (!isSuccessful)
			rr.processLogs = processLogs;
		
		return rr;
	}
	
	
}