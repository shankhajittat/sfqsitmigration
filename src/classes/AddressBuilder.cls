public with sharing class AddressBuilder {

	//Mandatory fields with defaults
	private String addressName;
	private Account addressAccount;
	private List<String> addressTypes = new List<String>();
	
	public AddressBuilder name(String addressName)
	{
		this.addressName = addressName;
		return this;
	}
	
	public AddressBuilder account(Account addressAccount)
	{
		this.addressAccount = addressAccount;
		return this;
	}
	
	public AddressBuilder addressType(String addressType)
	{
		addressTypes.add(addressType);
		return this;
	}
	
	public Address__c build(Boolean createInDatabase)
	{
		Address__c address = new Address__c(
			Name = this.addressName,
			Account_Address__c = this.addressAccount.Id
		);
		if (addressTypes.size() > 0){
			address.AddressType__c = StringUtils.joinArray(addressTypes, ';');
		}
		if (createInDatabase)
			insert address;
		
		return address;
	}
	
	
}