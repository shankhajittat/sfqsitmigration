public with sharing class mobileProductSelector {

	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// MObile product stuff
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public String educationMaterialsOrderId = ApexPages.currentPage().getParameters().get('parent');
	public String selectedBU = ApexPages.currentPage().getParameters().get('bu');
	public String cat = ApexPages.currentPage().getParameters().get('cat');
	public String sub = ApexPages.currentPage().getParameters().get('sub');
	public Map<String,String> allProducts = ApexPages.currentPage().getParameters();
	public List<Ed_Materials_List__c> currentProducts = new List<Ed_Materials_List__c>();
	public List<displayList> setList = new List<displayList>();
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public string geteducationMaterialsOrderId() {
		
		return this.educationMaterialsOrderId;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public string getBU() {
		
		return this.selectedBU;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public string getCat() {
		
		return this.cat;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// TESTING
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public String getTest() {
		
		String params = '';
		
		for(String key:allProducts.keySet()){  
			
			if( allProducts.get(key) != '0.0' && allProducts.get(key) != '' &&  allProducts.get(key) != null) {
				
				if(key != 'bu' && key != 'cat' && key != 'sub' && key != 'parent')  {
					
					params += '|| ' + key + ' - ' + allProducts.get(key);	
				}
			}
		}
		
		return params;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Get selected Products for this Education Material
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	 
		public void mobileProductSelectorLoad() {
		
			currentProducts = [Select Item_Description__c, Quantity__c from Ed_Materials_List__c Where Ed_Materials_Request__c = :educationMaterialsOrderId];
			if(sub == 'Save') {
				
				updateList();
			}
		}

	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Switch BU method
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public void updateList() {	
	
		List<Ed_Materials_List__c> mlForUpdate = new List<Ed_Materials_List__c>();
		List<Ed_Materials_List__c> mlForInsert = new List<Ed_Materials_List__c>();
		//List<Ed_Materials_List__c> mlForDelete = new List<Ed_Materials_List__c>();
		
		for(String key:allProducts.keySet()){  
			
			if(allProducts.get(key) != '0.0' && allProducts.get(key) != '' &&  allProducts.get(key) != null) {
				System.Debug('Loop 98: ' + allProducts.get(key));
				
				if(key != 'bu' && key != 'cat' && key != 'sub' && key != 'parent')  {
					System.Debug('Loop 101: ' + key);
					
					
					System.Debug('currentProducts.size(): ' + currentProducts.size());
					
					if(currentProducts.size() > 0) { /// A
						
						Boolean match = false;
						Ed_Materials_List__c allreadySlotted = new Ed_Materials_List__c();
						
						for(Ed_Materials_List__c prodSlotted:currentProducts) { // B
							
							if(prodSlotted.Item_Description__c == key) {  /// C
								
								match = true;
								allreadySlotted = prodSlotted;
							} /// EOF C
						} /// EOF B
							
						if(match) { /// D	
							
						//	try {
								
								allreadySlotted.Quantity__c = decimal.valueOf(allProducts.get(key));
								mlForUpdate.add(allreadySlotted); 
								
						//	} catch (Exception e) { 
								
							//}
							
						} else { /// D
							
							//try {
						
								Ed_Materials_List__c ml = new Ed_Materials_List__c();
								ml.Quantity__c = decimal.valueOf(allProducts.get(key));
								ml.Item_Description__c = key;
								ml.Ed_Materials_Request__c = educationMaterialsOrderId;
								mlForInsert.add(ml);
								
							//} catch (Exception e) {
								
							//}
							
						}	/// EOF D	

					} else { /// A
						
						//try {
							
							System.Debug('Loop 101: ' + key);
							
							Ed_Materials_List__c ml = new Ed_Materials_List__c();
							ml.Quantity__c = decimal.valueOf(allProducts.get(key));
							ml.Item_Description__c = key;
							ml.Ed_Materials_Request__c = educationMaterialsOrderId;
							mlForInsert.add(ml);
								
						//} catch (Exception e) {
								
						//}	
					} /// EOF A
				}
			}
		}
		
		for(displayList thisDL:setList) {
			
			if(thisDL.materialList.Id != null && thisDL.qty  == 0) {

				//mlForDelete.add(thisDL.materialList);

			}	
		}

		System.debug('mlForUpdate.size(): ' + mlForUpdate.size());
		System.debug('mlForInsert.size(): ' + mlForInsert.size());
		//System.debug('mlForDelete.size(): ' + mlForDelete.size());
		
		if(mlForUpdate.size() >0 ) {update mlForUpdate;}
		if(mlForInsert.size() >0 ) {insert mlForInsert;}
		//if(mlForDelete.size() >0 ) {delete mlForDelete;}
			
		currentProducts = [Select Item_Description__c, Quantity__c from Ed_Materials_List__c Where Ed_Materials_Request__c = :educationMaterialsOrderId];
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Get Sets
	//
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<displayList> getSets() {
		
		setList.clear();
		
		String buQuery = '%' + selectedBU + '%';
		String catQuery = '%' + cat + '%';
		List<Ed_Materials_Products__c> productList;
		
		if(selectedBU != null) {
			
			productList = [Select Name, Id From Ed_Materials_Products__c Where BU_2__c Like :buQuery And Category__c Like :catQuery And Freq_Used__c = true Order By Name Asc];
	
			
			for(Ed_Materials_Products__c thisProd:productList) {
				
				displayList thisSetList  = new displayList(thisProd,currentProducts);
				setList.add(thisSetList);
			}
		}
		
		return setList;
	}
	
	public void setSets(List<displayList> sets) {
		
		this.setList = sets;
	}
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Helper class
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public class displayList {
		
		public boolean slotted = false;
		public Integer qty = null;
		public Ed_Materials_Products__c prod = new Ed_Materials_Products__c();
		public Ed_Materials_List__c materialList = new Ed_Materials_List__c();
		
		public displayList(Ed_Materials_Products__c productToDisplay, List<Ed_Materials_List__c> currentProducts) {
			
			this.prod = productToDisplay;
			
			if(currentProducts.size() > 0) {
				
				for(Ed_Materials_List__c thisProd:currentProducts) {
				
					if(productToDisplay.Id == thisProd.Item_Description__c)	{
						
						this.slotted = true;
						this.materialList = thisProd;
						this.qty = materialList.Quantity__c.intValue();
					}
				}
			} 
		}
		
		public Integer getqty() {
			
			return this.qty;
		}
		public Void setqty(Integer qty) {
			
			this.qty = qty;
		}

		
		public boolean getSlotted() {
			
			return this.slotted;
		}
		
		public Ed_Materials_List__c getMaterialList() {
			
			return this.materialList;
		}
		
		public Ed_Materials_Products__c getProduct() {
			
			return this.prod;
		}
	}
	

	
}