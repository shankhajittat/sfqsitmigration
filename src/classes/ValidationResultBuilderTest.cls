@isTest(SeeAllData=true)
private class ValidationResultBuilderTest {

   static testMethod void addNullOrEmptyErrorTest() {
        APIv001.ValidationResult rr = null;
        
        ValidationResultBuilder vrBuilder = new ValidationResultBuilder(rr);
        rr = vrBuilder.addNullOrEmptyError('TestObject');
        
        System.debug('ValidationResult>>>' + rr);
        
        System.assert(rr != null && rr.isValid == false && rr.processLogs.size() > 0, 'Was expecting a valid ValidationResult object');
        
        
    }
    
    static testMethod void addApplicationErrorTest() {
        APIv001.ValidationResult rr = null;
        
        ValidationResultBuilder vrBuilder = new ValidationResultBuilder(rr);
        rr = vrBuilder.addApplicationError('Cannot create QuoteLine without a valid QuoteGroup');
        
        System.debug('ValidationResult>>>' + rr);
        
        System.assert(rr != null && rr.isValid == false && rr.processLogs.size() > 0, 'Was expecting a valid ValidationResult object');
    }
    
     static testMethod void addSystemErrorTest() {
        APIv001.ValidationResult rr = null;
        
        ValidationResultBuilder vrBuilder = new ValidationResultBuilder(rr);
        rr = vrBuilder.addSystemError('An unexpected error occured while saving Quote');
        
        System.debug('ValidationResult>>>' + rr);
        
        System.assert(rr != null && rr.isValid == false && rr.processLogs.size() > 0, 'Was expecting a valid ValidationResult object');
    }
    
     static testMethod void addValidationErrorTest() {
        APIv001.ValidationResult rr = null;
        
        ValidationResultBuilder vrBuilder = new ValidationResultBuilder(rr);
        rr = vrBuilder.addValidationError('Selected Account is on hold.');
        
        System.debug('ValidationResult>>>' + rr);
        
        System.assert(rr != null && rr.isValid == false && rr.processLogs.size() > 0, 'Was expecting a valid ValidationResult object');
    }
    
    static testMethod void addExceptionTest() {
        APIv001.ValidationResult rr = null;
        
        ValidationResultBuilder vrBuilder = new ValidationResultBuilder(rr);
        ApplicationException e = new ApplicationException('Exception Test');
        rr = vrBuilder.addException(e);
        
        System.debug('ValidationResult>>>' + rr);
        
        System.assert(rr != null && rr.isValid == false && rr.processLogs.size() > 0, 'Was expecting a valid ValidationResult object');
    }
    
   
}