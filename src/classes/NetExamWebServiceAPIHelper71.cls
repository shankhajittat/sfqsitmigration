global class NetExamWebServiceAPIHelper71
{

    public class MissingAccountInformationException extends Exception{}
    public class NetExamWebserviceException extends Exception{}

    
    //NetExam API Credentials    
    private static string ne_apiKey = 'y8b8BpkasUEKDIS234'; //'APIKey';
    private static string ne_apiUser = 'testing@netexam.com';//'APIUsername';
    private static string ne_apiPass = 'y7efhuh84t';//'APIPassword'; 
     

    Webservice static string SendContactToNetExam(string sfdcContactID, string firstName, string lastName, string email, string language, string userType, string userRole, string managerSFDCContactID, string accountName, string accountID, string accountCountry)
    {
                   
        string errorMsg = '';      
        
        if(accountID.length() == 0){
            return 'Error, no account information was associated with this contact: ' + errorMsg ;
        }
        

        //Check to make sure the required fields for the account object have the required information
        errorMsg = ValidateAccountData(accountName, accountID, accountCountry);        
        if(errorMsg != 'Success'){
            return 'Contact was not sent. ' + errorMsg;
        }
        
        //Check to make sure the required fields for the account object have the required information
        errorMsg = ValidateContactData(sfdcContactID, firstName, lastName, email, language, userType, userRole, managerSFDCContactID);
        if(errorMsg != 'Success'){
            return 'Contact was not sent. ' + errorMsg;
        }
             
        
        // Ensure that we have a valid country by setting a default
        integer accountCountryLength;
        accountCountryLength = accountCountry.length();
        if(accountCountryLength == 0){
            accountCountry = 'USA';
        }

        // Ensure that we have a valid language by setting a default
        integer languageLength;
        languageLength = language.length(); 
        if(languageLength == 0){
            language = 'English';
        } 
        
        Boolean emailOptIn = true;
        String sfdcUserId = '';
        
        //Try and pull the User Id from the User record if it exists
        List<User> objUser = [Select Id FROM User WHERE ContactId = :sfdcContactID LIMIT 1];        
        try {
                sfdcUserId = objUser[0].Id;
        }
        catch(Exception e){
            //Do Nothing with this exception
            System.debug('Error Finding User: ' + e);             
        } 
          
        //List<SelectOption> testOptions = getPicklistValues(new Account(Name = accountName), 'Industry');
        //system.assert(testOptions.size() > 0);        
                   

        webservicesNetexamComUserservice71.ArrayOfCustomField CustomFields = new webservicesNetexamComUserservice71.ArrayOfCustomField();
        CustomFields.CustomField=  new webservicesNetexamComUserservice71.CustomField[]{};
        
        // Add account as primary user group
        webservicesNetexamComUserservice71.UserUserGroup PrimaryGroup = new webservicesNetexamComUserservice71.UserUserGroup();
        PrimaryGroup.Name = accountName;
        PrimaryGroup.ExternalID = accountID;
        PrimaryGroup.IsPrimaryUserGroup = 1;
        PrimaryGroup.Country = accountCountry;
        
        
        // Create the array of user groups to send to NetExam. Only the PrimaryGroup is required.
        webservicesNetexamComUserservice71.ArrayOfUserUserGroup Groups = new webservicesNetexamComUserservice71.ArrayOfUserUserGroup();
        Groups.UserUserGroup =  new webservicesNetexamComUserservice71.UserUserGroup[]{};

        Groups.UserUserGroup.add(PrimaryGroup);      
                    
        // Send the Contact to NetExam
        errorMsg = UpsertUserWithUserGroups(ne_apiKey, ne_apiUser, ne_apiPass, sfdcContactID, firstName, lastName, email, language, userType, userRole, managerSFDCContactID, accountID, accountName, Groups, CustomFields,accountCountry,emailOptIn,sfdcUserId);
        if(errorMsg != 'Success'){
            return 'Contact was not sent. ' + errorMsg;
        }
        
        return 'Success';    
    
    }
    
    Public static string SendUserFromTrigger(string NEUserId, string strFName, string strLName, string strEmail, boolean isActive,string strLanguageLocaleKey  )
    {
        string ne_result = '';
        try{
        //ne_apiKey, ne_apiUser, ne_apiPass
        // Send the User to NetExam
            webservicesNetexamComUserservice71.UserServiceSoap ne_Webservice = new webservicesNetexamComUserservice71.UserServiceSoap();
           // webservicesNetexamComUserservice8.ApiResult Result = ne_Webservice.UpsertUserByUserTrigger(ne_apiKey,ne_apiUser,ne_apiPass ,NEUserId,strFName,strLName, strEmail,isActive, strLanguageLocaleKey);
           ne_result = ne_Webservice.UpsertUserByUserTrigger(ne_apiKey,ne_apiUser,ne_apiPass ,NEUserId,strFName,strLName, strEmail,isActive, strLanguageLocaleKey);
           // System.debug('Result: ' + Result.Code + ':' + Result.Description);
           
            ne_result = 'Success';
        
            
        }
        catch(Exception e){
            System.debug('Error: ' + e);
            ne_result =  e.getMessage(); 
        }
        
        return ne_result;
    }

    @Future(callout=true)
    public static void SendContactFromTrigger(string sfdcContactID, string firstName, string lastName, string email,string accountID)
    {       
        String accountName = '';
        String country = '';
        String language = 'English';
        String userType = 'Employee';
        String userRole = 'Student';
        String managerSFDCContactID = ''; 

                
        //Depending on your account setup, you may need to alter where the country is pulled from. If no country is available for the account, you can pull it from another relationship or default it.
        //Common country fields: Account.BillingCountry, Account.ShippingCountry. If you change the email here, also change the variable name below at the country assignment
        List<Account> objAccount = [Select BillingCountry, Name FROM Account WHERE Id = :accountID LIMIT 1];        
        if(accountID != 'test')
        {
            try {
                accountName = objAccount[0].Name;
            }
            catch(Exception e){
                throw new MissingAccountInformationException('Account Could Not Be Found'); 
            }
        }
            
        //if we survived, we must be able to get account information, lets try the optional country field.
        try{
          country = objAccount[0].BillingCountry; //If you changed the query above, be sure to change the name of the variable here
        }
        catch(Exception e){
          // No real problem here, we must not have permissions to the billing fields, default to USA
        }      
          
        if(country == null) {country = 'USA';} 
        
        String returnValue = SendContactToNetExam(
            sfdcContactID, 
            firstName, 
            lastName, 
            email, 
            language, 
            userType, 
            userRole, 
            managerSFDCContactID, 
            accountName, 
            accountID, 
            country
        );
            
        if( returnValue != 'Success' && accountID != 'test')            
        {
            throw new MissingAccountInformationException(returnValue); 
        }
        
        return;
    }    
    
    //This method will create a user in NetExam OR if the user already exists(checked by sfdcContactID) the user's information will be updated.
    public static string UpsertUserWithUserGroups(string apiKey, string apiUser, string apiPass, string sfdcContactID, string firstName, string lastName, string email, string language, string userType, string userRole, string managerSFDCContactID, string accountID, string accountName,  webservicesNetexamComUserservice71.ArrayOfUserUserGroup Groups, webservicesNetexamComUserservice71.ArrayOfCustomField CustomFields,string country,Boolean EmailOptIn,string SFDCUserID)
    {
        string ne_result = '';
       //INVOKE WEBSERVICE CALL TO ADD/UPDATE USER 
       try{
           webservicesNetexamComUserservice71.UserServiceSoap ne_Webservice = new webservicesNetexamComUserservice71.UserServiceSoap();
           webservicesNetexamComUserservice71.ApiResult Result = ne_Webservice.UpsertUserWithUserGroups(apiKey,apiUser,apiPass,sfdcContactID,firstName,lastName, email, language, userType, userRole, managerSFDCContactID, Groups, true, CustomFields, true,country,EmailOptIn,SFDCUserID);
           System.debug('Result: ' + Result.Code + ':' + Result.Description);
           ne_result = 'Success';
        }
        catch(System.CalloutException e){
            System.debug('Error: ' + e);            
            ne_result = e.getMessage().replace('Web service callout failed: WebService returned a SOAP Fault: ','').replace('faultcode=soap:Client faultactor=','');          
        }
        return ne_result;  
        
    }
    
    //This method will create a user group in Netexam OR if the group already exists(checked by AccountID) the group information will be updated.
    public static string AddEditUserGroup(string apiKey, string apiUser, string apiPass, string AccountName, string AccountID, string AccountCountry)
    {
        string ne_result = '';      
        try{
            webservicesNetexamComUserservice71.UserServiceSoap ne_Webservice = new webservicesNetexamComUserservice71.UserServiceSoap();
            string Result = ne_Webservice.AddEditUserGroup(apiKey , apiUser , apiPass , AccountName, AccountID, AccountCountry); 
            System.debug('Result: ' + Result);
            ne_result = 'Success';
        }
        catch(System.CalloutException e){
            System.debug('Error: ' + e);
            ne_result = e.getMessage();          
        }
        return ne_result;
    
    }

     //This method will Bulk upsert users in NetExam OR if the user already exists(checked by sfdcContactID) the user's information will be updated.
    //@Future(callout=true)
    public static void BulkUpsertUsers(string apiKey, string apiUser, string apiPass, webservicesNetexamComUserservice71.ArrayOfUser Users)
    {
        string ne_TransactionID = '0';

       //INVOKE WEBSERVICE CALL TO BulkUpsert Users 
       try{
           webservicesNetexamComUserservice71.UserServiceSoap ne_Webservice = new webservicesNetexamComUserservice71.UserServiceSoap();
           //webservicesNetexamComUserservice71.ApiResult Result = ne_Webservice.BulkUpsertUsers(apiKey,apiUser,apiPass,Users);
          
           //Get the Transaction ID. You can use the Transaction ID In the GetBulkUpsertTransactionLog method to get the results of your BulkUpsertUsers call 
           //ne_TransactionID = string.valueof(Result.Code);
        }
        catch(Exception e){
            System.debug('Error: ' + e);            
        }
        return; //ne_TransactionID;        
    }
    
    //This method simply checks to make sure that there is enough information to create the user group. 
    // If these values are missing, the webservice would return an error.
    // NOTE: Not all params are required, so not all params are checked.
    public static string ValidateAccountData(string accountName, string accountID, string accountCountry)
    {
        string errorMsg = '';
        integer accountIDLength;
        integer accountNameLength;

        accountIDLength = accountID.length();
        //Validate required fields
        if(accountIDLength == 0){
            errorMsg = errorMsg + '<br>Account ID';
        }
        
        accountNameLength = accountName.length();         
        if(accountNameLength == 0){
            errorMsg  = errorMsg + '<br>Account Name';
        }
        
        if(errorMsg.length() > 0){
            errorMsg = 'Missing required information: ' + errorMsg;
        }
        else{
            errorMsg = 'Success';
        }

        return errorMsg;
    }
    
    //This method simply checks to make sure that there is enough information to create the user group. 
    // If these values are missing, the webservice would return an error.
    // NOTE: Not all params are required, so not all params are checked.
    public static string ValidateContactData(string sfdcContactID, string firstName, string lastName, string email, string language, string userType, string userRole, string managerSFDCContactID)
    {
        string errorMsg = '';
        integer firstNameLength;
        integer lastNameLength;
        integer emailLength;
        integer userTypeLength;
        integer userRoleLength;
        
        firstNameLength = firstName.length();
        if(firstNameLength == 0){
            errorMsg  = errorMsg + '<br>First Name';
        }
        
        lastNameLength = lastName.length();
        if(lastNameLength == 0){
            errorMsg = errorMsg + '<br>Last Name';
        }
        
        emailLength = email.length();
        if(emailLength == 0){
            errorMsg = errorMsg + '<br>Email';
        }   

        userTypeLength = userType.length(); 
        if(userTypeLength == 0){
            errorMsg = errorMsg + '<br>User Type';
        }
 
        userRoleLength = userRole.length(); 
        if(userRoleLength == 0){
            errorMsg = errorMsg + '<br>User Role';
        }
        
        if(errorMsg.length() > 0){
            errorMsg = 'Missing required information: ' + errorMsg;
        }
        else{
            errorMsg = 'Success';
        }

        return errorMsg;
    
    }   
   
 

}