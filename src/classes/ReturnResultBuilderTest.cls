@isTest(SeeAllData=true)
private class ReturnResultBuilderTest {

    static testMethod void addNullOrEmptyErrorTest() {
        APIv001.ReturnResult rr = null;
        
        ReturnResultBuilder rrBuilder = new ReturnResultBuilder(rr);
        rr = rrBuilder.addNullOrEmptyError('','TestObject');
        
        System.debug('ReturnResult>>>' + rr);
        
        System.assert(rr != null && rr.isSuccessful == false && rr.processLogs.size() > 0, 'Was expecting a valid ReturnResult object');

    }
    
    static testMethod void addApplicationErrorTest() {
        APIv001.ReturnResult rr = null;
        
        ReturnResultBuilder rrBuilder = new ReturnResultBuilder(rr);
        rr = rrBuilder.addApplicationError('','Cannot create QuoteLine without a valid QuoteGroup');
        
        System.debug('ReturnResult>>>' + rr);
        
        System.assert(rr != null && rr.isSuccessful == false && rr.processLogs.size() > 0, 'Was expecting a valid ReturnResult object');
    }
    
     static testMethod void addSystemErrorTest() {
        APIv001.ReturnResult rr = null;
        
        ReturnResultBuilder rrBuilder = new ReturnResultBuilder(rr);
        rr = rrBuilder.addSystemError('','An unexpected error occured while saving Quote');
        
        System.debug('ReturnResult>>>' + rr);
        
        System.assert(rr != null && rr.isSuccessful == false && rr.processLogs.size() > 0, 'Was expecting a valid ReturnResult object');
    }
    
     static testMethod void addValidationErrorTest() {
        APIv001.ReturnResult rr = null;
        
        ReturnResultBuilder rrBuilder = new ReturnResultBuilder(rr);
        rr = rrBuilder.addValidationError('A10001','Selected Account is on hold.');
        
        System.debug('ReturnResult>>>' + rr);
        
        System.assert(rr != null && rr.isSuccessful == false && rr.processLogs.size() > 0, 'Was expecting a valid ReturnResult object');
    }
    
    static testMethod void addExceptionTest() {
        APIv001.ReturnResult rr = null;
        
        ReturnResultBuilder rrBuilder = new ReturnResultBuilder(rr);
        ApplicationException e = new ApplicationException('Exception Test');
        rr = rrBuilder.addException('A10001',e);
        
        System.debug('ReturnResult>>>' + rr);
        
        System.assert(rr != null && rr.isSuccessful == false && rr.processLogs.size() > 0, 'Was expecting a valid ReturnResult object');
    }
    
    static testMethod void addSaveResultsTest() {
        APIv001.ReturnResult rr = null;
        
        ReturnResultBuilder rrBuilder = new ReturnResultBuilder(rr);
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010').build(false);
        Database.Saveresult saveResult = Database.insert(vinnies);
        
        
        rr = rrBuilder.addSaveResults(new List<Database.Saveresult> { saveResult });
        
        System.debug('ReturnResult>>>' + rr);
        
        System.assert(rr != null && rr.isSuccessful == true, 'Was expecting a valid ReturnResult object');
    }
    
}