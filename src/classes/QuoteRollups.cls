public without sharing class QuoteRollups {

	private List<Quote_Line_Group__c> quoteLineGroups = null;
	public Set<Id> quoteIds = null;
	public Map<Id, Quote> quotes = null;
	public Map<Id, Quote> rollupQuotes = null;
	public List<Quote> quotesToUpdate = null;
	
	public QuoteRollups(List<Quote_Line_Group__c> quoteLineGroups)
	{
		this.quoteLineGroups = quoteLineGroups;
	}

	public void buildUniqueQuoteIds()
	{
		if (quoteLineGroups == null || quoteLineGroups.isEmpty())
			return;
		
		//Get unique quoteIds Ids
		quoteIds = new Set<Id>();
		for(Quote_Line_Group__c groupItem : quoteLineGroups){
			quoteIds.add(groupItem.QuoteId__c);
		}
	}

	public void buildQuotes()
	{
		if (quoteIds == null || quoteIds.isEmpty())
			return;
			
		//Get Map of Quotes
		quotes = new Map<Id, Quote>();
		
		for(List<Quote> listOfQuotes: [SELECT 	Id,
												Tax
				FROM Quote WHERE Id IN :quoteIds ]){
			for(Quote quote : listOfQuotes){
				quotes.put(quote.Id,quote);
			}
		}
	}
	
	public void buildAggregateRollups()
	{
		if (quotes == null || quotes.isEmpty())
			return;
		
		//Aggregate Rollups
		rollupQuotes = new Map<Id, Quote>();
		
		for(AggregateResult[] aggreResults : [SELECT QuoteId__c, 
												SUM(LineGroup_Total_Tax__c) RollupTotalTax,
												MIN(LineGroup_Status__c) RollupLineGroupStatus,
												SUM(LineGroup_TotalPrice_ImpMaterialGroup__c) RollupTtlPriceImpMatGrp,
												SUM(LineGroup_TotalPrice_InsMaterialGroup__c) RollupTtlPriceInsMatGrp,
												SUM(LineGroup_TotalPrice_OtherMaterialGroup__c) RollupTtlPriceOtherMatGrp,
												SUM(LineGroup_TotalStdCost_ImpMaterialGroup__c) RollupTtlCostImpMatGrp,
												SUM(LineGroup_TotalStdCost_InsMaterialGroup__c) RollupTtlCostInsMatGrp,
												SUM(LineGroup_TotalStdCost_OtherMaterialGrou__c) RollupTtlCostOtherMatGrp
											FROM Quote_Line_Group__c WHERE QuoteId__c IN :quoteIds 
											GROUP BY QuoteId__c]){
			for(AggregateResult aggreResult : aggreResults)
			{
				Id quoteId = (Id) aggreResult.get('QuoteId__c');
				Quote tempQuote = new Quote();
				
				tempQuote.Tax = (double)aggreResult.get('RollupTotalTax');
				tempQuote.TotalPrice_ImpMaterialGroup__c = (double)aggreResult.get('RollupTtlPriceImpMatGrp');
				tempQuote.TotalPrice_InsMaterialGroup__c = (double)aggreResult.get('RollupTtlPriceInsMatGrp');
				tempQuote.TotalPrice_OtherMaterialGroup__c = (double)aggreResult.get('RollupTtlPriceOtherMatGrp');
				tempQuote.TotalStdCost_ImpMaterialGroup__c = (double)aggreResult.get('RollupTtlCostImpMatGrp');
				tempQuote.TotalStdCost_InsMaterialGroup__c = (double)aggreResult.get('RollupTtlCostInsMatGrp');
				tempQuote.TotalStdCost_OtherMaterialGroup__c = (double)aggreResult.get('RollupTtlCostOtherMatGrp');
				
				System.debug('RollupLineGroupStatus>>>>>:' + aggreResult.get('RollupLineGroupStatus'));
				
				Object aggregateLineGroupStatus = aggreResult.get('RollupLineGroupStatus');
				
				if (aggregateLineGroupStatus == 'New' || aggregateLineGroupStatus == 'Submitted for Pricing' 
						|| aggregateLineGroupStatus == 'Invalid' || aggregateLineGroupStatus == 'Revalidate')
					tempQuote.Status = GlobalConstants.QS_STATUS_DRAFT;
				else if (aggregateLineGroupStatus == 'Validated')
					tempQuote.Status = GlobalConstants.QS_STATUS_PRICE_CONFIRMED;
				
				rollupQuotes.put(quoteId,tempQuote);
			}
		}
	}
	
	public void buildQuotesToUpdate()
	{
		//if no more QuoteLineItems then buildAggregateRollups method won't generate any AggregateRollups, 
		//still it needs to update with 0s.

		quotesToUpdate = new List<Quote>();
		
		for(Id quoteId : quotes.keySet()){
			Quote quote = quotes.get(quoteId);
			
			Quote rollupQuote = null;
			if (rollupQuotes.containsKey(quoteId))
			{
				rollupQuote = rollupQuotes.get(quoteId);
			}
			
			if (rollupQuote != null)
			{
				quote.Tax = rollupQuote.Tax;
				quote.TotalPrice_ImpMaterialGroup__c = rollupQuote.TotalPrice_ImpMaterialGroup__c;
				quote.TotalPrice_InsMaterialGroup__c = rollupQuote.TotalPrice_InsMaterialGroup__c;
				quote.TotalPrice_OtherMaterialGroup__c = rollupQuote.TotalPrice_OtherMaterialGroup__c;
				
				quote.TotalStdCost_ImpMaterialGroup__c = rollupQuote.TotalStdCost_ImpMaterialGroup__c;
				quote.TotalStdCost_InsMaterialGroup__c = rollupQuote.TotalStdCost_InsMaterialGroup__c;
				quote.TotalStdCost_OtherMaterialGroup__c = rollupQuote.TotalStdCost_OtherMaterialGroup__c;
				
				//system.debug('<<<<Updating Quote Status From rollupQuote as>>>>>: ' + rollupQuote.Status);
				quote.Status = rollupQuote.Status;
				quotesToUpdate.add(quote);
			}
			else
			{
				//default with 0. no more QuoteLineItems
				quote.Tax = 0.00;
				quote.TotalPrice_ImpMaterialGroup__c = 0.00;
				quote.TotalPrice_InsMaterialGroup__c = 0.00;
				quote.TotalPrice_OtherMaterialGroup__c = 0.00;
				quote.TotalStdCost_ImpMaterialGroup__c = 0.00;
				quote.TotalStdCost_InsMaterialGroup__c = 0.00;
				quote.TotalStdCost_OtherMaterialGroup__c = 0.00;
			    quote.Status = GlobalConstants.QS_STATUS_DRAFT;
				quotesToUpdate.add(quote);
			}
			
		}
	}
	
	public boolean updateRollups()
	{
		
		//Update Quote
		if (quotesToUpdate != null && quotesToUpdate.isEmpty() == false)
			update quotesToUpdate;
		
		return true;
	}
	
	public boolean rollupQuoteLineGroups()
	{
		buildUniqueQuoteIds();
		//System.assert(quoteIds != null && quoteIds.isEmpty() == false,'An error occured while execuing buildUniqueQuoteIds method');
		
		buildQuotes();
		//System.assert(quotes != null && quotes.isEmpty() == false,'An error occured while execuing buildQuotes method');
		
		buildAggregateRollups(); // if no more QuoteLineItems then this method won't generate any AggregateRollups, still it needs to update with 0s.
		
		buildQuotesToUpdate();
		//System.assert(quotesToUpdate != null && quotesToUpdate.isEmpty() == false,'An error occured while execuing buildQuotesToUpdate method');
		
		return updateRollups();
	}
	

}