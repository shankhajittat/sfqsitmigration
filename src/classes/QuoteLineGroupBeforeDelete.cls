public without sharing class QuoteLineGroupBeforeDelete {
	
	public List<Quote_Line_Group__c> quoteLineGroups = null;
	public Map<Id, Quote_Line_Group__c> quoteLineGroupsOldMap = null;
	public Map<Id, QuoteLineItem> itemsToBeDeleted = null;
	public List<Id> deleteItems = null;
	public Database.DeleteResult[] drDeletes;
	
	public QuoteLineGroupBeforeDelete(List<Quote_Line_Group__c> quoteLineGroups, Map<Id, Quote_Line_Group__c> quoteLineGroupsOldMap){
		this.quoteLineGroups = quoteLineGroups;
		this.quoteLineGroupsOldMap = quoteLineGroupsOldMap;
	}
	
	public void listItemsBelongingToGroup() {
		itemsToBeDeleted = new Map<Id, QuoteLineItem>();
		deleteItems = new List<Id> ();
		
		for (QuoteLineItem oneItem : [SELECT Quote_Line_Group__c FROM QuoteLineItem 
											WHERE Quote_Line_Group__c IN :quoteLineGroupsOldMap.keySet()]) {
	                      				 //WHERE Quote_Line_Group__c IN :Trigger.oldMap.keySet()]) {  changes this so that i could write a test for it
	                      				 
	        itemsToBeDeleted.put(oneItem.Id, oneItem);
	        deleteItems.Add(oneItem.Id);
	    }
	}
	
	public Boolean deleteItems() {
		Boolean isSuccess = true;
		
		if (deleteItems != null ) {
			drDeletes = Database.delete(deleteItems, false);
			
			for(Database.DeleteResult r :drDeletes) {
				QuoteLineItem qli = null;
	        	if (r.isSuccess() == false) {
	        		isSuccess = false;
	        		Id itemId = r.getId();    // Get item that delete failed on
	        		// Find the item in the map in order to get the group id - we need to add the errors to the group
	        		if (itemId != null && itemsToBeDeleted.containsKey(itemId)) {
	        			qli = itemsToBeDeleted.get(itemId);
	        		}
	        		// Add database errors to the group being deleted
	        		if (qli != null && qli.Quote_Line_Group__c != null) {
		        		for(Database.Error err : r.getErrors()) {
				            System.debug('>>>>>>>>>>>>>>The following error has occurred.' + err.getStatusCode() + ': ' + err.getMessage()); 
				            quoteLineGroupsOldMap.get(qli.Quote_Line_Group__c).addError('Error encountered while deleting Quote Line Items from Quote Line Group: '  + err.getStatusCode() + ': ' + err.getMessage());
				            //Trigger.oldMap.get(qli.Quote_Line_Group__c).addError('Error encountered while deleting Quote Line Items from Quote Line Group: '  + err.getStatusCode() + ': ' + err.getMessage());
				        }
	        		}
	        	} else {system.debug('>>>>>>>>> Delete of itemsuccessful');}
			}
		}	
		
		return isSuccess;
	}	
	
	public Boolean run() {
		listItemsBelongingToGroup();
		return deleteItems();
	}
}