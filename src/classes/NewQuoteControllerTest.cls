@isTest(SeeAllData=true)
private class NewQuoteControllerTest {

	public List<SelectOption> TransTypes {get; set;}
	
	static testMethod void NewQuoteControllerTestURLParameters() {
		// This test checks that everything is valid in the constructor 
		// This test checks the chooseAccount method
		
		// Create test transaction type
		//Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder().name('CBCTest').build(true);
		
		// Create Account
		Account vinnies = new AccountBuilder().name('St Vincents').build(true);	
		system.assertNotEquals(null, vinnies.Id, 'A problem occurred while creating the account');								
        							
        							
       
        
         //Test constructor 
		PageReference pageRef0 = Page.Quote_Account_Picker;
		pageRef0.getParameters().put('accId', vinnies.Id);
		pageRef0.getParameters().put('quoteType', null);
        Test.setCurrentPage(pageRef0);
        NewQuoteController testQuoteControllerFailQuoteTypeCondition1 = new  NewQuoteController();
        System.assertEquals(GlobalConstants.QuoteTypes.None, testQuoteControllerFailQuoteTypeCondition1.urlQuoteType, 'The constructor should have set the urlQuoteType to None (3)');
        //System.assertNotEquals(null, testQuoteControllerFailQuoteTypeCondition1.transactionTypes, 'Transaction Types list should not be null');
        
         // Test constructor - testing the catch when it tries to get the QuoteType parameter - should throw an error
		PageReference pRef = Page.Quote_Account_Picker;
		pRef.getParameters().put('accId', vinnies.Id);
		pRef.getParameters().put('quoteType', 'ABCDEF');
        Test.setCurrentPage(pRef);
        NewQuoteController testQuoteControllerFailAccountCondition2 = new  NewQuoteController();
        System.assertEquals(true, testQuoteControllerFailAccountCondition2.isError, 'The constructor should have thrown an error because quote type should be an integer');
        //System.assertEquals(null, testQuoteControllerFailAccountCondition2.transactionTypes, 'Transaction Types list be null');
        
        // Test constructor - should throw an error because valid quote types are 0,1,2
		PageReference pRef1 = Page.Quote_Account_Picker;
		pRef1.getParameters().put('accId', vinnies.Id);
		pRef1.getParameters().put('quoteType', String.valueOf(GlobalConstants.QuoteTypes.None.ordinal()));
        Test.setCurrentPage(pRef1);
        NewQuoteController testQuoteControllerFailAccountCondition3 = new  NewQuoteController();
        System.assertEquals(GlobalConstants.QuoteTypes.None, testQuoteControllerFailAccountCondition3.urlQuoteType, 'The constructor should have set the urlQuoteType to None');
        //System.assertNotEquals(null, testQuoteControllerFailAccountCondition3.transactionTypes, 'Transaction Types list should not be null');
        
        // Test Account Picker page instantiation with quote type 0 (Set buy)  which should redirect to QuoteType page
		PageReference pageRef1 = Page.Quote_Account_Picker;
		pageRef1.getParameters().put('accId', vinnies.Id);
		pageRef1.getParameters().put('quoteType', '0');
        Test.setCurrentPage(pageRef1);
        
        // Test constructor - it should set up transaction types
        NewQuoteController testQuoteController = new  NewQuoteController();
        System.assertEquals(GlobalConstants.QuoteTypes.CustomerQuote, testQuoteController.urlQuoteType, 'The constructor should have set the urlQuoteType to SetBuy (0)');
        //System.assertNotEquals(null, testQuoteController.transactionTypes, 'Transaction Types list should not be null');
        System.assertEquals(null, testQuoteController.sbPartnerAddresses, 'SB Partners list should be null because we have not chosen quote type yet');
        
        // Test constructor - the getter method for Quote Transaction Types
		List<SelectOption> quoteTypes = testQuoteController.getTransTypes();
        System.assertNotEquals(null, quoteTypes, 'Quote types list should have been created');
        /*
        // Check that transaction type CBCTest created above is set up in the list 
        Boolean foundTransType = false;
        for (SelectOption onetype: quoteTypes) {
        	if (onetype.getLabel() == 'CBCTest' && onetype.getValue() == quoteTransactionType.Id) {
        		foundTransType = true;
        	}
        }
    	System.assertEquals(true, foundTransType ,'Quote type of CBCTest should have been found in the list');
    	*/
    	
    	// Test chooseAccount method 
    	PageReference urlAfterchooseAccount = testQuoteController.chooseAccount();
    	System.assertNotEquals(null, urlAfterchooseAccount, 'a new url is generated');
    	System.assertEquals('/apex/QuoteType', urlAfterchooseAccount.getUrl(),'new url should redirect to VF page to choose quote transaction type');
    	
		// Test Account Picker page instantiation with a quote type 1 (CBC)  - should redirect from quoteaccountpicker page to a page that allows you to pick sb partner
		PageReference pageRef2 = Page.Quote_Account_Picker;
		pageRef2.getParameters().put('accId', vinnies.Id);
		pageRef2.getParameters().put('quoteType', '1');
        Test.setCurrentPage(pageRef2);
        NewQuoteController testQuoteController2 = new  NewQuoteController();
    	urlAfterchooseAccount = testQuoteController2.chooseAccount();
    	System.assertNotEquals(null, urlAfterchooseAccount, 'a new url is generated');
    	System.assertEquals('/apex/QuoteSBPartner', urlAfterchooseAccount.getUrl(),'new url should redirect to VF page to choose SB Partner');
    	
    	
    	 // Test Account Picker page instantiation with accId null (not put accId parameter) which should throw an error
		PageReference pageRef = Page.Quote_Account_Picker;
		pageRef.getParameters().put('quoteType', '0');
        Test.setCurrentPage(pageRef);
        NewQuoteController testQuoteControllerFailAccountCondition = new  NewQuoteController();
        urlAfterchooseAccount = testQuoteControllerFailAccountCondition.chooseAccount();
    	System.assertEquals(true, testQuoteControllerFailAccountCondition.isError, 'An error should have been thrown because account id is null');
    	System.assertEquals(null, urlAfterchooseAccount, 'The url should not have been generated - it should be null ');
    }
    
       
    
    
    
	static testMethod void NewQuoteControllerTestQuoteTypePage() {
    	
		// Create test transaction type
		/*
		Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('CBC')
        												.build(true);
        												// Create test transaction type
		Quote_Transaction_Type__c quoteTransactionTypeSP = new QuoteTransactionTypeBuilder()
        												.name('SP')
        												.build(true);
        */

        /*
        // Create custom setting mapping the above transaction type to the globalconstants enum QuoteTypes
        Quote_Types__c cs = new Quote_Types__c();
		cs.Name = 'CBCTest';  
		cs.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
		cs.Quote_Type_Enum_Index__c = 1;
		Database.SaveResult DR_QuoteTypeCS = Database.insert(cs);
        */
        						
        // Test Quote Type page instantiation for CBC quote type
		PageReference pageRef = Page.QuoteType;
		pageRef.getParameters().put('quoteType', '1');
        Test.setCurrentPage(pageRef);
        NewQuoteController testQuoteController = new  NewQuoteController();
        testQuoteController.selectTranType = GlobalConstants.QuoteTypes.CBC.name();// quoteTransactionType.Id;
    	PageReference urlAfterQuoteTypeSelected = testQuoteController.actionSelectQuoteType();
    	System.assertEquals(GlobalConstants.QuoteTypes.CBC, testQuoteController.urlQuoteType, 'urlQuoteType should be set to CBC');
    	System.assertNotEquals(null, urlAfterQuoteTypeSelected, 'a new url should have been generated');
    	System.assertEquals('/apex/QuoteSBPartner', urlAfterQuoteTypeSelected.getUrl(),'new url should redirect to VF page to choose SB partner');
    	
    	// Test Quote Type page instantiation for SurgeonPref quote type
		PageReference pageRefSP = Page.QuoteType;
		pageRefSP.getParameters().put('quoteType', '2');
        Test.setCurrentPage(pageRefSP);
        NewQuoteController testQuoteControllerSP = new  NewQuoteController();
        testQuoteControllerSP.selectTranType = null;  // Set to null so that an error is thrown - it should fail if the trans type is not set up
    	PageReference urlAfterQuoteTypeSelectedSP = testQuoteControllerSP.actionSelectQuoteType();
    	System.assertEquals(true, testQuoteControllerSP.isError, 'An error should have been throw because the custom setting test data for quote_type__c has not been created for Surgeon pref');
    	System.assertEquals(null, urlAfterQuoteTypeSelectedSP, 'A new url should have been null');
    	
    	/*
    	// Create custom setting mapping the above transaction type to the globalconstants enum QuoteTypes - for testing I've linked the same transaction type to custom settings for cbc and sp
        cs = new Quote_Types__c();
		cs.Name = 'SurgeonPrefTest';  
		cs.Quote_Transaction_Type_Id__c = quoteTransactionTypeSP.Id;
		cs.Quote_Type_Enum_Index__c = 2;
		DR_QuoteTypeCS = Database.insert(cs);
    	*/
    	
    	// Test Quote Type page instantiation for SurgeonPref quote type
		PageReference pageRefSP1 = Page.QuoteType;
		pageRefSP1.getParameters().put('quoteType', '2');
        Test.setCurrentPage(pageRefSP1);
        NewQuoteController testQuoteControllerSP1 = new  NewQuoteController();
        testQuoteControllerSP1.selectTranType = GlobalConstants.QuoteTypes.SurgeonPreference.name();//quoteTransactionType.Id;
    	PageReference urlAfterQuoteTypeSelectedSP1 = testQuoteControllerSP1.actionSelectQuoteType();
    	System.assertNotEquals(null, urlAfterQuoteTypeSelectedSP1, 'a new url should have been generated');
    	System.assertEquals('/apex/QuoteCreate', urlAfterQuoteTypeSelectedSP1.getUrl(),'new url should redirect to VF page to choose SB partner');
    	
    	PageReference urlAfterCancel = testQuoteController.cancel();
    	System.assertEquals('/home/home.jsp', urlAfterCancel.getUrl(),' New url should redirect to home page');
    	
    	
    }
    
    static testMethod void NewQuoteControllerTestSBPartnerPage() {
    	/*
		// Create test transaction type
		Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('CBC')
        												.build(true);
        */
        									
        // Create Account
		Account vinnies = new AccountBuilder().name('St Vincents').build(true);									
        												
        // Create SB Partners
        Address__c sbPartnerAddress = new AddressBuilder().name('SB Partner1').addressType('SB').account(vinnies).build(true);
		
        // Test Quote Type page instantiation 
		PageReference pageRef = Page.QuoteSBPartner;
		pageRef.getParameters().put('accId', vinnies.Id);
		pageRef.getParameters().put('quoteType', '1');
        Test.setCurrentPage(pageRef);
        
        // Constructor should populate SB Partners
        NewQuoteController testQuoteController = new  NewQuoteController();
        //System.assertNotEquals(null, testQuoteController.transactionTypes, 'Transaction Types list should be null because we know the quote type already');
        System.assertNotEquals(null, testQuoteController.sbPartnerAddresses, 'SB Partners list should not be null');
        System.assert(testQuoteController.sbPartnerAddresses.size() == 1, 'Only 1 test partner has been set up');
        
        testQuoteController.retURL = vinnies.Id;
        PageReference urlAfterCancel = testQuoteController.cancel();
    	System.assertEquals('/' + vinnies.Id, urlAfterCancel.getUrl(),' New url should redirect to account page');
    	
        List<SelectOption> sbPartnerAddresses = testQuoteController.getSBPartners();
        System.assertNotEquals(null, sbPartnerAddresses, 'SB Partner list should have been created');
    	System.assert(sbPartnerAddresses.size() == 1, 'SB Partner list should have one address in it');
    	System.assertEquals('SB Partner1', sbPartnerAddresses[0].getLabel() ,'SB Partner with name SB Partner1 should have been found in the list');
    	
    	testQuoteController.sbPartnerAddresses = null;
    	sbPartnerAddresses = testQuoteController.getSBPartners();
        System.assertNotEquals(null, sbPartnerAddresses, 'SB Partner list should have been created');
    	System.assert(sbPartnerAddresses.size() == 1, 'SB Partner list should have one address in it');
    	System.assertEquals('SB Partner1', sbPartnerAddresses[0].getLabel() ,'SB Partner with name SB Partner1 should have been found in the list');
    	
        
    	PageReference urlAfterSBPartnerSelected = testQuoteController.actionSelectSBPartner();
    	System.assertNotEquals(null, urlAfterSBPartnerSelected, 'a new url should have been generated');
    	System.assertEquals('/apex/QuoteCreate', urlAfterSBPartnerSelected.getUrl(),'new url should redirect to VF page that creates quote and related records via action method');
    	
    	
    	
    }
    
     static testMethod void NewCBCQuoteControllerTestCreateQuote() {
    	/*
		// Create test transaction type
		Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('CBC')
        												.build(true);
    	*/
    	    												
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
    	// Create SB Partner address
        Address__c sbPartner = new AddressBuilder()
        						  .name('SB Test Partner 1')
        						  .account(vinnies)
        						  .build(true);
        System.assert(sbPartner.Id <> null,'An error occured while creating SB Parter - Address__c.');	
        
        // Test Quote Type page instantiation 
		PageReference pageRef = Page.QuoteCreate;
		pageRef.getParameters().put('accId', vinnies.Id);
		pageRef.getParameters().put('quoteType', '1');
		pageRef.getParameters().put('SBPartner', sbPartner.Id);
        Test.setCurrentPage(pageRef);
        NewQuoteController testQuoteController = new  NewQuoteController();
    	PageReference CQ = testQuoteController.createQuote();
    	System.assertNotEquals(null, testQuoteController.CBCMaster.Id, 'A CBC master should have been created');
		System.assertNotEquals(null, testQuoteController.extensionOpportunity, 'A new opportunity should have been generated');
		System.assertNotEquals(null, testQuoteController.extensionQuote, 'A new quote should have been generated');		
		System.assertEquals(testQuoteController.CBCMaster.Id,testQuoteController.extensionQuote.Consignment_Business_Case__c, 'The CBC id should have been populated on the quote');
    	System.assertNotEquals(null, CQ, 'A new url should have been generated');
    	System.assertEquals('/apex/quoteeditor?id='+testQuoteController.extensionQuote.Id, CQ.getUrl(),'New url is incorrect');
    }
    
    
     static testMethod void NewSETBUYQuoteControllerTestCreateQuote() {
    	/*
		// Create test transaction type
		Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        */
        												
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
        
        // Test Quote Type page instantiation 
		PageReference pageRef = Page.QuoteCreate;
		pageRef.getParameters().put('accId', vinnies.Id);
		pageRef.getParameters().put('quoteType', '0');
        Test.setCurrentPage(pageRef);
        NewQuoteController testQuoteController = new  NewQuoteController();
    	PageReference CQ = testQuoteController.createQuote();
    	System.assertNotEquals(null, CQ, 'A new url should have been generated');
    	System.assertEquals('/apex/quoteeditor?id='+testQuoteController.extensionQuote.Id, CQ.getUrl(),'New url is incorrect');
		System.assertNotEquals(null, testQuoteController.extensionOpportunity, 'A new opportunity should have been generated');
		System.assertNotEquals(null, testQuoteController.extensionQuote, 'A new quote should have been generated');		
		
       
    }
    
   
    
    
     static testMethod void NewSPQuoteControllerTestCreateQuote() {
    	
		// Create test transaction type
		/*
		Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('SP')
        												.build(true);
        					*/
        												
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
    	// Create SB Partner address
        Address__c sbPartner = new AddressBuilder()
        						  .name('SB Test Partner 1')
        						  .account(vinnies)
        						  .build(true);
        System.assert(sbPartner.Id <> null,'An error occured while creating SB Parter - Address__c.');	
        
        // Test Quote Type page instantiation 
		PageReference pageRef = Page.QuoteCreate;
		pageRef.getParameters().put('accId', vinnies.Id);
		pageRef.getParameters().put('quoteType', '2');
		pageRef.getParameters().put('SBPartner', '');
        Test.setCurrentPage(pageRef);
        NewQuoteController testQuoteController = new  NewQuoteController();
        
        // Set accountid to null to test error condition
        testQuoteController.accountId = null;
        PageReference CQ = testQuoteController.createQuote();
    	System.assertEquals(null,CQ,'An exception should have occurred. The page should not redirected.');
    	
        // Test Error conditions by deleting organisation code , division code and distribution code from the account.
        testQuoteController.accountId = vinnies.Id;
        vinnies.OrganizationCode__c = null;
        update vinnies;
        CQ = testQuoteController.createQuote();
    	System.assertEquals(null,CQ,'An exception should have occurred. The page should not redirected.');
    	
    	vinnies.OrganizationCode__c = organizationCode;
    	vinnies.DistributionChannel__c = null;
        update vinnies;
        CQ = testQuoteController.createQuote();
        System.assertEquals(null,CQ,'An exception should have occurred. The page should not redirected.');
    	
    	vinnies.DistributionChannel__c = distributionChannel;
    	vinnies.Division__c = null;
    	update vinnies;
        CQ = testQuoteController.createQuote();
        System.assertEquals(null,CQ,'An exception should have occurred. The page not redirected.');
    	
        // Now set account organisation code , division code and distribution code values correctly and create the quote
        vinnies.Division__c = division;
        update vinnies;
    	CQ = testQuoteController.createQuote();
		System.assertNotEquals(null, testQuoteController.extensionOpportunity, 'A new opportunity should have been generated');
		System.assertNotEquals(null, testQuoteController.extensionQuote, 'A new quote should have been generated');	
    	System.assertNotEquals(null, CQ, 'A new url should have been generated');
    	System.assertEquals('/apex/quoteeditor?id='+testQuoteController.extensionQuote.Id, CQ.getUrl(),'New url is incorrect');
    }
    
    
    
}