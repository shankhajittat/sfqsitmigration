public class AutocompleteController {
	
	public String id{get;set;}
	public String pluginType{get;set;}
	public String domIdOne{get;set;}
	public String domIdTwo{get;set;}
	public String valueOne{get;set;}
	public String valueTwo{get;set;}
	
	public AutocompleteController() {
		System.debug('AC Controller constructor');
	}

	public interface AutoCompletePlugin {
		void init(Map<String,String> context);
		List<SObject> find(String searchTerm);
		String css();
		String jsRender();
		String jsHandleSelect(List<String> domIds, List<String> values);
		boolean filter(SObject oneSObject);
	} 
	
	private static AutoCompletePlugin createPlugin(String plt) {
        Type t = Type.forName(plt);
        return (AutoCompletePlugin)t.newInstance();
	} 
	
	public String getCustomCSS() {
		return createPlugin(pluginType).css();
	}
	
	public String getRenderJS() {
		return createPlugin(pluginType).jsRender();
	}

	public String getHandleSelectionJS() {
		return createPlugin(pluginType).jsHandleSelect(
			new String[] {domIdOne, domIdTwo},
			new String[] {valueOne, valueTwo});
	}
	
    @RemoteAction
    public static List<Set_Class__c> doSearch(String searchTerm, String pluginType, Map<String,String> context) {
    	
    	AutoCompletePlugin thePlugin = createPlugin(pluginType);
    	thePlugin.init(context);
    	
        List<SObject> unfilteredSearchList = thePlugin.find(searchTerm);
        
        // Exclude certain objects depending on the result of the filter method
        List<SObject> filteredSearchList = new List<SObject>();
        for(SObject oneInUnfilteredList : unfilteredSearchList){
        	
        	if (thePlugin.filter(oneInUnfilteredList)){        		
    			filteredSearchList.add(oneInUnfilteredList);    			
        	}
        }
		filteredSearchList.sort();
		return filteredSearchList;
	}
}