@isTest(seeAllData=false)
public class CSVReaderTest {
	
	@isTest
	public static void testBasicParse() {
		String csv = '"aa","bb","cc"';
		List<List<string>> parsed = CSVReader.Parse(csv);
		System.assertEquals(parsed.size(), 1, 'one line csv generate a single list');
		System.assertEquals(parsed.get(0).size(), 3, 'line should generate 3 fields');
	}

	@isTest
	public static void testSetClassParse() {
		String csv = TestingTools.getResourceBodyAsString('SetClass_JH230');
		List<List<String>> parsed = CSVReader.Parse(csv);
		System.debug(parsed);
		List<String> headerRow = parsed.get(0); 
		System.assertEquals(headerRow.size(), 8, 'SetClass files have 8 columns');
	}	
}