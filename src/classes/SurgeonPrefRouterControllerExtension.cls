public with sharing class SurgeonPrefRouterControllerExtension {
	public final Id surgPrefId;
	public Id surgPrefAccountId {get; set;}
	public String retURL;
	
	public SurgeonPrefRouterControllerExtension(ApexPages.StandardController stdController) {
        this.surgPrefId = stdController.getRecord().Id;
	}
	
	// use the retURL which is always passed by standard buttons to resolve the account id
	public Id getMasterRecordId() {
		try {
			retURL = ApexPages.currentPage().getParameters().get('retURL');
			if (retURL == null) {
				return null;
			} else {
				// If this page has been initiated from the custom SP tab then the url will not have a valid account so the account picker needs to be enabled
				String idInURL = retURL.substring(1);
				String validatedId = StringUtils.validateId(idInURL);
				return validatedId;
			}
		} catch (Exception e) {
	 		ApexPages.addMessages(e);
	 		return null;
		}
	}

	public PageReference route() {
		Boolean isNewSurgPref = this.surgPrefId == null;
		
		if (isNewSurgPref) {
			String masterId = getMasterRecordId();
			Boolean masterIsAccount = masterId != null && masterId.startsWith('001');
			
			if (masterIsAccount) {
				//PageReference pr = new PageReference('/apex/QuoteSBPartner?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.SurgeonPreference.ordinal() + '&retURL=' + retURL);
				PageReference pr = new PageReference('/apex/QuoteCreate?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.SurgeonPreference.ordinal() + '&retURL=' + retURL);
				pr.setRedirect(true);
				return pr;
			} else {
				PageReference pr = new PageReference('/apex/Quote_Account_Picker?quoteType=' + GlobalConstants.QuoteTypes.SurgeonPreference.ordinal());
				pr.setRedirect(true);
				return pr;
			}
		}else {
			// Redirect to a VF page that just runs an action to create quote and related records because we don't need any user input in this case
			Surgeon_Preference__c surgPref = [select Id, Surgeon__c from Surgeon_Preference__c where Id = :this.surgPrefId];
			
			PageReference target = Page.QuoteCreate;
			target.getParameters().put('accId',surgPref.Surgeon__c);
			target.getParameters().put('quoteType', String.valueof(GlobalConstants.QuoteTypes.SurgeonPreference.ordinal()));
			target.getParameters().put('SPref', surgPref.Id);
			target.setRedirect(true);
			return target;
		}
	}
}