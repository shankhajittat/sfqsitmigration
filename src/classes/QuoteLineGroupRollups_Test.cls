@isTest
private class QuoteLineGroupRollups_Test {

 static testMethod void testrollupQuoteLines_Insert() {
        QuoteLineItem quoteLine = getTestQuoteLineItem();
        insert quoteLine;
        
        //Start Test
        Test.startTest();
        
        //QuoteLineGroupRollups.rollupQuoteLines(new List<QuoteLineItem>{quoteLine});
        
        QuoteLineGroupRollups rollupQuoteLines = new QuoteLineGroupRollups(new List<QuoteLineItem>{quoteLine});
        
        rollupQuoteLines.buildUniqueQuoteLineGroupIds();
        System.debug('>>>>rollupQuoteLines.quoteLineGroupIds<<<<' + rollupQuoteLines.quoteLineGroupIds);
		System.assert(rollupQuoteLines.quoteLineGroupIds != null && rollupQuoteLines.quoteLineGroupIds.isEmpty() == false,
			'An error occured while execuing buildUniqueQuoteLineGroupIds method');
		
		rollupQuoteLines.buildQuoteLineGroups();
		System.assert(rollupQuoteLines.quoteLineGroups != null && rollupQuoteLines.quoteLineGroups.isEmpty() == false,
			'An error occured while execuing buildQuoteLineGroups method');
		
		rollupQuoteLines.buildAggregateRollups();
		System.assert(rollupQuoteLines.rollupLineGroups != null && rollupQuoteLines.rollupLineGroups.isEmpty() == false,
			'An error occured while execuing buildAggregateRollups method');
						
		rollupQuoteLines.buildQuoteGroupsToUpdate();
		System.assert(rollupQuoteLines.quoteGroupsToUpdate != null && rollupQuoteLines.quoteGroupsToUpdate.isEmpty() == false,
			'An error occured while execuing buildQuoteGroupsToUpdate method');
		
		boolean isUpdated = rollupQuoteLines.updateRollups();
		System.assert(isUpdated,'An error occured while execuing updateRollups method');
        
        Test.stopTest();
        
        //Validate Results
        
        //,Group_Total_Extended_Unit_Price__c, 
        Quote_Line_Group__c quoteLineGroupTestResult = getQuoteLineGroup(quoteLine.Quote_Line_Group__c);
		
		System.debug('<<<<<After Insert>>>>>' + quoteLineGroupTestResult);
		
		
        System.assertEquals(200.00, quoteLineGroupTestResult.LineGroup_Total_Extended_UnitPrice__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Extended_UnitPrice__c field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Price__c');
			
		System.assertEquals(20.00, quoteLineGroupTestResult.LineGroup_Total_Discount_Amount__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Discount_Amount__c field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Discount__c');
		
		
		System.assertEquals('Validated', quoteLineGroupTestResult.LineGroup_Status__c
			,'Expected the quoteLineGroupTestResult.LineGroup_Status__c field to be updated based on the rollup of QuoteLineItem.Price_Status__c');
				
    }
    
    static testMethod void testrollupQuoteLines_SingleMethod_Insert() {
        QuoteLineItem quoteLine = getTestQuoteLineItem();
        insert quoteLine;
        
        //Start Test
        Test.startTest();
        
        //QuoteLineGroupRollups.rollupQuoteLines(new List<QuoteLineItem>{quoteLine});
        
        QuoteLineGroupRollups rollupQuoteLines = new QuoteLineGroupRollups(new List<QuoteLineItem>{quoteLine});
		
		boolean isUpdated = rollupQuoteLines.rollupQuoteLines();
		System.assert(isUpdated,'An error occured while execuing rollupQuoteLines method');
        
        Test.stopTest();
        
        //Validate Results
        Quote_Line_Group__c quoteLineGroupTestResult = getQuoteLineGroup(quoteLine.Quote_Line_Group__c);
		
		System.debug('<<<<<After Insert>>>>>' + quoteLineGroupTestResult);
		
			
		System.assertEquals(200.00, quoteLineGroupTestResult.LineGroup_Total_Extended_UnitPrice__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Extended_UnitPrice__c field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Price__c');
			
		System.assertEquals(20.00, quoteLineGroupTestResult.LineGroup_Total_Discount_Amount__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Discount_Amount__c field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Discount__c');
		
		System.assertEquals('Validated', quoteLineGroupTestResult.LineGroup_Status__c
			,'Expected the quoteLineGroupTestResult.LineGroup_Status__c field to be updated based on the rollup of QuoteLineItem.Price_Status__c');
				
    }
    
    
    static testMethod void testrollupQuoteLines_Update() {
        QuoteLineItem quoteLine = getTestQuoteLineItem();
        insert quoteLine;
        
        quoteLine.Quantity = 5;
        update quoteLine;
        
        
        //Start Test
        Test.startTest();
        
        //QuoteLineGroupRollups.rollupQuoteLines(new List<QuoteLineItem>{quoteLine});
        
        QuoteLineGroupRollups rollupQuoteLines = new QuoteLineGroupRollups(new List<QuoteLineItem>{quoteLine});
        
        rollupQuoteLines.buildUniqueQuoteLineGroupIds();
        System.debug('>>>>rollupQuoteLines.quoteLineGroupIds<<<<' + rollupQuoteLines.quoteLineGroupIds);
		System.assert(rollupQuoteLines.quoteLineGroupIds != null && rollupQuoteLines.quoteLineGroupIds.isEmpty() == false,
			'An error occured while execuing buildUniqueQuoteLineGroupIds method');
		
		rollupQuoteLines.buildQuoteLineGroups();
		System.assert(rollupQuoteLines.quoteLineGroups != null && rollupQuoteLines.quoteLineGroups.isEmpty() == false,
			'An error occured while execuing buildQuoteLineGroups method');
		
		rollupQuoteLines.buildAggregateRollups();
		System.assert(rollupQuoteLines.rollupLineGroups != null && rollupQuoteLines.rollupLineGroups.isEmpty() == false,
			'An error occured while execuing buildAggregateRollups method');
						
		rollupQuoteLines.buildQuoteGroupsToUpdate();
		System.assert(rollupQuoteLines.quoteGroupsToUpdate != null && rollupQuoteLines.quoteGroupsToUpdate.isEmpty() == false,
			'An error occured while execuing buildQuoteGroupsToUpdate method');
		
		boolean isUpdated = rollupQuoteLines.updateRollups();
		System.assert(isUpdated,'An error occured while execuing updateRollups method');
		
        
        Test.stopTest();
        
        //Validate Results
       Quote_Line_Group__c quoteLineGroupTestResult = getQuoteLineGroup(quoteLine.Quote_Line_Group__c);
		
		System.debug('<<<<<After Update>>>>>' + quoteLineGroupTestResult);
		
				
		System.assertEquals(500.00, quoteLineGroupTestResult.LineGroup_Total_Extended_UnitPrice__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Extended_UnitPrice__c field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Price__c');
			
		System.assertEquals(50.00, quoteLineGroupTestResult.LineGroup_Total_Discount_Amount__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Discount_Amount__c field to be updated based on the rollup of QuoteLineItem.Extended_Unit_Discount__c');
		
		System.assertEquals('Validated', quoteLineGroupTestResult.LineGroup_Status__c
			,'Expected the quoteLineGroupTestResult.LineGroup_Status__c field to be updated based on the rollup of QuoteLineItem.Price_Status__c');				
    }
    
    
    static testMethod void testrollupQuoteLines_Delete() {
        QuoteLineItem quoteLine = getTestQuoteLineItem();
        insert quoteLine;
        
        delete quoteLine;
        
        
        //Start Test
        Test.startTest();
        
        //QuoteLineGroupRollups.rollupQuoteLines(new List<QuoteLineItem>{quoteLine});
        QuoteLineGroupRollups rollupQuoteLines = new QuoteLineGroupRollups(new List<QuoteLineItem>{quoteLine});
        
        rollupQuoteLines.buildUniqueQuoteLineGroupIds();
        System.debug('>>>>rollupQuoteLines.quoteLineGroupIds<<<<' + rollupQuoteLines.quoteLineGroupIds);
		System.assert(rollupQuoteLines.quoteLineGroupIds != null && rollupQuoteLines.quoteLineGroupIds.isEmpty() == false,
			'An error occured while execuing buildUniqueQuoteLineGroupIds method');
		
		rollupQuoteLines.buildQuoteLineGroups();
		System.assert(rollupQuoteLines.quoteLineGroups != null && rollupQuoteLines.quoteLineGroups.isEmpty() == false,
			'An error occured while execuing buildQuoteLineGroups method');
		
		rollupQuoteLines.buildAggregateRollups();
		//System.assert(rollupQuoteLines.rollupLineGroups != null && rollupQuoteLines.rollupLineGroups.isEmpty() == false,
		//	'An error occured while execuing buildAggregateRollups method');
		//if no more QuoteLineItems then buildAggregateRollups method won't generate any AggregateRollups, 
		//still it needs to update with 0s.
						
		rollupQuoteLines.buildQuoteGroupsToUpdate();
		System.assert(rollupQuoteLines.quoteGroupsToUpdate != null && rollupQuoteLines.quoteGroupsToUpdate.isEmpty() == false,
			'An error occured while execuing buildQuoteGroupsToUpdate method');
		
		boolean isUpdated = rollupQuoteLines.updateRollups();
		System.assert(isUpdated,'An error occured while execuing updateRollups method');
        
        Test.stopTest();
        
        //Validate Results
       Quote_Line_Group__c quoteLineGroupTestResult = getQuoteLineGroup(quoteLine.Quote_Line_Group__c);
		
		System.debug('<<<<<After Delete>>>>>' + quoteLineGroupTestResult);
		
			
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_Total_Extended_UnitPrice__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Extended_UnitPrice__c field to be 0');
			
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_Total_Discount_Amount__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Discount_Amount__c field to be 0');
						
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_Total_GrossMargin_Amount__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_GrossMargin_Amount__c field to be 0');
		
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_Total_NetMargin_Amount__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_NetMargin_Amount__c field to be 0');
		
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_Total_Landed_Cost__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Landed_Cost__c field to be 0');
		
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_TotalPrice_ExTax__c
			,'Expected the Quote_Line_Group__c.LineGroup_TotalPrice_ExTax__c field to be 0');
		
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_TotalPrice_ExTax__c
			,'Expected the Quote_Line_Group__c.LineGroup_TotalPrice_ExTax__c field to be 0');
		
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_TotalPrice_IncTax__c
			,'Expected the Quote_Line_Group__c.LineGroup_TotalPrice_ExTax__c field to be 0');
		
		System.assertEquals(0.00, quoteLineGroupTestResult.LineGroup_Total_Tax__c
			,'Expected the Quote_Line_Group__c.LineGroup_Total_Tax__c field to be 0');
			
		System.assertEquals('New', quoteLineGroupTestResult.LineGroup_Status__c
			,'Expected the quoteLineGroupTestResult.LineGroup_Status__c field to be updated based on the rollup of QuoteLineItem.Price_Status__c');
				
    }
   
    private static Quote_Line_Group__c getQuoteLineGroup(Id groupId)
    {
    	 Quote_Line_Group__c quoteLineGroup = [SELECT Id,
															LineGroup_Total_Discount_Amount__c,
															LineGroup_Total_Extended_UnitPrice__c,
															LineGroup_Total_GrossMargin_Amount__c,
															LineGroup_Total_NetMargin_Amount__c,
															LineGroup_Total_Landed_Cost__c,
															LineGroup_Total_Standard_Cost__c,
															LineGroup_TotalPrice_ExTax__c,
															LineGroup_TotalPrice_IncTax__c,
															LineGroup_Total_Quantity__c,
															NumberOfBOMsRequired__c,
															LineGroup_Total_ProposedQuantity__c,
															LineGroup_Total_Tax__c,
															LineGroup_Status__c,
															LineGroup_TotalPrice_ImpMaterialGroup__c,
															LineGroup_TotalPrice_InsMaterialGroup__c,
															LineGroup_TotalPrice_OtherMaterialGroup__c,
															LineGroup_TotalStdCost_ImpMaterialGroup__c,
															LineGroup_TotalStdCost_InsMaterialGroup__c,
															LineGroup_TotalStdCost_OtherMaterialGrou__c
				FROM Quote_Line_Group__c WHERE Id =: groupId ];
				
		return quoteLineGroup;
    }
    
    
    private static QuoteLineItem getTestQuoteLineItem(){
    	
    	Boolean createInDatabase = true;
        
       	String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(createInDatabase);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);

        
        
		//String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity o = new OpportunityBuilder()
        					.account(vinnies)
        					.priceBookId(customPricebook.Id).build(createInDatabase);
        System.assert(o.Id <> null,'An error occured while creating Opportunity.');

        Product2 product1 = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001.01')
        		.build(createInDatabase);
        System.assert(product1.Id <> null,'An error occured while creating Product.');
        
        Product2 bomProduct = new ProductBuilder()
        		.name('BOM-BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001BOM')
        		.build(createInDatabase);
        System.assert(bomProduct.Id <> null,'An error occured while creating Product.');
        
        Product2 product2 = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001.02')
        		.build(createInDatabase);
        System.assert(product2.Id <> null,'An error occured while creating Product.');
        
        Product2 product3 = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001.03')
        		.build(createInDatabase);
        System.assert(product3.Id <> null,'An error occured while creating Product.');
        
        
        //Create PricebookEntry for product
        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
        									.productId(product1.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
        
         //Create PricebookEntry for Custom Pricebook
        PricebookEntry pbCustomEntry1 = new PricebookEntryBuilder()
        									.productId(product1.Id)
        									.pricebookId(customPricebook.Id).build(createInDatabase);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbCustomEntry1.Id <> null,'An error occured while creating PricebookEntry.');
		pbCustomEntry1.MaterialGroup1__c = 'INS';
		pbCustomEntry1.StandardCost__c = 125.00;
		pbCustomEntry1.UnitPrice = 250.00;
		update pbCustomEntry1;

		
        //Create Set Class
        Set_Class__c setClass = new SetClassBuilder()
        								.equipmentNumber('TEST SC101').productCode('03.161.001BOM').build(createInDatabase);
        System.assert(setClass.Id <> null,'An error occured while creating SetClass.');
        
       
        //Create SetClass Items
        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
        								.header(setClass)
        								.product(product1)
        								.quantity(2).build(createInDatabase);
        
        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(createInDatabase);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        */
        
        //Create Quote
        //String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
        Quote quote = new QuoteBuilder()
        		.opportunity(o)
        		.priceBookId(customPricebook.Id)
        		.quoteType(GlobalConstants.QuoteTypes.CustomerQuote)
        		.build(createInDatabase);
        System.assert(quote.Id <> null,'An error occured while creating Quote.');
        
        //sync Opportunity and Quote lines.
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
       
        Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
        		.name('TEST SC101')
        		.quote(quote)
        		.setClass(setClass)
        		.groupType('Kit')
//        		.groupStatus('Validated')
        		.build(createInDatabase);
        System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup.');
        
        QuoteLineItem quoteLineItem = new QuoteLineItem(
						QuoteId=quote.Id,
						PriceBookEntryId=pbCustomEntry1.Id,
						UnitPrice=100.00,
						Quantity= 2,
						Discount = 10.00,
						Set_Class__c = setClass.Id,
						Set_Class_Item__c = setClassItem1.Id,
						Quote_Line_Group__c = quoteLineGroup.Id,
						Tax_Rate__c = 10.00,
						Standard_Cost__c = 30.00,
						Landed_Cost__c = 20.00,
						Sequence_Number__c = 10,
						Price_Status__c = 'Validated'
						);
		
		return quoteLineItem;
        
    }
}