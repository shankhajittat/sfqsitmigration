@isTest(SeeAllData=true)
private class ProcessLogToolsTest {

	static testMethod void insertUIErrorLogTest(){
		
		// Get the base URL.
		String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
		System.debug('Base URL: ' + sfdcBaseURL ); 
		// Get some parts of the base URL.
		System.debug('Host: ' + URL.getSalesforceBaseUrl().getHost());   
		System.debug('Protocol: ' + URL.getSalesforceBaseUrl().getProtocol());

		system.debug('Url>>>>' + new Url('https://login.salesforce.com').toExternalForm());
		system.debug('urlEncode>>>>' + EncodingUtil.urlEncode('https://login.salesforce.com', 'UTF-8'));
		system.debug('urlDecode1>>>>' + EncodingUtil.urlDecode('https://login.salesforce.com', 'UTF-8'));
		system.debug('urlDecode2>>>>' + EncodingUtil.urlDecode(EncodingUtil.urlEncode('https://login.salesforce.com', 'UTF-8'), 'UTF-8'));
		
		Id processLogId = ProcessLogTools.insertUIErrorLog('Quote UI Error - 1', 'Invalid TagId - Javascript Error', 'stackTraceString\nLine-101\nInvalid TagId - Javascript Error',
							 'https://login.salesforce.com', null, ProcessLogCommon.SourceObject.Quote, '12345', '123');
		system.assert(processLogId != null,'Was expecting valid processLogId.');
		system.debug('insertUIErrorLog executed successfully and generated ProcessLogId: '  + processLogId);
		List<ProcessLogItem__c> processLogItems = [Select p.SystemModstamp, p.SourceRecordNumber__c, p.SourceRecordId__c, p.SourceObject__c, p.SeverityType__c,
														p.SourceUrl__c,p.SourceFileName__c,p.StackTraceString__c, 
														p.ProcessLogId__c, p.Name, p.Message__c, p.LastModifiedDate, p.LastModifiedById, p.LastActivityDate, 
														p.IsResolved__c, p.IsDeleted, p.Id, p.ExceptionLineNumber__c, p.ErrorCode__c, p.CreatedDate, p.CreatedById 
													From ProcessLogItem__c p Where p.ProcessLogId__c =: processLogId];
		system.assert(processLogItems != null && processLogItems.size() > 0,'Was expecting processLogItems records.');
		system.debug('processLogItems count>>>' + processLogItems.size() +  ' processLogItems>>>' + processLogItems);
		
		processLogId = ProcessLogTools.insertUIErrorLog('Quote UI Error - 2', 'Invalid TagId - Javascript Error', 'stackTraceString\nLine-101\nInvalid TagId - Javascript Error',
							 null, null, ProcessLogCommon.SourceObject.Quote, '12345', '123');
		system.assert(processLogId != null,'Was expecting valid processLogId.');
		system.debug('insertUIErrorLog executed successfully and generated ProcessLogId: '  + processLogId);
		processLogItems = [Select p.SystemModstamp, p.SourceRecordNumber__c, p.SourceRecordId__c, p.SourceObject__c, p.SeverityType__c,
														p.SourceUrl__c,p.SourceFileName__c,p.StackTraceString__c, 
														p.ProcessLogId__c, p.Name, p.Message__c, p.LastModifiedDate, p.LastModifiedById, p.LastActivityDate, 
														p.IsResolved__c, p.IsDeleted, p.Id, p.ExceptionLineNumber__c, p.ErrorCode__c, p.CreatedDate, p.CreatedById 
													From ProcessLogItem__c p Where p.ProcessLogId__c =: processLogId];
		system.assert(processLogItems != null && processLogItems.size() > 0,'Was expecting processLogItems records.');
		system.debug('processLogItems count>>>' + processLogItems.size() +  ' processLogItems>>>' + processLogItems);
		
		processLogId = ProcessLogTools.insertUIErrorLog('Quote UI Error - 3', 'Invalid TagId - Javascript Error', 'stackTraceString\nLine-101\nInvalid TagId - Javascript Error',
							 'dfadsfadsf', null, ProcessLogCommon.SourceObject.Quote, '12345', '123');
		system.assert(processLogId != null,'Was expecting valid processLogId.');
		system.debug('insertUIErrorLog executed successfully and generated ProcessLogId: '  + processLogId);
		processLogItems = [Select p.SystemModstamp, p.SourceRecordNumber__c, p.SourceRecordId__c, p.SourceObject__c, p.SeverityType__c,
														p.SourceUrl__c,p.SourceFileName__c,p.StackTraceString__c, 
														p.ProcessLogId__c, p.Name, p.Message__c, p.LastModifiedDate, p.LastModifiedById, p.LastActivityDate, 
														p.IsResolved__c, p.IsDeleted, p.Id, p.ExceptionLineNumber__c, p.ErrorCode__c, p.CreatedDate, p.CreatedById 
													From ProcessLogItem__c p Where p.ProcessLogId__c =: processLogId];
		system.assert(processLogItems != null && processLogItems.size() > 0,'Was expecting processLogItems records.');
		system.debug('processLogItems count>>>' + processLogItems.size() +  ' processLogItems>>>' + processLogItems);
		
		
	}

	static testMethod void insertProcessLogTest(){
		ProcessLog pLog = new ProcessLog();
		pLog.title = 'insertProcessLogTest Unit Test - ' + DateTime.now();
		pLog.sourceObject = ProcessLogCommon.SourceObject.Quote;
		pLog.sourceProcess = ProcessLogCommon.SourceProcess.QuoteSave;
		pLog.sourceRecordId = null;
		pLog.sourceRecordNumber = null;
		pLog.ProcessLogItems = null;
		
		ProcessLogItem logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item Message';
		logItem.sourceRecordId = null;
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.Information;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems = new List<ProcessLogItem>{ logItem };
		
		Id processLogId = ProcessLogTools.insertProcessLog(pLog);
		system.assert(processLogId != null,'Was expecting valid processLogId.');
		system.debug('insertUIErrorLog executed successfully and generated ProcessLogId: '  + processLogId);
		List<ProcessLogItem__c> processLogItems = [Select p.SystemModstamp, p.SourceRecordNumber__c, p.SourceRecordId__c, p.SourceObject__c, p.SeverityType__c, 
														p.SourceUrl__c,p.SourceFileName__c,p.StackTraceString__c, 
														p.ProcessLogId__c, p.Name, p.Message__c, p.LastModifiedDate, p.LastModifiedById, p.LastActivityDate, 
														p.IsResolved__c, p.IsDeleted, p.Id, p.ExceptionLineNumber__c, p.ErrorCode__c, p.CreatedDate, p.CreatedById 
													From ProcessLogItem__c p Where p.ProcessLogId__c =: processLogId];
		system.assert(processLogItems != null && processLogItems.size() > 0,'Was expecting processLogItems records.');
		system.debug('processLogItems count>>>' + processLogItems.size() +  ' processLogItems>>>' + processLogItems);
			
	}

	static testMethod void getProcessLogsByQuoteIdTest(){
		Id testQuoteId = '0Q0900000011CRJCA2';
		
		ProcessLog pLog = new ProcessLog();
		pLog.title = 'insertProcessLogTest Unit Test - ' + DateTime.now();
		pLog.sourceObject = ProcessLogCommon.SourceObject.Quote;
		pLog.sourceProcess = ProcessLogCommon.SourceProcess.QuoteSave;
		pLog.sourceRecordId = testQuoteId;
		pLog.sourceRecordNumber = null;
		pLog.ProcessLogItems = null;
		pLog.ProcessLogItems = new List<ProcessLogItem>();
		
		ProcessLogItem logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 1 Message';
		logItem.sourceRecordId = '001900000121rl0';
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.ApplicationError;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 2 Message';
		logItem.sourceRecordId = '001900000121rl0';
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.ApplicationError;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 3 Message';
		logItem.sourceRecordId = null;
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.Information;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 4 Message';
		logItem.sourceRecordId = '00190000012Ooi0';
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.ValidationError;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		Id processLogId = ProcessLogTools.insertProcessLog(pLog);
		system.assert(processLogId != null,'Was expecting valid processLogId.');
		system.debug('insertUIErrorLog executed successfully and generated ProcessLogId: '  + processLogId);
		
		//Second log
		pLog = new ProcessLog();
		pLog.title = 'insertProcessLogTest Unit Test - ' + DateTime.now();
		pLog.sourceObject = ProcessLogCommon.SourceObject.Quote;
		pLog.sourceProcess = ProcessLogCommon.SourceProcess.QuoteGetPrices;
		pLog.sourceRecordId = testQuoteId;
		pLog.sourceRecordNumber = null;
		pLog.ProcessLogItems = null;
		pLog.ProcessLogItems = new List<ProcessLogItem>();
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 1-2 Message';
		logItem.sourceRecordId = '0069000000OIdZz';
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.ApplicationError;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 2-2 Message';
		logItem.sourceRecordId = '0Q0900000011CP8CAM';
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.ApplicationError;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 3-2 Message';
		logItem.sourceRecordId = null;
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.Information;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		logItem = new ProcessLogItem();
		logItem.message = 'ProcessLog Item 4-2 Message';
		logItem.sourceRecordId = '0069000000OIdZz';
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.ValidationError;
		logItem.sourceObject = null;
		logItem.errorCode = null;
		logItem.exceptionLineNumber = null;
		pLog.ProcessLogItems.add(logItem);
		
		processLogId = ProcessLogTools.insertProcessLog(pLog);
		system.assert(processLogId != null,'Was expecting valid processLogId.');
		system.debug('insertUIErrorLog executed successfully and generated ProcessLogId: '  + processLogId);
		
		
		QuoteProcessLogCollection qCollection = ProcessLogTools.getProcessLogsByQuoteId(testQuoteId);
		system.assert(qCollection != null, 'Was expecting a valid QuoteProcessLogCollection object');
		system.assert(qCollection.processLogsBySourceProcessMap != null, 'Was expecting a valid QuoteProcessLogCollection.processLogsBySourceProcessMap object');
		system.assert(qCollection.processLogItemsByQuoteLineItemIdMap != null, 'Was expecting a valid QuoteProcessLogCollection.processLogItemsByQuoteLineItemIdMap object');
		system.assert(qCollection.processLogMap != null, 'Was expecting a valid QuoteProcessLogCollection.processLogMap object');
		
		system.debug('qCollection.processLogsBySourceProcessMap=>' + qCollection.processLogsBySourceProcessMap);
		system.debug('qCollection.processLogItemsByQuoteLineItemIdMap=>' + qCollection.processLogItemsByQuoteLineItemIdMap);
		system.debug('qCollection.processLogMap=>' + qCollection.processLogMap);
	}

    static testMethod void processLogsToStringTest() {
        List<APIv001.ProcessLog> processLogs = new List<APIv001.ProcessLog>();
        
        APIv001.ProcessLog pl = new APIv001.ProcessLog();
        pl.recordId = '0Q0900000003Mle';
     	pl.logs = new List<APIv001.Log>();
     	
        
        APIv001.Log log = new APIv001.Log();
        log.code = '101';
        log.message = 'Invalid Item';
        log.severity = APIv001.SeverityType.ValidationError;
        pl.logs.add(log);
        
        log = new APIv001.Log();
        log.code = '102';
        log.message = 'Error while getting price details from SAP';
        log.severity = APIv001.SeverityType.ApplicationError;
        pl.logs.add(log);
        
        log = new APIv001.Log();
        log.code = '103';
        log.message = 'An information from Price request.';
        log.severity = APIv001.SeverityType.Information;
        pl.logs.add(log);
      
      	processLogs.add(pl);
        
        String processlogString = ProcessLogToolsAPIv001.processLogsToString(processLogs);
        System.assert(processlogString != null ,'Was expecting a string value');
        System.debug(processlogString);
    }
}