@IsTest(seeAllData=true)
public class SetClassParserTest {

	@IsTest
	public static void testRunWithEmptyFields() {				
		SetClassParser parser = new SetClassParser(Test.getStandardPricebookId(), TestingTools.getResourceBodyAsString('SetClass_JH227'));
		parser.run();
		System.assertEquals(parser.newSetClassItems.size(), 4, 'new set class items were created');
	}
	
	@IsTest
	public static void testParseAndCreate() {
				
		SetClassParser parser = new SetClassParser(Test.getStandardPricebookId(), TestingTools.getResourceBodyAsString('SetClass_JH230'));
		System.assert(parser.customPricebook != null, 'parser loads a pricebook when created');
		System.assert(parser.customPricebook.isActive, 'parser loads an active custom pricebook when created');
		
		parser.checkUniqueSetClassIds();
		System.assert(parser.setClassIdsParsed.contains('jh230'), 'JH230 should be the set class id');
		
		parser.loadProductCodes();
		System.assertEquals(parser.productCodes.size(), 50, 'JH230 should contain 50 unique items');
		
		parser.ensureProducts();
		System.assertEquals(parser.setProducts.size(), 50, 'database should now contain 50 matching products');
		for (Product2 p : parser.setProducts.values()) {
			System.assert(p.Id != null, 'All products in the set should have an Id');
		}
		
		parser.ensurePricebookEntries();
		System.assertEquals(parser.setPricebookEntriesByProductId.size(), 50, 'db should now have 50 default prices');
		
		// Create another product
		Product2 bomProduct = new ProductBuilder()
        		.name('BOM Product')
        		.code('03.162.002BOM')
        		.build(true);
        System.assert(bomProduct.Id <> null,'An error occured while creating Product 03.162.002BOM');
        
		// since we are using seealldata=true we must cater for a database with an existing active Set Class
		List<Set_Class__c> existingActives = [select Id, Active__c from Set_Class__c 
									   		  where EquipmentNumber__c = :parser.parsed.get(1).get(0)
									   		  and Active__c = true];
									   		  
		Set_Class__c existingActive = existingActives.size()==0?
			new SetClassBuilder().active(true).equipmentNumber(parser.parsed.get(1).get(0)).productCode('03.162.002BOM').build(true):
			existingActives.get(0);
		parser.updateExistingActiveSetClasses();
		System.assertEquals(parser.existingSetClasses.size(), 1, 'db should have 1 existing set class');
		for (Set_Class__c sc : parser.existingSetClasses) {
			System.assert(sc.Active__c == false, 'all existing set classes should be inactive');
		}
		
		parser.create();
		System.assert(parser.newSetClass.Id != null, 'new set class was created');
		System.assertEquals(parser.newSetClassItems.size(), 50, 'new set class items were created');
	}
	
}