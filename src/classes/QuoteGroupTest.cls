@isTest
private class QuoteGroupTest {

    static testMethod void populateQuotationFromSetClassCompleteTest1() {

        Boolean createInDatabase = true;
        
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(createInDatabase);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
        
        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity o = new OpportunityBuilder()
        					.account(vinnies)
        					.priceBookId(customPricebook.Id).build(createInDatabase);
        System.assert(o.Id <> null,'An error occured while creating Opportunity.');

        
        Product2 product = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001')
        		.build(createInDatabase);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        Product2 bomProduct = new ProductBuilder()
        		.name('BOM-BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001BOM')
        		.build(createInDatabase);
        System.assert(bomProduct.Id <> null,'An error occured while creating Product.');
        
   //Create PricebookEntry for product
        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
        
         //Create PricebookEntry for Custom Pricebook
        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(customPricebook.Id).build(createInDatabase);
        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');

        
        //Create Set Class
        Set_Class__c setClass = new SetClassBuilder()
        								.equipmentNumber('TEST SC101').productCode('03.161.001BOM').build(createInDatabase);
        System.assert(setClass.Id <> null,'An error occured while creating SetClass.');
        
       
        //Create SetClass Items
        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
        								.header(setClass)
        								.product(product)
        								.quantity(1).build(createInDatabase);
        
        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(createInDatabase);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        */
        
        //Create Quote
        //String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
        Quote quote = new QuoteBuilder()
        		.opportunity(o)
        		.priceBookId(customPricebook.Id)
        		.quoteType(GlobalConstants.QuoteTypes.CustomerQuote)
        		.build(createInDatabase);
        System.assert(quote.Id <> null,'An error occured while creating Quote.');
        
        //sync Opportunity and Quote lines.
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
        QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(quote.Id, setClass.Id, 'Kit');
        linePopulator.run();
        for(Database.Saveresult r :linePopulator.saveResultList)
        {
        	System.assert(r.isSuccess(),'Populate Run method failed.');
        }
        linePopulator.retrieveQuote();
        System.assert(linePopulator.quote.QuoteNumber != null, 'Quote number is required by Quotation page after populate');
        System.assert(linePopulator.quote.GrandTotal > 0, 'Quote grand total is required by Quotation page after populate');

    }

}