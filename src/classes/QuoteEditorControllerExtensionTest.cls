@isTest(SeeAllData=true)
private class QuoteEditorControllerExtensionTest {
	
	public class ValueException extends Exception{}

	private static QuoteEditorControllerExtension initExtension() {
		QuoteLineItem qli = TestingTools.getTestQuoteLineItem();
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote(Id=qli.QuoteId));
    	return new QuoteEditorControllerExtension(stdController);
	}
	
	@isTest
    static void testUserPreferences() {
    	QuoteEditorControllerExtension ext = initExtension();
    	System.assertNotEquals(0, ext.getQuoteUserHeaderSettings().length(), 'json Quote tooltip settings are present');
    	System.assertNotEquals(0, ext.getQuoteUserGroupSettings().length(), 'json Group tooltip settings are present');
    	System.assertNotEquals(0, ext.getQuoteUserItemSettings().length(), 'json Item tooltip settings are present');	
    }

	@isTest
    static void testAddItem() {
		QuoteLineItem qli = TestingTools.getTestQuoteLineItem();
		PricebookEntry pbe = [select Product2Id from PricebookEntry where Id = :qli.PricebookEntryId];
		
		System.assertEquals(null, QuoteEditorControllerExtension.addItem(qli.QuoteId, qli.Quote_Line_Group__c, null), 'addItem returns null when no productid is passed');
    	System.assertNotEquals(null, QuoteEditorControllerExtension.addItem(qli.QuoteId, qli.Quote_Line_Group__c, pbe.Product2Id),'item returned normally by add item');
    	
    	// Test that an exception was thrown when null is passed in for groupid
    	try{
    		QuoteLineItem newItem = QuoteEditorControllerExtension.addItem(qli.QuoteId, null, pbe.Product2Id);
    	} catch (QuoteEditorControllerExtension.ValueException e) {
    		System.assertEquals('Could not find any groups for this quote that have a group id of : null',e.getMessage(),'additem returns exception and null when it cant find group to add item to' );
    	}
    	
    	// Send in parameters of null to test a return of null
    	System.assertEquals(null, QuoteEditorControllerExtension.addItem(null, qli.Quote_Line_Group__c, pbe.Product2Id),'addItem returns null when no quoteid is passed');
    
    	//Create a product but don't create pricebookentry - this should cause an exception
    	Product2 product = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL MEDIUM')
	        		.code('99.161.001')
	        		.build(true);
	    System.assert(product.Id <> null,'An error occured while creating Product.');
	    
	    // Test that an exception was thrown when pricebookentry is not found for the product and pricebook
    	try{
    		QuoteLineItem newItem = QuoteEditorControllerExtension.addItem(qli.QuoteId, qli.Quote_Line_Group__c, product.Id);
    	} catch (QuoteEditorControllerExtension.ValueException e) {
    		System.assertEquals('Could not find pricebookentry for product : ' + product.Id, e.getMessage(),'additem throws an exception if the pricebook is missing' );
    	}
    	
    }

	@isTest
    static void testIsRevisable() {
    	QuoteEditorControllerExtension ext = initExtension();
    	System.assert(ext.getQuoteIsRevisable(), 'a new Quote is revisable');
    	
    	List<Quote> quotes = [SELECT Id FROM Quote WHERE Id = :ext.quoteFromStdController.Id LIMIT 1];
    	System.assertEquals(false, quotes.isEmpty(), 'Expected a quote');
    	Quote testQuote = quotes[0];
    	Report testReport = new Report();
    	// Found this in SF docs
    	// In Apex tests, report runs always ignore the SeeAllData annotation, regardless of whether the annotation is set to true or false. 
    	// This means that report results will include pre-existing data that the test didn’t create. 
    	// There is no way to disable the SeeAllData annotation for a report execution. To limit results, use a filter on the report.
    	List<Report> testReports = [SELECT Id FROM Report WHERE Name = 'ProcessLog' LIMIT 1];
		if (testReports.isEmpty() == true) {
			// You can't create a report in a test and give it the correct name - name is set to readonly for Reports
		} else {
			testReport = testReports[0];
		}
    	String processLogurl = ext.getProcessLogURL();
    	System.assertNotEquals(true, StringUtils.isNullOrWhiteSpace(processLogurl), 'Expecting a url for the report and quote passed as parameter to it' );
	    System.assertEquals('/'+testReport.Id+'?pv0=' + testQuote.Id, processLogurl,'Expecting a url for the report and quote passed as parameter to it' ); 
	    
	    // Can't delete report to check that the url is blank
    }
    @isTest
    static void testIsPrintable() {
    	QuoteEditorControllerExtension ext = initExtension();
    	List<Quote> quotes = [SELECT Id FROM Quote WHERE Id = :ext.quoteFromStdController.Id LIMIT 1];
    	System.assertEquals(false, quotes.isEmpty(), 'Expected a quote');
        System.debug('quotes=>'+quotes);
    	List<Attachment> attList  = [select Id from Attachment where parentId = :ext.quoteFromStdController.Id limit 1 ];
        
    	System.assertEquals(0,attList.size(),'Expecting no attachment to the Quote');
    	ext.generateQuoteDocAsAttachment();
	    attList  = [select Id from Attachment where parentId = :ext.quoteFromStdController.Id limit 1 ];
	    System.assertEquals(1,attList.size(),'Expecting no attachment to the Quote');
	    // Can't delete report to check that the url is blank
    }
}