public with sharing class ValidationResultBuilder {

	private APIv001.ValidationResult vr;
	
	public ValidationResultBuilder(APIv001.ValidationResult vr){
		if (vr == null)
			vr = new APIv001.ValidationResult();
			
		this.vr = vr;
	}
	
	public APIv001.ValidationResult addNullOrEmptyError(String objectNameOrAttribute)
	{
		APIv001.Log log = new APIv001.Log();
		log.message = 'value of ' + objectNameOrAttribute + ' is null or empty.';
		log.severity = APIv001.SeverityType.ValidationError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		
		vr.isValid = false;
		
		if (vr.processLogs == null)
			vr.processLogs = new List<APIv001.ProcessLog>();
			
		vr.processLogs.add(processLog);
		
		return this.vr;
	}
	
	public APIv001.ValidationResult addApplicationError(string message){
		
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.ApplicationError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		
		
		vr.isValid = false;
		
		if (vr.processLogs == null)
			vr.processLogs = new List<APIv001.ProcessLog>();
			
		vr.processLogs.add(processLog);
		
		return this.vr;
	}
	
	public APIv001.ValidationResult addSystemError(string message){
		
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.SystemError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		
		
		vr.isValid = false;
		
		if (vr.processLogs == null)
			vr.processLogs = new List<APIv001.ProcessLog>();
			
		vr.processLogs.add(processLog);
		
		return this.vr;
	}
	
	public APIv001.ValidationResult addValidationError(string message){
		
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.ValidationError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		
		
		vr.isValid = false;
		
		if (vr.processLogs == null)
			vr.processLogs = new List<APIv001.ProcessLog>();
			
		vr.processLogs.add(processLog);
		
		return this.vr;
	}
	
	public APIv001.ValidationResult addException(Exception e){

		APIv001.ProcessLog processLog  = null;
		
		if (e != null){
			processLog = new APIv001.ProcessLog();
			processLog.logs = new List<APIv001.Log>();
			
			APIv001.Log exceptionLog = new APIv001.Log();
			exceptionLog.message = 'Line Number: ' + e.getLineNumber() + ' Error Message: ' + e.getMessage();
			exceptionLog.severity = APIv001.SeverityType.SystemError;
			processLog.logs.add(exceptionLog);
			
			APIv001.Log stackTraceLog = new APIv001.Log();
			stackTraceLog.message = 'Exception TypeName: ' + e.getTypeName() + ' StackTraceString: ' + e.getStackTraceString();
			stackTraceLog.severity = APIv001.SeverityType.SystemError;
			processLog.logs.add(stackTraceLog);
			
		}	

		vr.isValid = false;
		
		if (processLog != null){
			if (vr.processLogs == null)
				vr.processLogs = new List<APIv001.ProcessLog>();
		
			vr.processLogs.add(processLog);
		}
		return this.vr;
		
	}
	
	
}