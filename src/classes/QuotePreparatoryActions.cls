public with sharing class QuotePreparatoryActions {
	
	// Some of the following properties are made public to expose them for test class purposes
	public List<Quote> newQuotes = null;
	private Map<Id, Quote> mapOldQuotes = null;
	public Integer daysAfterDocumentDate;
	private Database.SaveResult r;
	public Boolean terminateProcessing = false;
	public Quote_Expiry_Date_Default__c expiryDateRules;
	
	public QuotePreparatoryActions(List<Quote> newQuotes, List<Quote> oldQuotes) {
		this.newQuotes = newQuotes;
		
		if (oldQuotes != null) {
			mapOldQuotes = new Map<Id, Quote>();
			for (Quote oneOldQuote : oldQuotes) {
				mapOldQuotes.put(oneOldQuote.Id, oneOldQuote);			
			}
		}
	}
	
	public void retrieveExpiryDateCustomSetting() {
		if (newQuotes == null || newQuotes.isEmpty())
			return;
			
		try {
			//Get custom settings for delete criteria for quotes
			expiryDateRules = Quote_Expiry_Date_Default__c.getValues('Expiry_Date_Rules');
			
			if (expiryDateRules == null) {
				// Create a custom setting with default value
				Quote_Expiry_Date_Default__c cs = new Quote_Expiry_Date_Default__c();
				cs.Name = 'Expiry_Date_Rules';
				cs.Days_After_Document_Date__c = 30;
				r = Database.insert(cs);
			}
			if (expiryDateRules != null) {
				daysAfterDocumentDate = (Integer)expiryDateRules.Days_After_Document_Date__c;
			} else {
				daysAfterDocumentDate = 30;
			}
			
		} catch (Exception e) {
			system.debug('QuotePreparatoryActions=>' + e);
			
			terminateProcessing = true;
			if (r != null && r.isSuccess() == false) {
				String formattedErrorMessage;
				for (Database.Error err : r.getErrors()) {
		        	formattedErrorMessage += err.getStatusCode() + ': ' + err.getMessage() + ' ';
				}
		        handleErrors(formattedErrorMessage);
			} else {
				handleErrors(e.getMessage());
			}
		}
	}
	
	public void handleErrors(String errorMessage) {
		for(Quote eachQuote : newQuotes) {
		    eachQuote.addError(errorMessage);
		}
	}
	
	public void calculateQuoteExpiryDate() {
		if (newQuotes == null || newQuotes.isEmpty() || terminateProcessing)
			return;
			
		try {	
			for(Quote oneNewQuote : newQuotes) {
				//Determine if the quote is a setbuy - if it is then set the expiration date based on the custom setting
				GlobalConstants.QuoteTypes quoteType = QuoteTools.determineQuotetype(oneNewQuote.QuoteType__c);
				if(mapOldQuotes == null) {  
					//insert trigger 
					if (quoteType == GlobalConstants.QuoteTypes.CustomerQuote || quoteType == GlobalConstants.QuoteTypes.SetRequest
						|| quoteType == GlobalConstants.QuoteTypes.ChangeSet || quoteType == GlobalConstants.QuoteTypes.LoanSetRequest) {
						oneNewQuote.ExpirationDate = oneNewQuote.Document_Date__c.addDays(daysAfterDocumentDate);
					}
				} else {
					// udate trigger and doc date had been changed on setbuy
					if (mapOldQuotes.containsKey(oneNewQuote.Id)){
						Quote oneOldQuote = mapOldQuotes.get(oneNewQuote.Id);
						if ((quoteType == GlobalConstants.QuoteTypes.CustomerQuote || quoteType == GlobalConstants.QuoteTypes.SetRequest 
							|| quoteType == GlobalConstants.QuoteTypes.ChangeSet || quoteType == GlobalConstants.QuoteTypes.LoanSetRequest) 
							&& oneNewQuote.Document_Date__c != oneOldQuote.Document_Date__c) { 
							oneNewQuote.ExpirationDate = oneNewQuote.Document_Date__c.addDays(daysAfterDocumentDate);
						}
					}
				}
			}
		} catch (Exception e) {
			system.debug('QuotePreparatoryActions=>' + e);
			terminateProcessing = true;
			handleErrors(e.getMessage());
		}	
	}
	
	public void autoPopulate() {

		try{		
			retrieveExpiryDateCustomSetting();
			calculateQuoteExpiryDate();
		}
		catch (Exception e) {
			system.debug('QuotePreparatoryActions=>' + e);
			terminateProcessing = true;
			handleErrors(e.getMessage());
		}	
	}
}