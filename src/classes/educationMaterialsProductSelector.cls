public with sharing class educationMaterialsProductSelector {

	public Id educationMaterialsOrderId = ApexPages.currentPage().getParameters().get('emId');
	public String selectedBU;
	public Education_Materials__c theEM	= new Education_Materials__c();
	
	public List<Ed_Materials_List__c> currentProducts = new List<Ed_Materials_List__c>();
	public List<displayList> setList = new List<displayList>();
	public List<displayList> boneList = new List<displayList>();
	public List<displayList> mmList = new List<displayList>();
	
	public Integer boneCount = 0;
	public Integer setCount = 0;
	public Integer mmCount = 0;
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Accessors for Counts
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public Integer getBoneCount() {
		
		return boneCount;
	}
	public Integer getsetCount() {
		
		return setCount;
	}
	public Integer getmmCount() {
		
		return mmCount;
	}
	
	public Education_Materials__c getEm() {
		
		return this.theEm;
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Get selected Products for this Education Material
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
		public educationMaterialsProductSelector() {
		
			currentProducts = [Select Item_Description__c, Quantity__c from Ed_Materials_List__c Where Ed_Materials_Request__c = :educationMaterialsOrderId];
			theEM = [Select Wk_Stations__c, Participants__c, Name, Event_Start_Date__c, Event_End_Date__c From Education_Materials__c Where Id = :educationMaterialsOrderId];
		}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Create picklist of options for the different BUs
	// Current values: Bone, Spine, CMF
	//
	/////////////////////////////////////////////////////////////////////////////////////////////

		public List<SelectOption> getItems() {
           
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Trauma','Trauma'));
            options.add(new SelectOption('Spine','Spine'));
            options.add(new SelectOption('CMF','CMF'));
            return options;
     	}
     	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Accessors for Business Unit
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
		public String getBU() {
    	
    		return selectedBU;
   
   	 	}
            
    	public void setBU(String selectedBU) {
       
       		this.selectedBU = selectedBU;
       		
    	}
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Switch BU method
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public void switchBU() {
		
		currentProducts = [Select Item_Description__c, Quantity__c from Ed_Materials_List__c Where Ed_Materials_Request__c = :educationMaterialsOrderId];
		
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Return to ME
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public PageReference returnToMe() {
		
			PageReference pageRef = new PageReference('/' + educationMaterialsOrderId);
			pageRef.setRedirect(true);
			return pageRef;
			
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Switch BU method
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public void updateList() {
		
		List<Ed_Materials_List__c> mlForUpdate = new List<Ed_Materials_List__c>();
		List<Ed_Materials_List__c> mlForInsert = new List<Ed_Materials_List__c>();
		List<Ed_Materials_List__c> mlForDelete = new List<Ed_Materials_List__c>();
		
		for(displayList thisDL:setList) {
			
			if(thisDL.materialList.Id != null && thisDL.qty  == 0) {

				mlForDelete.add(thisDL.materialList);

			}	
			
			if(thisDL.qty > 0) {
				
				if(thisDL.materialList.Id == null) {
					
					Ed_Materials_List__c ml = new Ed_Materials_List__c();
					ml.Quantity__c = thisDL.qty;
					ml.Item_Description__c = thisDL.prod.id;
					ml.Ed_Materials_Request__c = educationMaterialsOrderId;
					mlForInsert.add(ml);

				}  else {
					
					thisDL.materialList.Quantity__c = thisDL.qty;
					mlForUpdate.add(thisDL.materialList); 
					
				}
			}	
		}
	
		for(displayList thisDL:boneList) {
			
			if(thisDL.materialList.Id != null && thisDL.qty  == 0) {

				mlForDelete.add(thisDL.materialList);

			}	
			
			if(thisDL.qty > 0) {
				
				if(thisDL.materialList.Id == null) {
					
					Ed_Materials_List__c ml = new Ed_Materials_List__c();
					ml.Quantity__c = thisDL.qty;
					ml.Item_Description__c = thisDL.prod.id;
					ml.Ed_Materials_Request__c = educationMaterialsOrderId;
					mlForInsert.add(ml);

				}  else {
					
					thisDL.materialList.Quantity__c = thisDL.qty;
					mlForUpdate.add(thisDL.materialList); 
					
				}
			}	
		}
		
		for(displayList thisDL:mmList) {
			
			if(thisDL.materialList.Id != null && thisDL.qty  == 0) {

				mlForDelete.add(thisDL.materialList);

			}	
			
			if(thisDL.qty > 0) {
				
				if(thisDL.materialList.Id == null) {
					
					Ed_Materials_List__c ml = new Ed_Materials_List__c();
					ml.Quantity__c = thisDL.qty;
					ml.Item_Description__c = thisDL.prod.id;
					ml.Ed_Materials_Request__c = educationMaterialsOrderId;
					mlForInsert.add(ml);

				}  else {
					
					thisDL.materialList.Quantity__c = thisDL.qty;
					mlForUpdate.add(thisDL.materialList); 
					
				}
			}	
			
			
		}
		System.debug('mlForUpdate.size(): ' + mlForUpdate.size());
		System.debug('mlForInsert.size(): ' + mlForInsert.size());
		System.debug('mlForDelete.size(): ' + mlForDelete.size());
		
		if(mlForUpdate.size() >0 ) {update mlForUpdate;}
		if(mlForInsert.size() >0 ) {insert mlForInsert;}
		if(mlForDelete.size() >0 ) {delete mlForDelete;}
			
		currentProducts = [Select Item_Description__c, Quantity__c from Ed_Materials_List__c Where Ed_Materials_Request__c = :educationMaterialsOrderId];
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Get Sets
	//
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<displayList> getSets() {
		
		setList.clear();
		
		String buQuery = '%' + selectedBU + '%';
		List<Ed_Materials_Products__c> productList;
		
		if(selectedBU != null) {
			
			productList = [Select Name, Id,AU_Set__c,Total_Inventory__c From Ed_Materials_Products__c Where BU_2__c Like :buQuery And Category__c = 'Set' And Freq_Used__c = true Order By Name Asc];
		
			setCount = productList.size();
			
			for(Ed_Materials_Products__c thisProd:productList) {
				
				displayList thisSetList  = new displayList(thisProd,currentProducts);
				setList.add(thisSetList);
			}
		}
		
		return setList;
	}
	
	public void setSets(List<displayList> sets) {
		
		this.setList = sets;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Get bones
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<displayList>  getBones() {
		
		boneList.clear();
		String buQuery = '%' + selectedBU + '%';
		List<Ed_Materials_Products__c> productList;
		
		if(selectedBU != null) {
			
			productList = [Select Name, Id,AU_Set__c,Catalog__c From Ed_Materials_Products__c Where BU_2__c Like :buQuery And (Category__c = 'Bone Kit (Spine)' or Category__c = 'Bone') And Freq_Used__c = true Order By Name Asc];
		
			boneCount = productList.size();
			
			for(Ed_Materials_Products__c thisProd:productList) {
				
				displayList thisSetList  = new displayList(thisProd,currentProducts);
				boneList.add(thisSetList);
			}
		} 
		
		return boneList;
	
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Get Marketing Material
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<displayList> getMarketingMaterials() {
	
		mmList.clear();
		String buQuery = '%' + selectedBU + '%';
		List<Ed_Materials_Products__c> productList;
		
		if(selectedBU != null) {
			
			productList = [Select Name, Id,Catalog__c,Category_Detail__c From Ed_Materials_Products__c Where BU_2__c Like :buQuery And Category__c = 'Marketing Material' And Freq_Used__c = true Order By Name Asc];
			
			mmCount = productList.size();
			
			for(Ed_Materials_Products__c thisProd:productList) {
				
				displayList thisSetList  = new displayList(thisProd,currentProducts);
				mmList.add(thisSetList);
			}
		}
		
		return mmList;
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Helper class
	//
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public class displayList {
		
		public boolean slotted = false;
		public Integer qty = 0;
		public Ed_Materials_Products__c prod = new Ed_Materials_Products__c();
		public Ed_Materials_List__c materialList = new Ed_Materials_List__c();
		
		public displayList(Ed_Materials_Products__c productToDisplay, List<Ed_Materials_List__c> currentProducts) {
			
			this.prod = productToDisplay;
			
			if(currentProducts.size() > 0) {
				
				for(Ed_Materials_List__c thisProd:currentProducts) {
				
					if(productToDisplay.Id == thisProd.Item_Description__c)	{
						
						this.slotted = true;
						this.materialList = thisProd;
						this.qty = materialList.Quantity__c.intValue();
					}
				}
			} 
		}
		
		public Integer getqty() {
			
			return this.qty;
		}
		public Void setqty(Integer qty) {
			
			this.qty = qty;
		}

		
		public boolean getSlotted() {
			
			return this.slotted;
		}
		
		public Ed_Materials_List__c getMaterialList() {
			
			return this.materialList;
		}
		
		public Ed_Materials_Products__c getProduct() {
			
			return this.prod;
		}
	}
	
	
	////////////////////////////////////////////
	//// UNIT TESTS
	/////////////////////////////////////////// 
	
	static testMethod void edMatProdSel() {
		
		Education_Materials__c newEm = new Education_Materials__c();
		newEm.Projected_O_Shipping_Date__c = System.Today();
		newEm.Proj_Received_Date_L__c = System.Today();
		insert newEm;
		
		newEm.Projected_O_Shipping_Date__c = System.Today().addDays(-1);
		update newEm;
		delete newEm;	
		
		Education_Materials__c newEm2 = new Education_Materials__c();
		newEm2.Projected_O_Shipping_Date__c = System.Today();
		newEm2.Proj_Received_Date_L__c = System.Today();
		insert newEm2;
		
		Education_Materials__c newEm3 = new Education_Materials__c();
		newEm2.Projected_O_Shipping_Date__c = System.Today();
		newEm2.Proj_Received_Date_L__c = System.Today();
		insert newEm3;
        Ed_Materials_Products__c emp = new Ed_Materials_Products__c();
        emp.BU_2__c = 'TestBU';
        emp.Freq_Used__c = true;
        emp.Category__c = 'TestCat';
        insert emp;
        emp = new Ed_Materials_Products__c();
        emp.BU_2__c = 'TestBU2';
        emp.Freq_Used__c = true;
        emp.Category__c = 'TestCat2';
        insert emp;
		List<Ed_Materials_Products__c> nEmProd = [Select Id,BU_2__c,Category__c from Ed_Materials_Products__c Where Freq_Used__c = true Limit 2 ];
		
		Ed_Materials_List__c nEmList = new Ed_Materials_List__c();
		nEmList.Quantity__c = 1;
		nEmList.Item_Description__c = nEmProd[0].Id;
		nEmList.Ed_Materials_Request__c = newEm2.Id;
		insert nEmList;
		
		PageReference pageRef = Page.mobileProductSelector;
		pageRef.getParameters().put('parent', newEm2.Id);
        pageRef.getParameters().put('bu', nEmProd[0].BU_2__c);
        pageRef.getParameters().put('cat', nEmProd[0].Category__c);
        pageRef.getParameters().put(nEmProd[0].id, '1');
        pageRef.getParameters().put(nEmProd[1].id, '1');
        Test.setCurrentPage(pageRef);                
        mobileProductSelector controller1 = new mobileProductSelector();
       	controller1.getBU();
       	controller1.getCat();
       	controller1.geteducationMaterialsOrderId();
       	controller1.getSets();
       	controller1.getTest();
       	controller1.sub = 'Save';
       	controller1.mobileProductSelectorLoad();
       	
       	PageReference pageRef2 = Page.educationMaterialsProductSelector;
		pageRef2.getParameters().put('emId', newEm2.Id);
        Test.setCurrentPage(pageRef2);                
        educationMaterialsProductSelector controller2 = new educationMaterialsProductSelector();
       	controller2.selectedBU =  nEmProd[0].BU_2__c;
       	controller2.getBU();
       	controller2.getBoneCount();
       	controller2.getBones();
       	controller2.getEm();
       	controller2.getItems();
       	controller2.getMarketingMaterials();
       	controller2.getmmCount();
       	controller2.getsetCount();
       	controller2.getSets();
       	controller2.updateList();
       	controller2.switchBU();
       	
       	controller2.setBU(nEmProd[0].BU_2__c);
		controller2.returnToMe();
       	
			
	}

}