public with sharing class ConsignmentBusinessCaseMasterDataTools {
	
	public static MasterDataAPIv001.ReturnResult upsertConsignmentBusinessCaseMasterRecordsV001(List<MasterDataAPIv001.ConsignmentBusinessCase> cbcMasterRecords){
		MasterDataAPIv001.ReturnResult rr = new MasterDataAPIv001.ReturnResult();
		rr.isSuccessful = true;
		rr.processLogs = new List<MasterDataAPIv001.ProcessLog>();
			
		ConsignmentBusinessCaseMasterDataV001 cbcMasterDataLoad = new ConsignmentBusinessCaseMasterDataV001();
		MasterDataAPIv001.ProcessLog pl = cbcMasterDataLoad.run(cbcMasterRecords);
		if (pl != null)
			rr.processLogs.add(pl);
		rr.isSuccessful = pl != null && pl.errorCount != null && pl.errorCount > 0 ? false : true;
		return rr;
	}

	public static void upsertConsignmentBusinessCaseMasterRecordsAndSaveProcessLogV001(List<MasterDataAPIv001.ConsignmentBusinessCase> cbcMasterRecords)  {
		
		ConsignmentBusinessCaseMasterDataV001 cbcMasterDataLoad = new ConsignmentBusinessCaseMasterDataV001();
		MasterDataAPIv001.ProcessLog pl = cbcMasterDataLoad.run(cbcMasterRecords);
		if (pl != null){
			ProcessLogTools.insertProcessLog(ProcessLogCommon.SourceObject.CBC, ProcessLogCommon.SourceProcess.CBCMasterDataLoad, pl);
		}

	}
}