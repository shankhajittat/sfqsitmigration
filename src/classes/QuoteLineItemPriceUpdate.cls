public with sharing class QuoteLineItemPriceUpdate {

	
	public APIv001.QuotePriceRequest quotePriceRequest = null;
	public List<QuoteLineItem> quoteLinesToUpdate = null;

	public Id processLogId {get; private set;}	
	private ProcessLogBuilder plBuilder = null;
	private Id sourceRecordId = null;
	private String sourceRecordNumber = null;
	private ProcessLogCommon.SourceObject sourceObject = ProcessLogCommon.SourceObject.Quote;
	
	public QuoteLineItemPriceUpdate(APIv001.QuotePriceRequest quote)
	{
		//system.debug('WM APIv001.QuotePriceRequest=>' + quote);
		
		quotePriceRequest = quote;
		
		String title = 'Quote Price SAP Response - ' + DateTime.now();
		ProcessLogCommon.SourceProcess sourceProcess = ProcessLogCommon.SourceProcess.QuoteGetPrices;
		sourceRecordId = quotePriceRequest != null ? quotePriceRequest.id : null;
		sourceRecordNumber = quotePriceRequest != null ? quotePriceRequest.quoteNumber : null;
			
		plBuilder = new ProcessLogBuilder(title,sourceObject,sourceProcess,sourceRecordId,sourceRecordNumber);

	}
	
	private String getProcessLogAsText(List<APIv001.ProcessLog> apiProcessLogs){
		String processLogAsText = '';
		for(APIv001.ProcessLog pl : apiProcessLogs){
			if (pl.logs == null) continue;
			for(APIv001.Log log : pl.logs){
				String message =  log.message == null ? '' : log.message;
				if (StringUtils.isNullOrWhiteSpace(message)) continue;
				
				String errorCode = log.code == null ? 'None' : String.valueOf(log.code);
				String messageWithErrorCode = message + ', ErrorCode: ' + 	errorCode;		
				processLogAsText = processLogAsText.length() > 0 ? '\n' + messageWithErrorCode : messageWithErrorCode;
			}
		}
		return processLogAsText;
	}
	
	public void buildQuoteLinesToUpdate()
	{
		if (quotePriceRequest == null){
			plBuilder.addWarning(null, null, 'QuotePrice Response object is null.', ProcessLogCommon.SourceObject.Quote, null, null);
			return;
		}
		
		if (quotePriceRequest.validationResult != null && quotePriceRequest.validationResult.processLogs != null && quotePriceRequest.validationResult.processLogs.size() > 0){
			plBuilder.addAPIv001Log(sourceRecordId, sourceRecordNumber,sourceObject,quotePriceRequest.validationResult.processLogs);
		}
		
		if (quotePriceRequest.Lines == null){
			plBuilder.addWarning(null, null, 'QuotePrice Response object does not have any items to process.', ProcessLogCommon.SourceObject.Quote, null, null);
			return;
		}
		
		quoteLinesToUpdate = new List<QuoteLineItem>();

		for(APIv001.QuoteLineItemPriceRequest qpd : quotePriceRequest.Lines)
		{
			if (qpd.Id == null) continue;
			
			system.debug('quotePriceRequest.Line=>' + qpd);
			
			QuoteLineItem ql = new QuoteLineItem();
			ql.Id = qpd.Id;
			if (qpd.validationResult != null && qpd.validationResult.processLogs != null && qpd.validationResult.processLogs.size() > 0)
				plBuilder.addAPIv001Log(qpd.Id, quotePriceRequest.quoteNumber,ProcessLogCommon.SourceObject.QuoteLineItem,qpd.validationResult.processLogs);
				
			if (qpd.unitPrice != null && qpd.unitPrice >= 0)
			{
				ql.Tax_Rate__c = qpd.taxRate == null ? 0 : qpd.taxRate;
				ql.Price_Status__c = 'Validated';
				//Following commented since it is not mapped from SAP, initially it was mapped with GP interface.
				//ql.Standard_Cost__c = qpd.unitStandardCost == null ? 0 : qpd.unitStandardCost;
				//ql.Landed_Cost__c = qpd.unitLandedCost == null ? 0 : qpd.unitLandedCost;
				
				//SFDC QuoteLine Standard ListPrice field is not updatable, so using a custom field.
				double netValue = qpd.unitPrice != null ? qpd.unitPrice : 0.0;
				double unitNetValue = qpd.quantity > 0 ? netValue / qpd.quantity : netValue;
				
				double unitContractPrice = qpd.unitContractPrice != null ? qpd.unitContractPrice : 0.0;
				String discountCode = StringUtils.isNullOrWhiteSpace(qpd.discountCode) ? '' : qpd.discountCode;
				
				ql.UnitListPrice__c = qpd.unitListPrice == null ? 0.0 : qpd.unitListPrice;
				ql.Currency_Code__c = qpd.currencyCode == null ? '' : qpd.currencyCode;
				ql.UOM__c = qpd.uom != null ? qpd.uom : '';
				ql.SAP_Discount__c = qpd.discount != null ? qpd.discount : 0.0;
				ql.SAP_NetValue__c = netValue;
				ql.SAP_DiscountCode__c = discountCode;
				ql.Unit_ContractPrice__c = unitContractPrice;
				ql.Contract_PriceCode__c = StringUtils.isNullOrWhiteSpace(qpd.contractPriceCode) ? '' : qpd.contractPriceCode;
								
				//if unitNetValue (SAP NetValue/Qty) < SAP Unit Contract Price
				if (unitNetValue < unitContractPrice){
					ql.UnitPrice = unitNetValue; //unitNetValue (SAP NetValue/Qty)
					ql.Contract_PriceCode__c =  discountCode; //override the ContractPrice code with Discount Code.
				}
				else{
					if (!StringUtils.isNullOrWhiteSpace(qpd.contractPriceCode) 
							&& qpd.unitContractPrice != null) {
						ql.UnitPrice = unitContractPrice;
						}
					else {
						ql.UnitPrice = unitNetValue; //unitNetValue (SAP NetValue/Qty)
					}					
				}
				ql.ValidationMessage__c = '';
			}
			else if (qpd.validationResult != null && qpd.validationResult.isValid == false
				&& qpd.unitPrice == null)
			{
				ql.Price_Status__c = 'Invalid';
				if (qpd.validationResult != null && qpd.validationResult.processLogs != null && qpd.validationResult.processLogs.size() > 0)
					ql.ValidationMessage__c = getProcessLogAsText(qpd.validationResult.processLogs);
				else
					ql.ValidationMessage__c = '';
			}
			else if (qpd.unitPrice == null)
			{
				ql.Price_Status__c = 'Revalidate';
				if (qpd.validationResult != null && qpd.validationResult.processLogs != null && qpd.validationResult.processLogs.size() > 0)
					ql.ValidationMessage__c = getProcessLogAsText(qpd.validationResult.processLogs);
				else
					ql.ValidationMessage__c = '';
			}
			quoteLinesToUpdate.add(ql);
		}
	}
	
	public List<Database.Saveresult> updateQuoteLines()
	{
		// create a boolean to catch any errors in case we need to rollback
		List<Database.Saveresult> saveResults = new List<Database.Saveresult>();
		
		//Update QuoteLineItems
		if (quoteLinesToUpdate != null && quoteLinesToUpdate.isEmpty() == false)
			saveResults = Database.update(quoteLinesToUpdate);
		
		for (Database.SaveResult sr : saveResults) {
		    if (sr.isSuccess()) {
		        System.debug('Successfully updated QuoteLine price details for QuoteLineId: ' + sr.getId());
		    }
		    else {
		        // Operation failed, so get all errors
		        System.debug('The following error has occurred while updating Quote Item(s) with price details=>' + sr.getErrors());  
		        plBuilder.addDatabaseErrors(sr.getId(), null, sr.getErrors(),ProcessLogCommon.SourceObject.QuoteLineItem); 
			}
		}
		
		return saveResults;
		
	}

	
	public APIv001.ReturnResult updatePriceDetails()
	{
		//system.debug('WebMethod Response PriceRequestObject=>' + this.quotePriceRequest);
		
		APIv001.ReturnResult result = new APIv001.ReturnResult();
		result.isSuccessful = true;
		
		ReturnResultBuilder rr = new ReturnResultBuilder(result);
		
		if (quotePriceRequest == null || quotePriceRequest.Lines == null){
			
			return result;
		}
		
		try {
			buildQuoteLinesToUpdate();

			List<Database.Saveresult> saveResults = updateQuoteLines();
			result = rr.addSaveResults(saveResults);
			
			system.debug('QuoteLineItemPriceUpdate.updatePriceDetails ReturnResult: ' + result);
			
		}
		catch (Exception e){
			system.debug('updatePriceDetails->updatePriceDetails:' + e);
			result = rr.addException('',e);
			plBuilder.addException(null,null, e,ProcessLogCommon.SourceObject.QuoteLineItem);
		}

		try{
			if (quotePriceRequest != null 
				&& quotePriceRequest.id != null 
				&& StringUtils.isNullOrWhiteSpace(quotePriceRequest.id) == false){
						
				Quote updateQuote = new Quote();
				updateQuote.Id = quotePriceRequest.id;
				updateQuote.Is_SAP_Request_IsActive__c = false;
				if (plBuilder.isValid()){
					updateQuote.SAP_Request_Response_Message__c = GlobalConstants.SFDC_SAP_SUCCESSFUL_RESPONSE_RECEIVED_FROM_SAP;
				}
				else{
					updateQuote.SAP_Request_Response_Message__c = GlobalConstants.SFDC_SAP_INVALID_RESPONSE_RECEIVED_FROM_SAP;
				}
				update updateQuote;
			}
		}
		catch (Exception e){
			system.debug('updatePriceDetails:updateQuote:' + e);
			result = rr.addException('',e);
			plBuilder.addException(null,null, e,ProcessLogCommon.SourceObject.QuoteLineItem);
		}
		
		processLogId = plBuilder.insertProcessLog();
			
		
		return result;
	}

}