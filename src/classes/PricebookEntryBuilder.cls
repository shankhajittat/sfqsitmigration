public with sharing class PricebookEntryBuilder {

//Mandatory fields with defaults
	private Boolean isActive = true;
	private Boolean useStandardPrice = false;
	private Double defaultUnitPrice = 100.00;
	
	private Id priceBookId;
	private Id productId;

	public PricebookEntryBuilder productId(Id productId)
	{
		this.productId = productId;
		return this;
	}
	
	
	public PricebookEntryBuilder pricebookId(Id priceBookId)
	{
		this.priceBookId = priceBookId;
		return this;
	}


	public PricebookEntry build(Boolean createInDatabase)
	{
		PricebookEntry pbe = new PricebookEntry(
					Product2Id = this.productId,
					Pricebook2Id = priceBookId,
					UnitPrice = 100.00,
					IsActive = isActive,
					UseStandardPrice = useStandardPrice,
					AvailableForSale__c = true
					);
			
		if (createInDatabase)
		{
			insert pbe;
		}
		
		return pbe;
	}
	
}