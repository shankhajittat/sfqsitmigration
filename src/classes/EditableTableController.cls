public class EditableTableController {

	public interface EditableTablePlugin {
		List<EditableTableGroup> getGroups(String parentId);
		SObject setValue(String rowId, String field, String value);
		List<SObject> setValues(Map<Id, String> rowToValue, String field);
		String deleteItems(List<Id> itemsToDelete);
		String deleteGroups(List<Id> itemsToDelete);
		Boolean getItemPricing(String quoteId, List<String> itemsForPricing);
		Boolean getGroupPricing(String quoteId, String groupForPricing);
		Boolean updateSetGroupProperty(Id recordId, String fieldName,String fieldValue);
		Boolean distributeDiscountAmountToTheQuote(String quoteId, String discountAmount);
	} 
	
	private static EditableTablePlugin createPlugin(String plt) {
        Type t = Type.forName(plt);
        return (EditableTablePlugin)t.newInstance();
	} 
	
	@RemoteAction
    public static List<EditableTableGroup> getGroups(String pluginType, String parentId) {
        return createPlugin(pluginType).getGroups(parentId);
    }

	@RemoteAction
    public static SObject setValue(String pluginType, String rowId, String field, String value) {
        return createPlugin(pluginType).setValue(rowId, field, value);
    }

	@RemoteAction
    public static List<SObject> setValues(String pluginType, Map<Id, String> rowToValue, String field) {
        return createPlugin(pluginType).setValues(rowToValue, field);
    }
    
    @RemoteAction
    public static String deleteItems(String pluginType, List<String> itemsToDelete) {
        return createPlugin(pluginType).deleteItems(itemsToDelete);
    }

	@RemoteAction
    public static String deleteGroups(String pluginType, List<String> groupsToDelete) {
        return createPlugin(pluginType).deleteGroups(groupsToDelete);
    }
    
    @RemoteAction
    public static Boolean getGroupPricing(String pluginType, String quoteId, String groupForPricing) {
        return createPlugin(pluginType).getGroupPricing(quoteId, groupForPricing);
    }
    
    @RemoteAction
    public static Boolean getItemPricing(String pluginType, String quoteId, List<String> itemsForPricing) {
        return createPlugin(pluginType).getItemPricing(quoteId, itemsForPricing);
    }
    
	@RemoteAction
    public static Boolean updateSetGroupProperty(String pluginType, Id recordId, String fieldName,String fieldValue) {
        return createPlugin(pluginType).updateSetGroupProperty(recordId,fieldName,fieldValue);
    }

    @RemoteAction
    public static Boolean distributeDiscountAmountToTheQuote(String pluginType, String quoteId, String discountAmount) {
        return createPlugin(pluginType).distributeDiscountAmountToTheQuote(quoteId, discountAmount);
    }
    
}