public with sharing class ReturnResultBuilder {
	private APIv001.ReturnResult rr;
	
	public ReturnResultBuilder(APIv001.ReturnResult rr){
		if (rr == null){
			rr = new APIv001.ReturnResult();
			rr.isSuccessful = true;
		}
		this.rr = rr;
	}
	
	public APIv001.ReturnResult addInformation(String recordId, String message)
	{
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.Information;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		if (!StringUtils.isNullOrWhiteSpace(recordId))
			processLog.recordId = recordId;
			
		if (rr.processLogs == null)
			rr.processLogs = new List<APIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	
	public APIv001.ReturnResult addWarning(String recordId, String message)
	{
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.Warning;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		if (!StringUtils.isNullOrWhiteSpace(recordId))
			processLog.recordId = recordId;
			
		if (rr.processLogs == null)
			rr.processLogs = new List<APIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public APIv001.ReturnResult addNullOrEmptyError(String recordId, String objectNameOrAttribute)
	{
		APIv001.Log log = new APIv001.Log();
		log.message = 'value of ' + objectNameOrAttribute + ' is null or empty.';
		log.severity = APIv001.SeverityType.ValidationError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		if (!StringUtils.isNullOrWhiteSpace(recordId))
			processLog.recordId = recordId;
			
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<APIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public APIv001.ReturnResult addApplicationError(String recordId, String message){
		
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.ApplicationError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		if (!StringUtils.isNullOrWhiteSpace(recordId))
			processLog.recordId = recordId;
		
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<APIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public APIv001.ReturnResult addSystemError(String recordId, String message){
		
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.SystemError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		if (!StringUtils.isNullOrWhiteSpace(recordId))
			processLog.recordId = recordId;
		
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<APIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public APIv001.ReturnResult addValidationError(String recordId, String message){
		
		APIv001.Log log = new APIv001.Log();
		log.message = message != null ? message : '';
		log.severity = APIv001.SeverityType.ValidationError;
				
		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
		processLog.logs = new List<APIv001.Log>{ log };
		if (!StringUtils.isNullOrWhiteSpace(recordId))
			processLog.recordId = recordId;
		
		rr.isSuccessful = false;
		
		if (rr.processLogs == null)
			rr.processLogs = new List<APIv001.ProcessLog>();
			
		rr.processLogs.add(processLog);
		
		return this.rr;
	}
	
	public APIv001.ReturnResult addException(String recordId, Exception e){

		APIv001.ProcessLog processLog  = null;
		
		if (e != null){
			processLog = new APIv001.ProcessLog();
			if (!StringUtils.isNullOrWhiteSpace(recordId))
				processLog.recordId = recordId;
			
			processLog.logs = new List<APIv001.Log>();
			
			APIv001.Log exceptionLog = new APIv001.Log();
			exceptionLog.message = 'Line Number: ' + e.getLineNumber() + ' Error Message: ' + e.getMessage();
			exceptionLog.severity = APIv001.SeverityType.SystemError;
			processLog.logs.add(exceptionLog);
			
			APIv001.Log stackTraceLog = new APIv001.Log();
			stackTraceLog.message = 'Exception TypeName: ' + e.getTypeName() + ' StackTraceString: ' + e.getStackTraceString();
			stackTraceLog.severity = APIv001.SeverityType.SystemError;
			processLog.logs.add(stackTraceLog);
			
		}	

		rr.isSuccessful = false;
		
		if (processLog != null){
			if (rr.processLogs == null)
				rr.processLogs = new List<APIv001.ProcessLog>();
		
			rr.processLogs.add(processLog);
		}
		return this.rr;
		
	}
	
	public APIv001.ReturnResult addSaveResults(List<Database.Saveresult> saveResults)
	{
		boolean isSuccessful = true;
		
		List<APIv001.ProcessLog> processLogs = new List<APIv001.ProcessLog>();
		for(Database.Saveresult r :saveResults)
        {
        	if (r.isSuccess() == false)
        	{
        		isSuccessful = false;
        		
        		APIv001.ProcessLog processLog = new APIv001.ProcessLog();
        		processLog.recordId = r.getId();
        		processLog.logs = APIv001Tools.transformSaveResultErrorsIntoListOfApiLogs(r.getErrors());
        		processLogs.add(processLog);
        	}
        }
		

		if (rr.isSuccessful && isSuccessful == false)
			rr.isSuccessful = isSuccessful;	/// need to look into collection
		if (!isSuccessful)
			rr.processLogs = processLogs;
		
		return rr;
	}
	
	
	
}