@isTest(SeeAllData=true)  // This is needed because Push topics are metadata and not treated the same way as normal sObjects - necessary for test method testSeedPushTopics
public class MaintainConfigurationDataTest {
	
	/*
	// This is a copy of the defaults that appear in MaintainConfigurationData - if the defaults are changed then change this too to complete the test.
	public static Map<String, String> defaultPushTopics = new Map<String, String>
		{'LineItemsWithValidPrice' => 'Select Id, UnitPrice , Price_Status__c, Group_Status__c, Total_Standard_Cost__c, Total_Price_IncTax__c, Total_Price_ExTax__c, Total_Landed_Cost__c, Unit_Discount_Amount__c, Unit_Net_Price__c, ListPrice, Subtotal, QuoteId, Discount, Discount_Amount__c, TotalPrice, Gross_Margin_Amount__c, Gross_Margin_Percent__c, Extended_Unit_Discount__c, Net_Margin_Percent__c,Net_Margin_Amount__c, Landed_Cost__c, Tax_Rate__c, Standard_Cost__c, Unit_Tax__c, Total_Tax__c, Contract_PriceCode__c, Unit_ContractPrice__c, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'Validated\',\'Revalidate\',\'Invalid\')',
		 'GetQuoteErpPricing' => 'Select Id from Quote where Status = \'Submitted for Pricing\'',
		 'RefreshQuoteWhenCompleted' => 'Select Id, Status from Quote where Status = \'Completed\'',
		 'CreateSalesOrder' => 'Select Id from Quote where Status = \'Approved\''};
	
	
	static testMethod void testUpdatePushTopics() {
		// Create push topic
		PushTopic lineItemsWithValidPricePushTopic = new PushTopic();
		lineItemsWithValidPricePushTopic.Name = 'LineItemsWithValidPrice1';
		lineItemsWithValidPricePushTopic.Query = 'Select Id, QuoteId, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'Validated\')';
		lineItemsWithValidPricePushTopic.ApiVersion = 29.0;
		insert lineItemsWithValidPricePushTopic;
		
		// Test method to update push topic
		MaintainConfigurationData.updatePushTopics('LineItemsWithValidPrice1', 'Select Id, QuoteId, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'Nonsense\')');
		List<PushTopic> thePushTopics = [Select  p.Query,  p.Name, p.Id 
										From PushTopic p
										Where p.Name = 'LineItemsWithValidPrice1' LIMIT 1];
		System.assert (thePushTopics.isEmpty() == false, 'The push topic LineItemsWithValidPrice1 should exist');
		PushTopic thePushTopic = thePushTopics[0];
		System.assertEquals(thePushTopic.Query , 'Select Id, QuoteId, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'Nonsense\')', 
			'The push topic has not been updated');
	}
	
	
	static testMethod void testCreatePushTopic() {
		
		MaintainConfigurationData.createPushTopic('NonsensePushTopic', 'Select Id, QuoteId, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'More Nonsense\')');
		
	    List<PushTopic> thePushTopics = [Select  p.Query,  p.Name, p.Id 
										From PushTopic p
										Where p.Name = 'NonsensePushTopic' LIMIT 1];
		System.assert (thePushTopics.isEmpty() == false, 'The push topic should have been created');
		PushTopic thePushTopic = thePushTopics[0];
		System.assertEquals(thePushTopic.Query , 'Select Id, QuoteId, Extended_Unit_Price__c from QuoteLineItem where Price_Status__c in (\'More Nonsense\')', 
			'The push topic has not been updated');
	}
	
	
	static testMethod void testSeedPushTopics() {
		
		MaintainConfigurationData.seedPushTopics(true, null, null);
		
		for (String topicName : defaultPushTopics.keySet()) {
		    String theQuery = defaultPushTopics.get(topicName);
		    List<PushTopic> thePushTopics = [Select  p.Query,  p.Name, p.Id 
											From PushTopic p
											Where p.Name = :topicName LIMIT 1];
			System.assert(thePushTopics.isEmpty() == false, 'The push topic has not been created/updated : ' + topicName);
			
			PushTopic thePushTopic = thePushTopics[0];
			System.assertEquals(thePushTopic.Query,theQuery, 'The Push Topic query has not been updated to the default query' );
		}
		
	}
	
	
	static testMethod void testCreateQuoteTransactionTypes() {
		
		MaintainConfigurationData.createQuoteTransactionTypes('Test Transaction');
		List<Quote_Transaction_Type__c> transactionTypes = [Select t.Id 
															From Quote_Transaction_Type__c t
															Where t.Name = 'Test Transaction' LIMIT 1];
		System.assert(transactionTypes.isEmpty() == false, 'The transaction type has not been created but should have been');
		
	}
	
	
	
	static testMethod void testSeedCustomSettingQuoteTypes() {
		//Create a Quote_Tranaction_Type__c record
		MaintainConfigurationData.createQuoteTransactionTypes('Test Transaction2');
		List<Quote_Transaction_Type__c> transactionTypes = [Select t.Id , t.Name
															From Quote_Transaction_Type__c t
															Where t.Name = 'Test Transaction2' LIMIT 1];
		System.assert(transactionTypes.isEmpty() == false, 'The transaction type Test Transaction2 has not been created but should have been');
		
		
		MaintainConfigurationData.seedCustomSettingQuoteTypes(transactionTypes[0].Id, transactionTypes[0].Name, 0);
		Quote_Types__c quoteTypes = Quote_Types__c.getValues(transactionTypes[0].Name);
		System.assertNotEquals(null, quoteTypes, 'Custom settings for Quote_Types__c that match transaction type Test Transaction2 should have been created');
		
		
	}
	
	
	
	static testMethod void testSeedDelete_Quote_Criteria_CustomSetting() {
		
		Delete_Quote_Criteria__c deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
		if (deleteQuoteCriteria != null) {
			delete deleteQuoteCriteria;
		}
		MaintainConfigurationData.seedDelete_Quote_Criteria_CustomSetting();
		
		// Custom settings Delete_Quote_Criteria__c should be populated 
		deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
		System.assertNotEquals(null, deleteQuoteCriteria, 'Custom settings for Delete_Quote_Criteria__c should have been created');
		
		
	}
	
	
	
	static testMethod void testRun() {
		
		MaintainConfigurationData.run();
		
		// Push topics should be seeded
		for (String topicName : defaultPushTopics.keySet()) {
		    String theQuery = defaultPushTopics.get(topicName);
		    List<PushTopic> thePushTopics = [Select  p.Query,  p.Name, p.Id 
											From PushTopic p
											Where p.Name = :topicName LIMIT 1];
			System.assert(thePushTopics.isEmpty() == false, 'testRun: The push topic has not been created/updated : ' + topicName);
			
			PushTopic thePushTopic = thePushTopics[0];
			System.assertEquals(thePushTopic.Query,theQuery, 'testRun: The Push Topic query has not been updated to the default query' );
			
			
		}
		
		// Transaction type Set Buy and custom setting for it should be seeded
		List<Quote_Transaction_Type__c> transactionTypes = [Select t.Id, t.Name From Quote_Transaction_Type__c t Where t.Name = 'Set Buy' LIMIT 1];
		System.assert(transactionTypes.isEmpty() == false, 'The transaction type Set Buy has not been created but should have been');
		Quote_Types__c quoteTypes = Quote_Types__c.getValues(transactionTypes[0].Name);
		System.assertNotEquals(null, quoteTypes, 'Custom settings for Quote_Types__c that match transaction type Set Buy should have been created');
		
		
		
		// Transaction type CBC and custom setting for it should be seeded
		transactionTypes = [Select t.Id, t.Name From Quote_Transaction_Type__c t Where t.Name = 'CBC' LIMIT 1];
		System.assert(transactionTypes.isEmpty() == false, 'The transaction type CBC has not been created but should have been');
		quoteTypes = Quote_Types__c.getValues(transactionTypes[0].Name);
		System.assertNotEquals(null, quoteTypes, 'Custom settings for Quote_Types__c that match transaction type Set Buy should have been created');
		
		
		// Transaction type Surgeon Preference and custom setting for it should be seeded
		transactionTypes = [Select t.Id, t.Name From Quote_Transaction_Type__c t Where t.Name = 'Surgeon Preference' LIMIT 1];
		System.assert(transactionTypes.isEmpty() == false, 'The transaction type Surgeon Preference has not been created but should have been');
		quoteTypes = Quote_Types__c.getValues(transactionTypes[0].Name);
		System.assertNotEquals(null, quoteTypes, 'Custom settings for Quote_Types__c that match transaction type Set Buy should have been created');
		
		
		
		// Custom settings Delete_Quote_Criteria__c should be populated 
		Delete_Quote_Criteria__c deleteQuoteCriteria = Delete_Quote_Criteria__c.getValues('Delete_Quote_Rules');
		System.assertNotEquals(null, deleteQuoteCriteria, 'Custom settings for Delete_Quote_Criteria__c should have been created');
			
	}
	*/
	
	
}