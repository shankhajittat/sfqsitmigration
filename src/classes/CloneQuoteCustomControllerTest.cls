@isTest(SeeAllData=true)
private class CloneQuoteCustomControllerTest {

    @isTest
    public static void testCloneQuoteCustomController_CopyQuote()
    {
    	/*
    		Test CopyQuote page and CloneQuoteCustomController
       	*/
    	PageReference pageCopyQuote = Page.CopyQuote;
    	Test.setCurrentPage(pageCopyQuote);
    	
    	// Test constructor
    	CloneQuoteCustomController customCloneController = new CloneQuoteCustomController();
    	PageReference nextPage = customCloneController.chosenAccount();
    	// Verify that page fails without parameters
        System.assertEquals(null, nextPage, 'chosenAccount should have thrown an exception');
        
    	/*
    		Create a Quote to test CopyQuote and Revise Quote
    	*/
    	String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
        // Test page instantiation
        PageReference pageRef = Page.QuoteCreate;
		pageRef.getParameters().put('accId', vinnies.Id);
		pageRef.getParameters().put('quoteType', '0'); 		//Set Buy - see GlobalConstants.QuoteTypes
        Test.setCurrentPage(pageRef);
        
    	// This constructor is tested in NewQuoteControllerTest.cls
    	NewQuoteController ext = new NewQuoteController();
    	
		// Test that opportunity and quote have been created and linked and the page reference is set up to redirect to quoteeditor page
		PageReference urlAfterCreateQuote = ext.createQuote();
		System.assertNotEquals(null, urlAfterCreateQuote, 'A new url should have been generated');
    	System.assertEquals('/apex/quoteeditor?id='+ext.extensionQuote.Id, urlAfterCreateQuote.getUrl(),'New url is incorrect');
		System.assertNotEquals(null, ext.extensionOpportunity, 'A new opportunity should have been generated');
		System.assertNotEquals(null, ext.extensionQuote, 'A new quote should have been generated');		
		    	
    	
    	/*
    		Test CopyQuote
    	*/
    	pageCopyQuote = Page.CopyQuote;
    	//Set the Parameter 
    	pageCopyQuote.getParameters().put('id', ext.extensionQuote.Id);
    	Test.setCurrentPage(pageCopyQuote);
    	
    	customCloneController = new CloneQuoteCustomController();
    	system.assert(customCloneController.sourceQuote.Id == ext.extensionQuote.Id, 'The default account being copied TO should be set up in the constructor to be the same account as the quote being copied FROM');
    	
    	nextPage = customCloneController.chosenAccount();
 		System.debug('Copy NextPage.getUrl()>>>>' + nextPage);
    	System.assertEquals('/apex/quoteeditor?id='+ customCloneController.newQuoteId, nextPage.getUrl(),'new url should be the same page');
    	System.assert(customCloneController.newQuoteId != null, 'copyQuote method failed.');
    	System.debug('Copy New QuoteId>>>:' + customCloneController.newQuoteId );
    	
    	//Set an invalid value for the id Parameter to fail.
    	try{
			pageCopyQuote = Page.CopyQuote;
	    	pageCopyQuote.getParameters().put('id', '');
	    	Test.setCurrentPage(pageCopyQuote);
	    	customCloneController = new CloneQuoteCustomController();
	    	nextPage = customCloneController.chosenAccount();
	 		System.debug('CopyQuote NextPage.getUrl()>>>>' + nextPage);
	 		System.assertEquals(null, nextPage,'new url should be null');
    	}
    	Catch(Exception e){
    		System.debug('Copy Unpected Exception>>>> 1 : ' + e);
    		System.assert(e != null, 'Was expected to receive Invalid Id error.');
    	}
    	
    	
    	//Delete Quote and Opportuntity to throw unpexpected error.
    	Quote deleteQuote = new Quote();
    	deleteQuote.Id = ext.extensionQuote.Id;
    	delete deleteQuote;
    	
    	//Trying to copy a deleted Quote,caught the exception in the parent method - QuoteTools.CloneQuotationAndRelatedRecords
    	pageCopyQuote = Page.CopyQuote;
    	pageCopyQuote.getParameters().put('id', ext.extensionQuote.Id);
    	Test.setCurrentPage(pageCopyQuote);
    	customCloneController = new CloneQuoteCustomController();
    	nextPage = customCloneController.chosenAccount();
 		System.debug('CopyQuote NextPage.getUrl()>>>>' + nextPage);
 		System.assertEquals(null, nextPage,'new url should be null');
    	    	
    }
    
    @isTest
    public static void testCloneQuoteCustomController_ReviseQuote()
    {    	
    	/*
    		Create a Quote to test CopyQuote and Revise Quote
    	*/
    	//Create an account
		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);

        // Test page instantiation
        PageReference pageRef = Page.QuoteCreate;
		pageRef.getParameters().put('accId', vinnies.Id);
		pageRef.getParameters().put('quoteType', '0'); 		//Set Buy - see GlobalConstants.QuoteTypes
        Test.setCurrentPage(pageRef);
        
		// Test constructor
    	NewQuoteController ext = new NewQuoteController();
    	
		// Test that opportunity and quote have been created and linked and the page reference is set up to redirect to quoteeditor page
		PageReference urlAfterCreateQuote = ext.createQuote();
		System.assertNotEquals(null, urlAfterCreateQuote, 'A new url should have been generated');
    	System.assertEquals('/apex/quoteeditor?id='+ext.extensionQuote.Id, urlAfterCreateQuote.getUrl(),'New url is incorrect');
		System.assertNotEquals(null, ext.extensionOpportunity, 'A new opportunity should have been generated');
		System.assertNotEquals(null, ext.extensionQuote, 'A new quote should have been generated');		
		
		//Update Quote Status as QS_STATUS_REJECTED to test Revise.
    	Quote updateQuote = new Quote();
    	updateQuote.Id = ext.extensionQuote.Id;
    	updateQuote.Status = GlobalConstants.QS_STATUS_REJECTED;
    	update updateQuote;
    	
    	    	
    	/*
    		Test ReviseQuote
    	*/
    	PageReference pageReviseQuote = Page.ReviseQuote;
    	Test.setCurrentPage(pageReviseQuote);
    	
    	CloneQuoteCustomController customCloneController = new CloneQuoteCustomController();
    	PageReference nextPage = customCloneController.reviseQuote();
    	// Verify that page fails without parameters
        System.assertEquals(null, nextPage);

		//Set the Parameter
		pageReviseQuote = Page.ReviseQuote;
    	pageReviseQuote.getParameters().put('id', ext.extensionQuote.Id);
    	Test.setCurrentPage(pageReviseQuote);
    	
    	// Try to test catch block in revise quote method by deleting the sourcequote id to force it to fail
    	customCloneController = new CloneQuoteCustomController();
    	customCloneController.sourceQuoteId = null;
    	nextPage = customCloneController.reviseQuote();
    	System.assertEquals(null, nextPage,'new url should be the same page');
    	
    	// Test valid revise quote
    	customCloneController = new CloneQuoteCustomController();
    	nextPage = customCloneController.reviseQuote();
 		System.debug('Revise NextPage.getUrl()>>>>' + nextPage);
 		System.assertEquals('/apex/quoteeditor?id='+ customCloneController.newQuoteId, nextPage.getUrl(),'new url should be the same page');
    	System.assert(customCloneController.newQuoteId != null, 'reviseQuote method failed.');
    	System.debug('Revise New QuoteId>>>:' + customCloneController.newQuoteId );
    	
    	// Test cancel button
    	PageReference cancelPage = customCloneController.cancel();
    	System.assertEquals('/apex/quoteeditor?id='+ ext.extensionQuote.Id, cancelPage.getUrl(),'new url should redirect to quote editor page');
    	
    	
    	
    	
    	//Update Quote Status as QS_STATUS_SUBMITTED_FOR_APPROVAL to test Revise fail.
    	updateQuote = new Quote();
    	updateQuote.Id = ext.extensionQuote.Id;
    	updateQuote.Status = GlobalConstants.QS_STATUS_SUBMITTED_FOR_APPROVAL;
    	update updateQuote;
    	
    	//Set the Parameter and ReviseQuote to fail with the QuoteStatus
		pageReviseQuote = Page.ReviseQuote;
    	pageReviseQuote.getParameters().put('id', ext.extensionQuote.Id);
    	Test.setCurrentPage(pageReviseQuote);
    	customCloneController = new CloneQuoteCustomController();
    	nextPage = customCloneController.reviseQuote();
 		System.debug('Revise NextPage.getUrl()>>>>' + nextPage);
 		System.assertEquals(null, nextPage,'new url should be null');
    	System.assert(customCloneController.newQuoteId == null, 'reviseQuote method  was expected to fail since Quote Status validation rule for ReviseQuote.');
    	
    	
    	//Set an invalid value for the id Parameter to fail.
    	try{
			pageReviseQuote = Page.ReviseQuote;
	    	pageReviseQuote.getParameters().put('id', '');
	    	Test.setCurrentPage(pageReviseQuote);
	    	customCloneController = new CloneQuoteCustomController();
	    	nextPage = customCloneController.reviseQuote();
	 		System.debug('Revise NextPage.getUrl()>>>>' + nextPage);
	 		System.assertEquals(null, nextPage,'new url should be null');
    	}
    	Catch(Exception e){
    		System.debug('Revise Unpected Exception 1 >>>>: ' + e);
    		System.assert(e != null, 'Was expected to receive Invalid Id error.');
    	}
    	
    	//Delete Quote and Opportuntity to throw unpexpected error.
    	
    	Quote deleteQuote = new Quote();
    	deleteQuote.Id = ext.extensionQuote.Id;
    	delete deleteQuote;
    	
    	//Trying to copy a deleted Quote, caught the exception in the parent method - QuoteTools.CloneQuotationAndRelatedRecords
    	pageReviseQuote = Page.ReviseQuote;
    	pageReviseQuote.getParameters().put('id', ext.extensionQuote.Id);
    	Test.setCurrentPage(pageReviseQuote);
    	customCloneController = new CloneQuoteCustomController();
    	nextPage = customCloneController.reviseQuote();
 		System.debug('Revise NextPage.getUrl()>>>>' + nextPage);
 		System.assertEquals(null, nextPage,'new url should be null');
    	
	    	
    }
    
}