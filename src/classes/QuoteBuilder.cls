public with sharing class QuoteBuilder {

	private static Integer sequence = 100;

	private String quoteName;

	//Mandatory fields with defaults
	private String status = 'Draft';
	private Opportunity quoteOpportunity;
	private Id priceBookId;
	//private Quote_Transaction_Type__c transactionType;
	private GlobalConstants.QuoteTypes quoteType;
	private Consignment_Business_Case__c quoteCBC;
	private Surgeon_Preference__c quoteSurgeonPref;
	private Address__c quoteSBPartner;
	private ActualBillOfMaterial__c actualBOM;
	
	/*
	public QuoteBuilder name(String quoteName)
	{
		this.quoteName = quoteName;
		return this;
	}
	*/
	
	public QuoteBuilder opportunity(Opportunity quoteOpportunity)
	{
		this.quoteOpportunity = quoteOpportunity;
		return this;
	}
	
	public QuoteBuilder priceBookId(Id priceBookId)
	{
		this.priceBookId = priceBookId;
		return this;
	}
	/*
	public QuoteBuilder quoteTransactionType(Quote_Transaction_Type__c transactionType)
	{
		this.transactionType = transactionType;
		return this;
	}
	*/
	
	public QuoteBuilder quoteType(GlobalConstants.QuoteTypes quoteType)
	{
		this.quoteType = quoteType;
		return this;
	}
	
	public QuoteBuilder consignmentBusinessCase(Consignment_Business_Case__c quoteCBC)
	{
		this.quoteCBC = quoteCBC;
		return this;
	}
	
	public QuoteBuilder surgeonPreference(Surgeon_Preference__c quoteSurgeonPref)
	{
		this.quoteSurgeonPref = quoteSurgeonPref;
		return this;
	}
	
	public QuoteBuilder actualBOM(ActualBillOfMaterial__c actualBOM)
	{
		this.actualBOM = actualBOM;
		return this;
	}
	
	public QuoteBuilder address(Address__c quoteSBPartner)
	{
		this.quoteSBPartner = quoteSBPartner;
		return this;
	}
	
	public Quote build(Boolean createInDatabase)
	{
		Quote quote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			OpportunityId = this.quoteOpportunity.Id,
			Status = this.status,
			Pricebook2Id = this.priceBookId,
			//Quote_Transaction_Type__c =this.transactionType.Id,
			QuoteType__c = quoteType.name(),
			Version__c = 1
		);
		
		if (createInDatabase){
			insert quote;
			quote = getInsertedQuote(quote.Id);
		}
		
		return quote;
	}
	
	public Quote buildCbcQuote(Boolean createInDatabase)
	{
		Quote quote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			OpportunityId = this.quoteOpportunity.Id,
			Status = this.status,
			Pricebook2Id = this.priceBookId,
			//Quote_Transaction_Type__c =this.transactionType.Id,
			QuoteType__c = GlobalConstants.QuoteTypes.CBC.name(),
			Address__c = this.quoteSBPartner.Id,
			Consignment_Business_Case__c = this.quoteCBC.Id,
			Version__c = 1
		);
		
		if (createInDatabase){
			insert quote;
			quote = getInsertedQuote(quote.Id);
		}
		
		return quote;
	}
	
	public Quote buildSurgeonPrefQuote(Boolean createInDatabase)
	{
		Quote quote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			OpportunityId = this.quoteOpportunity.Id,
			Status = this.status,
			Pricebook2Id = this.priceBookId,
			//Quote_Transaction_Type__c =this.transactionType.Id,
			QuoteType__c = GlobalConstants.QuoteTypes.SurgeonPreference.name(),
			Surgeon_Preference__c = this.quoteSurgeonPref.Id,
			Version__c = 1
		);
		
		if (createInDatabase){
			insert quote;
			quote = getInsertedQuote(quote.Id);
		}
		
		return quote;
	}
	
	public Quote buildSetRequestQuote(Boolean createInDatabase)
	{
		Quote quote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			OpportunityId = this.quoteOpportunity.Id,
			Status = this.status,
			Pricebook2Id = this.priceBookId,
			//Quote_Transaction_Type__c =this.transactionType.Id,
			QuoteType__c = GlobalConstants.QuoteTypes.SetRequest.name(),
			Version__c = 1
		);
		
		if (createInDatabase){
			insert quote;
			quote = getInsertedQuote(quote.Id);
		}

		return quote;
	}
	
	public Quote buildChangeSetQuote(Boolean createInDatabase)
	{
		Quote quote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			OpportunityId = this.quoteOpportunity.Id,
			Status = this.status,
			Pricebook2Id = this.priceBookId,
			//Quote_Transaction_Type__c =this.transactionType.Id,
			QuoteType__c = GlobalConstants.QuoteTypes.ChangeSet.name(),
			ActualBillOfMaterial__c = this.actualBOM.Id,
			Address__c = this.quoteSBPartner.Id,
			Version__c = 1
		);
		
		if (createInDatabase){
			insert quote;
			quote = getInsertedQuote(quote.Id);
		}

		return quote;
	}
	
	public Quote buildLoanSetRequestQuote(Boolean createInDatabase)
	{
		Quote quote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
			OpportunityId = this.quoteOpportunity.Id,
			Status = this.status,
			Pricebook2Id = this.priceBookId,
			//Quote_Transaction_Type__c =this.transactionType.Id,
			QuoteType__c = GlobalConstants.QuoteTypes.LoanSetRequest.name(),
			Version__c = 1
		);
		
		if (createInDatabase){
			insert quote;
			quote = getInsertedQuote(quote.Id);
		}

		return quote;
	}
	
	private Quote getInsertedQuote(Id quoteId){
		Quote quote = [select Id, QuoteNumber, Status, GrandTotal, ExpirationDate, Total_Net_Margin_Amount__c,  Total_Standard_Cost__c, Total_Net_Margin_Percent__c, 
			    		Total_Landed_Cost__c, Total_Gross_Margin_Percent__c, Total_Gross_Margin_Amount__c, Total_Extended_Unit_Price__c, Total_Discount_Percentage__c, 
			    		SystemModstamp, ShippingStreet, ShippingState, ShippingPostalCode, ShippingName, ShippingLongitude, ShippingLatitude, ShippingHandling, ShippingCountry, 
			    		ShippingCity, QuoteToStreet, QuoteToState, QuoteToPostalCode, QuoteToName, QuoteToLongitude, QuoteToLatitude, QuoteToCountry, 
			    		QuoteToCity, Phone, Name, LineItemCount, LastViewedDate, LastReferencedDate, LastModifiedDate, LastModifiedById, Fax, 
			    		Instruction_Comments__c, Instruction_Do_Not_Ship__c, Instruction_Other__c, Instruction_Ship_To_Address_OK__c, Instruction_Transfer__c, 
			    		Email, ERP_Document_Number__c, Document_Date__c, Discount, Description, Customer_Purchase_Order_Number__c, CreatedDate, CreatedById, ContactId, 
			    		BillingStreet, BillingState, BillingPostalCode, BillingName, BillingLongitude, BillingLatitude, BillingCountry, BillingCity, Pricebook2Id,
			    		AdditionalStreet, AdditionalState, AdditionalPostalCode, AdditionalName, AdditionalLongitude, AdditionalLatitude, AdditionalCountry, AdditionalCity, 
			    		Total_Discount_Amount__c, TotalPrice, Subtotal, Tax, TotalPrice_ExTax__c, Version__c, LastModifiedBy.Name,CreatedBy.Name,
			    		QuoteType__c, Address__c, Address__r.Name, 
			    		Consignment_Business_Case__c, Consignment_Business_Case__r.Agreement_From_Date__c,Consignment_Business_Case__r.Agreement_To_Date__c,
			    		Opportunity.Account.Name, Opportunity.OwnerId, Opportunity.Owner.Name, Opportunity.Account.Division__c, Opportunity.Account.OrganizationCode__c,
			    		SAP_Request_Response_Message__c,Opportunity.AccountId,Customer_Approval__c,Show_rebate_details_in_Document__c,Quote_Description__c,
			    		ActualBillOfMaterial__c 
			    	from Quote where Id = :quoteId];
		return quote;
	}
}