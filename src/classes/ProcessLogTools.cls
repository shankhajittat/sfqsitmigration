public with sharing class ProcessLogTools {
	
	public static Id insertUIErrorLog(String logTitle, String logMessage, String stackTraceString, String sourceUrl,
			String sourceFileName, ProcessLogCommon.SourceObject sourceObject, String errorCode, String exceptionLineNumber)
	{
		
		ProcessLog pLog = new ProcessLog();
		pLog.title = logTitle != null ? logTitle : 'UI Error Created On - ' + DateTime.now();
		pLog.sourceObject = sourceObject;
		pLog.sourceProcess = ProcessLogCommon.SourceProcess.UI;
		pLog.sourceRecordId = null;
		pLog.sourceRecordNumber = null;
		pLog.sourceUrl = sourceUrl;
		
		ProcessLogItem logItem = new ProcessLogItem();
		logItem.message = logMessage;
		logItem.stackTraceString = stackTraceString;
		logItem.sourceUrl = sourceUrl;
		logItem.sourceFileName = sourceFileName;
		logItem.sourceRecordId = null;
		logItem.sourceRecordNumber = null;
		logItem.severity = ProcessLogCommon.SeverityType.SystemError;
		logItem.sourceObject = sourceObject;
		logItem.errorCode = errorCode;
		logItem.exceptionLineNumber = exceptionLineNumber;
		
		pLog.ProcessLogItems = new List<ProcessLogItem>{ logItem };
		
		return insertProcessLog(pLog);
	}
	
	public static Id insertProcessLog(ProcessLogCommon.SourceObject sourceObject, ProcessLogCommon.SourceProcess sourceProcess, 
			MasterDataAPIv001.ProcessLog masterApiProcessLog){
		if (masterApiProcessLog == null || masterApiProcessLog.logs == null || masterApiProcessLog.logs.size() == 0) return null;
		
		ProcessLog pLog = new ProcessLog();
		pLog.title = masterApiProcessLog.title != null ? masterApiProcessLog.title : 'MasterData API Error Created On - ' + DateTime.now();
		pLog.sourceObject = sourceObject;
		pLog.sourceProcess = sourceProcess;
		pLog.sourceRecordId = null;
		pLog.sourceRecordNumber = null;
		pLog.sourceUrl = null;
		pLog.ProcessLogItems = null; 
		if (masterApiProcessLog.logs != null && masterApiProcessLog.logs.size() > 0){
			pLog.ProcessLogItems = new List<ProcessLogItem>();
			for(MasterDataAPIv001.Log mLogItem : masterApiProcessLog.logs){
				ProcessLogItem logItem = new ProcessLogItem();
				logItem.message = mLogItem.message;
				logItem.stackTraceString = null;
				logItem.sourceUrl = null;
				logItem.sourceFileName = null;
				logItem.sourceRecordId = mLogItem.sourceRecordId;
				logItem.sourceRecordNumber = mLogItem.sourceRecordNumber;
				logItem.sourceObject = null;
				logItem.errorCode = mLogItem.code;
				logItem.exceptionLineNumber = null;
				if (mLogItem.severity == MasterDataAPIv001.SeverityType.ValidationError)
					logItem.severity = ProcessLogCommon.SeverityType.ValidationError;
				else if (mLogItem.severity == MasterDataAPIv001.SeverityType.ApplicationError)
					logItem.severity = ProcessLogCommon.SeverityType.ApplicationError;
				else if (mLogItem.severity == MasterDataAPIv001.SeverityType.SystemError)
					logItem.severity = ProcessLogCommon.SeverityType.SystemError;
				else if (mLogItem.severity == MasterDataAPIv001.SeverityType.Warning)
					logItem.severity = ProcessLogCommon.SeverityType.Warning;
				else if (mLogItem.severity == MasterDataAPIv001.SeverityType.Information)
					logItem.severity = ProcessLogCommon.SeverityType.Information;
				else if (mLogItem.severity == MasterDataAPIv001.SeverityType.Other)
					logItem.severity = ProcessLogCommon.SeverityType.Other;
				else if (mLogItem.severity == MasterDataAPIv001.SeverityType.None)
					logItem.severity = ProcessLogCommon.SeverityType.None;		
							
				pLog.ProcessLogItems.add(logItem);
			}
		}
		
		return insertProcessLog(pLog);
	}
	
	public static Id insertProcessLog(ProcessLog pl){
		Id processLogId = null;
		
		if (pl == null || pl.ProcessLogItems == null || pl.ProcessLogItems.size() == 0) return null;
		
		try{
			
			ProcessLog__c newPL = new ProcessLog__c();
			if (!StringUtils.isNullOrWhiteSpace(pl.title))
				newPL.Title__c = pl.title;
			else
				newPL.Title__c = 'ProcessLog created on ' + DateTime.now();
			if (pl.sourceObject != null)
				newPL.SourceObject__c = pl.sourceObject.name();
			if (pl.sourceProcess != null)	
				newPL.SourceProcess__c = pl.sourceProcess.name();
			if (pl.sourceRecordId != null)	
				newPL.SourceRecordId__c = pl.sourceRecordId;
			if (!StringUtils.isNullOrWhiteSpace(pl.sourceRecordNumber))
				newPL.SourceRecordNumber__c = pl.sourceRecordNumber;
			if (!StringUtils.isNullOrWhiteSpace(pl.sourceUrl))
				newPL.SourceUrl__c =  EncodingUtil.urlEncode(pl.sourceUrl, 'UTF-8') ;

			newPL.AlertOnError__c = true; // from config
			newPL.IsResolved__c = false;
			//insert into db
			insert newPL;
			processLogId = newPL.Id;
			
			if (pl.ProcessLogItems != null && pl.ProcessLogItems.size() > 0){
				List<ProcessLogItem__c> newProcessLogItemList = new List<ProcessLogItem__c>();
				for(ProcessLogItem plItem : pl.ProcessLogItems){
					ProcessLogItem__c newPlItem = new ProcessLogItem__c();
					newPlItem.ProcessLogId__c = processLogId;
										
					if (!StringUtils.isNullOrWhiteSpace(plItem.exceptionLineNumber))
						newPlItem.ExceptionLineNumber__c = plItem.exceptionLineNumber;
						
					if (!StringUtils.isNullOrWhiteSpace(plItem.errorCode))
						newPlItem.ErrorCode__c = plItem.errorCode;
						
					newPlItem.IsResolved__c = false;
					
					if (!StringUtils.isNullOrWhiteSpace(plItem.message))
						newPlItem.Message__c = plItem.message;
					if (!StringUtils.isNullOrWhiteSpace(plItem.stackTraceString))
						newPlItem.StackTraceString__c = plItem.stackTraceString;
						
					if (plItem.severity != null)
						newPlItem.SeverityType__c = plItem.severity.name();
						
					if (plItem.sourceObject != null)
						newPlItem.SourceObject__c = plItem.sourceObject.name();
						
					if (plItem.sourceRecordId != null)	
						newPlItem.SourceRecordId__c = plItem.sourceRecordId;
						
					if (!StringUtils.isNullOrWhiteSpace(plItem.sourceRecordNumber))
						newPlItem.SourceRecordNumber__c = plItem.sourceRecordNumber;
					if (!StringUtils.isNullOrWhiteSpace(plItem.sourceUrl))
						newPlItem.SourceUrl__c =  EncodingUtil.urlEncode(plItem.sourceUrl, 'UTF-8') ;
					if (!StringUtils.isNullOrWhiteSpace(plItem.sourceFileName))
						newPlItem.SourceFileName__c = plItem.sourceFileName;
						
					newProcessLogItemList.add(newPlItem);
				}
				insert newProcessLogItemList;
			}
		}
		catch(Exception e){
			//how to handle an exception occurs here??
		}
		return processLogId;
	}
	
	private static ProcessLogCommon.SourceProcess convertToSourceProcessEnum(string sourceProcessName){
		if (StringUtils.isNullOrWhiteSpace(sourceProcessName)) return null;
		
		for(ProcessLogCommon.SourceProcess sp : ProcessLogCommon.SourceProcess.values()){
			if (sp.name() == sourceProcessName){
				return sp;
			}
		}
		return null;
	}
	
	public static QuoteProcessLogCollection getProcessLogsByQuoteId(Id quoteId){
		QuoteProcessLogCollection result = null;
		if (quoteId == null) return result;
		
		//1
		Map<ProcessLogCommon.SourceProcess,List<ProcessLog__c>> processLogsBySourceProcessMap = new Map<ProcessLogCommon.SourceProcess,List<ProcessLog__c>>();
		//2
		Map<Id,List<ProcessLogItem__c>> processLogItemsByQuoteLineItemIdMap = new Map<Id,List<ProcessLogItem__c>>();
		//3
		Map<ProcessLog__c,List<ProcessLogItem__c>> processLogMap = new Map<ProcessLog__c,List<ProcessLogItem__c>>();
		
		try{
			
			result = new QuoteProcessLogCollection();
			
			for(List<ProcessLog__c> pLogs : [Select p.WarningsCount__c, p.ValidationErrorCount__c, p.Title__c, p.SystemErrorCount__c, p.SourceUrl__c, 
											p.SourceRecordNumber__c, p.SourceRecordId__c, p.SourceProcess__c, p.SourceObject__c, p.ProcessLogItemsCount__c, p.OwnerId, 
											p.Name, p.LastModifiedDate, p.LastModifiedById, p.IsResolved__c, 
											p.IsDeleted, p.Id, p.CreatedDate, p.CreatedById, p.ApplicationErrorCount__c, p.AlertOnError__c,p.TotalErrorCount__c, 
												(Select Id, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, 
												Message__c, SeverityType__c, SourceObject__c, SourceRecordId__c, SourceRecordNumber__c, 
												ErrorCode__c, IsResolved__c, ProcessLogId__c, ExceptionLineNumber__c, StackTraceString__c, SourceUrl__c, 
												SourceFileName__c 
												From ProcessLogItems__r) 
											From ProcessLog__c p Where p.SourceRecordId__c != null 
																		And (p.SourceRecordId__c =: quoteId)
																		And (p.IsResolved__c = false) order By p.CreatedDate desc ]){
				for(ProcessLog__c pLog : pLogs){
					
					ProcessLogCommon.SourceProcess spKey = convertToSourceProcessEnum(pLog.SourceProcess__c);
					if (spKey == null)
						spKey = ProcessLogCommon.SourceProcess.Other;											
					
					//1st
					List<ProcessLog__c> tempProcessLogList = processLogsBySourceProcessMap.get(spKey);
					if (tempProcessLogList == null){
						tempProcessLogList = new List<ProcessLog__c>();
					}
					tempProcessLogList.add(pLog);
					processLogsBySourceProcessMap.put(spKey, tempProcessLogList);
					
					//
					List<ProcessLogItem__c> tempProcessLogItemList = new List<ProcessLogItem__c>();
					for(ProcessLogItem__c pItem : pLog.ProcessLogItems__r){
						//
						tempProcessLogItemList.add(pItem);
						
						//2nd
						if (pItem.SourceRecordId__c != null){
							List<ProcessLogItem__c> tempPlItemList = processLogItemsByQuoteLineItemIdMap.get(pItem.SourceRecordId__c);
							if (tempPlItemList == null){
								tempPlItemList = new List<ProcessLogItem__c>();
							}
							tempPlItemList.add(pItem);
							processLogItemsByQuoteLineItemIdMap.put(pItem.SourceRecordId__c,tempPlItemList);
						}
					}
					
					//3rd
					processLogMap.put(pLog,tempProcessLogItemList);
				
				}
			}
		}
		catch(Exception e){
			//
		}

		result.processLogsBySourceProcessMap = processLogsBySourceProcessMap;		
		result.processLogItemsByQuoteLineItemIdMap = processLogItemsByQuoteLineItemIdMap;
		result.processLogMap = processLogMap;
				
		return result;
	}
	
	
}