public with sharing class ObjectFieldsUserPreference {

	public enum ObjectFieldsUserPreference_ObjectSections
	{
		Header1,
		Header1Tooltip,
		Header2,
		Header2Tooltip,
		Group1Header,
		Group1Tooltip,
		Group2Header,
		Group2Tooltip,
		Item1Header,
		Item1Tooltip,
		Item2Header,
		Item2Tooltip, 
		Footer1,
		Footer1Tooltip,
		Footer2,
		Footer2Tooltip,
		DefaultBUSelection,
		None
	}
	
	private String objectName;
	private String objectSection;
	private List<SelectOption> selectedColums;
	private Id userId;
	
	public ObjectFieldsUserPreference(String objectName, 
								ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections objectSection,
								List<SelectOption> selectedColums
								)
	{
		
		if (objectName == null || objectName == '')
		{
			if(ApexPages.currentPage() != null){  
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must pass valid ObjectName to initialize ObjectFieldsUserPreference class.'));
			}
		}
		
		
		this.objectName = objectName;
		this.objectSection = objectSection.name();
		this.selectedColums = selectedColums != null ? selectedColums : new List<SelectOption>();
		this.userId = UserInfo.getUserId();
	}
	
	public ObjectFieldsUserPreference(String objectName, 
								ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections objectSection
								)
	{
		
		if (objectName == null || objectName == '')
		{
			if(ApexPages.currentPage() != null){  
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must pass valid ObjectName to initialize ObjectFieldsUserPreference class.'));
			}
		}
		
		
		this.objectName = objectName;
		this.objectSection = objectSection.name();
		this.selectedColums = new List<SelectOption>();
		this.userId = UserInfo.getUserId();
	}
	
	
	public boolean upsertUserPreference()
	{
		boolean result = false;
		boolean newRecord = false;
		
		try
		{
			Object_Fields_User_Preference__c up = retrieveUserPreference();
			if (up == null)
			{
				//Create new User Preference
				Object_Fields_User_Preference__c newUP = new ObjectFieldsUserPreferenceBuilder()
					     	.setUser(this.userId)
					     	.setObjectName(this.objectName)
					     	.setObjectSection(this.objectSection)
					     	.build(true);
				
				System.assert(newUP <> null,'An error occured while creating User Preference record.');
	            System.assert(newUP.Id <> null,'Failed to Create User Preference record.');
				
				up = retrieveUserPreference();
				newRecord = true;
				
			}
			
			//Select o.ObjectFields_UserPreference__c, o.Name, o.Id, o.Display_Order__c From Object_Fields_User_Preference_Detail__c o
			if (selectedColums != null)
			{
				if (newRecord == false)
				{
					//Delete existing details for UP
					delete [SELECT d.ObjectFields_UserPreference__c From Object_Fields_User_Preference_Detail__c d Where d.ObjectFields_UserPreference__c = : up.Id];
				}
				
				List<Object_Fields_User_Preference_Detail__c> upDetails = new List<Object_Fields_User_Preference_Detail__c>();
				double displayOrder = 1;
				for(SelectOption c : selectedColums)
				{
					System.debug('ThomasT' + c.getvalue());
					
					Object_Fields_User_Preference_Detail__c newUserPreferenceDetail = new Object_Fields_User_Preference_Detail__c();
					newUserPreferenceDetail.Name = c.getLabel();
					newUserPreferenceDetail.ObjectField_API_Name__c = c.getvalue();
					newUserPreferenceDetail.Display_Order__c = displayOrder;
					newUserPreferenceDetail.ObjectFields_UserPreference__c = up.Id;
					upDetails.add(newUserPreferenceDetail);
					displayOrder++;
				}
				insert upDetails;
			}
			
			result = true;
		}
		catch(Exception e){
        	result = false;
    	}
    	
		return result;
	}
	
	
	public Object_Fields_User_Preference__c retrieveUserPreference()
	{
		Object_Fields_User_Preference__c result = null;
		boolean err = false;
		
		try
		{
			result = [Select o.User__c, o.Object_Section__c, o.Object_Name__c, o.Name, o.Id 
        				From Object_Fields_User_Preference__c o   												
        				Where o.User__c = : this.userId And o.Object_Name__c = : this.objectName And o.Object_Section__c = : this.objectSection  LIMIT 1];
			
		}
		catch(Exception e){
        	err = true;
    	}
		
		
		return result;
	}
	
	public List<Object_Fields_User_Preference_Detail__c> retrieveUserPreferenceDetails()
	{
		List<Object_Fields_User_Preference_Detail__c> result = null;
		boolean err = false;
		
		try
		{
			Object_Fields_User_Preference__c up = retrieveUserPreference();
			if (up != null)
			{
				result = [Select d.Name,d.ObjectField_API_Name__c, d.Display_Order__c From Object_Fields_User_Preference_Detail__c d 
							Where d.ObjectFields_UserPreference__c = : up.Id  Order By d.Display_Order__c asc];
			}			
		}
		catch(Exception e){
        	err = true;
    	}

		return result;
	}
	
	public Set<SelectOption> retrieveUserPreferenceDetailsAsSetSelectOption()
	{
		Set<SelectOption> result = new Set<SelectOption>();
		boolean err = false;
		
		try
		{	
			List<Object_Fields_User_Preference_Detail__c> upDetails = retrieveUserPreferenceDetails();
			if (upDetails != null)
			{
				for(Object_Fields_User_Preference_Detail__c d : upDetails){
					result.add(new SelectOption(d.ObjectField_API_Name__c,d.Name));
				}
			}			
		}
		catch(Exception e){
        	err = true;
    	}

		return result;
	}
	
	public List<SelectOption> retrieveUserPreferenceDetailsAsListSelectOption()
	{
		List<SelectOption> result = new List<SelectOption>();
		boolean err = false;
		
		try
		{	
			List<Object_Fields_User_Preference_Detail__c> upDetails = retrieveUserPreferenceDetails();
			if (upDetails != null)
			{
				for(Object_Fields_User_Preference_Detail__c d : upDetails){
					result.add(new SelectOption(d.ObjectField_API_Name__c,d.Name));
				}
			}			
		}
		catch(Exception e){
        	err = true;
    	}

		return result;
	}
	
	public List<Map<String,String>> retrieveUserPreferenceDetailsAsListOfMap()
	{
		List<Map<String,String>> result = new List<Map<String,String>>();
		boolean err = false;
		
		try
		{	
			List<Object_Fields_User_Preference_Detail__c> upDetails = retrieveUserPreferenceDetails();
			if (upDetails != null)
			{
				for(Object_Fields_User_Preference_Detail__c d : upDetails){
					Map<String,String> mapObject = new Map<String,String>();
					mapObject.put('apiName',d.ObjectField_API_Name__c);
					mapObject.put('label',d.Name);
					result.add(mapObject);
				}
			}			
		}
		catch(Exception e){
        	err = true;
    	}

		return result;
	}
	
}