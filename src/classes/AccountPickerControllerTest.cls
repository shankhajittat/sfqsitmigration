@isTest
private class AccountPickerControllerTest {

	@IsTest 
    public static void testAccountPickerController() {   
    	
    	
		//Create an account
		Account vinnies = new AccountBuilder().name('St Vincents').build(true);
		
		PageReference pageRef = Page.QuoteType;
		pageRef.getParameters().put('accId', vinnies.Id);
        Test.setCurrentPage(pageRef);
        
    	AccountPickerController theAccountPicker = new AccountPickerController();
    	theAccountPicker.accountId = vinnies.Id;
    	PageReference urlForRedirection = theAccountPicker.route();
    	System.assertNotEquals(null, urlForRedirection, 'a new url is generated');
    	System.assertEquals('/apex/Quote_Account_Picker?accId='+vinnies.Id, urlForRedirection.getUrl(),'new url should redirect to Quote_Account_Picker page to choose the account');
    	
    	/*
        // Test page instantiation
		PageReference pageRef = Page.CreateQuote2_VF;
		pageRef.getParameters().put('id', vinnies.Id);
        Test.setCurrentPage(pageRef);
        
        
        theAccountPicker.accountId = vinnies.Id;
    	PageReference urlAfterchooseAccount = theAccountPicker.chooseAccount();
    	System.assertNotEquals(null, urlAfterchooseAccount, 'a new url is generated');
    	System.assertEquals('/apex/CreateQuote2_VF?id='+vinnies.Id, urlAfterchooseAccount.getUrl(),'new url should redirect to VF page to create opportunity and quote');
    	*/
    }
}