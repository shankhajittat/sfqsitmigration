@isTest(seeAllData=true) // required for tests that use pricebooks
private class CreateQuoteControllerExtensionTest {
	
	@IsTest 
    public static void testCreateQuoteControllerExtension() {  
		
		String organizationCode = 'AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').setErpAccountId('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel And Division__c =: division LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel + division)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.division(division)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel + division);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	

        // Test page instantiation
		PageReference pageRef = Page.CreateQuote2_VF;
		pageRef.getParameters().put('id', vinnies.Id);
        Test.setCurrentPage(pageRef);
        
		// Test constructor
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Account());
    	CreateQuoteControllerExtension ext = new CreateQuoteControllerExtension(stdController);
    	System.assert(ext.account.Id != null, 'constructor should load the account sobject');
    	System.assert(ext.account.Name != null, 'constructor should load the account name for display');
		System.assert(ext.priceBook.Id != null, 'constructor should load the standard pricebook'); 
		
		// Test that opportunity and quote have been created and linked and the page reference is set up to redirect to quoteeditor page
		PageReference urlAfterautoRunCreateQuote = ext.autoRunCreateQuote();
		System.assertNotEquals(null, urlAfterautoRunCreateQuote, 'a new url should have been generated');
    	System.assertEquals('/apex/quoteeditor?id='+ext.extensionQuote.Id, urlAfterautoRunCreateQuote.getUrl(),'new url is incorrect');
		System.assertNotEquals(null, ext.extensionOpportunity, 'a new opportunity should have been generated');
		System.assertNotEquals(null, ext.extensionQuote, 'a new quote should have been generated');		
		
    	// Check that opportunity and quote have been linked
    	for (List<Opportunity> opportunities : [SELECT o.Id, o.SyncedQuoteId
    										   From Opportunity o
    										   WHERE o.Id = :ext.extensionOpportunity.Id]){
    		
    		System.assertEquals(1, opportunities.size(), 'There should not be more than one parent opportunity for the Quote');
    		//There should only be one opportunity now
    		for (Opportunity oneOpportunity : opportunities){
    			System.assertEquals(ext.extensionQuote.Id, oneOpportunity.SyncedQuoteId, 'Opportunity and Quote have not been linked');
    		}
    	}
    	
    	// Test an invalid situation where the account id is null
    	ext.theAccountId = null;
    	PageReference urlAfterInvalidautoRunCreateQuote = ext.autoRunCreateQuote();
    	System.assertEquals(null, urlAfterInvalidautoRunCreateQuote, 'the Url should be null');
    	
    	
    }  
    
}