@IsTest
public with sharing class AutocompleteAccountPluginTest {
	
	@IsTest
	public static void testAutocompleteAccountPlugin() {
		
		AutoCompleteAccountPlugin plugin = new AutoCompleteAccountPlugin();
		
		plugin.init(new Map<String,String>());
		
		// Create some accounts to be returned 
		Account vinnies = new AccountBuilder().name('St Vincents').build(true);
		List<Account> testAccount1 = [SELECT Id,Name from Account WHERE Name='St Vincents'];
        System.assert(testAccount1.size() <> 0,'Test account - St Vincents - failed to load.');
        
        //Check that the find returns the account
	    List<Map<String, String>> unfilteredSearchList = plugin.find('St');
	    System.assert(unfilteredSearchList <> null, 'Search returned empty'); 
	    
		String disabledReason = plugin.filter(new Map<String,String>());
		system.assertEquals(null, disabledReason, 'Filter should not disable any Account');
		
	}
	
}