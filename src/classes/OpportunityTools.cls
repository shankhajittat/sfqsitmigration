public with sharing class OpportunityTools {

	public static void UpdateOpportunitySyncQuoteId(Id opportunityId, Id quoteId)
	{

		
		Opportunity opp = [Select o.SyncedQuoteId, o.Id From Opportunity o WHERE o.Id = :opportunityId];
		if (opp != null)
		{
			opp.SyncedQuoteId = quoteId;
			update opp;
		}
	}
	
	
	
	
}