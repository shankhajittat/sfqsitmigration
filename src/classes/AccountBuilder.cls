public with sharing class AccountBuilder {

	private static Integer sequence = 100;

	//Mandatory fields with defaults
	private String accountName;
	private String accountNumber; // = 'A-' + sequence++;
	private String erpAccountId;
	private String organizationCode;
	private String distributionChannel;
	private String division;
	
	public AccountBuilder name(String accountName)
	{
		this.accountName = accountName;
		return this;
	}
	/*
	public AccountBuilder setErpAccountId(String erpAccountId)
	{
		this.erpAccountId = erpAccountId;
		return this;
	}
	*/
	
	public AccountBuilder accountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
		return this;
	}
	
	public AccountBuilder organizationCode(String organizationCode)
	{
		this.organizationCode = organizationCode;
		return this;
	}
	
	public AccountBuilder distributionChannel(String distributionChannel)
	{
		this.distributionChannel = distributionChannel;
		return this;
	}
	
	public AccountBuilder division(String division)
	{
		this.division = division;
		return this;
	}
	
	public Account build(Boolean createInDatabase)
	{
		if (StringUtils.isNullOrWhiteSpace(this.organizationCode))
			this.organizationCode = 'UT-AU';
		if (StringUtils.isNullOrWhiteSpace(this.distributionChannel))
			this.distributionChannel = '01';
			
			
		Account account = new Account(
			Name = this.accountName,
			AccountNumber = this.accountNumber,
			ERP_Account_ID__c = this.accountNumber + '|' + this.organizationCode + '|' + this.distributionChannel,
			OrganizationCode__c = this.organizationCode,
			DistributionChannel__c = this.distributionChannel
		);
		
		if (this.division != null)
			account.Division__c = this.division;
			
		if (createInDatabase)
			insert account;
		
		return account;
	}
	
	
}