public with sharing class APIv001Tools {

	public static List<APIv001.Log> transformSaveResultErrorsIntoListOfApiLogs(List<Database.Error> srDatbaseErrorList)
	{
		
		List<APIv001.Log> logList = new List<APIv001.Log>();
		
		for(Database.Error err : srDatbaseErrorList){
			APIv001.Log log = new APIv001.Log();
			
			log.severity = APIv001.SeverityType.SystemError;
			log.code = String.valueOf(err.getStatusCode().ordinal());
			log.message = 'Error Code: ' + err.getStatusCode() + '\nError Message: ' + err.getMessage();
			
			logList.add(log);
		}
		
		return logList;

	}
	
	
	
}