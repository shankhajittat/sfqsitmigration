public class CreateQuoteControllerExtension {
	
	// NB THIS PGM WILL NO LONGER BE USED ONCE CBC AND SP CHANGES GO IN AND CAN THEN BE DELETED ALONG WITH THE CreateQuote2_VF page
	
    private final ApexPages.Standardcontroller stdController;
    public Account account {get; set;}
    public Pricebook2 priceBook {get; private set;}
    public Opportunity extensionOpportunity {get; private set;}
    public Quote extensionQuote{get;set;}
    public String theAccountId {get; set;}
        
    public CreateQuoteControllerExtension (ApexPages.StandardController stdController) {
    	try {
		        this.stdController = stdController;
		        theAccountId = stdController.getId();
		        
		        this.account = [SELECT a.Name, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, 
		        					a.ShippingCountry, a.ShippingCity, a.BillingStreet, a.BillingState, 
		        					a.BillingPostalCode, a.BillingCountry, a.BillingCity,
		        					a.OrganizationCode__c,a.DistributionChannel__c,a.Division__c FROM Account a WHERE a.Id =: theAccountId];
		        					
		        if (StringUtils.isEmpty(this.account.OrganizationCode__c)){
		        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid OrganizationCode.');
		        	return;
		        }
		        if (StringUtils.isEmpty(this.account.DistributionChannel__c)){
		        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid DistributionChannel.');
		        	return;
		        }
		        if (StringUtils.isEmpty(this.account.Division__c)){
		        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid Division.');
		        	return;
		        }
		        	
		        /*
		        //Standard PriceBook     
		        this.priceBook = [SELECT p.Name, p.IsStandard, p.IsDeleted, p.IsActive, p.Id, p.Description 
		        				  from Pricebook2 p 
		        				  WHERE p.IsStandard=true LIMIT 1];
		        */
		        
		        try{
			        this.priceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: account.OrganizationCode__c 
			    						And DistributionChannel__c =: account.DistributionChannel__c And Division__c =: account.Division__c LIMIT 1];
			    	if (this.priceBook.IsActive == false){
			    		throw new ApplicationException('PriceBook ' + this.priceBook + ' is Inactive. Pricebook must be active inorder to create Quote.');
		        		return;
			    	}
		        }
		        catch(Exception e){
			    	this.priceBook = null;
			    }
			    if (this.priceBook == null){
		        	Pricebook2 customPricebook = new PriceBook2Builder()
		        			.name(account.OrganizationCode__c + account.DistributionChannel__c + account.Division__c)
		        			.organizationCode(account.OrganizationCode__c)
		        			.distributionChannel(account.DistributionChannel__c)
		        			.division(account.Division__c)
		        			.build(true);
		        			
		        	this.priceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: account.OrganizationCode__c 
			    						And DistributionChannel__c =: account.DistributionChannel__c And Division__c =: account.Division__c LIMIT 1];
			    }
		        
        }
	 	catch (Exception e) {
	 		ApexPages.addMessages(e);
	 		return;
	 	}		  
    }
     
    // Code we will invoke on page load.
    public PageReference autoRunCreateQuote() {
		try {
	 			
	        	if (theAccountId == null) {
	            	// Display the Visualforce page's content if no Id is passed over
	            	return null;
	        	}
	 
        		//Create Opportunity
        		String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
            	this.extensionOpportunity = new Opportunity(
                	Name = nextOpportunityDocumentNumber,
                	CloseDate = Date.today().addDays(30),
                	Amount = 0.0,
                	StageName = 'Qualification',
                	AccountId = this.account.Id,
                	Pricebook2Id = this.priceBook.Id
                	);
                
             	Database.insert(this.extensionOpportunity);
        
            	//Create Quote
            	String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
            	this.extensionQuote = new Quote(
                	Name = nextQuoteDocumentNumber,
                	OpportunityId = this.extensionOpportunity.Id,
                	Status = 'Draft',
                	Pricebook2Id = this.priceBook.Id,
                	Version__c = 1,
                	BillingName = '',
                	BillingStreet = this.account.BillingStreet,
                	BillingCity = this.account.BillingCity,
                	BillingState = this.account.BillingState,
                	BillingCountry = this.account.BillingCountry,
                	BillingPostalCode = this.account.BillingPostalCode,
                	ShippingName = '',
                	ShippingStreet = this.account.ShippingStreet,
                	ShippingCity = this.account.ShippingCity,
                	ShippingState = this.account.ShippingState,
                	ShippingCountry = this.account.ShippingCountry,
                	ShippingPostalCode = this.account.ShippingPostalCode
                	);
            	Database.insert(this.extensionQuote);
            	
            	//link together
            	OpportunityTools.UpdateOpportunitySyncQuoteId(this.extensionOpportunity.Id, this.extensionQuote.Id);
        
				// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
		}
	 	catch (Exception e) {
	 		ApexPages.addMessages(e);
	 		return null;
	 	}
	 	 
    }
 
}