/*

This class needs to be changes, the template needs to be changed based on new columns.
this not in use.

*/


public class SetClassParser {

	/*
	insert an active setclass item for each record parsed	
	insert an setclass item for each record parsed	
	*/
	
	public List<List<String>> parsed;
	public Set<String> setClassIdsParsed = new Set<String>();
	public List<Set_Class__c> existingSetClasses = new List<Set_Class__c>();
	public Set<String> productCodes = new Set<String>();
	public Map<String, Product2> setProducts = new Map<String, Product2>(); 
	public Map<Id, PricebookEntry> setPricebookEntriesByProductId = new Map<Id, PricebookEntry>(); 
	public Id standardPricebookId;
	public Pricebook2 customPricebook;
	
	public Set_Class__c newSetClass; 
	public List<Set_Class_Items__c> newSetClassItems = new List<Set_Class_Items__c>(); 
	
	public SetClassParser(Id standardPriceBookId, String csv) {
		//need to have the following three information part of the csv file.
		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01';
        this.standardPricebookId = standardPriceBookId;
        
		parsed = CSVReader.Parse(csv);

	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	if (customPricebook.Id == null)
        		throw new ParseException('An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
		
	}
	
	public void run() {
		checkUniqueSetClassIds();
		loadProductCodes();
		ensureProducts();
		ensurePricebookEntries();
		updateExistingActiveSetClasses();
		create(); 
	}
	
	/* predicate used to filter out csv rows that have no data in fields. excel save-as csv sometimes generates these */
	public boolean isParsedRowValid(List<String> row) {
		return row.get(1) != null && row.get(1).length() > 0  // set class id
			&& row.get(3) != null && row.get(3).length() > 0  // item number
			&& row.get(4) != null && row.get(4).length() > 0; // quantity
	}
	
	public void checkUniqueSetClassIds() {
		for (Integer i = 0; i < parsed.size(); i++) {
			if (i != 0) {
				if (isParsedRowValid(parsed.get(i))) {
					setClassIdsParsed.add(parsed.get(i).get(0));
				}							
			}
		}
		if (setClassIdsParsed.size() != 1) {			
			throw new ParseException('Multiple Set Class Ids found during import: '+setClassIdsParsed);
		}
	}
		
	public Map<Schema.sObjectType, List<Sobject>> updateSetClass() {
		Map<Schema.sObjectType, List<Sobject>> results = new Map<Schema.sObjectType, List<Sobject>>();				
		return results;
	}
	
	/* populate the set of product codes from the csv data */
	public void loadProductCodes() {
		for (Integer i = 0; i < parsed.size(); i++) {
			if (i != 0) {
				if (isParsedRowValid(parsed.get(i))) {
					productCodes.add(parsed.get(i).get(3));
				}							
			}
		}
	}
	
	/* find any products that are not in the database and create them.
	   load any existing products into the map as well as the created products  */
	public void ensureProducts() {
		for (List<Product2> products : [select Id, Name, ProductCode
										from Product2
										where ProductCode in :productCodes]) {
			for (Product2 p : products) {
				setProducts.put(p.ProductCode, p);
			}
		}
		List<Product2> newProducts = new List<Product2>(); 
		for (String pc : productCodes) {
			
			// IMPORTANT : these lines are not covered in a db with products already in place
			// but will be covered when running in an empty sandbox. this is because seealldata=true
			// to allow access to the standard standardPricebook. this explains why coverage % will be different
			// when run in an empty sandbox
			
			if (setProducts.get(pc) == null) {
				// product not in database so we need to create one
				newProducts.add(new Product2(Name=pc, ProductCode=pc, IsActive=true));
			}
		}
		if (newProducts.size() > 0) {
			insert newProducts;
			for (Product2 p : newProducts) {
				setProducts.put(p.ProductCode, p);
			}
		}
	}
	
	/* ensure that each product in the Set Class has a standardPricebook entry in the Set Class standardPricebook */
	public void ensurePricebookEntries() {
		
		/*
		for (List<PricebookEntry> pbes : [select Id, Product2Id from PricebookEntry 
										  where Pricebook2Id = :standardPricebook.Id
										  // notice that Apex allows a collection of sobjects instead of a Set<Id> for IN
										  and Product2Id in :setProducts.values()]) {
			for (PricebookEntry pbe : pbes) {
				setPricebookEntriesByProductId.put(pbe.Product2Id, pbe);
			}
		}
		*/
		
		Map<Id,PricebookEntry> standardPriceBookEntries = new Map<Id,PricebookEntry>();
		for (List<PricebookEntry> pbes : [select Id, Product2Id from PricebookEntry 
										  where Pricebook2Id = :standardPricebookId
										  // notice that Apex allows a collection of sobjects instead of a Set<Id> for IN
										  and Product2Id in :setProducts.values()]) {
			for (PricebookEntry pbe : pbes) {
				standardPriceBookEntries.put(pbe.Product2Id, pbe);
			}
		}
		
		for (List<PricebookEntry> pbes : [select Id, Product2Id from PricebookEntry 
										  where Pricebook2Id = :customPricebook.Id
										  // notice that Apex allows a collection of sobjects instead of a Set<Id> for IN
										  and Product2Id in :setProducts.values()]) {
			for (PricebookEntry pbe : pbes) {
				setPricebookEntriesByProductId.put(pbe.Product2Id, pbe);
			}
		}
		
		List<PricebookEntry> newEntries = new List<PricebookEntry>();
		for (Product2 setProduct : setProducts.values())  {
			if (setPricebookEntriesByProductId.get(setProduct.Id) == null) {
				if (standardPriceBookEntries.get(setProduct.Id) == null){
					newEntries.add(new PricebookEntry(Pricebook2Id = standardPricebookId, Product2Id = setProduct.Id, UnitPrice = 0, IsActive=true));
				}
				//Custom PriceBook
				newEntries.add(new PricebookEntry(Pricebook2Id = customPricebook.Id, Product2Id = setProduct.Id, UnitPrice = 0, IsActive=true, AvailableForSale__c = true));
			}
		}
		insert newEntries;
		for (PricebookEntry e : newEntries) {
			setPricebookEntriesByProductId.put(e.Product2Id, e);
		}
	}
	
	public void updateExistingActiveSetClasses() {
		for (Set_Class__c sc : [select Id, Active__c from Set_Class__c 
								where EquipmentNumber__c = :parsed.get(1).get(0)
								and Active__c = true]) {
			existingSetClasses.add(sc);
			if (sc.Active__c) {
				// workflow and unique field ensures only one active per Name so can do this update in a loop
				// without sacrificing efficieny or risking governor limits 
				sc.Active__c = false;
				update sc;
			}			
		}
	} 
	
	public void create() {
		newSetClass = new Set_Class__c(Active__c=true, Name=parsed.get(1).get(0), EquipmentNumber__c=parsed.get(1).get(0));
		insert newSetClass;
		for (Integer i = 0; i < parsed.size(); i++) {
			if (i != 0) {
				if (isParsedRowValid(parsed.get(i))) {
					List<String> row = parsed.get(i);
					if (setProducts.get(row.get(3)) == null) {
						throw new ParseException('product not found: '+row);
					}
					newSetClassItems.add(
						new Set_Class_Items__c(Set_Class__c=newSetClass.Id,
											   Product__c=setProducts.get(row.get(3)).Id,
											   Quantity__c=Double.valueOf(row.get(4))));
				}
			}
		}
		insert newSetClassItems;
	}
	
	public class ParseException extends Exception {}

}