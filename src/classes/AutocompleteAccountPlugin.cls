public class AutocompleteAccountPlugin implements AutocompleteNGController.AutoCompletePlugin {

	public void init(Map<String,String> context){}
	
	public List<Map<String, String>> find(String searchTerm) {
		//String searchTermWild = '*' + String.escapeSingleQuotes(searchTerm) + '*';
		String searchTermWild = String.escapeSingleQuotes(searchTerm) + '*';
    	List<List<SObject>> searchList = [FIND :searchTermWild IN ALL FIELDS 
    								  	  RETURNING Account (Id)];   
    	
    	List<String> accountGroupCodesToFilter = CustomSettingsTools.getAllAccountAutoCompleteAccountGroupCodes();
    	if (accountGroupCodesToFilter == null || accountGroupCodesToFilter.size()== 0)
    		accountGroupCodesToFilter.add('0001'); //default Sold to Customer
    	  	   								  	  
    	List<Map<String, String>> results = new List<Map<String, String>>();
    	// for best performance use bulk fetch for SOQL							  	  
    	for (List<Account> accs : [select ac.Name, ac.Owner.FirstName, ac.Owner.LastName,ac.AccountGroupCode__c
    							   from Account ac
    							   where ac.Id in :searchList.get(0) And (ac.AccountGroupCode__c IN :accountGroupCodesToFilter) ]) {
    		for (Account a : accs) {
	    		results.add(new Map<String, String>
					{'id' 		=> a.Id,
					 'name' 	=> a.Name + ' (AG: ' + a.AccountGroupCode__c + ')',
					 'value' 	=> a.Name,
					 'owner' 	=> a.Owner.FirstName+' '+a.Owner.LastName});
	    	}
    	}
    	//'owner' 	=> a.Owner.FirstName+' '+a.Owner.LastName
    	return results;
	}
	
	
	public String filter(Map<String, String> oneSObject) {		
		return null;
	}
	
}