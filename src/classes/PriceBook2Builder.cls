public with sharing class PriceBook2Builder {
	
	private static Integer sequence = 100;

	private String priceBookName;
	private String organizationCode;
	private String distributionChannel;
	private String division;
	
	//Mandatory fields with defaults
	private Boolean isActive = true;

	public PriceBook2Builder name(String priceBookName)
	{
		this.priceBookName = priceBookName;
		return this;
	}

	public PriceBook2Builder organizationCode(String organizationCode)
	{
		this.organizationCode = organizationCode;
		return this;
	}
	
	public PriceBook2Builder distributionChannel(String distributionChannel)
	{
		this.distributionChannel = distributionChannel;
		return this;
	}
	
	public PriceBook2Builder division(String division)
	{
		this.division = division;
		return this;
	}
	
	public Pricebook2 build(Boolean createInDatabase)
	{

		Pricebook2 pricebook2 = new Pricebook2(
			Name = this.priceBookName,
			IsActive = this.isActive,
			Description = this.priceBookName
		);
		
		
		if (this.organizationCode != null)
			pricebook2.OrganizationCode__c = this.organizationCode;
		if (this.distributionChannel != null)
			pricebook2.DistributionChannel__c = this.distributionChannel;
		
		/*
		Business Logic has changed, Division is moved to PBE. 
		if (this.division != null)
			pricebook2.Division__c = this.division;
		*/
		
		if (createInDatabase)
			insert pricebook2;
		
		return pricebook2;
	}
}