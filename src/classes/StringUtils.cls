global with sharing class StringUtils {
	global static final String EMPTY = '';
    global static final String LF = '\n';
    global static final String CR = '\r';
    global static final Integer INDEX_NOT_FOUND = -1;

    private static final Integer PAD_LIMIT = 8192;

	global static String leftPad(String str, Integer size, String padStr) {
        if (str == null) {
            return null;
        }
        if (isEmpty(padStr)) {
            padStr = ' ';
        }
        Integer padLen = padStr.length();
        Integer strLen = str.length();
        Integer padCharCount = size - strLen;
        if (padCharCount <= 0) {
            return str;
        }
        if (padCharCount == padLen) {
            return padStr + str;
        } else if (padCharCount < padLen) {
            return padStr.substring(0, padCharCount) + str;
        } else {
            String padding = '';
            for (Integer i = 0; i < padCharCount; i++) {
                padding += padStr.substring(Math.mod(i,padLen),Math.mod(i,padLen)+1);
            }
            return padding + str;
        }
    }
    
    
    global static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
    
    global static boolean isNullOrEmpty(String str) {
        return str == null || str.length() == 0;
    }
    
    global static boolean isNullOrWhiteSpace(String str) {
        return str == null || str.trim() == null || str.trim().length() == 0;
    }
    
    global static String validateId(String Idparam) {
        String id = String.escapeSingleQuotes(Idparam);
        if((id.length() == 15 || id.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', id)) {
            return id;
        }
        return null;
	}
	
	global static String joinArray(Object[] objectArray) {
        return joinArray(objectArray, null);
    }
    
    global static String joinArray(Object[] objectArray, String separator) {
        if (objectArray == null) {
            return null;
        }
        return joinArray(objectArray, separator, 0, objectArray.size());
    }

    global static String joinArray(Object[] objectArray, String separator, Integer startIndex, Integer endIndex) {
        if (objectArray == null) {
            return null;
        }
        if (separator == null) {
            separator = EMPTY;
        }

        String buf = '';
        if(startIndex < 0){
            startIndex = 0;
        }
        Boolean isFirst = true;
        for (Integer i = startIndex; i < endIndex && i < objectArray.size(); i++) {
            if(objectArray[i] != null) {
	            if(isFirst){
	                isFirst = false;
	            } else {
	                buf += separator;
	            }
	            buf += objectArray[i];
            }
        }
        return buf;
    }
    
}