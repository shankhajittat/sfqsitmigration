public with sharing class QuoteRouterControllerExtension {

	public final Id quoteId;
	public Id accountId {get; set;}
	public String retURL;
	public String quoteType;
	public QuoteRouterControllerExtension(ApexPages.StandardController stdController) {
        this.quoteId = stdController.getRecord().Id;
	}
	
	// use the retURL which is always passed by standard buttons to resolve the account id
	public Id getMasterRecordId() {
		try {
			retURL = ApexPages.currentPage().getParameters().get('retURL');
			quoteType = ApexPages.currentPage().getParameters().get('type');
			if (retURL == null) {
				return null;
			} else {
				// If this page has been initiated from the custom quote tab then the url will not have a valid account so the account picker needs to be enabled
				String idInURL = retURL.substring(1);
				String validatedId = StringUtils.validateId(idInURL);
				return validatedId;
			}
		} catch (Exception e) {
	 		ApexPages.addMessages(e);
	 		return null;
		}
	}

	public PageReference route() {
		Boolean isNewQuote = this.quoteId == null;
		
		if (isNewQuote) {
			String masterId = getMasterRecordId();
			Boolean masterIsAccount = masterId != null && masterId.startsWith('001');
			Boolean masterIsOpp = masterId != null && masterId.startsWith('006');
			
			
			
			if (masterIsAccount) {
				//PageReference pr = new PageReference('/apex/QuoteType?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.CustomerQuote.ordinal() +
				//'&retURL=' + retURL);
				PageReference pr;
				if(quoteType <> null && quoteType == 'SetRequest')
				{
				 	pr = new PageReference('/apex/QuoteCreate?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.SetRequest.ordinal() +
					'&retURL=' + retURL);
				}
				else if(quoteType <> null && quoteType == 'LoanSetRequest')
				{
				 	pr = new PageReference('/apex/QuoteCreate?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.LoanSetRequest.ordinal() +
					'&retURL=' + retURL);
				}
				else if(quoteType <> null && quoteType == 'ChangeSet')
				{
				 	pr = new PageReference('/apex/QuoteCreate?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.ChangeSet.ordinal() +
					'&retURL=' + retURL);
				}
				else
				{
					pr = new PageReference('/apex/QuoteCreate?accId='+ masterId + '&quoteType=' + GlobalConstants.QuoteTypes.CustomerQuote.ordinal() +
					'&retURL=' + retURL);
				}
				pr.setRedirect(true);
				return pr;
			} else if (masterIsOpp) {
				Opportunity o = [select Account.Id from Opportunity where Id = :masterId];
				PageReference pr = new PageReference('/apex/QuoteType?accId='+o.Account.Id + '&retURL=' + retURL);
				pr.setRedirect(true);
				return pr;
			} else {
				PageReference pr = new PageReference('/apex/Quote_Account_Picker?quoteType=' + GlobalConstants.QuoteTypes.CustomerQuote.ordinal());
				pr.setRedirect(true);
				return pr;
			}		
			return null; 	
		} else {
			Quote__c q = [select Quote__r.Id from Quote__c where Id = :this.quoteId];
			PageReference target = Page.QuoteEditor;
			target.getParameters().put('id',q.Quote__r.Id);
			target.setRedirect(true);
			return target;
		}
	}

    


}