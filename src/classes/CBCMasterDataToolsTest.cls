@isTest(SeeAllData=true)
private class CBCMasterDataToolsTest {
	
	/*
	static testMethod void upsertConsignmentBusinessCaseMasterRecordsV001Test_2() {
        //cover testcoverage. checking object as null.
        List<MasterDataAPIv001.ConsignmentBusinessCase> consignmentBusinessCases = null;
        MasterDataAPIv001.ReturnResult rr = ConsignmentBusinessCaseMasterDataTools.upsertConsignmentBusinessCaseMasterRecordsV001(consignmentBusinessCases);
        System.debug('ReturnResult>>>>>:' + rr);
        System.assert(rr.isSuccessful == false,'Was expecting False');

	}
	*/
	
    
    static testMethod void upsertConsignmentBusinessCaseMasterRecordsV001Test_1() {
        
        List<MasterDataAPIv001.ConsignmentBusinessCase> consignmentBusinessCases = new List<MasterDataAPIv001.ConsignmentBusinessCase>();
        
        String cbcCustomerNumber = 'CBCA1010';
        String cbcCustomerShipTo = 'CBCPF1010';
        String cbcReferenceNumber1 = 'CBCKR10-1000023-1';
		String cbcReferenceNumber2 = 'CBCKR10-1000043-1';
		Integer cbcMaster1ItemsCount = 0;
		Integer cbcMaster2ItemsCount = 0;
		
        MasterDataAPIv001.OrganizationContext orgContext = new MasterDataAPIv001.OrganizationContext();
        orgContext.organizationCode = 'UT-AU10';
        orgContext.distributionChannel = '01';
        orgContext.division = '01';
        
        String productCode1 = '1916G';
        String productCode2 = 'W8321';
        String productCode3 = 'ECS29A';
        String productCode4 = '1696G';
        String productCode5 = '1735G';
        String productCode6 = '1901GB';
        String productCode7 = '1903GB';
        String productCode8 = '1916G';
        String productCode9 = 'ACE36E';
        String productCode10 = 'NOT-FOUND-ITEM-101';
        String productCode11 = '1904GB';
        
        //Create Test Account
        //
       
        Account account = new AccountBuilder()
        	.name('CBC - St Vinceant')
        	.accountNumber(cbcCustomerNumber)
        	.organizationCode(orgContext.organizationCode)
        	.distributionChannel(orgContext.distributionChannel)
        	.division(orgContext.division)
        	.build(true);
        System.assert(account.Id <> null,'An error occured while creating Account record.');
        
        
        Address__c pfShipToAddress = new Address__c(
        	Account_Address__c = account.Id,
			Name = 'CBCPF1010',
			ERP_Id__c = 'CBCPF1010',
			Address_Name__c = 'CBC-SB',
			AddressType__c = 'SB',
			Street1__c = 'PF Street1',
			City__c = 'PF City',
			Country__c = 'Australia',
			Postal_Code__c = '2000',
			Telephone1__c = '23232323',
			Mobile1__c = '9787867',
			Email__c = 'pfemail@email.com'
		);
        insert pfShipToAddress;
        System.assert(pfShipToAddress.Id <> null,'An error occured while creating pfAddress record.');
        
        pfShipToAddress = new Address__c(
        	Account_Address__c = account.Id,
			Name = 'CBCPF1010',
			ERP_Id__c = 'CBCPF1010',
			Address_Name__c = 'CBC-WE',
			AddressType__c = 'WE',
			Street1__c = 'PF Street1',
			City__c = 'PF City',
			Country__c = 'Australia',
			Postal_Code__c = '2000',
			Telephone1__c = '23232323',
			Mobile1__c = '9787867',
			Email__c = 'pfemail@email.com'
		);
        insert pfShipToAddress;
        System.assert(pfShipToAddress.Id <> null,'An error occured while creating pfAddress record.');
        	    
	    Pricebook2 pricebook = null;
	    try{
	    	pricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: orgContext.organizationCode 
	    			And DistributionChannel__c =: orgContext.distributionChannel LIMIT 1];
	    }
	    catch(Exception e){
	    	pricebook = null;
	    }
	    if (pricebook == null){
        	pricebook = new PriceBook2Builder()
        			.name(orgContext.organizationCode + orgContext.distributionChannel)
        			.organizationCode(orgContext.organizationCode)
        			.distributionChannel(orgContext.distributionChannel)
        			.build(true);
        	System.assert(pricebook.Id <> null,'An error occured while creating Pricebook: ' + orgContext.organizationCode + orgContext.distributionChannel);
	    }
	    
        //Create Test Products
        Product2 product = new ProductBuilder()
        		.name(productCode1 +'-Description')
        		.code(productCode1)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        PricebookEntry pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        //
        product = new ProductBuilder()
        		.name(productCode2 +'-Description')
        		.code(productCode2)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        //
        product = new ProductBuilder()
        		.name(productCode3 +'-Description')
        		.code(productCode3)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
       	//
        product = new ProductBuilder()
        		.name(productCode4 +'-Description')
        		.code(productCode4)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
       //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
       	//
        product = new ProductBuilder()
        		.name(productCode5 +'-Description')
        		.code(productCode5)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        //
        product = new ProductBuilder()
        		.name(productCode6 +'-Description')
        		.code(productCode6)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
       //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        //
        product = new ProductBuilder()
        		.name(productCode7 +'-Description')
        		.code(productCode7)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        //
        product = new ProductBuilder()
        		.name(productCode8 +'-Description')
        		.code(productCode8)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        product = new ProductBuilder()
        		.name(productCode9 +'-Description')
        		.code(productCode9)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
       //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        product = new ProductBuilder()
        		.name(productCode11 +'-Description')
        		.code(productCode11)
        		.build(true);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
       //Create PricebookEntry for product
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        pbEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(pricebook.Id).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        
        
        
        MasterDataAPIv001.ConsignmentBusinessCase cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = cbcReferenceNumber1;
        cbc.customerNumber = cbcCustomerNumber;
        cbc.shipTo = cbcCustomerShipTo;
        cbc.description = 'CBCKR10-1000023-1 Description';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 1000;
        cbc.department = 'CBCKR10 Department';
        cbc.canOverride = true;
        cbc.isDeleted = false;
       	cbc.Items = new List<MasterDataAPIv001.ConsignmentBusinessCaseItem>();
       	
		MasterDataAPIv001.ConsignmentBusinessCaseItem sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 50;
		sapCBCItem.productCode = productCode1;
		sapCBCItem.originalQuantity = 36;
		sapCBCItem.currentBalance = 36;
		sapCBCItem.newBalance = 36;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 54;
		sapCBCItem.productCode = productCode2;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 55;
		sapCBCItem.productCode = productCode3;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		//NOT FOUND PRODUCT
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 56;
		sapCBCItem.productCode = productCode10;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		//Null PRODUCT
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 57;
		sapCBCItem.productCode = null;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		//Delete flag for New CBC
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 58;
		sapCBCItem.productCode = productCode3;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		sapCBCItem.isDeleted = true;
		cbc.Items.add(sapCBCItem);
		
		cbcMaster1ItemsCount = cbc.Items.size();
		consignmentBusinessCases.add(cbc);
		

		//Second CBC
		cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = cbcReferenceNumber2;
        cbc.customerNumber = cbcCustomerNumber;
        cbc.shipTo = cbcCustomerShipTo;
        cbc.description = 'CBCKR10-1000043-1 Description';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 2000;
        cbc.department = 'CBCKR10 Department';
        cbc.canOverride = true;
        cbc.isDeleted = false;
       	cbc.Items = new List<MasterDataAPIv001.ConsignmentBusinessCaseItem>();
       	
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 2;
		sapCBCItem.productCode = productCode4;
		sapCBCItem.originalQuantity = 36;
		sapCBCItem.currentBalance = 181;
		sapCBCItem.newBalance = 181;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 3;
		sapCBCItem.productCode = productCode5;
		sapCBCItem.originalQuantity = 24;
		sapCBCItem.currentBalance = 36;
		sapCBCItem.newBalance = 36;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 4;
		sapCBCItem.productCode = productCode6;
		sapCBCItem.originalQuantity = 300;
		sapCBCItem.currentBalance = 320.004;
		sapCBCItem.newBalance = 320.004;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 5;
		sapCBCItem.productCode = productCode7;
		sapCBCItem.originalQuantity = 180;
		sapCBCItem.currentBalance = 180;
		sapCBCItem.newBalance = 180;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 6;
		sapCBCItem.productCode = productCode8;
		sapCBCItem.originalQuantity = 36;
		sapCBCItem.currentBalance = 72;
		sapCBCItem.newBalance = 72;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 427;
		sapCBCItem.productCode = productCode8;
		sapCBCItem.originalQuantity = 24;
		sapCBCItem.currentBalance = 0;
		sapCBCItem.newBalance = 0;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 603;
		sapCBCItem.productCode = productCode9;
		sapCBCItem.originalQuantity = 60;
		sapCBCItem.currentBalance = 48.996;
		sapCBCItem.newBalance = 48.996;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		cbcMaster2ItemsCount = cbc.Items.size();
		consignmentBusinessCases.add(cbc);
		
		//3rd CBC with blank reference number and customer info
		cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = '';
        cbc.customerNumber = '';
        cbc.shipTo = '';
        cbc.description = 'CBCKR10-1000043-1 Description';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 2000;
        cbc.department = 'CBCKR10 Department';
        cbc.canOverride = true;
       	cbc.Items = new List<MasterDataAPIv001.ConsignmentBusinessCaseItem>();
       	consignmentBusinessCases.add(cbc);
        
        //4th CBC with blank customer info
		cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = 'CBC-BLANK-CUST';
        cbc.customerNumber = '';
        cbc.shipTo = '';
        cbc.description = 'CBCKR10-1000043-1 Description';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 2000;
        cbc.department = 'CBCKR10 Department';
        cbc.canOverride = true;
       	cbc.Items = new List<MasterDataAPIv001.ConsignmentBusinessCaseItem>();
       	consignmentBusinessCases.add(cbc);
       	
       	 //5th CBC with no Items
       	 
		cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = 'CBC-NOTFOUNDCUST';
        cbc.customerNumber = 'NOCUST101';
        cbc.shipTo = 'NOSBSHIPTO101';
        cbc.description = 'CBC-NOTFOUNDCUST';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 2000;
        cbc.department = 'CBC-NOTFOUNDCUST Department';
        cbc.canOverride = true;
       	cbc.Items = null;
       	consignmentBusinessCases.add(cbc);
        
        
        MasterDataAPIv001.ReturnResult rr = ConsignmentBusinessCaseMasterDataTools.upsertConsignmentBusinessCaseMasterRecordsV001(consignmentBusinessCases);
        System.assert(rr.isSuccessful == false,'Was expecting False');
        System.debug('ReturnResult>>>>>:' + rr);
        
        //Retrieve 1st newly created CBC Master Record
        Consignment_Business_Case__c cbc1 = [Select c.TotalValue__c, c.SystemModstamp, c.ShippingLocation__c, c.RecordSource__c, c.OwnerId, c.Name, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.ERP_Id__c, c.Description__c, c.Department__c, c.CreatedDate, c.CreatedById, c.CanOverride__c, c.Agreement_To_Date__c, c.Agreement_From_Date__c, c.Account__c 
        										From Consignment_Business_Case__c c Where c.Name =: cbcReferenceNumber1 LIMIT 1];
        System.debug('1st CBC Master Insert Record: ' + cbc1);
        
        List<Consignment_Business_Case_Item__c> cbc1CreatedItems = [Select c.SystemModstamp, c.Product__c, c.Product_Description__c, c.OriginalQuantity__c, c.Name, c.Line_Number__c, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.CreatedDate, c.CreatedById, c.Consignment_Business_Case__c, c.CanOverride__c, c.Agreed_Quantity__c, c.Actual_Quantity__c 
        														From Consignment_Business_Case_Item__c c Where c.Consignment_Business_Case__c =: cbc1.Id];
        System.debug('1st CBC Master Record Object Items Count: ' + cbc1CreatedItems.size());
        System.debug('1st CBC Master Record Inserted Items: ' + cbc1CreatedItems);
        System.assert((cbcMaster1ItemsCount - 3) == cbc1CreatedItems.size(), 'Was expecting ' + (cbcMaster1ItemsCount - 3) + ' record(s), but created ' + cbc1CreatedItems.size() + ' record(s)');
        
        
         //Retrieve 2nd newly created CBC Master Record
        Consignment_Business_Case__c cbc2 = [Select c.TotalValue__c, c.SystemModstamp, c.ShippingLocation__c, c.RecordSource__c, c.OwnerId, c.Name, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.ERP_Id__c, c.Description__c, c.Department__c, c.CreatedDate, c.CreatedById, c.CanOverride__c, c.Agreement_To_Date__c, c.Agreement_From_Date__c, c.Account__c 
        										From Consignment_Business_Case__c c Where c.Name =: cbcReferenceNumber2 LIMIT 1];
        System.debug('2nd CBC Master Inserted Record: ' + cbc2);
        
        List<Consignment_Business_Case_Item__c> cbc2CreatedItems = [Select c.SystemModstamp, c.Product__c, c.Product_Description__c, c.OriginalQuantity__c, c.Name, c.Line_Number__c, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.CreatedDate, c.CreatedById, c.Consignment_Business_Case__c, c.CanOverride__c, c.Agreed_Quantity__c, c.Actual_Quantity__c 
        														From Consignment_Business_Case_Item__c c Where c.Consignment_Business_Case__c =: cbc2.Id];

        System.debug('2nd CBC Master Record Object Items Count: ' + cbc2CreatedItems.size());
        System.debug('2nd CBC Master Record Inserted Items: ' + cbc2CreatedItems);     
        //duplicate product 1  
        System.assert((cbcMaster2ItemsCount - 1) == cbc2CreatedItems.size(), 'Was expecting ' + (cbcMaster2ItemsCount - 1) + ' record(s), but created ' + cbc2CreatedItems.size() + ' record(s)');

        
        
        //
        //	Update Logic To Test
        //
        consignmentBusinessCases = new List<MasterDataAPIv001.ConsignmentBusinessCase>();
        
        cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = cbcReferenceNumber1;
        cbc.customerNumber = cbcCustomerNumber;
        cbc.shipTo = cbcCustomerShipTo;
        cbc.description = 'CBCKR10-1000023-1 Description';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 5000;
        cbc.department = 'CBCKR10 Department';
        cbc.canOverride = true;
       	cbc.Items = new List<MasterDataAPIv001.ConsignmentBusinessCaseItem>();
       	
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 50;
		sapCBCItem.productCode = productCode1;
		sapCBCItem.originalQuantity = 36;
		sapCBCItem.currentBalance = 30;
		sapCBCItem.newBalance = 30;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 54;
		sapCBCItem.productCode = productCode2;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 55;
		sapCBCItem.productCode = productCode3;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		//NOT FOUND PRODUCT
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 56;
		sapCBCItem.productCode = productCode10;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		//Insert new line part of Update
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 57;
		sapCBCItem.productCode = productCode11;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		//Null PRODUCT
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 58;
		sapCBCItem.productCode = null;
		sapCBCItem.originalQuantity = 1;
		sapCBCItem.currentBalance = 1;
		sapCBCItem.newBalance = 1;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		cbcMaster1ItemsCount = cbc.Items.size();
		consignmentBusinessCases.add(cbc);
		

		//Second CBC
		cbc = new MasterDataAPIv001.ConsignmentBusinessCase();
        cbc.organizationContext = orgContext;
        cbc.erpReferenceNumber = cbcReferenceNumber2;
        cbc.customerNumber = cbcCustomerNumber;
        cbc.shipTo = cbcCustomerShipTo;
        cbc.description = 'CBCKR10-1000043-1 Description';
        cbc.agreementFromDate = Date.today();
        cbc.agreementToDate = Date.today().addYears(50);
        cbc.lastUpdatedDate = Date.today().addDays(-1);
        cbc.totalValue = 6000;
        cbc.department = 'CBCKR10 Department';
        cbc.canOverride = true;
        cbc.isDeleted = true;
       	cbc.Items = new List<MasterDataAPIv001.ConsignmentBusinessCaseItem>();
       	
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 2;
		sapCBCItem.productCode = productCode4;
		sapCBCItem.originalQuantity = 36;
		sapCBCItem.currentBalance = 181;
		sapCBCItem.newBalance = 181;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 3;
		sapCBCItem.productCode = productCode5;
		sapCBCItem.originalQuantity = 24;
		sapCBCItem.currentBalance = 36;
		sapCBCItem.newBalance = 36;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 4;
		sapCBCItem.productCode = productCode6;
		sapCBCItem.originalQuantity = 300;
		sapCBCItem.currentBalance = 320.004;
		sapCBCItem.newBalance = 320.004;
		sapCBCItem.canOverride = true;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 5;
		sapCBCItem.productCode = productCode7;
		sapCBCItem.originalQuantity = 180;
		sapCBCItem.currentBalance = 160;
		sapCBCItem.newBalance = 160;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 6;
		sapCBCItem.productCode = productCode8;
		sapCBCItem.originalQuantity = 36;
		sapCBCItem.currentBalance = 72;
		sapCBCItem.newBalance = 72;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 427;
		sapCBCItem.productCode = productCode8;
		sapCBCItem.originalQuantity = 24;
		sapCBCItem.currentBalance = 20;
		sapCBCItem.newBalance = 20;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		cbc.Items.add(sapCBCItem);
		
		sapCBCItem = new MasterDataAPIv001.ConsignmentBusinessCaseItem();
		sapCBCItem.sequenceNumber = 603;
		sapCBCItem.productCode = productCode9;
		sapCBCItem.originalQuantity = 60;
		sapCBCItem.currentBalance = 48.996;
		sapCBCItem.newBalance = 48.996;
		sapCBCItem.canOverride = false;
		sapCBCItem.lastUpdatedDate = Date.today().addDays(-1);
		sapCBCItem.isDeleted = true;
		cbc.Items.add(sapCBCItem);
		
		cbcMaster2ItemsCount = cbc.Items.size();
		consignmentBusinessCases.add(cbc);
		
		rr = ConsignmentBusinessCaseMasterDataTools.upsertConsignmentBusinessCaseMasterRecordsV001(consignmentBusinessCases);
        System.assert(rr.isSuccessful == true,'Was expecting true');
        System.debug('ReturnResult>>>>>:' + rr);
        
        //Retrieve 1st newly created CBC Master Record
        Consignment_Business_Case__c cbcUpdated1 = [Select c.TotalValue__c, c.SystemModstamp, c.ShippingLocation__c, c.RecordSource__c, c.OwnerId, c.Name, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.ERP_Id__c, c.Description__c, c.Department__c, c.CreatedDate, c.CreatedById, c.CanOverride__c, c.Agreement_To_Date__c, c.Agreement_From_Date__c, c.Account__c 
        										From Consignment_Business_Case__c c Where c.Name =: cbcReferenceNumber1 LIMIT 1];
        System.debug('1st CBC Master Upserted Record: ' + cbcUpdated1);
        System.assert(cbc1.TotalValue__c <> cbcUpdated1.TotalValue__c,'Was expecting different total value');
        
        List<Consignment_Business_Case_Item__c> cbc1UpdatedItems = [Select c.SystemModstamp, c.Product__c, c.Product_Description__c, c.OriginalQuantity__c, c.Name, c.Line_Number__c, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.CreatedDate, c.CreatedById, c.Consignment_Business_Case__c, c.CanOverride__c, c.Agreed_Quantity__c, c.Actual_Quantity__c 
        														From Consignment_Business_Case_Item__c c Where c.Consignment_Business_Case__c =: cbcUpdated1.Id];
        
        System.debug('1st CBC Master Record Object Items Count: ' + cbc1UpdatedItems.size());
        System.debug('1st CBC Master Record Upserted Items: ' + cbc1UpdatedItems);
        System.assert((cbcMaster1ItemsCount - 2) == cbc1UpdatedItems.size(), 'Was expecting ' + (cbcMaster1ItemsCount - 2) + ' record(s), but created ' + cbc1UpdatedItems.size() + ' record(s)');
 
        
        
         //Retrieve 2nd newly created CBC Master Record
        Consignment_Business_Case__c cbcUpdated2 = [Select c.TotalValue__c, c.SystemModstamp, c.ShippingLocation__c, c.RecordSource__c, c.OwnerId, c.Name, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.ERP_Id__c, c.Description__c, c.Department__c, c.CreatedDate, c.CreatedById, c.CanOverride__c, c.Agreement_To_Date__c, c.Agreement_From_Date__c, c.Account__c 
        										From Consignment_Business_Case__c c Where c.Name =: cbcReferenceNumber2 LIMIT 1];
        System.debug('2nd CBC Master Upserted Record: ' + cbcUpdated2);
        
        List<Consignment_Business_Case_Item__c> cbc2UpdatedItems = [Select c.SystemModstamp, c.Product__c, c.Product_Description__c, c.OriginalQuantity__c, c.Name, c.Line_Number__c, c.LastModifiedDate, c.LastModifiedById, c.IsDeleted, c.Id, c.ErpLastUpdatedDate__c, c.CreatedDate, c.CreatedById, c.Consignment_Business_Case__c, c.CanOverride__c, c.Agreed_Quantity__c, c.Actual_Quantity__c 
        														From Consignment_Business_Case_Item__c c Where c.Consignment_Business_Case__c =: cbcUpdated2.Id];

        System.debug('2nd CBC Master Record Object Items Count: ' + cbc2UpdatedItems.size());
        System.debug('2nd CBC Master Record Upserted Items: ' + cbc2UpdatedItems);       
        System.assert(cbcMaster2ItemsCount - 2 == cbc2UpdatedItems.size(), 'Was expecting ' + (cbcMaster2ItemsCount - 2) + ' record(s), but created ' + cbc2UpdatedItems.size() + ' record(s)');

        
    }
    
}