@isTest(seeAllData=true) // required for tests that use pricebooks
private class QuotationControllerExtensionTests {

	@IsTest 
    public static void testAccountIdMode() {   
    	
		String organizationCode = 'AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').setErpAccountId('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel And Division__c =: division LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel + division)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.division(division)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel + division);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
        
 		SetClassParser parser = new SetClassParser(TestingTools.getResourceBodyAsString('SetClass_JH230'));
 		parser.run();
 		Set_Class__c setClass = parser.newSetClass;
    	  	
    	PageReference pageRef = Page.Quotation;
    	pageRef.getParameters().put('accId', vinnies.Id);
        Test.setCurrentPage(pageRef);
        
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote());
    	QuotationControllerExtension ext = new QuotationControllerExtension(stdController);
    	
    	System.assert(ext.account.Id != null, 'constructor should load the account sobject');
    	System.assert(ext.account.Name != null, 'constructor should load the account name for display');

    	System.assert(ext.getPricebook().Id != null, 'constructor should load the standard pricebook');
    	
    	System.assertEquals(ext.getGroupsWithRollups().size(), 0, 'No groups in account mode');
    	
    	ext.setClassId = setClass.Id;
    	PageReference urlAfterFirstSetClass = ext.chooseSet();
    	System.assertNotEquals(null, urlAfterFirstSetClass, 'a new url is generated');
    	System.assertNotEquals(null, ext.extensionQuote, 'a new quote is generated');
    	System.assertEquals('/apex/quotation?id='+ext.extensionQuote.Id, urlAfterFirstSetClass.getUrl(),'new url should be the same page');
    	System.assertEquals(1, ext.getGroupsWithRollups().size(), 'A group is present after first set add in account mode');
    	
    	//Test updateGroupsInViewState method
    	ext.updateGroupsInViewState();
    	System.assertEquals(1, ext.groups.size());
    	
    	//***************************
    	//test addItem Action Method - begin
    	//***************************
    	Product2 newProduct = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('T.03.161.001.T')
        		.build(true);
        System.assert(newProduct.Id <> null,'An error occured while creating Product.');
        
       //Create PricebookEntry for product
        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
        									.productId(newProduct.Id)
        									.pricebook(standardPriceBook).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
        
         //Create PricebookEntry for Custom Pricebook
        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
        									.productId(newProduct.Id)
        									.pricebook(customPricebook).build(true);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');

        
        double existngItemCountForGroup = 0;
        if (ext.groups.containsKey(setClass.Id)) {
        	ItemGroupWrapper grp = ext.groups.get(setClass.Id);
        	System.assertNotEquals(null, grp, 'Could not find Group from GroupViewState');
        	existngItemCountForGroup = grp.Items.size();
        }
        
        ext.addingGroupClassId = setClass.Id;
        ext.productId = newProduct.Id;
        PageReference addNewItem = ext.addItem();
        Id newQuotationLineId = ext.newQuotationLineId;
        
        double newItemCountForGroup = 0;
        if (ext.groups.containsKey(setClass.Id)) {
        	ItemGroupWrapper grp = ext.groups.get(setClass.Id);
        	System.assertNotEquals(null, grp, 'Could not find Group from GroupViewState');
        	newItemCountForGroup = grp.Items.size();
        }
        System.assertEquals(newItemCountForGroup, existngItemCountForGroup + 1,'New Item failed to add.');
        System.debug('>>>>><<<<<Item Count: Existing: ' + existngItemCountForGroup + ' New Count: ' + newItemCountForGroup);
    	//***************************
    	//test addItem Action Method - end
    	//***************************
    	
    	//test getLineItemFromViewState method
    	QuoteLineItem getQuoteLineItem = ext.getLineItemFromViewState(newQuotationLineId);
    	System.assert(getQuoteLineItem <> null, 'Could not find newly created Item in the ItemViewState Collection');
    	
    	//test updateItem with Quantity
    	ext.itemIdToUpdate = newQuotationLineId;
    	ext.fieldToUpdate = 'quantity';
    	ext.valueToUpdate = '25';
    	PageReference pr1 = ext.updateItem();
    	
    	QuoteLineItem updatedQuoteLineItemWithQty = ext.getLineItemFromViewState(newQuotationLineId);
    	System.assertEquals(updatedQuoteLineItemWithQty.Quantity, 25.00, 'UpdateIem action method failed to Update Quantity');
    	
    	//test updateItem with discountP
    	ext.itemIdToUpdate = newQuotationLineId;
    	ext.fieldToUpdate = 'discountP';
    	ext.valueToUpdate = '10';
    	PageReference pr2 = ext.updateItem();
    	
    	QuoteLineItem updatedQuoteLineItemWithDiscountP = ext.getLineItemFromViewState(newQuotationLineId);
    	System.assertEquals(updatedQuoteLineItemWithDiscountP.Discount, 10.00, 'UpdateIem action method failed to Update Discount%');
    	
    	//test updateItem with discountA
    	ext.itemIdToUpdate = newQuotationLineId;
    	ext.fieldToUpdate = 'discountA';
    	ext.valueToUpdate = '5';
    	PageReference pr3 = ext.updateItem();
    	
    	QuoteLineItem updatedQuoteLineItemWithDiscountA = ext.getLineItemFromViewState(newQuotationLineId);
    	System.assertEquals(updatedQuoteLineItemWithDiscountA.Discount, 5 / updatedQuoteLineItemWithDiscountA.UnitPrice * 100, 'UpdateIem action method failed to Update Discount%');
    	
    	//test deleteItem
    	System.Debug('newQuotationLineId: '+ newQuotationLineId);
    	ext.itemIdToDelete = newQuotationLineId;    	
    	PageReference prDelete1 = ext.deleteItem();
    	
    }
    
}