@isTest
global class WebMethodsPriceRequestSoapAPIv1MockImpl implements WebServiceMock {

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       
       WebMethodsPriceRequestSoapAPIv1.processPricingResponse respElement = 
           new WebMethodsPriceRequestSoapAPIv1.processPricingResponse();
       
       WebMethodsPriceRequestSoapAPIv1.pricingReply resp = new WebMethodsPriceRequestSoapAPIv1.pricingReply();
       resp.PriceDetailResponse = new  WebMethodsPriceRequestSoapAPIv1.PriceDetailResponse();
       resp.PriceDetailResponse.isSuccessful = 'true';
       
       respElement.pricingReply = resp;
       response.put('response_x', respElement);
   }
	
}