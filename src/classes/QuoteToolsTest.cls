@isTest(SeeAllData=true)
public class QuoteToolsTest {
	
	static testMethod void testDistributeDiscountPercentageToTheQuote()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			Quote quote = [Select Id,Name, Total_Extended_Unit_Price__c,Total_Discount_Amount__c From Quote Where Id = :sourceQuoteId];
			system.debug('quote=>' + quote);
			
			try{
				QuoteTools.distributeDiscountPercentageToTheQuote(sourceQuoteId,101);
				System.assert(false, 'was expected to fail');
			}
			catch(Exception e){
				System.assert(true, 'Exception ' + e);
			}
			
			try{
				QuoteTools.distributeDiscountPercentageToTheQuote(sourceQuoteId,-1);
				System.assert(false, 'was expected to fail');
			}
			catch(Exception e){
				System.assert(true, 'Exception ' + e);
			}
			
			//Update QuoteLines with Price.
			List<QuoteLineItem> quoteLinesToUpdate = new List<QuoteLineItem>();
			for (List<QuoteLineItem> items : [Select q.Id From QuoteLineItem q
											WHERE q.QuoteId = :sourceQuoteId]) {
				for (QuoteLineItem item : items) {
					QuoteLineItem updateItem = new QuoteLineItem();
					updateItem.Id = item.Id;
					updateItem.UnitPrice = 100;
					quoteLinesToUpdate.add(updateItem);
				}
			}
			if (quoteLinesToUpdate.size() > 0)
				update quoteLinesToUpdate;
			
			QuoteTools.distributeDiscountPercentageToTheQuote(sourceQuoteId,5);
			
			quote = [Select Id,Name, Total_Extended_Unit_Price__c,Total_Discount_Amount__c From Quote Where Id = :sourceQuoteId];
			system.debug('quote=>' + quote);
			system.assert(quote.Total_Discount_Amount__c > 0,'was expecting a valid Discount Amount');
			system.debug('passed distributeDiscountPercentageToTheQuote');
			
		}
	}
	
	static testMethod void testDistributeDiscountAmountToTheQuote()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			Quote quote = [Select Id,Name, Total_Extended_Unit_Price__c,Total_Discount_Amount__c From Quote Where Id = :sourceQuoteId];
			system.debug('quote=>' + quote);
			
			try{
				QuoteTools.distributeDiscountAmountToTheQuote(null,100);
				System.assert(false, 'was expected to fail');
			}
			catch(Exception e){
				System.assert(true, 'Exception ' + e);
			}
			
			try{
				QuoteTools.distributeDiscountAmountToTheQuote(sourceQuoteId,-1);
				System.assert(false, 'was expected to fail');
			}
			catch(Exception e){
				System.assert(true, 'Exception ' + e);
			}
			
			try{
				Double discountAmount = quote.Total_Extended_Unit_Price__c * 2;
				QuoteTools.distributeDiscountAmountToTheQuote(sourceQuoteId,discountAmount);
				System.assert(false, 'was expected to fail');
			}
			catch(Exception e){
				System.assert(true, 'Exception ' + e);
			}
			
			//Update QuoteLines with Price.
			List<QuoteLineItem> quoteLinesToUpdate = new List<QuoteLineItem>();
			for (List<QuoteLineItem> items : [Select q.Id From QuoteLineItem q
											WHERE q.QuoteId = :sourceQuoteId]) {
				for (QuoteLineItem item : items) {
					QuoteLineItem updateItem = new QuoteLineItem();
					updateItem.Id = item.Id;
					updateItem.UnitPrice = 100;
					quoteLinesToUpdate.add(updateItem);
				}
			}
			if (quoteLinesToUpdate.size() > 0)
				update quoteLinesToUpdate;
			
			QuoteTools.distributeDiscountAmountToTheQuote(sourceQuoteId,100);
			
			quote = [Select Id,Name, Total_Extended_Unit_Price__c,Total_Discount_Amount__c From Quote Where Id = :sourceQuoteId];
			system.debug('quote=>' + quote);
			system.assert(quote.Total_Discount_Amount__c > 0,'was expecting a valid Discount Amount');
			system.debug('passed distributeDiscountPercentageToTheQuote');
			
		}
	}
	
	
	static testMethod void testGetAllSubRoleIds()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			User userRole = [Select u.Id, u.UserRoleId, u.UserRole.ParentRoleId From User u Where u.Id =: runAsUser.Id];
			Map<Id,sObject> currentRoleIds = QuoteTools.getAllParentRoleIds(userRole.UserRole.ParentRoleId);
			System.debug('currentRoleIds>>>>> : ' + currentRoleIds);
		}
	}

	/*
	static testMethod void testPopulateQuoteApprovers(){
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		System.debug('testQuote>>>>' + testQuote);
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			boolean isQuoteUpdatedWithApprovers = QuoteTools.populateQuoteApprovers(sourceQuoteId);
			System.assertEquals(isQuoteUpdatedWithApprovers,true,'Was expecting True. populateQuoteApprovers method failed.');
			
			Quote updatedQuote = [Select Id,Name,Level2_Approver1_User__c,Level3_Approver1_User__c From Quote Where Id =: sourceQuoteId];
			System.debug('updatedQuote>>>>>:' + updatedQuote);
			System.assert(updatedQuote.Level2_Approver1_User__c != null, 'Was expecting a valid Level2_Approver1_User__c');
			System.assert(updatedQuote.Level3_Approver1_User__c != null, 'Was expecting a valid Level2_Approver1_User__c');
		}
	}
	*/
	
	static testMethod void testGetAPIQuote()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			APIv001.Quote apiQuote = QuoteTools.getQuote(sourceQuoteId);
			System.debug('apiQuote>>>>>:' + apiQuote);
			System.assert(apiQuote != null, 'Was expecting a valid API Quote');
		}
	}

	static testMethod void testGetQuotePriceRequest()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			
			Quote updateQuoteStatus = new Quote();
			updateQuoteStatus.id = sourceQuoteId;
			updateQuoteStatus.Status = GlobalConstants.QS_STATUS_SUBMITTED_FOR_PRICING;
			update updateQuoteStatus;
			
			APIv001.QuotePriceRequest apiQuotePriceRequest = QuoteTools.getQuotePriceRequest(sourceQuoteId);

			System.debug('apiQuote>>>>>:' + apiQuotePriceRequest);
			System.assert(apiQuotePriceRequest != null, 'Was expecting a valid API QuotePriceRequest');
			System.assert(apiQuotePriceRequest.userContext != null, 'Was expecting a valid API QuotePriceRequest.userContext');
		}
	}

	static testMethod void testSubmitQuoteWithAllQuoteLinesForPricing()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			
			//Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
			//need to impliment external webservice mock

			//Create UnitTest Prerequisite Data.
	        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();	
	
			Test.startTest() ;
    	 	// This causes a fake response to be generated
    	 	
        	Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        	// Call the method that invokes a callout
        	
			Boolean result =  QuoteTools.submitQuoteWithAllQuoteLinesForPricing(sourceQuoteId);
			
			System.assert(result == true, 'Was expecting True');
			System.debug('submitQuoteWithAllQuoteLinesForPricing executed successfully.');
			Test.stopTest();
		}
	}

	static testMethod void testSubmitQuoteWithSelectedGroupForPricing()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			
			Quote_Line_Group__c quoteGroup = [Select Id From Quote_Line_Group__c Where QuoteId__c =: sourceQuoteId LIMIT 1];
			
			//Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
			//need to impliment external webservice mock
	
			//Create UnitTest Prerequisite Data.
        	UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();	
        
			Test.startTest() ;
    	 	// This causes a fake response to be generated

        	Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        	// Call the method that invokes a callout
        	
			Boolean result =  QuoteTools.submitQuoteWithSelectedGroupForPricing(sourceQuoteId, quoteGroup.Id);
			
			System.assert(result == true, 'Was expecting True');
			System.debug('submitQuoteWithAllQuoteLinesForPricing executed successfully.');
			Test.stopTest();
		}
	}
		

	static testMethod void testSubmitQuoteWithSelectedItemsForPricing()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			
			List<Id> quoteLineItemIds = new List<Id>();
			for (List<QuoteLineItem> items : [Select  q.Id
	        				  				  From QuoteLineItem q
	                          				  WHERE q.QuoteId =: sourceQuoteId]) {
	    		for (QuoteLineItem item : items) {
	    			quoteLineItemIds.add(item.Id);
	    		}
	    	}
			
			//Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
			//need to impliment external webservice mock

			//Create UnitTest Prerequisite Data.
	        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();	
	
			Test.startTest() ;
			
    	 	// This causes a fake response to be generated
        	Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        	
        	// Call the method that invokes a callout
			Boolean result =  QuoteTools.submitQuoteWithSelectedItemsForPricing(sourceQuoteId, quoteLineItemIds);
			
			System.assert(result == true, 'Was expecting True');
			System.debug('submitQuoteWithAllQuoteLinesForPricing executed successfully.');
		
			Test.stopTest();
		}
	}
	

	static testMethod void copyQuotationAndRelatedRecords() {
        
        
        User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
	        QuoteTools.QuotationCloneTypes cloneType = QuoteTools.QuotationCloneTypes.CopyQuote;
	        
	        Id newQuoteId = QuoteTools.cloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, null,null);
	        System.assert(newQuoteId != null,'CloneQuotationAndRelatedRecords method failed to create new Quote');
	        System.debug('newQuoteId>>>>: ' + newQuoteId);
		}
    }
    
    static testMethod void copyQuotationAndRelatedRecordsForDifferentAccount() {
        
        
        User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'copyQuotationAndRelatedRecordsForDifferentAccount: Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		 
		System.runAs(runAsUser){
			
			//Create another account
			Account testAccount2 = new AccountBuilder().name('TestAccount2').accountNumber('STV1020').build(true);
		    System.assert(testAccount2.Id <> null,'An error occured while creating TestAccount2 Account record.');
	       
	        QuoteTools.QuotationCloneTypes cloneType = QuoteTools.QuotationCloneTypes.CopyQuote;
	        
	        // Copy quote onto new account
	        Id newQuoteId = QuoteTools.cloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, testAccount2.Id,null);
	        System.assert(newQuoteId != null,'CloneQuotationAndRelatedRecords method failed to create new Quote');
	        System.debug('newQuoteId>>>>: ' + newQuoteId);
	        
	        List<Quote> quotes = [SELECT Id, Opportunity.AccountId From Quote WHERE Id = :newQuoteId LIMIT 1];
			System.assert(quotes.isEmpty() == false, 'New Quote should have been created but was not.');
			Quote copiedQuote = quotes[0];
	        
			System.assert(copiedQuote.Opportunity.AccountId == testAccount2.Id, 'The account should be TestAccount2 on Opportunity');
	        
		}
    }
    
    static testMethod void reviseQuotationAndRelatedRecords() {
        
        User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			Quote updateQuoteStatus = new Quote();
	        updateQuoteStatus.Id = sourceQuoteId;
	        updateQuoteStatus.Status = GlobalConstants.QS_STATUS_APPROVED;
	        update updateQuoteStatus;
	        
	        QuoteTools.QuotationCloneTypes cloneType = QuoteTools.QuotationCloneTypes.ReviseQuote;
	        
	        Id newQuoteId = QuoteTools.CloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, null,null);
	        System.assert(newQuoteId != null,'CloneQuotationAndRelatedRecords method failed to create new Quote');
	        System.debug('newQuoteId>>>>: ' + newQuoteId);
		}
    }
    
    static testMethod void reviseAnApprovedQuotationAndRelatedRecords() {
        
        User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){  
	        QuoteTools.QuotationCloneTypes cloneType = QuoteTools.QuotationCloneTypes.ReviseQuote;
	        
	        Quote updateQuoteStatus = new Quote();
	        updateQuoteStatus.Id = sourceQuoteId;
	        updateQuoteStatus.Status = GlobalConstants.QS_STATUS_PRICE_CONFIRMED;
	        update updateQuoteStatus;
	        
	        // Test: An approved quote that is not lost should fail validation and not get revised
	        Id newQuoteId = null;
	        try{
	        	newQuoteId = QuoteTools.cloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, null, null);
	        }
	        catch (Exception ex){ //
	        	 }
	        System.assert(newQuoteId == null,'CloneQuotationAndRelatedRecords method should have failed validation and not Revised the quote, but did.');
        
        
			// Test : An approved quote that is lost should pass the validation and successfully be revised  
			//updateQuoteStatus.Customer_Approval__c = 'Lost';
			updateQuoteStatus.Status = GlobalConstants.QS_STATUS_COMPLETED_LOST;
	        update updateQuoteStatus;
        	newQuoteId = null;
        	try{
	        	newQuoteId = QuoteTools.cloneQuotationAndRelatedRecords(sourceQuoteId, cloneType, null,null);
	        }
	        catch (Exception e){ //
	        	 }
	        System.assert(newQuoteId != null,'CloneQuotationAndRelatedRecords method should have passed validation and should have revised the quote, but did not');
        
		}
    }
        
    static testMethod void updateQuoteAsWonAndSendQuoteToERPTest(){
    	User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){  
	        Quote updateQuoteStatus = new Quote();
	        updateQuoteStatus.Id = sourceQuoteId;
	        updateQuoteStatus.Status = GlobalConstants.QS_STATUS_APPROVED;
	        update updateQuoteStatus;
	        
	        //Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
			//need to impliment external webservice mock

			//Create UnitTest Prerequisite Data.
	        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
	        
	        Test.startTest() ;
    	 	// This causes a fake response to be generated
    	 	
        	Test.setMock(WebServiceMock.class, new WebMethodsQuotationSoapAPIv1MockImpl());
        	// Call the method that invokes a callout
	        
	        
	      	QuoteTools.sendCustomerQuoteToSAP(sourceQuoteId);
			System.debug('sendCustomerQuoteToSAP executed successfully.');
			Test.stopTest();
        	
		}
    } 
    
    static testMethod void updateQuoteAsWonAndSendQuoteToERP_CBC_Test(){
    	User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getCBCandSPTestQuote(GlobalConstants.QuoteTypes.CBC);
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid CBC testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){  
	        Quote updateQuoteStatus = new Quote();
	        updateQuoteStatus.Id = sourceQuoteId;
	        updateQuoteStatus.Status = GlobalConstants.QS_STATUS_APPROVED;
	        update updateQuoteStatus;
	        
	        //Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
			//need to impliment external webservice mock

			//Create UnitTest Prerequisite Data.
	        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
	        
	        Test.startTest() ;
    	 	// This causes a fake response to be generated
    	 	
        	Test.setMock(WebServiceMock.class, new WebMethodsCBCAgreementSoapAPIv1Mock());
        	// Call the method that invokes a callout
	        
	        
	      	QuoteTools.sendCBCToSAP(sourceQuoteId);
        	//System.assert(result == true,'updateQuoteAsWonAndSendQuoteToERP (CBC) method was expecting True');
			System.debug('sendCBCToSAP (CBC) executed successfully.');
			Test.stopTest();
        	
		}
    } 
 
 	static testMethod void updateQuoteAsWonAndSendQuoteToERP_SurgeonPref_Test(){
    	User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getCBCandSPTestQuote(GlobalConstants.QuoteTypes.SurgeonPreference);
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid SurgeonPref testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){  
	        Quote updateQuoteStatus = new Quote();
	        updateQuoteStatus.Id = sourceQuoteId;
	        updateQuoteStatus.Status = GlobalConstants.QS_STATUS_APPROVED;
	        update updateQuoteStatus;
	        
	        //Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
			//need to impliment external webservice mock

			//Create UnitTest Prerequisite Data.
	        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
	        
	        Test.startTest() ;
    	 	// This causes a fake response to be generated
    	 	
        	Test.setMock(WebServiceMock.class, new WebMethodsSurgeonPrefSoapAPIv1Mock());
        	// Call the method that invokes a callout
	        
	        
	      	QuoteTools.sendSurgeonPrefernceToSAP(sourceQuoteId);
        	//System.assert(result == true,'updateQuoteAsWonAndSendQuoteToERP (SurgeonPref) method was expecting True');
			System.debug('sendSurgeonPrefernceToSAP (SurgeonPref) executed successfully.');
			Test.stopTest();
        	
		}
    } 
      
    static testMethod void generateUniqueRandomStringTest(){
    	String uniqueString = QuoteTools.generateUniqueRandomString();
    	system.assert(uniqueString != null, 'Was expecting a string');
    }
    
    static testMethod void updateQuoteErpDetailUpdateErpDocNumberTest()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			APIv001.Quote apiQuote = QuoteTools.getQuote(sourceQuoteId);
			System.debug('apiQuote>>>>>:' + apiQuote);
			System.assert(apiQuote != null, 'Was expecting a valid API Quote');
			
			apiQuote.erpDocumentNumber = 'SAPQ-121212';
			
			APIv001.ReturnResult rr = QuoteTools.updateQuoteErpDetail(apiQuote);
			System.assert(rr.isSuccessful == true, 'Was expecting isSuccessful is True');
		}
	}
	
	static testMethod void updateQuoteErpDetailInvalidLinesTest()
	{
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null. getTestQuote() didn\'t as expected.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser){
			APIv001.Quote apiQuote = QuoteTools.getQuote(sourceQuoteId);
			System.debug('apiQuote>>>>>:' + apiQuote);
			System.assert(apiQuote != null, 'Was expecting a valid API Quote');
			
			if (apiQuote.Lines != null){
				for(APIv001.QuoteLine ql : apiQuote.Lines)
				{
					List<APIv001.ProcessLog> pLogs = new List<APIv001.ProcessLog>();
					APIv001.ProcessLog plLog = new APIv001.ProcessLog();
					List<APIv001.Log> logs = new List<APIv001.Log>();
					APIv001.Log log = new APIv001.Log();
					log.code = '101';
					log.message = 'unit test message';
					log.severity = APIv001.SeverityType.ValidationError;
					logs.add(log);
					plLog.logs = logs;
					pLogs.add(plLog);
					
					APIv001.ValidationResult vr = new  APIv001.ValidationResult();
					vr.isValid = false;
					vr.processLogs = pLogs;
					
					ql.validationResult = vr;
					
					break;
				}
			}
			
			APIv001.ReturnResult rr = QuoteTools.updateQuoteErpDetail(apiQuote);
			System.assert(rr.isSuccessful == true, 'Was expecting isSuccessful is True');
		}
	}
	
}