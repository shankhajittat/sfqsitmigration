public with sharing class NewQuoteController {
	
	// *************  INFORMATION  ***********
	// A quote can be initiated from different places and enter the wizard on different pages. 
	// Some wizard pages are also skipped depending on quote type and where quote navigation was initiated from.
	//
	// The following places initiate a quote create/edit :
	// 1. Account has a quote related list - NEW directs to Page 4 of Wizard where a Set Buy quote is created
	// 2. Opportunity has a quote related list - NEW directs to Page 2 of Wizard (Choose type of quote), EDIT bypasses this controller and goes straight to quoteeditor page.
	// 3. Quote custom tab page for the quote shadow object - NEW directs to Page 1 of Wizard (Choose account), EDIT bypasses this controller and goes straight to quoteeditor page.
	// 4. CBC custom tab - NEW directs to Page 1 of Wizard (Choose account), EDIT directs to Page 4 of Wizard (Create quote) since editing a CBC actually results in a new Quote.
	// 5. CBC related list on Account - NEW directs to Page 3 of Wizard (Choose SB Partner), EDIT directs to Page 4 of Wizard (Create quote) since editing a CBC actually results in a new Quote.
	// 6. SP custom tab - NEW directs to Page 1 of Wizard (Choose account), EDIT directs to Page 4 of Wizard (Create quote) since editing a SP actually results in a new Quote.
	// 7. SP related list on Account - NEW directs to Page 3 of Wizard (Choose SB Partner), EDIT directs to Page 4 of Wizard (Create quote) since editing a SP actually results in a new Quote.
	// 8. AccountPicker page - this page will fall away once cbc and sp go in. Redirects to Page 1 of the Wizard).
	
	public GlobalConstants.QuoteTypes urlQuoteType {get; set;}
	public String accountId {get;set;}
	public String selectTranType {get; set;} 
	//public List<Quote_Transaction_Type__c> transactionTypes {get; set;}
	public List<String> transactionTypes {get; set;}
	public List<Address__c> sbPartnerAddresses {get; set;}
    public Pricebook2 priceBook {get; private set;}
    public Opportunity extensionOpportunity {get; private set;}
    public Account account {get; set;}
    public Quote extensionQuote{get;set;}
	public String selectSBPartner {get; set;} 
	public String urlSBPartner {get; set;}
	public String urlSPId {get; set;}
	public String urlaBOMId {get; set;}
	public boolean isError {get; set;}
	public boolean cameFromOpportunity {get; set;}
	public Consignment_Business_Case__c cbcMaster {get; set;}
	public Surgeon_Preference__c spMaster {get; set;}
	public ActualBillOfMaterial__c actualBOMMaster {get; set;}
	public Address__c SBPartnerAddress {get; set;}
	public String retURL;
	
    
	public NewQuoteController() {
		isError = false;
		accountId = ApexPages.currentPage().getParameters().get('accId');
		
		// Get the quote type from the URL - see the valid quote types in GlobalConstants.QuoteTypes
		Integer quoteTypeParam;
		try {
			quoteTypeParam = Integer.ValueOf(ApexPages.currentPage().getParameters().get('quoteType'));
		} catch (NullPointerException e) {
			quoteTypeParam = GlobalConstants.QuoteTypes.None.ordinal();  // Enum value None
		} catch (Exception e) {
			ApexPages.addMessages(e);
			isError = true;  // In this case isError is set just for testing purposes
			return;
		}
		urlQuoteType = QuoteTools.determineQuotetype(quoteTypeParam);
		
		urlSBPartner = ApexPages.currentPage().getParameters().get('SBPartner');  
		urlSPId = ApexPages.currentPage().getParameters().get('SPref'); // This is only passed in when it's an EDIT surg pref
		urlaBOMId = ApexPages.currentPage().getParameters().get('aBOMId'); // This is only passed in when it's an EDIT Actual BOM
		// retURL gives us a way of knowing where the quote has been initiated from. 
		// We need it for 1.Navigation for previous buttons 2.To know if a new opportunity needs to be created
		retURL = ApexPages.currentPage().getParameters().get('retURL');
		retURL = retURL != null? StringUtils.validateId(retURL.substring(1)) : null;
		cameFromOpportunity = retURL != null && retURL.startsWith('006')? true : false;
		if (urlQuoteType == GlobalConstants.QuoteTypes.CBC) {
			// Determine SB Partners
			determineSBPartner();
		} 
		listQuoteTypes();
		
	}
	
	// ***********************
    // (PAGE 4 of WIZARD) ****     QuoteCreate page is a blind page - there is no UI necessary for this page - all it does is runs the action here to create the quote and all necessary records 
    // ***********************
    public PageReference createQuote() {
    	system.debug('called createQuote=>' + urlQuoteType);
    	if (urlQuoteType == GlobalConstants.QuoteTypes.CustomerQuote) {
			// This action method creates opportunity and quote and related records - it replaces the createquotecontrollerextension where we used to create the quote
			try {
				getAccount();
				getPricebook();
        		createOpportunity();
            	//Create Quote
            	//String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
            	createNewQuote('', urlQuoteType);
            	// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
            	
			}
		 	catch (Exception e) {isError = true; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); return null;}
    	} 	
    	else if (urlQuoteType == GlobalConstants.QuoteTypes.SetRequest){
    		try {
				getAccount();
				getPricebook();
        		createOpportunity();
            	//Create Quote
            	//String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
            	createNewQuote('', urlQuoteType);
            	// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
            	
			}
		 	catch (Exception e) {isError = true; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); return null;}
    	} 
    	else if (urlQuoteType == GlobalConstants.QuoteTypes.ChangeSet){
    		try {
				getAccount();
				getPricebook();
				getActualBOMMaster();
        		createOpportunity();
            	//Create Quote
            	//String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
            	createNewQuote('', urlQuoteType);
            	createGroupFromMasterAndPopulateItems(actualBOMMaster.Id);
            	// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
            	
			}
		 	catch (Exception e) {isError = true; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); return null;}
    	}
    	else if (urlQuoteType == GlobalConstants.QuoteTypes.LoanSetRequest){
    		try {
				getAccount();
				getPricebook();
        		createOpportunity();
            	//Create Quote
            	createNewQuote('', urlQuoteType);
            	// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
            	
			}
		 	catch (Exception e) {isError = true; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); return null;}
    	}
    	else if (urlQuoteType == GlobalConstants.QuoteTypes.CBC) {
    		
    		try{
    			getAccount();
    			system.debug('getAccount=> passed');
    			getPricebook();
				system.debug('getPricebook=> passed');
    			getCBCMaster(); // find a cbc record using sbpartner id that has been selected by the user. Then find partner ship to details in the list in viewstate to populate details on the quote
    			if (isError == true) {
    				return null;
    			}
    			createOpportunity();
    			system.debug('createOpportunity=> passed');
            	//String nextQuoteDocumentNumber = CustomSettingsTools.getNextCBCQuotationNumber();
            	createNewQuote('', urlQuoteType);
            	system.debug('createNewQuote=> passed');
            	createGroupFromMasterAndPopulateItems(cbcMaster.Id);
            	
            	// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
    			
    		} catch(Exception e) {isError = true; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); return null;}
    		
    	} else if (urlQuoteType == GlobalConstants.QuoteTypes.SurgeonPreference) {
    		
    		try{
    			getAccount();
    			getPricebook();
    			getSPMaster();   // If we are editing a sp master then we will determine the SP master here, otherwise it will be empty
    			if (isError == true) {
    				return null;
    			}
    			createOpportunity();
            	//String nextQuoteDocumentNumber = CustomSettingsTools.GetNextSPQuotationNumber();
            	createNewQuote('', urlQuoteType);
            	createGroupFromMasterAndPopulateItems(spMaster.Id);
            	
            	// Redirect the user to the new Quotation VF page passing the new quote id to it
				PageReference pr = Page.QuoteEditor;
				pr.getParameters().put('id',this.extensionQuote.Id);
				pr.setRedirect(true);
				return pr;
    			
    		} catch(Exception e) {isError = true; ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); return null;}
    	}
    	
		return null;
	}
	
	// This method cancels the wizard, and returns the user to the home page
    public PageReference cancel() {
    	PageReference pr;
    	if (StringUtils.isNullOrEmpty(retUrl)) {
    		pr = new PageReference('/home/home.jsp?');
    	} else {
	    	pr = new PageReference('/' + retUrl);
    	}
    	pr.setRedirect(true);
		return pr;
    }
	
	
	// Get all quote types in the system
	public void listQuoteTypes() {
		selectTranType = GlobalConstants.QuoteTypes.CustomerQuote.name();
		
		// If quote type has been passed in the url then set the selectTransType to it , as though the user went throught the process of selecting the quote type on page 2 of wizard
		if (urlQuoteType != GlobalConstants.QuoteTypes.None) {
			selectTranType = urlQuoteType.name();
		}
	}

	// This is the getter to populate the quote types dropdownlist - the VF Page QuoteType.Page binds to it
    public List<SelectOption> getTransTypes() {
        List<SelectOption> options = new List<SelectOption>();
        /*
        for (Quote_Transaction_Type__c oneTranType : transactionTypes) {
        	//String trantypeid = oneTranType.Id;
        	options.add(new SelectOption(oneTranType.Id, oneTranType.Name));
        }
        */
        for(GlobalConstants.QuoteTypes qt : GlobalConstants.QuoteTypes.values()){
        	options.add(new SelectOption(qt.name(), qt.name()));
        }
         return options;
    }
     
	// Get all SB Partners for the account
	public void determineSBPartner() {
		String sbAddressCode = CustomSettingsTools.getKeyValue(GlobalConstants.SAP_SB_PARTNERFUNCTION_CODE);
		system.debug('CustomSettingsTools.getKeyValue+sbAddressCode=>' + sbAddressCode);
		if (StringUtils.isNullOrWhiteSpace(sbAddressCode))
			sbAddressCode = 'SB';
				
		List<Address__c> sbps = [SELECT Id, Name, Street1__c, City__c, State__c, Country__c, Postal_Code__c from Address__c 
								WHERE Account_Address__c = :accountId And AddressType__c INCLUDES(:sbAddressCode)];
		List<Consignment_Business_Case__c> cbcs = [SELECT Id, ShippingLocation__c FROM Consignment_Business_Case__c WHERE Account__c = :accountId];
		// Build a list of SB partners that have cbc agreements 
		Map<Id, Id> sbPartnersMap = new Map<Id, Id>();
		for(Consignment_Business_Case__c cbc : cbcs) {
			if (!StringUtils.isNullOrEmpty(cbc.ShippingLocation__c)) {
				sbPartnersMap.put(cbc.ShippingLocation__c,cbc.ShippingLocation__c);
			}
		}
		
		sbPartnerAddresses = new List<Address__c> ();
		// Only show the SB addresses that don't have CBC agreements
		for (Address__c oneSBPartnerAddress : sbps) {
			If (StringUtils.isNullOrWhiteSpace(oneSBPartnerAddress.Name)) continue;
			Id theSBId = sbPartnersMap.get(oneSBPartnerAddress.Id);
			if (StringUtils.isNullOrEmpty(theSBId)) {
				//excldue any SB AccountNumber which starts with 0008 (AccountGroup ZSST) - invalid data model, no account group available at address level, so checking the first 4 digit is 0008, then exclude.
				if (oneSBPartnerAddress.Name.startsWith('0008')) continue;
				
				sbPartnerAddresses.Add(oneSBPartnerAddress);
			}
		}
		
		// Set the default SB partner value to the first one in the list if it was not passed in the url
		if (sbPartnerAddresses != null && sbPartnerAddresses.size() > 0 ) {
			selectSBPartner = sbPartnerAddresses[0].Id;
		}
		if (StringUtils.isNullOrEmpty(urlSBPartner) == false){
			selectSBPartner = (Id)urlSBPartner;
		}
	}
	
	// This is the getter to populate the SB Partners dropdownlist - the VF page QuoteSBPartner.Page binds to it.
    public List<SelectOption> getSBPartners() {
    	if (sbPartnerAddresses == null) {
    		determineSBPartner();
    	}
        List<SelectOption> options = new List<SelectOption>();
        for (Address__c oneSBPartner : sbPartnerAddresses) {
        	options.add(new SelectOption(oneSBPartner.Id, oneSBPartner.Name));
        }
        return options;
    }
	
	public void getAccount() {
		
		if (accountId == null) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Could not find account to create Quote for'));
    		return;
	    }
		
		this.account = [SELECT a.Name, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, 
		        					a.ShippingCountry, a.ShippingCity, a.BillingStreet, a.BillingState, 
		        					a.BillingPostalCode, a.BillingCountry, a.BillingCity,
		        					a.OrganizationCode__c,a.DistributionChannel__c,a.Division__c FROM Account a WHERE a.Id =: accountId];
		        					
		        					
        if (StringUtils.isEmpty(this.account.OrganizationCode__c)){
        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid OrganizationCode.');
        }
        if (StringUtils.isEmpty(this.account.DistributionChannel__c)){
        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid DistributionChannel.');
        }
        if (StringUtils.isEmpty(this.account.Division__c)){
        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid Division.');
        }
		        
	}
	
	public void getPricebook() {
		
		try{
	        this.priceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: account.OrganizationCode__c 
	    						And DistributionChannel__c =: account.DistributionChannel__c LIMIT 1];
	    	if (this.priceBook.IsActive == false){
	    		throw new ApplicationException('PriceBook ' + this.priceBook + ' is Inactive. Pricebook must be active inorder to create Quote.');
	    	}
		} catch(Exception e){
			this.priceBook = null;
		}
	    
	    if (this.priceBook == null){
        	Pricebook2 customPricebook = new PriceBook2Builder()
        			.name(account.OrganizationCode__c + account.DistributionChannel__c)
        			.organizationCode(account.OrganizationCode__c)
        			.distributionChannel(account.DistributionChannel__c)
        			.build(true);
        			
        	this.priceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: account.OrganizationCode__c 
	    						And DistributionChannel__c =: account.DistributionChannel__c LIMIT 1];
	    }
	}  
	
	
	public void createOpportunity() {
		
		//Create Opportunity unless we came from the Opportunity, in which case we use the opportunity we came from
		
		if (cameFromOpportunity == true) {
			this.extensionOpportunity = new Opportunity(
				Id = (Id)retURL
			);
		} else {
			//String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
	    	this.extensionOpportunity = new Opportunity(
	        	Name = 'OPP-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
	        	CloseDate = Date.today().addDays(30),
	        	Amount = 0.0,
	        	StageName = GlobalConstants.OPP_STAGENAME_QUALIFICAITON,
	        	AccountId = this.account.Id,
	        	Pricebook2Id = this.priceBook.Id,
	        	Type = GlobalConstants.OPP_TYPE 
	        	);
	        
	     Database.insert(this.extensionOpportunity);
		}
	}
	
	public void createNewQuote(String quoteName, GlobalConstants.QuoteTypes quoteType) {
		//        	Name = quoteName,
		//Populate the default BU Start
		/**
		Same UI method has been used to ensure less maintainance  effort
		*/
		ObjectFieldsUserPreference objectFieldUserPreference = new ObjectFieldsUserPreference('Quote',
    	ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.DefaultBUSelection);
		List<SelectOption> selectedDefaultBUAsSelectOptionsList = objectFieldUserPreference.retrieveUserPreferenceDetailsAsListSelectOption();
		String selectedDefaultBUSelectOptions = null;
		if(!selectedDefaultBUAsSelectOptionsList.IsEmpty())	
		{
			selectedDefaultBUSelectOptions = selectedDefaultBUAsSelectOptionsList[0].getValue();
		}
		//Populate the default BU End
		this.extensionQuote = new Quote(
			Name = 'QU-' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS')),
        	QuoteType__c = quoteType.name(),
        	Sales_Consultant__c = Userinfo.getUserId(),
        	OpportunityId = this.extensionOpportunity.Id,
        	Status = GlobalConstants.QS_STATUS_DRAFT,
        	Pricebook2Id = this.priceBook.Id,
        	Version__c = 1,
        	BillingName = '',
        	BillingStreet = this.account.BillingStreet,
        	BillingCity = this.account.BillingCity,
        	BillingState = this.account.BillingState,
        	BillingCountry = this.account.BillingCountry,
        	BillingPostalCode = this.account.BillingPostalCode
        	);
        	if (quoteType == GlobalConstants.QuoteTypes.CBC || quoteType == GlobalConstants.QuoteTypes.ChangeSet){
        		this.extensionQuote.ShippingName = this.SBPartnerAddress.Name;
        		this.extensionQuote.ShippingStreet = this.SBPartnerAddress.Street1__c;
        		this.extensionQuote.ShippingCity = this.SBPartnerAddress.City__c;
        		this.extensionQuote.ShippingState = this.SBPartnerAddress.State__c;
        		this.extensionQuote.ShippingCountry = this.SBPartnerAddress.Country__c;
        		this.extensionQuote.ShippingPostalCode = this.SBPartnerAddress.Postal_Code__c;
        	}
        	else{
        		this.extensionQuote.ShippingName = '';
        		this.extensionQuote.ShippingStreet = this.account.ShippingStreet;
        		this.extensionQuote.ShippingCity = this.account.ShippingCity;
        		this.extensionQuote.ShippingState = this.account.ShippingState;
        		this.extensionQuote.ShippingCountry = this.account.ShippingCountry;
        		this.extensionQuote.ShippingPostalCode = this.account.ShippingPostalCode;
        		
        	}
        	if(selectedDefaultBUSelectOptions<> null)	
        	{
        		this.extensionQuote.Business_Unit__c = selectedDefaultBUSelectOptions;
        	}
            if(quoteType == GlobalConstants.QuoteTypes.CBC) {	
            	this.extensionQuote.Consignment_Business_Case__c = this.cbcMaster.Id;
            	this.extensionQuote.Address__c = SBPartnerAddress.Id;
            	this.extensionQuote.Document_Date__c = this.cbcMaster.Agreement_From_Date__c == null? System.now().date() : this.cbcMaster.Agreement_From_Date__c;
            	this.extensionQuote.ExpirationDate = this.cbcMaster.Agreement_To_Date__c;
            	this.extensionQuote.ERP_Document_Number__c = this.cbcMaster.Name;
            } else  if(quoteType == GlobalConstants.QuoteTypes.SurgeonPreference) {
            	this.extensionQuote.Document_Date__c = this.spMaster.ValidFromDate__c == null? System.now().date() : this.spMaster.ValidFromDate__c;
            	this.extensionQuote.ExpirationDate = this.spMaster.ValidToDate__c;
            	this.extensionQuote.ERP_Document_Number__c = this.spMaster.Name;
            	this.extensionQuote.Surgeon_Preference__c = this.spMaster.Id == null? null: this.spMaster.Id;
            }
            else  if(quoteType == GlobalConstants.QuoteTypes.ChangeSet) {
            		Date validFromDate = System.now().date();
	            	if (this.actualBOMMaster.ValidFromDate__c != null){
		            	try{
		            		validFromDate = Date.parse(this.actualBOMMaster.ValidFromDate__c);
		            	}
		            	catch(Exception e){}
            		}
            		Date validToDate = System.now().date();
            		if (this.actualBOMMaster.ValidToDate__c != null){
		            	try{
		            		validToDate = Date.parse(this.actualBOMMaster.ValidToDate__c);
		            	}
		            	catch(Exception e){}
            		}
            	this.extensionQuote.Document_Date__c = validFromDate;
            	this.extensionQuote.ExpirationDate = validToDate;
            	this.extensionQuote.ERP_Document_Number__c = this.actualBOMMaster.EquipmentNumber__c;
            	this.extensionQuote.ActualBillOfMaterial__c = this.actualBOMMaster.Id == null? null: this.actualBOMMaster.Id;
            	this.extensionQuote.Address__c = SBPartnerAddress.Id;
            }
        	Database.insert(this.extensionQuote);
        	Id quoteId = this.extensionQuote.Id;
        	
        	this.extensionQuote = [select Id, QuoteNumber, Status, GrandTotal, ExpirationDate, Total_Net_Margin_Amount__c,  Total_Standard_Cost__c, Total_Net_Margin_Percent__c, 
							    		Total_Landed_Cost__c, Total_Gross_Margin_Percent__c, Total_Gross_Margin_Amount__c, Total_Extended_Unit_Price__c, Total_Discount_Percentage__c, 
							    		SystemModstamp, ShippingStreet, ShippingState, ShippingPostalCode, ShippingName, ShippingLongitude, ShippingLatitude, ShippingHandling, ShippingCountry, 
							    		ShippingCity, QuoteToStreet, QuoteToState, QuoteToPostalCode, QuoteToName, QuoteToLongitude, QuoteToLatitude, QuoteToCountry, 
							    		QuoteToCity, Phone, Name, LineItemCount, LastViewedDate, LastReferencedDate, LastModifiedDate, LastModifiedById, Fax, 
							    		Instruction_Comments__c, Instruction_Do_Not_Ship__c, Instruction_Other__c, Instruction_Ship_To_Address_OK__c, Instruction_Transfer__c, 
							    		Email, ERP_Document_Number__c, Document_Date__c, Discount, Description, Customer_Purchase_Order_Number__c, CreatedDate, CreatedById, ContactId, 
							    		BillingStreet, BillingState, BillingPostalCode, BillingName, BillingLongitude, BillingLatitude, BillingCountry, BillingCity, Pricebook2Id,
							    		AdditionalStreet, AdditionalState, AdditionalPostalCode, AdditionalName, AdditionalLongitude, AdditionalLatitude, AdditionalCountry, AdditionalCity, 
							    		Total_Discount_Amount__c, TotalPrice, Subtotal, Tax, TotalPrice_ExTax__c, Version__c, LastModifiedBy.Name,CreatedBy.Name,
							    		QuoteType__c, Address__c, Address__r.Name, 
							    		Consignment_Business_Case__c, Consignment_Business_Case__r.Agreement_From_Date__c,Consignment_Business_Case__r.Agreement_To_Date__c,
							    		Opportunity.Account.Name, Opportunity.OwnerId, Opportunity.Owner.Name, Opportunity.Account.Division__c, Opportunity.Account.OrganizationCode__c,
							    		SAP_Request_Response_Message__c,Opportunity.AccountId,Customer_Approval__c,Show_rebate_details_in_Document__c,Quote_Description__c,
							    		ActualBillOfMaterial__c 
							    	from Quote where Id = :quoteId];
        	
        	//link together
        	OpportunityTools.UpdateOpportunitySyncQuoteId(this.extensionOpportunity.Id, this.extensionQuote.Id);
        
	}
	
	
	public void createGroupFromMasterAndPopulateItems(Id editableMasterRecordId) {
		QuoteGroupLineItemsPopulator populateGroupAndLineItems = new QuoteGroupLineItemsPopulator(this.extensionQuote.Id, 'Kit', selectTranType);
		if (urlQuoteType == GlobalConstants.QuoteTypes.CBC) {
			populateGroupAndLineItems.runCBC();
		} else if (urlQuoteType == GlobalConstants.QuoteTypes.SurgeonPreference) {
			populateGroupAndLineItems.runSurgeonPref();
		}
		else if (urlQuoteType == GlobalConstants.QuoteTypes.ChangeSet) {
			populateGroupAndLineItems.runChangeSet();
		}
	}
	
	
	
	public void getCBCMaster() {
		isError = false;
		
		// Get SB Partner in view state for creating quote later and to find the correct CBC
		if (StringUtils.isNullOrEmpty(urlSBPartner)) {
			SBPartnerAddress = null;
			for (Address__c one : sbPartnerAddresses) {
				if(one.Id == selectSBPartner) {
					SBPartnerAddress = one;
					break;  // Found the right one
				}
			}
		} else {
			SBPartnerAddress = [SELECT Id, Name, Street1__c, City__c, State__c, Country__c, Postal_Code__c FROM Address__c WHERE Id = :urlSBPartner];
		}
		
		If (SBPartnerAddress == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An SB Partner must be selected for this quote type'));
			isError = true;
			return;
		}
		List<Consignment_Business_Case__c> CBCs = [SELECT Id, Name,  Agreement_From_Date__c, Agreement_To_Date__c, ShippingLocation__c 
												   FROM Consignment_Business_Case__c
												   WHERE ShippingLocation__c = :SBPartnerAddress.Id];
		if(CBCs.size() > 1 ) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'More than one CBC master has been found for this SB Partner'));
			isError = true;
		} else if (CBCs.isEmpty() == true) {
			createMasterCBCRecord();
		} else if (CBCs.size() == 1) {
			this.cbcMaster = CBCs[0];
		}
	}
	
	
	
	public void createMasterCBCRecord() {
		
		this.cbcMaster = new Consignment_Business_Case__c(
                	Name = 'CBC' + account.OrganizationCode__c + SBPartnerAddress.Name,
                	ERP_Id__c = 'CBC' + account.OrganizationCode__c + SBPartnerAddress.Name  + '|' + account.OrganizationCode__c + '|' + account.DistributionChannel__c,
                	Account__c = account.Id,
                	ShippingLocation__c = SBPartnerAddress.Id,
                	RecordSource__c = 'SFDC'
                	);
		Database.insert(this.cbcMaster);
	}
	
	
	
	public void getSPMaster() {
		isError = false;
		// NOTE this may change depending on SP rules which are unclear at the moment. This code is just using the first SB partner, even though you choose one
		// A new custom lookup will be needed on SP if we want to have one SP for each SB Partner address, then this code can change.
		List<Surgeon_Preference__c> SPs = [SELECT Id, ValidFromDate__c, ValidToDate__c , Name
										   FROM Surgeon_Preference__c
										   WHERE Id = :urlSPId];
		if(SPs.size() > 1 ) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'More than one SP master has been found for the Id'));
			isError = true;
		} else if (SPs.isEmpty() == true) {
			this.spMaster = new Surgeon_Preference__c();
			//creategetSPMaster();
		} else if (SPs.size() == 1) {
			this.spMaster = SPs[0];
		}
	}
	
	public void getActualBOMMaster() {
		isError = false;
		List<ActualBillOfMaterial__c> actualBOMs = [SELECT Id, Name, EquipmentNumber__c, ValidFromDate__c, ValidToDate__c, Customer__c, PartnerFunctionAccount__c
										   FROM ActualBillOfMaterial__c
										   WHERE Id = :urlaBOMId];
		if(actualBOMs.size() > 1 ) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'More than one ActualBOM master has been found for the Id'));
			isError = true;
		} else if (actualBOMs.isEmpty() == true) {
			this.actualBOMMaster = new ActualBillOfMaterial__c();
		} else if (actualBOMs.size() == 1) {
			this.actualBOMMaster = actualBOMs[0];
			
			this.SBPartnerAddress = [SELECT Id, Name, Street1__c, City__c, State__c, Country__c, Postal_Code__c FROM Address__c WHERE Id = :actualBOMMaster.PartnerFunctionAccount__c];
			
		}
	}
	
	/*
	public void createMasterSPRecord() {
		if (SBPartnerAddress == null) {
			for (Address__c one : sbPartnerAddresses) {
				if(one.Id == selectSBPartner) {
					SBPartnerAddress = one;
					break;  // Found the right one
				}
			}
			If (SBPartnerAddress == null) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An SB Partner must be selected for this quote type'));
				isError = true;
				return;
			}
		}
		this.spMaster = new Surgeon_Preference__c(
                	Name = 'SP' + account.OrganizationCode__c + SBPartnerAddress.Name,
                	Surgeon__c = account.Id
                	);
		Database.insert(this.spMaster);
	}*/
	
	
	
    
    // ***********************
    // (PAGE 1 of WIZARD) ****     An account is chosen from account picker page 
    // ***********************
    // 
	public PageReference chooseAccount() {
		if (accountId == null || String.isEmpty(accountId)) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must choose an account for the Quote'));
    		isError = true;
		 	return null;
		} else {
			isError = false;
		}
		
		PageReference pr;
		if (urlQuoteType == GlobalConstants.QuoteTypes.CBC) {
			determineSBPartner();
    		pr = new PageReference('/apex/QuoteSBPartner?');
		} else {
			pr = new PageReference('/apex/QuoteType?');	
		}
		return pr;
	}
	
	
    // ***********************
    // (PAGE 2 of WIZARD) ****    A Quote type (CBC/SP/Set Buy) is selected from QuoteType page 
    // ***********************
    public PageReference actionSelectQuoteType() {
    	isError = false;
    	PageReference SBPartnerPage;
		urlQuoteType = QuoteTools.determineQuotetype(selectTranType);   
		
		// It's possible that there will be transaction types with no Custom setting set up to map which screen to go to next. Give an error if custom setting has not been set up.
    	if (urlQuoteType == GlobalConstants.QuoteTypes.CustomerQuote || urlQuoteType == GlobalConstants.QuoteTypes.SetRequest 
    			|| urlQuoteType == GlobalConstants.QuoteTypes.ChangeSet || urlQuoteType == GlobalConstants.QuoteTypes.LoanSetRequest 
    			|| urlQuoteType == GlobalConstants.QuoteTypes.SurgeonPreference) {
    		SBPartnerPage = new PageReference('/apex/QuoteCreate?');
    	} else if (urlQuoteType == GlobalConstants.QuoteTypes.CBC){
    		determineSBPartner();
    		SBPartnerPage = new PageReference('/apex/QuoteSBPartner?');
    	} else {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'A custom setting for QuoteTypes needs to be set up for this transaction type'));
    		isError = true;
    	}  
    		
    	return SBPartnerPage;
    }
    
    // ***********************
    // (PAGE 3 of WIZARD) ****     SB Partner is chosen from QuoteSBPartner page 
    // ***********************
    public PageReference actionSelectSBPartner() {
    	
    	// Get SB Partner in view state for creating quote later
		SBPartnerAddress = null;
		for (Address__c one : sbPartnerAddresses) {
			if(one.Id == selectSBPartner) {
				SBPartnerAddress = one;
				break;  // Found the right one
			}
		}
		If (SBPartnerAddress == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'An SB Partner must be selected for this quote type')); isError = true; return null;
		}
    	
    	PageReference QuoteCreatePage = new PageReference('/apex/QuoteCreate?');
    	return QuoteCreatePage;
    }
    

    
	
}