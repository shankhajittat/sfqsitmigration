public with sharing class MetadataTools {

    public static List<SelectOption> getSObjectFieldsLabelAndApiName(String sObjectName, Map<String,String> apiFieldsToExclude)
    {
        List<SelectOption> returnResults = new List<SelectOption>();
        
        if (apiFieldsToExclude == null)
            apiFieldsToExclude = new Map<String,String>();
        
        SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        Map<String,Schema.SObjectField> mFields = sObjectType.getDescribe().fields.getMap();
        
        for(String key : mFields.keySet()){
            System.debug('GetType >>>>: ' + mFields.get(key).getDescribe().getName() + ' - ' + mFields.get(key).getDescribe().getLabel() + ' : '+ mFields.get(key).getDescribe().getType());
            if (key == 'issyncing' || key == 'isdeleted') continue;
            
            if (mFields.get(key).getDescribe().isAccessible() && 
                    (
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.base64 &&
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.DataCategoryGroupReference &&
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.EncryptedString &&
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.Reference
                    )
                ){
                    
                    String fieldAPIName = mFields.get(key).getDescribe().getName();
                    System.debug('fieldAPIName>>>>>: ' + fieldAPIName);
                    
                    if (apiFieldsToExclude.size() > 0 && apiFieldsToExclude.containsKey(fieldAPIName))
                        continue;
                    
                    returnResults.add(new SelectOption(fieldAPIName,mFields.get(key).getDescribe().getLabel()));
            }
        }
        
        
        return returnResults;
    }
    
    public static Set<String> getSObjectAccessibleFieldsApiName(String sObjectName)
    {
        Set<String> returnResults = new Set<String>();

        
        SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        Map<String,Schema.SObjectField> mFields = sObjectType.getDescribe().fields.getMap();
        
        for(String key : mFields.keySet()){
            //System.debug('GetType >>>>: ' + mFields.get(key).getDescribe().getName() + ' - ' + mFields.get(key).getDescribe().getLabel() + ' : '+ mFields.get(key).getDescribe().getType());
            if (key == 'issyncing' || key == 'isdeleted') continue;
            
            if (mFields.get(key).getDescribe().isAccessible() && 
                    (
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.base64 &&
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.DataCategoryGroupReference &&
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.EncryptedString &&
                    mFields.get(key).getDescribe().getType() != Schema.DisplayType.Reference
                    )
                ){
                    
                    String fieldAPIName = mFields.get(key).getDescribe().getName();
                    //System.debug('fieldAPIName>>>>>: ' + fieldAPIName);
                    returnResults.add(fieldAPIName);
            }
        }
        
        
        return returnResults;
    }
    
}