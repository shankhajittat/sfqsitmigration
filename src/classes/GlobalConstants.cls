global with sharing class GlobalConstants {
	
	public enum QuoteTypes
	{
		CustomerQuote,
		CBC,
		SurgeonPreference,
		SetRequest,
		ChangeSet,
		LoanSetRequest,
		None
	}
	
	public static final String SFDC_CRYPTOKEY_NAME = 'CrypToKey';
	public static final String WEBMETHODS_CALLOUT_TIMEOUT = 'WEBMETHODS_CALLOUT_TIMEOUT';
	public static final String WEBMETHODS_SAP_PRICING_ENDPOINT_URL = 'WEBMETHODS_SAP_PRICING_ENDPOINT_URL';
	public static final String WEBMETHODS_SAP_QUOTATION_ENDPOINT_URL = 'WEBMETHODS_SAP_QUOTATION_ENDPOINT_URL';
	public static final String WEBMETHODS_SAP_SP_ENDPOINT_URL = 'WEBMETHODS_SAP_SP_ENDPOINT_URL';
	public static final String WEBMETHODS_SAP_CBC_ENDPOINT_URL = 'WEBMETHODS_SAP_CBC_ENDPOINT_URL';
	
	public static final String WEBMETHODS_SAP_CREDENTIAL_USERNAME = 'WEBMETHODS_SAP_CREDENTIAL_USERNAME';
	public static final String WEBMETHODS_SAP_CREDENTIAL_PASSWORD = 'WEBMETHODS_SAP_CREDENTIAL_PASSWORD';
	
	public static final String OPP_STAGENAME_QUALIFICAITON = 'Qualification';
	public static final String OPP_TYPE = 'GPQ';
	
	
	/*
		QUOTE STATUS
	*/
	public static final String QS_STATUS_DRAFT = 'Draft';
	public static final String QS_STATUS_SUBMITTED_FOR_PRICING = 'Submitted for Pricing';
	public static final String QS_STATUS_PRICE_CONFIRMED = 'Price Confirmed';
	public static final String QS_STATUS_SUBMITTED_FOR_APPROVAL = 'Submitted for Approval';
	public static final String QS_STATUS_APPROVED = 'Approved';
	public static final String QS_STATUS_REJECTED = 'Rejected';
	public static final String QS_STATUS_RELEASED_TO_CUSTOMER = 'Released to Customer';
	public static final String QS_STATUS_SUBMITTED_TO_ERP = 'Submitted to ERP';
	public static final String QS_STATUS_COMPLETED_WON = 'Completed - Won';
	public static final String QS_STATUS_COMPLETED_LOST = 'Completed - Lost';
	public static final String QS_STATUS_INCOMPLETE = 'Incomplete';
	public static final String QS_STATUS_COMPLETED = 'Completed';
	
	public static final String QS_STATUS_CANCELLED = 'Cancelled';
	public static final String QS_STATUS_DENIED = 'Denied';
	public static final String QS_STATUS_NEW = 'New';
	public static final String QS_STATUS_REVISED = 'Revised';
	
	/*
		QuoteLine Status
	*/
	public static final String QL_STATUS_INVALID = 'Invalid';	
	public static final String QL_STATUS_NEW = 'New';	
	public static final String QL_STATUS_REVALIDATE = 'Revalidate';	
	public static final String QL_STATUS_SUBMITTEDFORPRICING = 'Submitted for Pricing';	
	public static final String QL_STATUS_PROCESSING = 'Processing';	
	public static final String QL_STATUS_VALIDATED = 'Validated';	

	
	
	public static final String QS_CUSTOMER_APPROVAL_LOST = 'Lost';
	public static final String QS_CUSTOMER_APPROVAL_WON = 'Won';
	public static final String KEY_WEBMETHODS_SAP_BOOKING_ENDPOINT_URL = 'WEBMETHODS_SAP_BOOKING_ENDPOINT_URL';
	
	public static final Integer CONST_BOOKING_HISTORY_NO_OF_DAYS = 120;
	public static final Integer CONST_WEBMETHODS_SAP_DEFAULT_TIMEOUT = 120000;
	public static final String KEY_WEBMETHODS_SAPBOOKING_HISTORY_NO_OF_DAY = 'WEBMETHODS_BOOKING_HISTORY_NO_OF_DAY';
	public static final String KEY_WEBMETHODS_SAPBOOKING_TIMEOUT = 'WEBMETHODS_SAPBOOKING_TIMEOUT';
	
	/*
		Quote - SAP Response / Request Message
	*/
	public static final String SFDC_SAP_SUBMITTED_TO_SAP_WAITING_FOR_SAP_RESPONSE = 'submitted…! waiting for response';
	public static final String SFDC_SAP_SUCCESSFUL_RESPONSE_RECEIVED_FROM_SAP = 'response received - successful';
	public static final String SFDC_SAP_INVALID_RESPONSE_RECEIVED_FROM_SAP = 'response received - see error(s)';
	public static final String SFDC_SAP_CONNECTION_ERROR = 'Unexpected Connection Error between SFDC and wM/SAP - see error(s)';
	
	
	//QuoteSettingKeyValue - Key Constants
	public static final String SAP_SB_PARTNERFUNCTION_CODE = 'SAP_SB_PARTNERFUNCTION_CODE';
	public static final String RBOM_TRAINING_SET_STARTSWITH = 'RBOM_TRAINING_SET_STARTSWITH';
	public static final String RBOM_STANDARD_SET_MATERIALGROUP = 'RBOM_STANDARD_SET_MATERIALGROUP';
	public static final String CONST_MISC_BOM_CODE = 'Misc';
	public static final String CONST_GRP_TYP_KIT = 'Kit';
	public static final String CONST_RECORD_ACTION_MODE_CREATE = 'Create';
}