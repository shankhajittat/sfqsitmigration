@IsTest
public class TestingTools {

	public static String getResourceBodyAsString(String resourceName) {
		List<StaticResource> sr = [Select body from StaticResource where Name = :resourceName];
		System.assertEquals(sr.size(), 1);
		return sr.get(0).Body.toString();		
	}
	
	/*
		Create 4 level UserRoles, 
		Create new Users linked to these UserRoles in a hierarchy model.
		Create users with System Administrator profile
		Create Account, PriceBook, SetClass, SetClass Items, Opporuntity, Quote, Quote_Line_Group__c & QuoteLineItem
		
		Return Map object with QuoteId and the RunAs User
	*/
	public static Map<Id,User> getTestQuote(){
    	
    	Map<Id,User> result = null;
    	
    	Boolean createInDatabase = true;
        Quote quote = null;
        
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01';
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        UserRole roleLevel4 = new UserRole();
        roleLevel4.Name = 'CEO';
        insert roleLevel4;
        
        UserRole roleLevel3 = new UserRole();
        roleLevel3.Name = 'VP';
        roleLevel3.ParentRoleId = roleLevel4.Id;
        insert roleLevel3;
        
        UserRole roleLevel2 = new UserRole();
        roleLevel2.Name = 'Director';
        roleLevel2.ParentRoleId = roleLevel3.Id;
        insert roleLevel2;
        
        UserRole roleLevel1 = new UserRole();
        roleLevel1.Name = 'Manager';
        roleLevel1.ParentRoleId = roleLevel2.Id;
        insert roleLevel1;
        
        User ceo = new User(Alias = 'CEO101', Email='ceo101@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='CEO-101', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel4.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='ceo101@testorg.com');
      	insert ceo;
      	
        User vp = new User(Alias = 'VP101', Email='vp101@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='VP-101', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel3.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='vp101@testorg.com');
      	insert vp;
      	
      	User director = new User(Alias = 'Diret101', Email='director101@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='Director-101', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel2.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='director101@testorg.com');
      	insert director;
      	
        
        User u = new User(Alias = 'TestU101', Email='testuser101@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='TestUser-101', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel1.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='standarduser101@testorg.com');
      	insert u;	
      	
      	System.debug('<<<<<<<user created successfully.......!>>>>>>' + u);
     	
        System.runAs(u)
        {
	        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
		        .organizationCode(organizationCode)
	        	.distributionChannel(distributionChannel)
	        	.division(division)
		        .build(createInDatabase);
	        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
	        	        
	        Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
	    	
	        
	        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
	
	        Opportunity o = new OpportunityBuilder()
	        					.account(vinnies)
	        					.priceBookId(customPricebook.Id).build(createInDatabase);
	        System.assert(o.Id <> null,'An error occured while creating Opportunity.');
	
	        Product2 product = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
	        		.code('03.161.001')
	        		.build(createInDatabase);
	        System.assert(product.Id <> null,'An error occured while creating Product.');
	        
	        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
	        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(customPricebook.Id).build(createInDatabase);
	        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
	        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');
	        
	        //Create Set Class
	        Set_Class__c setClass = new SetClassBuilder()
	        								.equipmentNumber('TEST SC101').productCode('03.161.001').build(createInDatabase);
	        System.assert(setClass.Id <> null,'An error occured while creating SetClass.');
	        
	       
	        //Create SetClass Items
	        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
	        								.header(setClass)
	        								.product(product)
	        								.quantity(2).build(createInDatabase);
	        
	        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
	        /*
	        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
	        												.name('Set Buy')
	        												.setLevel2_Approver1_RoleName('Director')
	        												.setLevel3_Approver1_RoleName('VP')
	        												.setDestinationTransactionDocumentTypeId('SALE CONTRACT')
        													.setDestinationTransactionDocumentLocation('01')
	        												.build(createInDatabase);
	        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
	        
	        //Create custom setting to map Quote_Transaction_Type__c to GlobalConstants.QuoteTypes enum 
	        Quote_Types__c quoteType = Quote_Types__c.getValues(quoteTransactionType.Name);
			
			
			if (quoteType == null) {
				Quote_Types__c cs = new Quote_Types__c();
				cs.Name = quoteTransactionType.Name;  
				cs.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
				cs.Quote_Type_Enum_Index__c = 0;
				Database.SaveResult DR_cs = Database.insert(cs);
				System.assertEquals(true,DR_cs.isSuccess(),'>>>>>Custom setting for Set Buy failed to insert');
		
			} else {
				quoteType.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
				quoteType.Quote_Type_Enum_Index__c = 0;
				Database.SaveResult DR_cs = Database.update(quoteType);
				System.assertEquals(true,DR_cs.isSuccess(),'>>>>>Custom setting for Set Buy failed to update');
			}
	        */
	        
	        //Create Quote
	        //String nextQuoteDocumentNumber = CustomSettingsTools.getNextQuotationNumber();
	        quote = new QuoteBuilder()
	        		.opportunity(o)
	        		.priceBookId(customPricebook.Id)
	        		.quoteType(GlobalConstants.QuoteTypes.CustomerQuote)
	        		.build(createInDatabase);
	        System.assert(quote.Id <> null,'An error occured while creating Quote.');
	        
	        //sync Opportunity and Quote lines.
	        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
	        
	        
	       
	        Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
	        		.name('TEST SC101')
	        		.quote(quote)
	        		.setClass(setClass)
	        		.groupType('Kit')
	        		.groupStatus('Validated')
	        		.build(createInDatabase);
	        System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup.');
	
			
			QuoteLineItem quoteLineItem = new QuoteLineItem(
							QuoteId=quote.Id,
							PriceBookEntryId=pbCustomEntry.Id,
							UnitPrice=100.00,
							Quantity= 2,
							Discount = 10.00,
							Set_Class__c = setClass.Id,
							Set_Class_Item__c = setClassItem1.Id,
							Quote_Line_Group__c = quoteLineGroup.Id,
							Tax_Rate__c = 10.00,
							Standard_Cost__c = 30.00,
							Landed_Cost__c = 20.00,
							Sequence_Number__c = 10,
							Price_Status__c = 'Validated'
							);
			insert quoteLineItem;
		
        }
        
        System.debug('<<<<<<<quote created successfully.......!>>>>>>' + quote);
        
        if (quote != null){
        	result = new Map<Id,User>();
        	result.put(quote.Id, u);
        }
	
		return result;
        
    }
    
    public static QuoteLineItem getTestQuoteLineItem(){
    	
    	Boolean createInDatabase = true;
        
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(createInDatabase);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
	    
        Opportunity o = new OpportunityBuilder()
        					.account(vinnies)
        					.priceBookId(customPricebook.Id).build(createInDatabase);
        System.assert(o.Id <> null,'An error occured while creating Opportunity.');

        Product2 product = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('03.161.001')
        		.build(createInDatabase);
        System.assert(product.Id <> null,'An error occured while creating Product.');
        
        //Create Standard PricebookEntry for product
        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
          //Create Custom PricebookEntry for product
        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(customPricebook.Id).build(createInDatabase);
        //System.assert(pbEntry != null &&  pbEntry.size() > 0, 'An error occured while creating PricebookEntry.');
        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        
        //Create Set Class
        Set_Class__c setClass = new SetClassBuilder()
        								.equipmentNumber('TEST SC101').productCode('03.161.001').build(createInDatabase);
        System.assert(setClass.Id <> null,'An error occured while creating SetClass.');
        
       
        //Create SetClass Items
        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
        								.header(setClass)
        								.product(product)
        								.quantity(2).build(createInDatabase);
        
        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.setLevel2_Approver1_RoleName('Director')
	        											.setLevel3_Approver1_RoleName('VP')
        												.setDestinationTransactionDocumentTypeId('SALE CONTRACT')
        												.setDestinationTransactionDocumentLocation('01')
        												.build(createInDatabase);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        
        
        //Create custom setting to map Quote_Transaction_Type__c to GlobalConstants.QuoteTypes enum 
        Quote_Types__c quoteType = Quote_Types__c.getValues(quoteTransactionType.Name);
				
		if (quoteType == null) {
			Quote_Types__c cs = new Quote_Types__c();
			cs.Name = quoteTransactionType.Name;  
			cs.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
			cs.Quote_Type_Enum_Index__c = 0;
			Database.SaveResult DR_cs = Database.insert(cs);
			System.assertEquals(true,DR_cs.isSuccess(),'>>>>>Custom setting for Set Buy failed to insert');
	
		} else {
			quoteType.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
			quoteType.Quote_Type_Enum_Index__c = 0;
			Database.SaveResult DR_cs = Database.update(quoteType);
			System.assertEquals(true,DR_cs.isSuccess(),'>>>>>Custom setting for Set Buy failed to update');
		}
        */
        
        //Create Quote
        Quote quote = new QuoteBuilder()
        		.opportunity(o)
        		.priceBookId(customPricebook.Id)
        		.quoteType(GlobalConstants.QuoteTypes.CustomerQuote)
        		.build(createInDatabase);
        System.assert(quote.Id <> null,'An error occured while creating Quote.');
        
        //sync Opportunity and Quote lines.
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
        
       
        Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
        		.name('TEST SC101')
        		.quote(quote)
        		.setClass(setClass)
        		.groupType('Kit')
        		.groupStatus('Validated')
        		.build(createInDatabase);
        System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup.');

		
		QuoteLineItem quoteLineItem = new QuoteLineItem(
						QuoteId=quote.Id,
						PriceBookEntryId=pbCustomEntry.Id,
						UnitPrice=10.00,
						Quantity= 2,
						Discount = 10.00,
						Set_Class__c = setClass.Id,
						Set_Class_Item__c = setClassItem1.Id,
						Quote_Line_Group__c = quoteLineGroup.Id,
						Tax_Rate__c = 10.00,
						Standard_Cost__c = 30.00,
						Landed_Cost__c = 20.00,
						Sequence_Number__c = 10,
						Price_Status__c = 'New'
						);
		insert quoteLineItem;
	
								
		return quoteLineItem;
        
    }	
    
    
    
	/*
		CREATE CBC or Surgeon Preference QUOTE and related records - note that this method is the same as getTestQuote except that it is CBC specific
		Create 4 level UserRoles, 
		Create new Users linked to these UserRoles in a hierarchy model.
		Create users with System Administrator profile
		Create Account, PriceBook, SetClass, SetClass Items, Opporuntity, Quote_Transaction_Type__c, Quote, Quote_Line_Group__c & QuoteLineItem
		
		Return Map object with QuoteId and the RunAs User
	*/
	public static Map<Id,User> getCBCandSPTestQuote(GlobalConstants.QuoteTypes pQuoteType){
		
    	
    	Map<Id,User> result = null;
    	
    	Boolean createInDatabase = true;
        Quote quote = null;
        Consignment_Business_Case__c quoteCBC;
        Surgeon_Preference__c quoteSurgeonPref;
        
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01';
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        UserRole roleLevel4 = new UserRole();
        roleLevel4.Name = 'CEO';
        insert roleLevel4;
        
        UserRole roleLevel3 = new UserRole();
        roleLevel3.Name = 'VP';
        roleLevel3.ParentRoleId = roleLevel4.Id;
        insert roleLevel3;
        
        UserRole roleLevel2 = new UserRole();
        roleLevel2.Name = 'Director';
        roleLevel2.ParentRoleId = roleLevel3.Id;
        insert roleLevel2;
        
        UserRole roleLevel1 = new UserRole();
        roleLevel1.Name = 'Manager';
        roleLevel1.ParentRoleId = roleLevel2.Id;
        insert roleLevel1;
        
        User ceo = new User(Alias = 'CEO201', Email='ceo201@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='CEO-201', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel4.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='ceo201@testorg.com');
      	insert ceo;
      	
        User vp = new User(Alias = 'VP201', Email='vp201@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='VP-201', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel3.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='vp201@testorg.com');
      	insert vp;
      	
      	User director = new User(Alias = 'Diret201', Email='director201@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='Director-201', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel2.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='director201@testorg.com');
      	insert director;
      	
        
        User u = new User(Alias = 'TestU201', Email='testuser201@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='TestUser-201', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel1.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='standarduser201@testorg.com');
      	insert u;	
      	
      	System.debug('<<<<<<<user created successfully.......!>>>>>>' + u);
     	
        System.runAs(u)
        {
	        Account vinnies = new AccountBuilder().name('Vinnies').accountNumber('STV2010')
		        .organizationCode(organizationCode)
	        	.distributionChannel(distributionChannel)
	        	.division(division)
		        .build(createInDatabase);
	        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
	        	        
	        Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
	    	
	        
	        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
	
	        Opportunity o = new OpportunityBuilder()
	        					.account(vinnies)
	        					.priceBookId(customPricebook.Id).build(createInDatabase);
	        System.assert(o.Id <> null,'An error occured while creating Opportunity.');
	
	        Product2 product = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
	        		.code('99.161.001-CBC-SP')
	        		.build(createInDatabase);
	        System.assert(product.Id <> null,'An error occured while creating Product.');
	        system.debug('Product Created=>' + product);
	        
	        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
	        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(customPricebook.Id).build(createInDatabase);
	        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');
	        /*
	        //Create quote type for CBC 
	        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
	        												.name(pQuoteType.name())
	        												.setLevel2_Approver1_RoleName('Director')
	        												.setLevel3_Approver1_RoleName('VP')
	        												.setDestinationTransactionDocumentTypeId('SALE CONTRACT')
        													.setDestinationTransactionDocumentLocation('01')
	        												.build(createInDatabase);
	        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
	        
	        
	        //Create custom setting to map Quote_Transaction_Type__c to GlobalConstants.QuoteTypes enum 
	        Quote_Types__c quoteType = Quote_Types__c.getValues(quoteTransactionType.Name);
					
			if (quoteType == null) {
				Quote_Types__c cs = new Quote_Types__c();
				cs.Name = quoteTransactionType.Name;  
				cs.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
				cs.Quote_Type_Enum_Index__c = pQuoteType == GlobalConstants.QuoteTypes.CBC? 1 : 2;
				Database.SaveResult DR_cs = Database.insert(cs);
				System.assertEquals(true,DR_cs.isSuccess(),'>>>>>Custom setting for Set Buy failed to insert');
		
			} else {
				quoteType.Quote_Transaction_Type_Id__c = quoteTransactionType.Id;
				quoteType.Quote_Type_Enum_Index__c = pQuoteType == GlobalConstants.QuoteTypes.CBC? 1 : 2;
				Database.SaveResult DR_cs = Database.update(quoteType);
				System.assertEquals(true,DR_cs.isSuccess(),'>>>>>Custom setting for Set Buy failed to update');
			}
	        */
	        
	        // Create SB Partner address
	        Address__c quoteAddress = new AddressBuilder()
	        						  .name('SB Test Partner 1')
	        						  .account(vinnies)
	        						  .build(createInDatabase);
	        System.assert(quoteAddress.Id <> null,'An error occured while creating SB Parter - Address__c.');						  
	        
	        // Create CBC master and quote for CBC OR SP master and quote for SP depending on which one you are building
	        if (pQuoteType == GlobalConstants.QuoteTypes.CBC) {
		        // Create CBC Master
		        quoteCBC = new ConsignmentBusinessCaseBuilder()
		        										.name('CBC' + organizationCode + distributionChannel + division + quoteAddress.Name)
		        										.account(vinnies)
		        										.address(quoteAddress)
		        										.build(createInDatabase);
		        System.assert(quoteCBC.Id <> null,'An error occured while creating Consignment_Business_Case.');	
		        
		        //Create CBC Quote
		        //String nextQuoteDocumentNumber = CustomSettingsTools.getNextCBCQuotationNumber();
		        quote = new QuoteBuilder()
		        		.opportunity(o)
		        		.priceBookId(customPricebook.Id)
		        		.quoteType(GlobalConstants.QuoteTypes.CBC)
		        		.address(quoteAddress)
		        		.consignmentBusinessCase(quoteCBC)
		        		.buildCbcQuote(createInDatabase);
		        System.assert(quote.Id <> null,'An error occured while creating CBC Quote.');
	        								
	        } else {
	        	// Create SP Master
		        quoteSurgeonPref = new SurgeonPreferenceBuilder()
		        										.name('SP' + organizationCode + distributionChannel + division + quoteAddress.Name)
		        										.account(vinnies)
		        										.build(createInDatabase);
		        //Create SP Quote
		        //String nextQuoteDocumentNumber = CustomSettingsTools.getNextCBCQuotationNumber();
		        quote = new QuoteBuilder()
		        		.opportunity(o)
		        		.priceBookId(customPricebook.Id)
		        		.quoteType(GlobalConstants.QuoteTypes.SurgeonPreference)
		        		.surgeonPreference(quoteSurgeonPref)
		        		.buildSurgeonPrefQuote(createInDatabase);
		        System.assert(quote.Id <> null,'An error occured while creating SP Quote.');
	        
	        }
	        
	        //sync Opportunity.
	        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
	        
	        if (pQuoteType == GlobalConstants.QuoteTypes.CBC) {
	        	Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
	        		.name('MISC 1')
	        		.quote(quote)
	        		.groupType('Kit')
	        		.groupStatus('Validated')
	        		.consignmentBusinessCase(quoteCBC)
	        		.buildCBCGroup(createInDatabase);
	        	System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup.');
	        } else {
	        	Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
	        		.name('MISC 1')
	        		.quote(quote)
	        		.groupType('Kit')
	        		.groupStatus('Validated')
	        		.surgeonPreference(quoteSurgeonPref)
	        		.buildSPGroup(createInDatabase);
	        	System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup.');
	        }
        }
        
        System.debug('<<<<<<<quote created successfully.......!>>>>>>' + quote);
        
        if (quote != null){
        	result = new Map<Id,User>();
        	result.put(quote.Id, u);
        }
	
		return result;
        
    }
    

	//Consignment SetBuild / ChangeSet from Actual BOM
	public static Map<Id,User> getChangeSetTestQuote(){
		
    	
    	Map<Id,User> result = null;
    	
    	Boolean createInDatabase = true;
        Quote quote = null;
        ActualBillOfMaterial__c actualBOM;
        
        String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01';
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        UserRole roleLevel4 = new UserRole();
        roleLevel4.Name = 'CEO';
        insert roleLevel4;
        
        UserRole roleLevel3 = new UserRole();
        roleLevel3.Name = 'VP';
        roleLevel3.ParentRoleId = roleLevel4.Id;
        insert roleLevel3;
        
        UserRole roleLevel2 = new UserRole();
        roleLevel2.Name = 'Director';
        roleLevel2.ParentRoleId = roleLevel3.Id;
        insert roleLevel2;
        
        UserRole roleLevel1 = new UserRole();
        roleLevel1.Name = 'Manager';
        roleLevel1.ParentRoleId = roleLevel2.Id;
        insert roleLevel1;
        
        User u = new User(Alias = 'TestU201', Email='testuser201@testorg.com', 
      		EmailEncodingKey='ISO-8859-1', LastName='TestUser-201', LanguageLocaleKey='en_US', 
      		LocaleSidKey='en_AU', ProfileId = p.Id, UserRoleId = roleLevel1.Id,
      		TimeZoneSidKey='Australia/Sydney', UserName='standarduser201@testorg.com');
      	insert u;	
      	
      	System.debug('<<<<<<<user created successfully.......!>>>>>>' + u);
     	
        System.runAs(u)
        {
	        Account vinnies = new AccountBuilder().name('Vinnies').accountNumber('STV2010')
		        .organizationCode(organizationCode)
	        	.distributionChannel(distributionChannel)
	        	.division(division)
		        .build(createInDatabase);
	        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
	        	        
	        Pricebook2 customPricebook = null;
		    try{
		    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
		    			And DistributionChannel__c =: distributionChannel LIMIT 1];
		    			
		    	if (customPricebook.IsActive == false){
		    		Pricebook2 updatePricebook = new Pricebook2();
		    		updatePricebook.Id = customPricebook.Id;
		    		updatePricebook.IsActive = true;
		    		update updatePricebook;
		    	}
		    }
		    catch(Exception e){
		    	customPricebook = null;
		    }
		    if (customPricebook == null){
	        	customPricebook = new PriceBook2Builder()
	        			.name(organizationCode + distributionChannel)
	        			.organizationCode(organizationCode)
	        			.distributionChannel(distributionChannel)
	        			.build(true);
	        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
		    }
	    	System.debug('CustomPriceBook>>>>:' + customPricebook);
	    	
	        
	        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
	
	        Opportunity o = new OpportunityBuilder()
	        					.account(vinnies)
	        					.priceBookId(customPricebook.Id).build(createInDatabase);
	        System.assert(o.Id <> null,'An error occured while creating Opportunity.');
	
	        Product2 product = new ProductBuilder()
	        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
	        		.code('99.161.001-CS')
	        		.build(createInDatabase);
	        System.assert(product.Id <> null,'An error occured while creating Product.');
	        
	        //Create PricebookEntry for product
	        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
	        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
	        
	         //Create PricebookEntry for Custom Pricebook
	        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
	        									.productId(product.Id)
	        									.pricebookId(customPricebook.Id).build(createInDatabase);
	        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');
	        
	        Product2 actualBOMproduct = new ProductBuilder()
	        		.name('ABOM-BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
	        		.code('ABOM-99.161.001')
	        		.build(createInDatabase);
	        System.assert(actualBOMproduct.Id <> null,'An error occured while creating Actual BOM Product.');
	        
	     	actualBOM = new ActualBillOfMaterial__c(Name = 'ABOM-99.161.001',Active__c = true,ActualBOMProduct__c = actualBOMproduct.Id,
	     													Customer__c = vinnies.Id,DistributionChannel__c= distributionChannel,Division__c=division,
	     													EquipmentNumber__c='ABOM-101',EquipmentType__c='NORM-L',
	     													OrganizationCode__c=organizationCode,ValidFromDate__c = '20140101',ValidToDate__c='20200101');
	     	insert actualBOM;
	        System.assert(actualBOM.Id <> null,'An error occured while creating Actual BOM.');	
	        
	         // Create SB Partner address
	        Address__c quoteAddress = new AddressBuilder()
	        						  .name('SB Test Partner 1')
	        						  .account(vinnies)
	        						  .build(createInDatabase);
	        System.assert(quoteAddress.Id <> null,'An error occured while creating SB Parter - Address__c.');		
	        
	        //Create CBC Quote
	        //String nextQuoteDocumentNumber = CustomSettingsTools.getNextQuotationNumber();
	        quote = new QuoteBuilder()
	        		.opportunity(o)
	        		.priceBookId(customPricebook.Id)
	        		.quoteType(GlobalConstants.QuoteTypes.ChangeSet)
	        		.actualBOM(actualBOM)
	        		.address(quoteAddress)
	        		.buildChangeSetQuote(createInDatabase);
	        System.assert(quote.Id <> null,'An error occured while creating CBC Quote.');
	        
	        //sync Opportunity.
	        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
	        
        }
        
        System.debug('<<<<<<<Set Request Quote created successfully.......!>>>>>>' + quote);
        
        if (quote != null){
        	result = new Map<Id,User>();
        	result.put(quote.Id, u);
        }
	
		return result;
        
    }
    
}