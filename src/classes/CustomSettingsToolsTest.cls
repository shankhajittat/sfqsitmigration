@isTest(SeeAllData=true)
private class CustomSettingsToolsTest {
	
	/*
	DEV
		https://ap1.salesforce.com/a0X/l
	
		CustomSettingsTools.upsertSetting('CrypToKey',
			'adkzqfg01q09wM20',false);
			
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_PRICING_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_PriceSimulationSBox_v1.webservices.provider:priceSimulation_Provider/MA700_GPQ_MARS_PriceSimulationSBox_v1_webservices_provider_priceSimulation_Provider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_QUOTATION_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_QuotationSBox_v1.webservices.provider:quotationProvider/MA700_GPQ_MARS_QuotationSBox_v1_webservices_provider_quotationProvider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_SP_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_SurgeonSBox_v1.webservices.provider:surgeonProvider/MA700_GPQ_MARS_SurgeonSBox_v1_webservices_provider_surgeonProvider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_CBC_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_CBCAgreementSBox_v1.webservices.provider:CBCAgreement_Provider/MA700_GPQ_MARS_CBCAgreementSBox_v1_webservices_provider_CBCAgreement_Provider_Port'
			,true);
			
			
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_USERNAME',
			'MA700_GPQ_Mars_users',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_PASSWORD',
			'GP@Mar700',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_CALLOUT_TIMEOUT',
			'120000',false);
	
	SIT
		
		CustomSettingsTools.upsertSetting('CrypToKey',
			'adkzqfg01q09wM20',false);
			
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_PRICING_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_PriceSimulation_v1.GPQToBroker.webservices.Provider:priceSimulation_provider/MA700_GPQ_MARS_PriceSimulation_v1_GPQToBroker_webservices_Provider_priceSimulation_provider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_QUOTATION_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_Quotation_v1.GPQToBroker.webservices.Provider:quotationRequest/MA700_GPQ_MARS_Quotation_v1_GPQToBroker_webservices_Provider_quotationRequest_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_SP_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_SurgeonList_v1.GPQToBroker.webservices:surgeonProvider/MA700_GPQ_MARS_SurgeonList_v1_GPQToBroker_webservices_surgeonProvider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_CBC_ENDPOINT_URL'
			,'https://mddis8apd.apx.jnj.com/ws/MA700_GPQ_MARS_CBCAgreement_v1.GPQToBroker.webservices.provider:CBCProvider/MA700_GPQ_MARS_CBCAgreement_v1_GPQToBroker_webservices_provider_CBCProvider_Port'
			,true);
			
			
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_USERNAME',
			'MA700_GPQ_Mars_users',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_PASSWORD',
			'GP@Mar700',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_CALLOUT_TIMEOUT',
			'120000',false);
				
			
	UAT
	
		CustomSettingsTools.upsertSetting('CrypToKey',
			'adkzqfg01q09wM20',false);
			
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_PRICING_ENDPOINT_URL'
			,'https://mddis8apq.apx.jnj.com/ws/MA700_GPQ_MARS_PriceSimulation_v1.GPQToBroker.webservices.Provider:priceSimulation_provider/MA700_GPQ_MARS_PriceSimulation_v1_GPQToBroker_webservices_Provider_priceSimulation_provider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_QUOTATION_ENDPOINT_URL'
			,'https://mddis8apq.apx.jnj.com/ws/MA700_GPQ_MARS_Quotation_v1.GPQToBroker.webservices.Provider:quotationRequest/MA700_GPQ_MARS_Quotation_v1_GPQToBroker_webservices_Provider_quotationRequest_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_SP_ENDPOINT_URL'
			,'https://mddis8apq.apx.jnj.com/ws/MA700_GPQ_MARS_SurgeonList_v1.GPQToBroker.webservices:surgeonProvider/MA700_GPQ_MARS_SurgeonList_v1_GPQToBroker_webservices_surgeonProvider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_CBC_ENDPOINT_URL'
			,'https://mddis8apq.apx.jnj.com/ws/MA700_GPQ_MARS_CBCAgreement_v1.GPQToBroker.webservices.provider:CBCProvider/MA700_GPQ_MARS_CBCAgreement_v1_GPQToBroker_webservices_provider_CBCProvider_Port'
			,true);
			
			
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_USERNAME',
			'MA700_GPQ_Mars_users',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_PASSWORD',
			'GP@Mar700',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_CALLOUT_TIMEOUT',
			'120000',false);
	
	
	PROD
	
		CustomSettingsTools.upsertSetting('CrypToKey',
			'adkzqfg01q09wM20',false);
			
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_PRICING_ENDPOINT_URL'
			,'https://mddis8app.apx.jnj.com/ws/MA700_GPQ_MARS_PriceSimulation_v1.GPQToBroker.webservices.Provider:priceSimulation_provider/MA700_GPQ_MARS_PriceSimulation_v1_GPQToBroker_webservices_Provider_priceSimulation_provider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_QUOTATION_ENDPOINT_URL'
			,'https://mddis8app.apx.jnj.com/ws/MA700_GPQ_MARS_Quotation_v1.GPQToBroker.webservices.Provider:quotationRequest/MA700_GPQ_MARS_Quotation_v1_GPQToBroker_webservices_Provider_quotationRequest_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_SP_ENDPOINT_URL'
			,'https://mddis8app.apx.jnj.com/ws/MA700_GPQ_MARS_SurgeonList_v1.GPQToBroker.webservices:surgeonProvider/MA700_GPQ_MARS_SurgeonList_v1_GPQToBroker_webservices_surgeonProvider_Port'
			,true);
		QuoteSettingTools.upsertSetting('WEBMETHODS_SAP_CBC_ENDPOINT_URL'
			,'https://mddis8app.apx.jnj.com/ws/MA700_GPQ_MARS_CBCAgreement_v1.GPQToBroker.webservices.provider:CBCProvider/MA700_GPQ_MARS_CBCAgreement_v1_GPQToBroker_webservices_provider_CBCProvider_Port'
			,true);
			
			
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_USERNAME',
			'MA700_GPQ_Mars_users',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_SAP_CREDENTIAL_PASSWORD',
			'GP@Mar700',true);
		CustomSettingsTools.upsertSetting('WEBMETHODS_CALLOUT_TIMEOUT',
			'120000',false);
			
	*/
	
	 static testMethod void upsertQuoteSettingTest() {
		//adkzqfg01q09wM20
		String key = 'MyUserName';
		String value = 'thomas.thankachan';
		String encryptedValue = null;
		String decryptedValue = null;
		
		//Create CryptoKey if the key does not exist.
		CustomSettingsTools.createTestCryptToKey();
		
		try{
			encryptedValue = CustomSettingsTools.upsertSetting(key, value, true);
		}
		catch(Exception e){
			system.debug('Exception: '  +e);
		}
		
		system.debug('Encrypted Value: ' + encryptedValue);
		
		
		try{
			decryptedValue = CustomSettingsTools.getSecuredKeyValue(key);
		}
		catch(Exception e){
			system.debug('Exception: '  +e);
		}
		
		system.debug('Decrypted Value: ' + decryptedValue);
		
		system.assert(value == decryptedValue, 'Was expecting same value. ' + value  + ' <> ' + decryptedValue);
		
		//Update test
		value = 'thomas.thankachan101';
		
		try{
			encryptedValue = CustomSettingsTools.upsertSetting(key, value, true);
		}
		catch(Exception e){
			system.debug('Exception: '  +e);
		}
		
		system.debug('Updated Encrypted Value: ' + encryptedValue);
		
		
		try{
			decryptedValue = CustomSettingsTools.getSecuredKeyValue(key);
		}
		catch(Exception e){
			system.debug('Exception: '  +e);
		}
		
		system.debug('Updated Decrypted Value: ' + decryptedValue);
		
		system.assert(value == decryptedValue, 'Was expecting same value. ' + value  + ' <> ' + decryptedValue);
		
	}
	/*
    static testMethod void getNextQuotationNumberTest() {
        
        String quoteNumber = CustomSettingsTools.getNextQuotationNumber();
        System.debug('quoteNumber>>>>' + quoteNumber);
        System.assertEquals('QU-', quoteNumber.substring(0,3), 'Expected the first 3 characters to begin with QU- for set buy quotes');
        System.assert(quoteNumber != null && quoteNumber.length() > 0,'Was expecting a valid Quote Number');
    }
    
    static testMethod void getNextCBCQuotationNumberTest1() {
        
        String quoteNumber = CustomSettingsTools.getNextCBCQuotationNumber();
        System.debug('quoteNumber>>>>' + quoteNumber);
        System.assertEquals('CBC-', quoteNumber.substring(0,4), 'Expected the first 4 characters to begin with CBC- for CBC quotes');
        System.assert(quoteNumber != null && quoteNumber.length() > 0,'Was expecting a valid CBC Quote Number');
    }
    
    static testMethod void getNextSPQuotationNumberTest() {
        
        String quoteNumber = CustomSettingsTools.getNextSPQuotationNumber();
        System.debug('quoteNumber>>>>' + quoteNumber);
        System.assertEquals('SP-', quoteNumber.substring(0,3), 'Expected the first 3 characters to begin with SP- for surgeon preference quotes');
        System.assert(quoteNumber != null && quoteNumber.length() > 0,'Was expecting a valid SP Quote Number');
    }
    
    static testMethod void getNextCBCQuotationNumberTest2() {
    	// Test situation where custom setting data is not there - example empty sandbox
    	
    	Quotation_Document_Number_Setting__c customSettingQuoteDocNum = Quotation_Document_Number_Setting__c.getValues('CBCQuoteNextDocumentNumberKey');
    	if (customSettingQuoteDocNum != null) {
    		Database.DeleteResult result = Database.delete(customSettingQuoteDocNum);
    		System.assertEquals(true,result.isSuccess(),'>>>>>Custom setting for CBCQuoteNextDocumentNumberKey failed to delete');
    	}
        
        String quoteNumber = CustomSettingsTools.getNextCBCQuotationNumber();
        System.debug('quoteNumber>>>>' + quoteNumber);
        System.assert(quoteNumber != null && quoteNumber.length() > 0,'Was expecting a valid CBC Quote Number');
        System.assertEquals('CBC-', quoteNumber.substring(0,4), 'Expected the first 4 characters to begin with CBC- for CBC quotes');
    }
    
    static testMethod void GetNextOpportunityNumberTest1() {
        
        String oppNumber = CustomSettingsTools.GetNextOpportunityNumber();
        System.debug('oppNumber>>>>' + oppNumber);
        System.assert(oppNumber != null && oppNumber.length() > 0,'Was expecting a valid Opportunity Number');
    }
    
    static testMethod void GetNextOpportunityNumberTest2() {
    	// Test the situation where custom setting is not there - example empty sandbox
    	
    	Opportunity_Document_Number_Setting__c customSettingOppDocNum = Opportunity_Document_Number_Setting__c.getValues('OpportunityNextDocumentNumberKey');
    	if (customSettingOppDocNum != null) {
    		Database.DeleteResult result = Database.delete(customSettingOppDocNum);
    		System.assertEquals(true,result.isSuccess(),'>>>>>Custom setting for OpportunityNextDocumentNumberKey failed to delete');
    	}
        
        String oppNumber = CustomSettingsTools.GetNextOpportunityNumber();
        System.debug('oppNumber>>>>' + oppNumber);
        System.assert(oppNumber != null && oppNumber.length() > 0,'Was expecting a valid Opportunity Number');
        System.assertEquals('OPP-', oppNumber.substring(0,4), 'Expected the first 4 characters to begin with OPP- for opportunities');
    }
    */
    
}