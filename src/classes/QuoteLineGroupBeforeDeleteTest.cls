@isTest(seeAllData=true)
private class QuoteLineGroupBeforeDeleteTest {
	
	// This test has 4 tests in it: 
	//		One to test the individual methods.
	//		One to test the main method.
	//		One to test an error. 
	//		One to test the QuoteLineGroup trigger
	
	// This test runs all the individual methods but not the main method
	static testMethod void QuoteLineGroupBeforeDeleteTest1() {
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Test1: Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			// Get the quote that the testing tool created
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Test1: Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'Test1: The quote created in testing tools and the one found here are not the same but should be');
			
			// Get the group that the testing tool created
			Quote_Line_Group__c aGroup = [SELECT Id From Quote_Line_Group__c WHERE QuoteId__c = :sourceQuoteId 
																			 AND Name = 'TEST SC101'];
			System.assertNotEquals(null, aGroup, 'Test1: A group should have been found - testing tool should have created it');
			List<Quote_Line_Group__c> listOfGroups = new List<Quote_Line_Group__c>();
			listOfGroups.Add(aGroup);
			Map<Id, Quote_Line_Group__c> mapOfGroups = new Map<Id, Quote_Line_Group__c>();
			mapOfGroups.put(aGroup.Id,aGroup); 
			
			// Get the line item that the testing tool created for the group
			QuoteLineItem anItem = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id]; 
			System.assertNotEquals(null, anItem, 'Test1: An item for the group should have been found but did not - testing tool should have created it');
			
			
			//Start Test
        	Test.startTest();
        	
			// Test constructor
			QuoteLineGroupBeforeDelete theQuoteLineGroupBeforeDelete = new QuoteLineGroupBeforeDelete(listOfGroups, mapOfGroups);
			System.assert(theQuoteLineGroupBeforeDelete.quoteLineGroups.size() > 0, 'Test1: The constructor should have populated the list of quoteLineGroups');
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap, 'Test1: The constructor should have populated the map of groups being deleted');
	    	Boolean contains = theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap.containsKey(aGroup.Id);
			System.assertEquals(contains, True, 'Test1: The constructor has not set up the map correctly - it should contain the group id that testing tool created');
	    	Quote_Line_Group__c theGroupInTheMap = theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap.get(aGroup.Id);
	    	System.assertNotEquals(null, theGroupInTheMap, 'Test1: The constructor has not set up the map correctly - the group object should be a value in the map but it is null');
	    	System.assertEquals(theGroupInTheMap.Id, aGroup.Id, 'Test1: The constructor has not set up the map correctly - the group object in the map does not have the same id as the one in the keyset');
	    	
	    	// Test the listItemsBelongingToGroup method
	    	theQuoteLineGroupBeforeDelete.listItemsBelongingToGroup();
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.itemsToBeDeleted, 'Test1: Map of Items belonging to this group should not be null - testing tool set up an item for the group');
	    	System.assert(theQuoteLineGroupBeforeDelete.itemsToBeDeleted.size() > 0, 'Test1: Map of Items belonging to this group has no entries. It should have one entry of an item created by testing tool');
	    	contains = theQuoteLineGroupBeforeDelete.itemsToBeDeleted.containsKey(anItem.Id);
	    	System.assertEquals(contains, True, 'Test1: The Map of items for the group does not contain an Id for the test Item');
	    	QuoteLineItem theItemInTheMap = theQuoteLineGroupBeforeDelete.itemsToBeDeleted.get(anItem.Id);
	    	System.assertNotEquals(null, theItemInTheMap, 'Test1: The Map value for the keyset does not contain the test item');
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.deleteItems, 'Test1: List of Items belonging to this group that will be deleted, should not be null - testing tool set up an item for the group');
	    	System.assertEquals(1, theQuoteLineGroupBeforeDelete.deleteItems.size(), 'Test1: Expecting only one item to be in the List of items belonging to the test group, but found more than 1');
	    	System.assertEquals(anItem.Id, theQuoteLineGroupBeforeDelete.deleteItems[0], 'Test1: The List of item ids belonging to the test group does not contain the id of the test item but it should');
	    	
	    	// Delete the item
	    	Boolean isDeleted = theQuoteLineGroupBeforeDelete.deleteItems();
	    	System.assertEquals(true, isDeleted, 'Test1: A problem was encountered deleting the item belonging to the test group');
	    	List<QuoteLineItem> items = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id LIMIT 1];
	    	System.assertEquals(true, items.isEmpty(), 'Test1: The item belonging to the test group should have been deleted');
			
	    	Test.stopTest();
		}
	}
	
	// This test just tests the main method
	static testMethod void QuoteLineGroupBeforeDeleteTest2() {
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Test2: Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			// Get the quote that the testing tool created
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Test2: Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'Test2: The quote created in testing tools and the one found here are not the same but should be');
			
			// Get the group that the testing tool created
			Quote_Line_Group__c aGroup = [SELECT Id From Quote_Line_Group__c WHERE QuoteId__c = :sourceQuoteId 
																			 AND Name = 'TEST SC101'];
			System.assertNotEquals(null, aGroup, 'Test2: A group should have been found - testing tool should have created it');
			List<Quote_Line_Group__c> listOfGroups = new List<Quote_Line_Group__c>();
			listOfGroups.Add(aGroup);
			Map<Id, Quote_Line_Group__c> mapOfGroups = new Map<Id, Quote_Line_Group__c>();
			mapOfGroups.put(aGroup.Id,aGroup); 
			
			// Get the line item that the testing tool created for the group
			QuoteLineItem anItem = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id]; 
			System.assertNotEquals(null, anItem, 'Test2: An item for the group should have been found but did not - testing tool should have created it');
			
			
			//Start Test
        	Test.startTest();
        	
			// Test constructor
			QuoteLineGroupBeforeDelete theQuoteLineGroupBeforeDelete = new QuoteLineGroupBeforeDelete(listOfGroups, mapOfGroups);
			System.assert(theQuoteLineGroupBeforeDelete.quoteLineGroups.size() > 0, 'Test2: The constructor should have populated the list of quoteLineGroups');
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap, 'Test2: The constructor should have populated the map of groups being deleted');
	    	Boolean contains = theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap.containsKey(aGroup.Id);
			System.assertEquals(contains, True, 'Test2: The constructor has not set up the map correctly - it should contain the group id that testing tool created');
	    	Quote_Line_Group__c theGroupInTheMap = theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap.get(aGroup.Id);
	    	System.assertNotEquals(null, theGroupInTheMap, 'Test2: The constructor has not set up the map correctly - the group object should be a value in the map but it is null');
	    	System.assertEquals(theGroupInTheMap.Id, aGroup.Id, 'Test2: The constructor has not set up the map correctly - the group object in the map does not have the same id as the one in the keyset');
	    	
	    	// Test run method
	    	Boolean isDeleted = theQuoteLineGroupBeforeDelete.run();
	    	System.assertEquals(true, isDeleted, 'Test2: A problem was encountered deleting the item belonging to the test group');
	    	List<QuoteLineItem> items = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id LIMIT 1];
	    	System.assertEquals(true, items.isEmpty(), 'Test2: The item belonging to the test group should have been deleted');
			
	    	Test.stopTest();
		}
	}
	
	
	// This test tests an error condition
	static testMethod void QuoteLineGroupBeforeDeleteTest3() {
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Test3 : Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			// Get the quote that the testing tool created
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Test3: Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'Test3: The quote created in testing tools and the one found here are not the same but should be');
			
			// Get the group that the testing tool created
			Quote_Line_Group__c aGroup = [SELECT Id From Quote_Line_Group__c WHERE QuoteId__c = :sourceQuoteId 
																			 AND Name = 'TEST SC101'];
			System.assertNotEquals(null, aGroup, 'Test3: A group should have been found - testing tool should have created it');
			List<Quote_Line_Group__c> listOfGroups = new List<Quote_Line_Group__c>();
			listOfGroups.Add(aGroup);
			Map<Id, Quote_Line_Group__c> mapOfGroups = new Map<Id, Quote_Line_Group__c>();
			mapOfGroups.put(aGroup.Id,aGroup); 
			
			// Get the line item that the testing tool created for the group
			QuoteLineItem anItem = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id]; 
			System.assertNotEquals(null, anItem, 'Test3: An item for the group should have been found but did not - testing tool should have created it');
			
			
			//Start Test
        	Test.startTest();
        	
			// Test constructor
			QuoteLineGroupBeforeDelete theQuoteLineGroupBeforeDelete = new QuoteLineGroupBeforeDelete(listOfGroups, mapOfGroups);
			System.assert(theQuoteLineGroupBeforeDelete.quoteLineGroups.size() > 0, 'Test3: The constructor should have populated the list of quoteLineGroups');
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap, 'Test3: The constructor should have populated the map of groups being deleted');
	    	Boolean contains = theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap.containsKey(aGroup.Id);
			System.assertEquals(contains, True, 'Test3: The constructor has not set up the map correctly - it should contain the group id that testing tool created');
	    	Quote_Line_Group__c theGroupInTheMap = theQuoteLineGroupBeforeDelete.quoteLineGroupsOldMap.get(aGroup.Id);
	    	System.assertNotEquals(null, theGroupInTheMap, 'Test3: The constructor has not set up the map correctly - the group object should be a value in the map but it is null');
	    	System.assertEquals(theGroupInTheMap.Id, aGroup.Id, 'Test3: The constructor has not set up the map correctly - the group object in the map does not have the same id as the one in the keyset');
	    	
	    	// Test the listItemsBelongingToGroup method
	    	theQuoteLineGroupBeforeDelete.listItemsBelongingToGroup();
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.itemsToBeDeleted, 'Test3: Map of Items belonging to this group should not be null - testing tool set up an item for the group');
	    	System.assert(theQuoteLineGroupBeforeDelete.itemsToBeDeleted.size() > 0, 'Test3: Map of Items belonging to this group has no entries. It should have one entry of an item created by testing tool');
	    	contains = theQuoteLineGroupBeforeDelete.itemsToBeDeleted.containsKey(anItem.Id);
	    	System.assertEquals(contains, True, 'Test3: The Map of items for the group does not contain an Id for the test Item');
	    	QuoteLineItem theItemInTheMap = theQuoteLineGroupBeforeDelete.itemsToBeDeleted.get(anItem.Id);
	    	System.assertNotEquals(null, theItemInTheMap, 'Test3: The Map value for the keyset does not contain the test item');
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.deleteItems, 'Test3: List of Items belonging to this group that will be deleted, should not be null - testing tool set up an item for the group');
	    	System.assertEquals(1, theQuoteLineGroupBeforeDelete.deleteItems.size(), 'Test3: Expecting only one item to be in the List of items belonging to the test group, but found more than 1');
	    	System.assertEquals(anItem.Id, theQuoteLineGroupBeforeDelete.deleteItems[0], 'Test3: The List of item ids belonging to the test group does not contain the id of the test item but it should');
	    	
	    	
	    	// Delete the item
	    	Boolean isDeleted = theQuoteLineGroupBeforeDelete.deleteItems();
	    	System.assertEquals(true, isDeleted, 'Test3: A problem was encountered deleting the item belonging to the test group');
	    	List<QuoteLineItem> items = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id LIMIT 1];
	    	System.assertEquals(true, items.isEmpty(), 'Test3: The item belonging to the test group should have been deleted');
	    	
	    	// Now cause an error condition by trying to delete the same items again
	    	System.assertNotEquals(null, theQuoteLineGroupBeforeDelete.deleteItems, 'Test3: deleteItems contains null but should still have item to be deleted in it');
			isDeleted = theQuoteLineGroupBeforeDelete.deleteItems();
	    	
	    	Test.stopTest();
	    	
	    	System.assertEquals(false, isDeleted, 'Test3: The delete should have failed. We are forcing an error to occur, but no error occurred - the item to be deleted should not exist and should have caused the delete to fail');
	    	List<QuoteLineItem> items3 = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id LIMIT 1];
	    	System.assertEquals(true, items3.isEmpty(), 'Test3: The item belonging to the test group should have been deleted in the first call to deleteItems() but it exists');
	    	
		}
	}
	
	
	// This test is a direct delete of groups in order to test the QuoteLineGroup trigger
	static testMethod void QuoteLineGroupBeforeDeleteTest4() {
		
		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Test4 : Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
			// Get the quote that the testing tool created
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Test4: Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'Test4: The quote created in testing tools and the one found here are not the same but should be');
			
			// Get the group that the testing tool created
			Quote_Line_Group__c aGroup = [SELECT Id From Quote_Line_Group__c WHERE QuoteId__c = :sourceQuoteId 
																			 AND Name = 'TEST SC101'];
			System.assertNotEquals(null, aGroup, 'Test4: A group should have been found - testing tool should have created it');
			List<Quote_Line_Group__c> listOfGroups = new List<Quote_Line_Group__c>();
			listOfGroups.Add(aGroup);
			Map<Id, Quote_Line_Group__c> mapOfGroups = new Map<Id, Quote_Line_Group__c>();
			mapOfGroups.put(aGroup.Id,aGroup); 
			
			// Get the line item that the testing tool created for the group
			QuoteLineItem anItem = [SELECT Id From QuoteLineItem WHERE Quote_Line_Group__c = :aGroup.Id]; 
			System.assertNotEquals(null, anItem, 'Test4: An item for the group should have been found but did not - testing tool should have created it');
			
			
			//Start Test
        	Test.startTest();
        	
        	Database.DeleteResult[] drDeletes  = Database.delete(listOfGroups, false);
			
			Boolean isDeleteSuccessful = true;
			for(Database.DeleteResult r :drDeletes) {
				if (r.isSuccess() == false) {
					isDeleteSuccessful = false;
				}
			}
			System.assertEquals(true,isDeleteSuccessful,'Test4: An error occurred trying to delete test group');
        	
			Test.stopTest();
		}
	}
	
	
}