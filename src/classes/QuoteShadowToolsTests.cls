@IsTest
public with sharing class QuoteShadowToolsTests {

	private static Quote createQuote(Account account, String name) {

		System.assert(StringUtils.isEmpty(account.OrganizationCode__c) == false,'Was expecting a valid Account OrganizationCode');
		System.assert(StringUtils.isEmpty(account.DistributionChannel__c) == false,'Was expecting a valid Account DistributionChannel');
		System.assert(StringUtils.isEmpty(account.Division__c) == false,'Was expecting a valid Account Division');		
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: account.OrganizationCode__c 
	    			And DistributionChannel__c =: account.DistributionChannel__c LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(account.OrganizationCode__c + account.DistributionChannel__c)
        			.organizationCode(account.OrganizationCode__c)
        			.distributionChannel(account.DistributionChannel__c)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + account.OrganizationCode__c + account.DistributionChannel__c);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity o = new OpportunityBuilder().account(account).priceBookId(customPricebook.Id).build(true);
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        */
        return new QuoteBuilder().opportunity(o).priceBookId(customPricebook.Id).quoteType(GlobalConstants.QuoteTypes.CustomerQuote).build(true);		
	}

	@IsTest
	public static void testCreateShadowCRUD() {
		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        

        Quote quote1 = createQuote(vinnies, 'My Quote 101');
        
        System.assertEquals(1, [select Quote__c, Opportunity__c, Name from Quote__c 
       		where Quote__r.Opportunity.AccountId = :vinnies.Id].size(),
       		'a single shadow is created after insert');
       		
       	update quote1;

        System.assertEquals(1, [select Quote__c, Opportunity__c, Name from Quote__c 
       		where Quote__r.Opportunity.AccountId = :vinnies.Id].size(),
       		'a single shadow only exists after edit');
	}
	
	@IsTest
	public static void testCreateShadowRecords() {
		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        Quote quote1 = createQuote(vinnies, 'My Quote 101');
        Quote quote2 = createQuote(vinnies, 'My Quote 102');
       	Set<Id> quoteIds = new Set<Id>(new Id[] {quote1.Id,quote2.Id});
       	Set<Id> oppIds = new Set<Id>(new Id[] {quote1.OpportunityId,quote2.OpportunityId});
       	Set<String> quoteNames = new Set<String>(new String[] {quote1.Name,quote2.Name});
       	
       	System.debug(quote1);
       	
       	QuoteShadowTools tools = new QuoteShadowTools();	
       	List<Quote__c> shadows = [
       		select Quote__c, Opportunity__c, Name, Account__c, Opportunity__r.AccountId
       		from Quote__c 
       		where Quote__r.Opportunity.AccountId = :vinnies.Id];
       	
       	System.assertEquals(2, shadows.size(), 'two shadow records should be created');
       	
       	Set<Id> shadowQuoteIds = new Set<Id>();
       	Set<Id> shadowOppIds = new Set<Id>();
       	Set<Id> shadowAccIds = new Set<Id>();
       	Set<String> shadowQuoteNames = new Set<String>();
       	
       	for (Quote__c shadow : shadows) {
       		shadowQuoteIds.add(shadow.Quote__c);
       		shadowOppIds.add(shadow.Opportunity__c);
       		shadowAccIds.add(shadow.Opportunity__r.AccountId);
       		shadowQuoteNames.add(shadow.Name);
       		System.assert(shadow.Id != null, 'Shadow record has been saved');
	   		System.assertEquals(vinnies.Id, shadow.Account__c, 'account FK is populated');
       	}
   		System.assertEquals(oppIds, shadowOppIds, 'Opp FKs are populated');
   		System.assertEquals(quoteIds, shadowQuoteIds, 'Quote FKs are populated');
   		System.assertEquals(quoteNames, shadowQuoteNames, 'Quote shadow names are populated');

       		
	}
	
}