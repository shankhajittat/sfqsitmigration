@IsTest
public class QuotePopulatorIntegrationTest {
 	
 	@IsTest
 	public static void testSetClassPopulationCounts() { 		
	        
 		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Pricebook2 customPricebook = new PriceBook2Builder()
    			.name(organizationCode + distributionChannel)
    			.organizationCode(organizationCode)
    			.distributionChannel(distributionChannel)
    			.build(true);
    	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	
    	SetClassParser parser = new SetClassParser(Test.getStandardPricebookId(), TestingTools.getResourceBodyAsString('SetClass_JH230'));
 		parser.run();
 		integer setCount = parser.newSetClassItems.size();
 		System.assertEquals(setCount, 50, 'new set class items were created');
 		
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');        
        
        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity o = new OpportunityBuilder().account(vinnies).priceBookId(customPricebook.Id).build(true);
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
        */
        //String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
        Quote quote = new QuoteBuilder().opportunity(o).priceBookId(customPricebook.Id).quoteType(GlobalConstants.QuoteTypes.CustomerQuote).build(true);
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
        // This tests the constructor that takes quote, set class and group as parameters - the next test checks the other constructor
        QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(quote.Id, parser.newSetClass.Id, 'Kit');
        System.debug('Quote PricebookId: ' + [SELECT q.Pricebook2Id FROM Quote q WHERE q.Id = :quote.Id]);
        
        String groupId = linePopulator.run();
        for(Database.Saveresult r :linePopulator.saveResultList)
        {
        	System.assert(r.isSuccess(),'Populate Save Should Complete!');
        }
        System.assertEquals(linePopulator.quoteLineItemList.size(), setCount, 'lines in quote should be same as set class lines');
 		
 		
 		// Now instantiate a new linepopulator with an invalid quote id and try to run the populator. This should return null for the group id.
 		QuoteGroupLineItemsPopulator linePopulator2 = new QuoteGroupLineItemsPopulator(null, parser.newSetClass.Id, 'Kit');
 		groupId = linePopulator2.run(); 
        System.assertEquals(null, groupId, 'Expected line populator to return null since quote id was invalid');
        
 	} 
 	
 	@IsTest
 	public static void testSetClassPopulationMultipleGroups() {
 		
 		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Pricebook2 customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
    	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
    	 		
 		SetClassParser parser = new SetClassParser(Test.getStandardPricebookId(), TestingTools.getResourceBodyAsString('SetClass_JH230'));
 		parser.run();
 		integer setCount = parser.newSetClassItems.size();
 		System.assertEquals(setCount, 50, 'new set class items were created');
 		
        Account vinnies = new AccountBuilder().name('St Vinceant').accountNumber('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        	        
        //String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
        Opportunity o = new OpportunityBuilder().account(vinnies).priceBookId(customPricebook.Id).build(true);
        /*
        Quote_Transaction_Type__c quoteTransactionType = new QuoteTransactionTypeBuilder()
        												.name('Set Buy')
        												.build(true);
        System.assert(quoteTransactionType.Id <> null,'An error occured while creating quoteTransactionType.');
       	*/
        //String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
        Quote quote = new QuoteBuilder().opportunity(o).priceBookId(customPricebook.Id).quoteType(GlobalConstants.QuoteTypes.CustomerQuote).build(true);
        OpportunityTools.UpdateOpportunitySyncQuoteId(o.Id, quote.Id);
        
        // This tests the constructor that only takes quote and set class as parameters - the first test checks the other constructor
        QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(quote.Id, parser.newSetClass.Id);
        System.debug('Quote PricebookId: ' + [SELECT q.Pricebook2Id FROM Quote q WHERE q.Id = :quote.Id]);
        
        String groupId = linePopulator.run();
        System.assertNotEquals(null, groupId,'Line populator failed to create a group for the quote and set class');
        for(Database.Saveresult r :linePopulator.saveResultList)
        {
        	System.assert(r.isSuccess(),'Populate Save Should Complete!');
        }
        System.assertEquals(linePopulator.quoteLineItemList.size(), setCount, 'lines in quote should be same as set class lines');
 		
 		
 		// Now run the linepopulator again to create a second group with the same set class for the quote
 		groupId = linePopulator.run();
        System.assertNotEquals(null, groupId,'Line populator failed to create a second group for the quote and set class');
        for(Database.Saveresult r :linePopulator.saveResultList)
        {
        	System.assert(r.isSuccess(),'Populate Save Should Complete for second group!');
        }
        System.assertEquals(linePopulator.quoteLineItemList.size(), setCount, 'Lines in second quote should be same as set class lines');
 		
 	} 
 	
 	
 	@IsTest
 	public static void testCBCPopulator() {
 		
 		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getCBCandSPTestQuote(GlobalConstants.QuoteTypes.CBC);
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid CBC testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
		
			String organizationCode = 'UT-AU10';
        	String distributionChannel = '01';
        	String division = '01'; 	
		
			// Get the quote created in the testing tools
			Quote theTestQuote = [SELECT Id, Consignment_Business_Case__c, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Transaction Type on quote should be populated');
			
			// Get the consignment business case created in the testing tools
			Consignment_Business_Case__c theTestcbc = [SELECT Id, Name, Account__c, ShippingLocation__c 
														From Consignment_Business_Case__c 
														Where Id = :theTestQuote.Consignment_Business_Case__c];
			System.assertNotEquals(null, theTestcbc.Id, 'Could not find quote created by testing tool');
			
			// Get Product created in testing tools
			Product2 product = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = '99.161.001-CBC-SP'];
	        System.assertNotEquals(null, Product.Id, 'Could not find product created by testing tool');		
			
			// Create CBC item 
			Consignment_Business_Case_Item__c oneCBCItem = new Consignment_Business_Case_Item__c(
																	Line_Number__c = 1,
																	Actual_Quantity__c = 3,
																	Agreed_Quantity__c = 3,
																	OriginalQuantity__c = 5,
																	Consignment_Business_Case__c = theTestcbc.Id,
																	Product__c = product.Id);
			Database.insert(oneCBCItem);
			System.assertNotEquals(null, oneCBCItem.Id, 'An error occurred creating a CBC item');
			
			
			 // Test constructor
			 QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(theTestQuote.Id,'Kit', theTestQuote.QuoteType__c);
			 system.assertEquals(theTestQuote.Id,linePopulator.quoteId,'Quote id has not been set up in constructor correctly');
			 //system.assertEquals(theTestcbc.Id,linePopulator.CBCaseOrSurgeonPrefId,'CBC id has not been set up in constructor correctly');
			 system.assertEquals('Kit',linePopulator.groupType,'Group type has not been set up in constructor correctly');
			 system.assertEquals(theTestQuote.QuoteType__c,linePopulator.quoteType.name(),'Quote transaction type has not been set up in constructor correctly');
	    	 
	    	 
	    	 // Test runcbc
	    	 linePopulator.runCBC();
	    	 System.assertEquals(theTestQuote.Id, linePopulator.quote.Id, 'The populator should have retrieved the quote created by testing tools');
	    	 List<Quote_Line_Group__c> theGroups = [SELECT Id, Name FROM Quote_Line_Group__c WHERE QuoteId__c = :theTestQuote.Id AND Name = 'Miscellaneous'];
	    	 
	    	 System.assert(theGroups.isEmpty() == false, 'A miscellaneous group should have been created');
	    	 System.assertEquals(1, theGroups.size(), 'Only one miscellaneous group should have been returned');
	    	 System.assertNotEquals(null, theGroups[0].Id, 'A miscellaneous group should have been created');
	    	 
	    	 QuoteLineItem item = [SELECT q.Id, q.Product2Id, q.Quote_Line_Group__c  FROM QuoteLineItem q WHERE q.Quote_Line_Group__c = :theGroups[0].Id  AND q.Product2Id = :product.Id AND q.QuoteId = :theTestQuote.Id];
	    	 System.assertNotEquals(null, item.Id, 'A QuoteLineItem should have been created from the cbcitem');
		
		
	 		// Now instantiate a new linepopulator with an invalid quote id and try to run the populator. This should not create the group
	 		QuoteGroupLineItemsPopulator linePopulator2 = new QuoteGroupLineItemsPopulator(null,'Kit', theTestQuote.QuoteType__c);
	 		linePopulator2.runCBC();
	        System.assertEquals(null, linePopulator2.quote, 'Expected line populator to return null since quote id was invalid');
	        System.assertEquals(null, linePopulator2.quoteLineGroup, 'Expected line populator to return null since quote id was invalid');
        
		}
 	} 
 	
 	
 	
 	@IsTest
 	public static void testSPPopulator() {
 		
 		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getCBCandSPTestQuote(GlobalConstants.QuoteTypes.SurgeonPreference);
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid SP testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
		
			String organizationCode = 'UT-AU10';
        	String distributionChannel = '01';
        	String division = '01'; 	
		
			// Get the quote created in the testing tools
			Quote theTestQuote = [SELECT Id, Surgeon_Preference__c, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Get the consignment business case created in the testing tools
			Surgeon_Preference__c theTestSP = [SELECT Id, Name, Surgeon__c
														From Surgeon_Preference__c 
														Where Id = :theTestQuote.Surgeon_Preference__c];
			System.assertNotEquals(null, theTestSP.Id, 'Could not find Surgeon Preference created by testing tool');
			
			// Get Product created in testing tools
			Product2 product = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = '99.161.001-CBC-SP'];
	        System.assertNotEquals(null, Product.Id, 'Could not find product created by testing tool');		
			
			// Create CBC item 
			Surgeon_Preference_Item__c oneSPItem = new Surgeon_Preference_Item__c(
																	Line_Number__c = 1,
																	Quantity__c = 3,
																	Surgeon_Preference__c = theTestSP.Id,
																	Product__c = product.Id);
			Database.insert(oneSPItem);
			System.assertNotEquals(null, oneSPItem.Id, 'An error occurred creating a SP item');
			
			
			 // Test constructor
			 QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(theTestQuote.Id, 'Kit', theTestQuote.QuoteType__c);
			 system.assertEquals(theTestQuote.Id,linePopulator.quoteId,'Quote id has not been set up in constructor correctly');
			 //system.assertEquals(theTestSP.Id,linePopulator.CBCaseOrSurgeonPrefId,'CBC id has not been set up in constructor correctly');
			 system.assertEquals('Kit',linePopulator.groupType,'Group type has not been set up in constructor correctly');
			 system.assertEquals(theTestQuote.QuoteType__c,linePopulator.quoteType.name(),'Quote transaction type has not been set up in constructor correctly');
	    	 
	    	 
	    	 // Test runsp
	    	 linePopulator.runSurgeonPref();
	    	 System.assertEquals(theTestQuote.Id, linePopulator.quote.Id, 'The populator should have retrieved the quote created by testing tools');
	    	 List<Quote_Line_Group__c> theGroups = [SELECT Id, Name FROM Quote_Line_Group__c WHERE QuoteId__c = :theTestQuote.Id AND Name = 'Miscellaneous'];
	    	 System.assert(theGroups.isEmpty() == false, 'A miscellaneous group should have been created');
	    	 System.assertEquals(1, theGroups.size(), 'Only one miscellaneous group should have been returned');
	    	 System.assertNotEquals(null, theGroups[0].Id, 'A miscellaneous group should have been created');
	    	 
	    	 QuoteLineItem item = [SELECT q.Id, q.Product2Id, q.Quote_Line_Group__c  FROM QuoteLineItem q WHERE q.Quote_Line_Group__c = :theGroups[0].Id  AND q.Product2Id = :product.Id AND q.QuoteId = :theTestQuote.Id];
	    	 System.assertNotEquals(null, item.Id, 'A QuoteLineItem should have been created from the cbcitem');
			
			// Now instantiate a new linepopulator with an invalid quote id and try to run the populator. This should not create the group
	 		QuoteGroupLineItemsPopulator linePopulator2 = new QuoteGroupLineItemsPopulator(null,'Kit', theTestQuote.QuoteType__c);
	 		linePopulator2.runSurgeonPref();
	        System.assertEquals(null, linePopulator2.quote, 'Expected line populator to return null since quote id was invalid');
	        System.assertEquals(null, linePopulator2.quoteLineGroup, 'Expected line populator to return null since quote id was invalid');
        
		}
 	} 
 	
 	@IsTest
 	public static void testAddBOM2BOM() {
 		
 		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
		
			String organizationCode = 'UT-AU10';
        	String distributionChannel = '01';
        	String division = '01'; 	
		
			// Get the quote created in testing tools
			Quote theTestQuote = [SELECT Id, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Quote Type on quote should be populated');
			
			// Get the quote group created by testing tool
			Quote_Line_Group__c theTestGroup = [SELECT Id, QuoteId__c, SetClassId__c FROM Quote_Line_Group__c WHERE QuoteId__c = :theTestQuote.Id];
			System.assertNotEquals(null, theTestGroup.Id, 'Could not find quote group created by testing tool');
			
			// Get Product created in testing tools
			Product2 product = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = '03.161.001'];
	        System.assertNotEquals(null, Product.Id, 'Could not find product created by testing tool');		
			
			
			// Add the BOM
			QuoteGroupLineItemsPopulator linePopulator1 = new QuoteGroupLineItemsPopulator(theTestQuote.Id, theTestGroup.SetClassId__c);
			 String newGroup = linePopulator1.run();
			 System.assertNotEquals(null, newGroup, 'A new group should have been created');
			 Id newGroupAsId = newGroup;
			 // Test constructor
			 QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(theTestQuote.Id, theTestGroup.SetClassId__c, newGroupAsId);
			 system.assertEquals(theTestQuote.Id,linePopulator.quoteId,'Quote id has not been set up in constructor correctly');
			 system.assertEquals(theTestGroup.SetClassId__c,linePopulator.referenceBOMId,'Set class id has not been set up in constructor correctly');
			 system.assertEquals(newGroupAsId,linePopulator.groupId,'Group id has not been set up in constructor correctly');
			 
			 
	    	 // Test runAddBOM2BOM
	    	 Boolean isBOMSuccesfullyAdded = linePopulator.runAddBOM2BOM();
	    	 System.assertEquals(true, isBOMSuccesfullyAdded, 'The BOM was not successfully added to the existing BOM');
	    	 List<QuoteLineItem> theItems = [SELECT Id, Quantity, Proposed_Quantity__c, Sequence_Number__c, Quote_Line_Group__c
	    	 								 FROM QuoteLineItem 
	    	 								 WHERE Quote.Id = :theTestQuote.Id AND Quote_Line_Group__c = :newGroupAsId];
	    	 
	    	 System.assert(theItems.isEmpty() == false, 'An error occurred finding the group items');
	    	 system.debug('>>>>>>>> the items in the bom ' + theItems);
	    	 System.assertEquals(1, theItems.size(), 'Only one item should have been returned');
	    	 System.assertEquals(4, theItems[0].Quantity, 'The item quatity should have been doubled when the item was added in the BOM2BOM');
	    	 
		}
 	} 
 	
 	 @IsTest
 	public static void testChangeSetPopulator() {
 		
 		User runAsUser = null;
		Id sourceQuoteId = null;
		
		// Create test data
		Map<Id,User> testQuote = TestingTools.getChangeSetTestQuote();
		System.assert(testQuote != null && testQuote.size() > 0, 'Was expecting a valid SetRequest(ActualBOM) testQuote object, not a null.');
		for(Id quoteId :testQuote.keySet()){
			sourceQuoteId = quoteId;
			runAsUser = (User)testQuote.get(quoteId);
			break;
		}
		
		System.runAs(runAsUser) {
		
			String organizationCode = 'UT-AU10';
        	String distributionChannel = '01';
        	String division = '01'; 	
		
			// Get the quote created in the testing tools
			Quote theTestQuote = [SELECT Id, ActualBillOfMaterial__c, QuoteType__c From Quote Where Id = :sourceQuoteId];
			System.assertNotEquals(null, theTestQuote.Id, 'Could not find quote created by testing tool');
			System.assertEquals(sourceQuoteId, theTestQuote.Id, 'The quote created in testing tools and the one found here are not the same but should be');
			System.assertNotEquals(null, theTestQuote.QuoteType__c, 'Transaction Type on quote should be populated');
			
			// Get the consignment business case created in the testing tools
			ActualBillOfMaterial__c actaulBOM = [SELECT Id, Name
														From ActualBillOfMaterial__c 
														Where Id = :theTestQuote.ActualBillOfMaterial__c];
			System.assertNotEquals(null, actaulBOM.Id, 'Could not find quote created by testing tool');
			
			// Get Product created in testing tools
			Product2 product = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = '99.161.001-CS'];
	        System.assertNotEquals(null, Product.Id, 'Could not find product created by testing tool');		
			
			// Create Actual BOM item 
			ActualBillOfMaterialItem__c oneActualBOMItem = new ActualBillOfMaterialItem__c(
																	Line_Number__c = 1,
																	Quantity__c = 3,
																	SerialNumber__c = 'SN121212',
																	UnitOfMeasure__c = 'each',
																	Actual_BillOfMaterial__c = actaulBOM.Id,
																	Product__c = product.Id);
			Database.insert(oneActualBOMItem);
			System.assertNotEquals(null, oneActualBOMItem.Id, 'An error occurred creating a ActualBOM item');
			
			
			 // Test constructor
			 QuoteGroupLineItemsPopulator linePopulator = new QuoteGroupLineItemsPopulator(theTestQuote.Id,'Kit', theTestQuote.QuoteType__c);
			 system.assertEquals(theTestQuote.Id,linePopulator.quoteId,'Quote id has not been set up in constructor correctly');
			 //system.assertEquals(theTestcbc.Id,linePopulator.CBCaseOrSurgeonPrefId,'CBC id has not been set up in constructor correctly');
			 system.assertEquals('Kit',linePopulator.groupType,'Group type has not been set up in constructor correctly');
			 system.assertEquals(theTestQuote.QuoteType__c,linePopulator.quoteType.name(),'Quote transaction type has not been set up in constructor correctly');
	    	 
	    	 
	    	 // Test runSetRequest
	    	 linePopulator.runChangeSet();
	    	 System.assertEquals(theTestQuote.Id, linePopulator.quote.Id, 'The populator should have retrieved the quote created by testing tools');
	    	 List<Quote_Line_Group__c> theGroups = [SELECT Id, Name FROM Quote_Line_Group__c WHERE QuoteId__c = :theTestQuote.Id];
	    	 
	    	 System.assert(theGroups.isEmpty() == false, 'A miscellaneous group should have been created');
	    	 System.assertEquals(1, theGroups.size(), 'Only one miscellaneous group should have been returned');
	    	 System.assertNotEquals(null, theGroups[0].Id, 'A miscellaneous group should have been created');
	    	 
	    	 QuoteLineItem item = [SELECT q.Id, q.Product2Id, q.Quote_Line_Group__c  FROM QuoteLineItem q WHERE q.Quote_Line_Group__c = :theGroups[0].Id  AND q.Product2Id = :product.Id AND q.QuoteId = :theTestQuote.Id];
	    	 System.assertNotEquals(null, item.Id, 'A QuoteLineItem should have been created from the cbcitem');
		
		}
 	} 
 	
}