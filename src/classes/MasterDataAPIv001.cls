global class MasterDataAPIv001 {

	// cannot use inheritance for data objects in API
	// https://success.salesforce.com/ideaView?id=08730000000bYanAAE
	// so make the status a field of every data object
	
	/*
		ValidationError - Errors caused by business validation rules for the user / support team.
		ApplicationError - Errors caused by a method after passing through the Validation phase - kind of unexpected error for the user / support team.
		SystemError - Error caused by an unexpected Exception for the user / support team.
		Warning - A warning message for the user (not an error) / support team
		Information - An information message for the user / support team.
		Other - other type of messages
		None - no message.
	*/
	global enum SeverityType {
		ValidationError,
		ApplicationError,
		SystemError,
		Warning,
		Information,
		Other,
		None
	}
	
	global enum ActionModeValue {
		InsertMode,
		UpdateMode,
		DeleteMode
	}
	
	global class Log {
		webservice String code {get;set;}
		webservice String message {get;set;}
		webservice SeverityType severity {get;set;}
		webservice Id sourceRecordId {get;set;}
		webservice String sourceRecordNumber {get;set;}
	}
		
	global class ProcessLog {
		webservice String title {get;set;}
		webservice String recordId {get;set;} //(PK) of the actual record being processed.
		webservice String sourceRecordNumber {get;set;}
		webservice Integer errorCount {get;set;}
		webservice List<Log> logs {get;set;}
	}
	
	global class ValidationResult {
		global ValidationResult() {
			isValid = true;
			processLogs = null;
		}
		
		webservice boolean isValid {get;set;}
		webservice List<ProcessLog> processLogs {get;set;}
	}
	
	global class ReturnResult {
		global ReturnResult(){
			isSuccessful = true;
			processLogs = null;
		}
		
		webservice boolean isSuccessful {get;set;}
		webservice List<ProcessLog> processLogs {get;set;}
	}
	
	global class OrganizationContext{
		webservice String organizationCode {get;set;}
		webservice String distributionChannel {get; set;}
		webservice String division	{get; set;}
	}
	
	global class ProductHierarchy {
		webservice String code {get;set;}	
		webservice String description {get;set;}
		webservice Integer level {get;set;}
		webservice ProductHierarchy parentPH {get;set;}
	}

	global class UOM {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class ProductType {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class ProductGroup {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global enum TrackingOption {
		Lot,
		Serial,
		None
	}
	
	global class Rebate {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class ProductRegistrationReference {
		webservice String registrationNumber {get;set;}
		webservice Date registrationDate {get;set;}
		webservice String description {get;set;}
	}
	
	
	global class Product {
		webservice ValidationResult validationResult {get;set;}
		webservice OrganizationContext organizationContext {get;set;}
		
		webservice String productCode {get;set;}	
		webservice String productDescription {get;set;}
		webservice String productShortDescription {get;set;}
		webservice String productLongDescription {get;set;}
		webservice ProductType productType {get;set;}
		webservice TrackingOption trackingOption {get; set;}
		webservice UOM uom {get;set;}
		webservice Double standardCost {get;set;}
		webservice Rebate rebate  {get;set;}
		webservice Boolean isAvailableForSale {get;set;}
		webservice ProductRegistrationReference registrationReference {get;set;}
		webservice String eanCode {get;set;}
		webservice Double grossWeight {get;set;}
		webservice String sterile {get;set;}
		webservice ProductGroup productGroup {get;set;}
		webservice ProductHierarchy localProductHierarchy {get;set;}
		webservice String globalProductHierarchyCode {get;set;}
		webservice ProductHierarchy globalProductHierarchy {get;set;}
		webservice String country {get;set;}
		webservice Boolean isDeleted {get;set;}
		webservice String materialGroup1 {get;set;}
		webservice String materialGroup2 {get;set;}
		webservice String materialGroup3 {get;set;}
		webservice String materialGroup4 {get;set;}
		webservice String materialGroup5 {get;set;}
		webservice String genItemCategoryGroup {get;set;}
		webservice String myMedisetItemCategory {get;set;}
		webservice String materialStatusCode {get;set;}
		webservice String productReferenceMaterial  {get;set;} //RebateCode
		webservice String materialStatusCodeDescription   {get;set;}
	}

	/*
		Object Model for Consignment Business Case
	*/
	global class ConsignmentBusinessCase{
		webservice ValidationResult validationResult {get;set;}
		webservice OrganizationContext organizationContext {get;set;}
		
		webservice String erpReferenceNumber {get;set;}
		webservice String customerNumber {get;set;}
		webservice String shipTo	{get;set;}
		webservice Date	agreementFromDate {get;set;}
		webservice Date agreementToDate {get;set;}
		webservice String description {get;set;}
		webservice String department  {get;set;}
		webservice Double totalValue {get;set;}
		webservice Date lastUpdatedDate {get;set;}
		webservice Boolean canOverride {get;set;}
		webservice String createdBy {get;set;}
		webservice Boolean isDeleted {get;set;}
		webservice List<ConsignmentBusinessCaseItem> Items {get;set;}
	}
	
	global class ConsignmentBusinessCaseItem{
		webservice ValidationResult validationResult {get;set;}

		webservice Integer sequenceNumber {get;set;}
		webservice String productCode {get; set;}
		webservice double originalQuantity {get; set;}
		webservice double currentBalance {get; set;}
		webservice double adjustmentQuantity {get; set;}
		webservice String adjustmentSign {get; set;}
		webservice double newBalance {get; set;}
		webservice String personnelNumber {get; set;}
		webservice Boolean canOverride {get;set;}
		webservice Date lastUpdatedDate {get;set;}
		webservice DateTime lastUpdatedTime {get;set;}
		webservice Boolean isDeleted {get;set;}
	}
	
	/*
		Object Model for Surgeon Preference
	*/
	global class SurgeonPreference{
		webservice ValidationResult validationResult {get;set;}
		webservice OrganizationContext organizationContext {get;set;}
		
		webservice String erpReferenceNumber {get;set;}
		webservice String customerNumber {get;set;}
		webservice PreferenceType preferenceType {get;set;}
		webservice String description {get;set;}
		webservice Date	validFromDate {get;set;}
		webservice Date validToDate {get;set;}
		webservice List<SurgeonPreferenceItem> Items {get;set;}
	}
	
	global class PreferenceType {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class SurgeonPreferenceItem{
		webservice ValidationResult validationResult {get;set;}

		webservice Integer sequenceNumber {get;set;}
		webservice String productCode {get; set;}
		webservice double quantity {get; set;}
		webservice UOM uom {get; set;}
		webservice String description {get; set;}
		webservice String bomIndicator {get; set;}
		webservice Boolean isDeleted {get;set;}
	}

/*
		Object Model for Surgeon Preference
	*/
	global class ReferenceBillOfMaterial{
		webservice ValidationResult validationResult {get;set;}
		webservice OrganizationContext organizationContext {get;set;}

		webservice String erpReferenceNumber {get;set;}
		webservice String description {get;set;}
		webservice BillOfMaterialType billOfMaterialType {get;set;}
		webservice String bomReferenceProductCode {get;set;}
		webservice String plant {get;set;}
		webservice Double cost {get;set;}
		webservice String equipmentCategory {get;set;}
		webservice String serialNumber {get;set;}
		webservice String	validFromDate {get;set;}
		webservice String validToDate {get;set;}
		webservice String systemStatus {get;set;}
		webservice String systemStatus4 {get;set;}
		webservice String systemStatus30Desc {get;set;}
		webservice String stockType {get;set;}
		webservice String storageLocation {get;set;}
		webservice String stockBatch {get;set;}
		webservice String specialStockIndicator {get;set;}
		webservice String customerNumber {get;set;}
		webservice String salesOrderNumber {get;set;}
		webservice List<ReferenceBillOfMaterialItem> Items {get;set;}
	}
	
	global class BillOfMaterialType {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class Tray {
		webservice Integer trayNumber {get;set;}
		webservice String description {get;set;}
		webservice Double trayQuantity {get;set;}
		webservice Integer position {get;set;}
	}
	
	global class ReferenceBillOfMaterialItem{
		webservice ValidationResult validationResult {get;set;}

		webservice Integer sequenceNumber {get;set;}
		webservice String productCode {get; set;}
		webservice double quantity {get; set;}
		webservice double minimumQuantity {get; set;}
		webservice String uom {get;set;}
		webservice String batchNumber {get;set;}
		webservice String serialNumber {get;set;}
		webservice Tray tray {get; set;}
		webservice Boolean isDeleted {get;set;}
	}
	
	
	/*
		Object Model for Customer - Account
	*/
	global class Account{
		webservice ValidationResult validationResult {get;set;}
		webservice OrganizationContext organizationContext {get;set;}

		webservice String accountNumber {get;set;}
		webservice String accountName {get;set;}
		webservice String accountName2 {get;set;}
		webservice String title {get;set;}
		webservice AccountGroup accountGroup {get;set;}
		webservice AccountClass accountClass {get;set;}
		webservice String customerGroup {get;set;}
		webservice Address accountAddress {get;set;}
		webservice String abcClass {get;set;}
		webservice CurrencyDetail currencyDetail {get;set;}
		webservice TermsOfPayment termsOfPayment {get;set;}
		webservice List<Address> PartnerFunctionAddresses {get;set;}
	}
	
	global class AccountGroup {
		global AccountGroup(){
			code = '';
			description = null;
		}
		global AccountGroup(string code, string description){
			this.code = code;
			this.description = description;
		}
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class AccountClass {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class ContactType {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class TermsOfPayment {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class CurrencyDetail {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	global class AddressType {
		webservice String code {get;set;}
		webservice String description {get;set;}
	}
	
	
	global class Address {
		webservice AddressType addressType {get;set;}
		webservice String addressCode {get;set;}
		webservice String addressName {get;set;}
		webservice String street1 {get;set;}
		webservice String street2 {get;set;}
		webservice String street3 {get;set;}
		webservice String city {get;set;}
		webservice String stateOrProvince {get;set;}
		webservice String zipOrPostalCode {get;set;}
		webservice String country {get;set;}
		webservice Double longitue {get;set;}
		webservice Double latitude {get;set;}
		webservice String email {get;set;}
		webservice String telePhone1 {get;set;}
		webservice String telePhone2 {get;set;}
		webservice String mobile1 {get;set;}
		webservice String mobile2 {get;set;}
		webservice String addressContact {get;set;}
		webservice Boolean isDeleted {get;set;}
		webservice String defaultPartner {get;set;}
	}
	
	/*
		SFDC team to impliment and host.
		WebMethods will call the following SFDC webservice method to Insert/Update Product Master details from RDW. 
	*/
	webservice static ReturnResult upsertProductMasterRecords(List<Product> products) {
		ProductMasterDataTools.upsertProductMasterRecordsAndSaveProcessLogV001(products);
		
		ReturnResult rr = new ReturnResult();
		rr.isSuccessful= true; //returning true for wM.
		return rr;
	}
	
	/*
		SFDC team to impliment and host.
		WebMethods will call the following SFDC webservice method to Insert/Update Account (Customer) and PartnerFunction Master details from RDW. 
	*/
	webservice static ReturnResult upsertCustomerMasterRecords(List<Account> accounts)  {
		AccountMasterDataTools.upsertAccountMasterRecordsAndSaveProcessLogV001(accounts);
		
		ReturnResult rr = new ReturnResult();
		rr.isSuccessful= true; //returning true for wM.
		return rr;
	}
	
	/*
		SFDC team to impliment and host.
		WebMethods will call the following SFDC webservice method to Insert/Update CBC Master details from RDW. 
	*/
	webservice static ReturnResult upsertConsignmentBusinessCaseMasterRecords(List<ConsignmentBusinessCase> consignmentBusinessCases)  {
		ConsignmentBusinessCaseMasterDataTools.upsertConsignmentBusinessCaseMasterRecordsAndSaveProcessLogV001(consignmentBusinessCases);

		ReturnResult rr = new ReturnResult();
		rr.isSuccessful= true; //returning true for wM.
		return rr;
	}
	
	/*
		SFDC team to impliment and host.
		WebMethods will call the following SFDC webservice method to Insert/Update SP Master details from RDW. 
	*/
	webservice static ReturnResult upsertSurgeonPreferenceMasterRecords(List<SurgeonPreference> surgeonPreferences)  {
		SurgeonPreferenceMasterDataTools.upsertSurgeonPreferenceMasterRecordsAndSaveProcessLogV001(surgeonPreferences);

		ReturnResult rr = new ReturnResult();
		rr.isSuccessful= true; //returning true for wM.
		return rr;
	}
	
	/*
		SFDC team to impliment and host.
		WebMethods will call the following SFDC webservice method to Insert/Update ReferenceBillOfMaterial Master details from RDW. 
	*/
	webservice static ReturnResult upsertReferenceBillOfMaterialMasterRecords(List<ReferenceBillOfMaterial> referenceBillOfMaterials)   {
		ReferenceBillOfMaterialMasterDataTools.upsertReferenceBillOfMaterialRecordsAndSaveProcessLogV001(referenceBillOfMaterials);
		
		ReturnResult rr = new ReturnResult();
		rr.isSuccessful= true; //returning true for wM.
		return rr;
	}
	
}