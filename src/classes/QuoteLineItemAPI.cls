public class QuoteLineItemAPI {

	public class QuoteLineItemDetail  {
		public String companyCode {get;set;}
		public Id quoteId	{get;set;}
		public String quoteNumber	{get;set;}
		public String customerNumber	{get;set;}
		public String productCode	{get;set;}
		public double quantity	{get;set;}
		
	}
	
	
	public class QuoteLineItemPriceDetail  {
		public double unitPrice	{get;set;}
		public double unitListPrice	{get;set;}
		public double unitStandardCost	{get;set;}
		public double unitLandedCost	{get;set;}
		public double taxRate	{get;set;}
		public String taxScheduleId	{get;set;}
	}
	
	
	//web service operation API for retrieving QuoteLineItemsDetail for ERP or other interface.
	private static List<QuoteLineItemDetail> getQuoteLineItemsDetail(List<Id> quoteLineIds){
		
		
		return null;
	}
	
	
	//web service operation API for Updating QuoteLineItem Price details from ERP.
	private static List<QuoteLineItemPriceDetail> updateQuoteLineItemsPriceDetail(List<QuoteLineItemPriceDetail> quoteLines){
		//This webservice accept collection of QuoteLineItemPriceDetail objects and
		
		return null;
	}
	

}