@isTest(SeeAllData=true)
private class QuoteUserPreferenceControllerTest {

    static testMethod void saveSelectedHeaderTooltipFields() {
        //Populate BU List Test Data
        Integer buListCount = [select count() from Default_BU_List__c];
		List<Default_BU_List__c> defBUoptions = new List<Default_BU_List__c>();
		defBUoptions.add( new Default_BU_List__c(Name='XXXX'));
		defBUoptions.add( new Default_BU_List__c(Name='YYYY'));
		insert defBUoptions;
        PageReference pageRef = Page.quoteUserPreference;
    	Test.setCurrentPage(pageRef);
    	    	
    	QuoteUserPreferenceController quoteController = new QuoteUserPreferenceController();
    	Component.Apex.SelectList selList = quoteController.getAllDefaultBUSelectOptions();
		System.assertEquals(2,selList.ChildComponents.size()-buListCount,'Expected to populate 2 BU Options SelectOptions as child component.');
		quoteController.selectedDefaultBUSelectOptions = defBUoptions.get(0).Name;
    	double headerColumnsCount = quoteController.allHeaderTooltipFieldsExcludingSelectedFields.size();
    	
    	SelectOption selectOneItem =  quoteController.allHeaderTooltipFieldsExcludingSelectedFields[0];
    	quoteController.selectedHeaderTooltipFields.add(selectOneItem);
    	
    	quoteController.allHeaderTooltipFieldsExcludingSelectedFields.remove(0);
    	
    	PageReference pr = quoteController.saveAllAndClose();
    	system.debug('headerColumnsCount>>> :' + headerColumnsCount);
    	system.debug('allHeaderTooltipFieldsExcludingSelectedFields size>>> :' + quoteController.allHeaderTooltipFieldsExcludingSelectedFields.size());
    	
    	System.assert(quoteController.allHeaderTooltipFieldsExcludingSelectedFields.size() == headerColumnsCount - 1, 'adding an item from header tooltip failed.');
    	
    	system.debug(quoteController.allHeaderTooltipFieldsExcludingSelectedFields);
    	system.debug(quoteController.selectedHeaderTooltipFields);
    	quoteController = new QuoteUserPreferenceController();
    	System.assertEquals(defBUoptions.get(0).Name,quoteController.selectedDefaultBUSelectOptions,'Expected BU Preference value to be same after save.');
    	
    }
    
    
    
}