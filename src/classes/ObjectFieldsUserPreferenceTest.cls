@isTest(seeAllData=true)
private class ObjectFieldsUserPreferenceTest {

	static testMethod void upsertUserPreference_insert()
    {
    	
    	ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					new List<SelectOption>{new SelectOption('TotalCost__c', 'TotalCost'),
    					new SelectOption('TotalTax__c', 'TotalTax')});
    	
    	boolean isInserted = objectFieldsUP.upsertUserPreference();
    	System.assert(isInserted,'An error occured while executing upsertUserPreference method.');
    	
    }
    
    static testMethod void upsertUserPreference_update()
    {
    	
    	ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					new List<SelectOption>{new SelectOption('TotalCost__c', 'TotalCost'),
    					new SelectOption('TotalTax__c', 'TotalTax')});
    	
    	boolean isInserted = objectFieldsUP.upsertUserPreference();
    	System.assert(isInserted,'An error occured while executing upsertUserPreference method - insert.');
    	Set<SelectOption> upDetails_Inserted = objectFieldsUP.retrieveUserPreferenceDetailsAsSetSelectOption();
    	System.assert(upDetails_Inserted != null && upDetails_Inserted.size() == 2,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - insert.');
    	
    	//Update
    	ObjectFieldsUserPreference objectFieldsUP_Update = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    						new List<SelectOption>{new SelectOption('TotalCost__c', 'Total Cost'),
    												new SelectOption('TotalTax__c', 'Total Tax'),
    												new SelectOption('TotalDiscount__c', 'Total Discount')});
    	
    	boolean isUpdated = objectFieldsUP_Update.upsertUserPreference();
    	System.assert(isUpdated,'An error occured while executing upsertUserPreference method - update.');
    	Set<SelectOption> upDetails_Updated = objectFieldsUP_Update.retrieveUserPreferenceDetailsAsSetSelectOption();
    	System.assert(upDetails_Updated != null && upDetails_Updated.size() == 3,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - update.');
    }
    

	static testMethod void retrieveUserPreferenceDetailsAsSetSelectOption()
    {
    	
    	ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					new List<SelectOption>{new SelectOption('TotalCost__c', 'TotalCost'),
    					new SelectOption('TotalTax__c', 'TotalTax')});
    	
    	boolean isInserted = objectFieldsUP.upsertUserPreference();
    	System.assert(isInserted,'An error occured while executing upsertUserPreference method - insert.');
    	Set<SelectOption> upDetails_Inserted = objectFieldsUP.retrieveUserPreferenceDetailsAsSetSelectOption();
    	System.assert(upDetails_Inserted != null && upDetails_Inserted.size() == 2,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - insert.');
    	
    	//Initilaize object to retrieve
    	ObjectFieldsUserPreference objectFieldsUP_Retrieve = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip);
    	
    	Set<SelectOption> upDetails = objectFieldsUP_Retrieve.retrieveUserPreferenceDetailsAsSetSelectOption();
    	System.assert(upDetails != null && upDetails.size() == 2,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - Retrieve.');
    	System.debug(upDetails);
    }

	static testMethod void retrieveUserPreferenceDetailsAsListSelectOption()
    {
    	
    	ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					new List<SelectOption>{new SelectOption('TotalCost__c', 'TotalCost'),
    					new SelectOption('TotalTax__c', 'TotalTax')});
    	
    	boolean isInserted = objectFieldsUP.upsertUserPreference();
    	System.assert(isInserted,'An error occured while executing upsertUserPreference method - insert.');
    	Set<SelectOption> upDetails_Inserted = objectFieldsUP.retrieveUserPreferenceDetailsAsSetSelectOption();
    	System.assert(upDetails_Inserted != null && upDetails_Inserted.size() == 2,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - insert.');
    	
    	//Initilaize object to retrieve
    	ObjectFieldsUserPreference objectFieldsUP_Retrieve = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip);
    	
    	List<SelectOption> upDetails = objectFieldsUP_Retrieve.retrieveUserPreferenceDetailsAsListSelectOption();
    	System.assert(upDetails != null && upDetails.size() == 2,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - Retrieve.');
    	System.debug(upDetails);
    }
    
    
    static testMethod void retrieveUserPreferenceDetailsAsListOfMap()
    {
    	
    	ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					new List<SelectOption>{new SelectOption('TotalCost__c', 'TotalCost'),
    					new SelectOption('TotalTax__c', 'TotalTax')});
    	
    	boolean isInserted = objectFieldsUP.upsertUserPreference();
    	System.assert(isInserted,'An error occured while executing upsertUserPreference method - insert.');
    	Set<SelectOption> upDetails_Inserted = objectFieldsUP.retrieveUserPreferenceDetailsAsSetSelectOption();
    	System.assert(upDetails_Inserted != null && upDetails_Inserted.size() == 2,'An error occured while executing retrieveUserPreferenceDetailsAsSetSelectOption method - insert.');
    	
    	//Initilaize object to retrieve
    	ObjectFieldsUserPreference objectFieldsUP_Retrieve = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip);
    	
    	List<Map<String,String>> upDetails = objectFieldsUP_Retrieve.retrieveUserPreferenceDetailsAsListOfMap();
    	System.assert(upDetails != null && upDetails.size() == 2,'An error occured while executing List<Map<String,String>> method - Retrieve.');
    	System.debug('List of Map Result>>>>>: ' + upDetails);
    }
    
    static testMethod void temp()
    {
    	ObjectFieldsUserPreference objectFieldsUP_Retrieve = new ObjectFieldsUserPreference('Quote',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Header1Tooltip);
    	
    	List<Map<String,String>> upDetails = objectFieldsUP_Retrieve.retrieveUserPreferenceDetailsAsListOfMap();
    	System.debug('Thomas>>>>>: ' + upDetails);
    }

    static testMethod void updateObjectFieldsUserPreference_Trigger_Insert() {
     
     	Object_Fields_User_Preference__c newUP = new ObjectFieldsUserPreferenceBuilder()
				     	.setUser(UserInfo.getUserId())
				     	.setObjectName('Quote')
				     	.setObjectSection(ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip)
				     	.build(true);
     	System.assert(newUP.Id <> null,'An error occured while creating ObjectField UserPreference record.');
     	
        Object_Fields_User_Preference__c objectUP = [Select o.User__c, o.Object_Section__c, o.Object_Name__c, o.Name, o.Id 
        													From Object_Fields_User_Preference__c o   												
        													Where o.Id = : newUP.Id LIMIT 1];
        												
        												
        System.assert(objectUP <> null,'An error occured while retrieving ObjectField UserPreference record.');
        
        System.assert(objectUP.Name <> null,'Trigger failed to Update Name field.');
        
        System.debug('<<<<User Preference>>>>>>: ' + objectUP);
    }
    
    static testMethod void updateObjectFieldsUserPreference_Trigger_Update() {
     
     	Object_Fields_User_Preference__c newUP = new ObjectFieldsUserPreferenceBuilder()
				     	.setUser(UserInfo.getUserId())
				     	.setObjectName('Quote')
				     	.setObjectSection(ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip)
				     	.build(true);
     	System.assert(newUP.Id <> null,'An error occured while creating ObjectField UserPreference record.');
     	
        Object_Fields_User_Preference__c objectUP = [Select o.User__c, o.Object_Section__c, o.Object_Name__c, o.Name, o.Id 
        													From Object_Fields_User_Preference__c o   												
        													Where o.Id = : newUP.Id LIMIT 1];
        												
        												
        System.assert(objectUP <> null,'An error occured while retrieving ObjectField UserPreference record.');
        
        System.assert(objectUP.Name <> null,'Trigger failed to Update Name field.');
        
        System.debug('<<<<User Preference>>>>>>: ' + objectUP);
        
        //Update
        Object_Fields_User_Preference__c updateUP = new Object_Fields_User_Preference__c();
        updateUP.Id = objectUP.Id;
        updateUP.Object_Section__c = ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Header.name();
        update updateUP;
        
        
        Object_Fields_User_Preference__c updatedUP = [Select o.User__c, o.Object_Section__c, o.Object_Name__c, o.Name, o.Id 
        													From Object_Fields_User_Preference__c o   												
        													Where o.Id = : newUP.Id LIMIT 1];
        
        System.assert(updatedUP <> null,'An error occured while retrieving ObjectField UserPreference record.');
        
        System.assert(updatedUP.Name <> null,'Trigger failed to Update Name field.');
        System.assert(updatedUP.Name <> objectUP.Name,'Trigger failed to Update Name field.');
        
        System.debug('<<<<Updated User Preference>>>>>>: ' + updatedUP);
        
        
    }
    
      
    
    static testMethod void upsertUserPreference_addMessage()
    {
    	
    	try
    	{
    		ObjectFieldsUserPreference objectFieldsUP = new ObjectFieldsUserPreference('',
    					ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections.Group1Tooltip,
    					new List<SelectOption>{new SelectOption('TotalCost__c', 'TotalCost'),
    					new SelectOption('TotalTax__c', 'TotalTax')});
    					
    		System.assert(objectFieldsUP <> null,'Was expecting null.');
    	}
    	catch(Exception e)
    	{
    		System.debug('ApexPages.addMessage : ' + e.getMessage());
    	
    	}
    	
    
    }
    
}