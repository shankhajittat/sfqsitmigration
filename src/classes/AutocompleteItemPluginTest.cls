@IsTest(seealldata=true)
public with sharing class AutocompleteItemPluginTest {

	
	@IsTest
	//This first test checks a quote that has sets on it already
	public static void test2AutocopleteItemPlugin(){
		
		String organizationCode = 'AU10';
        String distributionChannel = '01';
        String division = '01'; 
        
        Account vinnies = new AccountBuilder().name('St Vinceant').setErpAccountId('STV1010')
			.organizationCode(organizationCode)
			.distributionChannel(distributionChannel)
			.division(division)
	        .build(true);
        System.assert(vinnies.Id <> null,'An error occured while creating Account record.');
        
        Pricebook2 standardPriceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE IsStandard=true LIMIT 1];
        System.assert(standardPriceBook.Id <> null,'Standard Pricebook not found.');
        
 		System.debug('standardPriceBook>>>>:' + standardPriceBook);
	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel And Division__c =: division LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel + division)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.division(division)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel + division);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);
        
	    // Taken from QuotationControllerExtensionTest.cls to create a quote and add the first set class to it
       	SetClassParser parser = new SetClassParser(TestingTools.getResourceBodyAsString('SetClass_JH230'));
 		parser.run();
 		Set_Class__c setClass = parser.newSetClass;
 		  	
        PageReference pageRef = Page.Quotation;
    	pageRef.getParameters().put('accId', vinnies.Id);
        Test.setCurrentPage(pageRef);
        
    	ApexPages.StandardController stdController = new ApexPages.StandardController(new Quote());
    	QuotationControllerExtension ext = new QuotationControllerExtension(stdController);
    	
    	System.assert(ext.account.Id != null, 'constructor should load the account sobject');
    	System.assert(ext.account.Name != null, 'constructor should load the account name for display');
		System.assert(ext.getPricebook().Id != null, 'constructor should load the standard pricebook');
    	System.assertEquals(ext.getGroupsWithRollups().size(), 0, 'No groups in account mode');
    	
    	ext.setClassId = setClass.Id;
    	PageReference urlAfterFirstSetClass = ext.chooseSet();
    	System.assertNotEquals(null, ext.extensionQuote, 'a new quote is generated');
    	
    	//Add an item to the set class
    	Product2 newProduct = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL SMALL')
        		.code('T.03.161.001.T')
        		.build(true);
        System.assert(newProduct.Id <> null,'An error occured while creating Product.');
        
        //Create PricebookEntry for product
        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
        									.productId(newProduct.Id)
        									.pricebook(standardPriceBook).build(true);
        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
        
         //Create PricebookEntry for Custom Pricebook
        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
        									.productId(newProduct.Id)
        									.pricebook(customPricebook).build(true);
        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');
        
        
        double existngItemCountForGroup = 0;
        if (ext.groups.containsKey(setClass.Id)) {
        	ItemGroupWrapper grp = ext.groups.get(setClass.Id);
        	System.assertNotEquals(null, grp, 'Could not find Group from GroupViewState');
        	existngItemCountForGroup = grp.Items.size();
        }
        
        ext.addingGroupClassId = setClass.Id;
        ext.productId = newProduct.Id;
        PageReference addNewItem = ext.addItem();
        Id newQuotationLineId = ext.newQuotationLineId;
        
        double newItemCountForGroup = 0;
        if (ext.groups.containsKey(setClass.Id)) {
        	ItemGroupWrapper grp = ext.groups.get(setClass.Id);
        	System.assertNotEquals(null, grp, 'Could not find Group from GroupViewState');
        	newItemCountForGroup = grp.Items.size();
        }
        System.assertEquals(newItemCountForGroup, existngItemCountForGroup + 1,'New Item failed to add.');
        System.debug('>>>>><<<<<Item Count: Existing: ' + existngItemCountForGroup + ' New Count: ' + newItemCountForGroup);
        
        //Now that a quote exists with a set class and an item we can test the find method for autocompleteitemplugin
    	
    	AutoCompleteItemPlugin theTestItemPlugin2 = new AutoCompleteItemPlugin();
		
    	 //Set up context with Quote id
        Map<String,String> context = new Map<String,String>();
        context.put('id1',ext.setClassId);
		context.put('id2',ext.extensionQuote.Id);
		theTestItemPlugin2.init(context);
    	
    	// Test where item exists on quote - SOSL results do not get returned in TESTS - see web docs as to why the next three lines are here
    	Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = ext.productId;
        Test.setFixedSearchResults(fixedSearchResults);
      
		List<SObject> unfilteredSearchList = theTestItemPlugin2.find('BENDING');
		System.assert(unfilteredSearchList <> null, 'Search returned empty'); 
	    system.assert(unfilteredSearchList.size() > 0, 'Search should not return empty'); 
       
		boolean filter = theTestItemPlugin2.filter(newProduct);
		system.assertEquals(false, filter, 'Item should be filtered out because it exists on the set class and quote already');
		
		// Check that styling has been set up
		string cssStyling = theTestItemPlugin2.css();
		system.assert(cssStyling.length() > 0, 'cssStyling should not be empty');
		
		// Check that render returns a value
		string jsRender = theTestItemPlugin2.jsRender();
		system.assert(jsRender.length() > 0, 'jsRender should not be empty');
		
		//jsHandleSelect mimics the user selecting an account
		string jsHandleSelect = theTestItemPlugin2.jsHandleSelect(new String[] {null, null},
																new String[] {null, null});
		system.assert(jsHandleSelect.length() > 0, 'jsHandleSelect should not be empty');
	}
}