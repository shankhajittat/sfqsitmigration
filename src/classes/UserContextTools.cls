public with sharing class UserContextTools {

	public static APIv001.UserContext getCurrentUserContext(){
		APIv001.UserContext uc = new APIv001.UserContext();
		uc.wwid = '00000';
		uc.organisationCode = 'SYNAU';
		uc.distributionChannel = '01';
		uc.division = '01';
		uc.userADLoginId = 'ap/test';
		return uc;
	}
	
	public static APIv001.UserContext getUserContext(Id userId){
		APIv001.UserContext uc = new APIv001.UserContext();
		uc.wwid = '00000';
		uc.organisationCode = 'SYNAU';
		uc.distributionChannel = '01';
		uc.division = '01';
		uc.userADLoginId = 'ap/test';
		return uc;
	}
	
}