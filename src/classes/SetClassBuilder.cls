public with sharing class SetClassBuilder {

	private static Integer sequence = 100;

	//Mandatory fields with defaults
	private String setClassName;
	private String divisionCode;
	private String orgCode;
	private Boolean active = false;
	private String productCode;
	private String equipmentNumber;
	private String equipmentDescription;

	public SetClassBuilder name(String setClassName)
	{
		this.setClassName = setClassName;
		return this;
	}

	public SetClassBuilder equipmentNumber(String equipmentNumber)
	{
		this.equipmentNumber = equipmentNumber;
		return this;
	}
	
	public SetClassBuilder equipmentDescription(String equipmentDescription)
	{
		this.equipmentDescription = equipmentDescription;
		return this;
	}
	
	public SetClassBuilder productCode(String productCode)
	{
		this.productCode = productCode;
		return this;
	}
	
	public SetClassBuilder active(boolean active)
	{
		this.active = active;
		return this;
	}

	public SetClassBuilder division(String d)
	{
		this.divisionCode = d;
		return this;
	}

	public SetClassBuilder orgCode(String o)
	{
		this.orgCode = o;
		return this;
	}

	public Set_Class__c build(Boolean createInDatabase)
	{
		Set_Class__c setClass = new Set_Class__c(
			Name = this.productCode,
			EquipmentDescription__c = this.equipmentDescription,
			Active__c = this.active,
			Division__c = divisionCode,
			OrganizationCode__c = orgCode,
			EquipmentNumber__c = this.equipmentNumber
		);
		
		if (createInDatabase)
			insert setClass;
		
		return setClass;
	}
	
}