public with sharing class SetClassItemBuilder {

	private static Integer sequence = 100;


	private Set_Class__c setClass;
	private Product2 prod;
	private Double qty;

	public SetClassItemBuilder header(Set_Class__c setClass)
	{
		this.setClass = setClass;
		return this;
	}
	
	public SetClassItemBuilder product(Product2 p)
	{
		this.prod = p;
		return this;
	}
	
	public SetClassItemBuilder quantity(Double qty)
	{
		this.qty = qty;
		return this;
	}

	
	public Set_Class_Items__c build(Boolean createInDatabase)
	{
		Set_Class_Items__c setClassItem = new Set_Class_Items__c(
			Set_Class__c = this.setClass.Id,
			Product__c = this.prod.Id,
			Quantity__c = this.qty
		);
		
		if (createInDatabase)
			insert setClassItem;
		
		return setClassItem;
	}


}