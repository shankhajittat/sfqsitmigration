public class QuotationControllerExtension {
    
    // using a distinct property name to ensure view binds to view state from this extension
    // instead the quote property of the standard controller
    public Quote extensionQuote{get;set;}
    private Pricebook2 priceBook;
    
    // only used in testing
    public Pricebook2 getPriceBook() {
    	return pricebook;
    }
    
    public Map<Id, ItemGroupWrapper> groups {
		get {
			if (groups == null) {
				groups = new Map<Id, ItemGroupWrapper>(); 
			}
			return groups;
		}
		set;
    } 
    
    public List<ItemGroupWrapper> getGroupsWithRollups() {
    	List<ItemGroupWrapper> grps = groups.values();
    	grps.sort();
    	return grps;
    }
     
    // Declare standardController controller
    private ApexPages.StandardController controller;
    
    public Id accountId{get;set;}    
    public Account account {get; set;}
    public String setClassCode {get; set;}
    public String setClassId {get; set;}
    public String productId {get; set;}			//it is using for AutoComplete Item Search.
    public String addingGroupClassId {get; set;} 	//it is using for AutoComplete Item Search.
    public String message {get; set;}
    public Id newQuotationLineId {get; set;} //for testing only
                
    public void updateGroupsInViewState() {
    	if (this.extensionQuote != null) {
    		
	    	Map<Id, ItemGroupWrapper> newGroups = new Map<Id, ItemGroupWrapper>();
	    	 
	    	// populate the newGroups map in local scope
	    	for (List<QuoteLineItem> items : [Select q.Set_Class__c, q.Set_Class_Item__r.Set_Class__c, q.Set_Class_Item__r.Set_Class__r.Name, 
	    									  q.UnitPrice, q.TotalPrice, q.Subtotal, q.QuoteId, q.Price_Status__c,
	        								  q.Quantity, q.PricebookEntryId, q.PricebookEntry.Product2.ProductCode, q.ListPrice, q.LineNumber, 
	        								  q.Id, q.Discount, q.Description, q.Discount_Amount__c
	        				  				  From QuoteLineItem q
	                          				  WHERE q.QuoteId = :this.extensionQuote.id 
	                          				  order by q.PricebookEntry.Product2.ProductCode]) {
	    		for (QuoteLineItem item : items) {
	    			ItemGroupWrapper grp = newGroups.get(item.Set_Class__c);
	    			if (grp == NULL) {
	    				grp = new ItemGroupWrapper(
	    					item.Set_Class__c,
	    					item.Set_Class_Item__r.Set_Class__r.Name,
	    					null,null,null);
	    				newGroups.put(item.Set_Class__c,grp);
	    			}
	    			grp.items.add(new ItemWrapper(item, false));
	    		}
	    		
	    	}
	    	
	    	// copy across any properties that should persist after the update
	    	Map<Id, ItemWrapper> oldItemLookup = new Map<Id, ItemWrapper>();
	    	for (ItemGroupWrapper grp : groups.values()) {
	    		for (ItemWrapper item : grp.items) {
	    			oldItemLookup.put(item.item.Id, item);
				}
    		}
	    	for (ItemGroupWrapper grp : newGroups.values()) {
	    		
	    		// copy group selected property across so that group selections persist through a rerender
	    		if (groups.containsKey(grp.setClassId)) {
	    			grp.selected = groups.get(grp.setClassId).selected;
	    		}
	    		
	    		for (ItemWrapper item : grp.items) {
	    			ItemWrapper oldItem = oldItemLookup.get(item.item.Id);
	    			if (oldItem != null) { // need this check for initial loading when there are no old items
	    				item.selected = oldItem.selected;
	    			}
				}
    		}
	    	
	    	// copy the local map data into the instance property
	    	groups.clear();
	    	for (Id groupId : newGroups.keySet()) {
	    		groups.put(groupId, newGroups.get(groupId));
	    	}
	    	
    	}
    }
    
    public QuoteLineItem getLineItemFromViewState(Id itemId) {
    	for (ItemGroupWrapper grp : groups.values()) {
    		for (ItemWrapper item : grp.items) {
    			if (item.item.Id == itemId) {
    				return item.item;
    			}
    		}
    	}
    	return null;
    }
        
    public QuotationControllerExtension(ApexPages.StandardController stdController)
    {
    	try{
	        controller = stdController;
	        this.extensionQuote = (Quote)stdController.getRecord();
	        // re-query the quote to ensure join data is present
	        updateQuoteInViewState();
	            
	        // query the account when the page is being used for a new Quote
	        this.accountId = ApexPages.currentPage().getParameters().get('accid');        
	        this.accountId = accountId==null?this.extensionQuote.Opportunity.AccountId:accountId;
	        // TODO this query is not required when the quote has an Id since updateQuoteInViewState
	        // includes the Account Name in its query     
	       	this.account = [SELECT a.Name, a.ShippingStreet, a.ShippingState, a.ShippingPostalCode, 
			        					a.ShippingCountry, a.ShippingCity, a.BillingStreet, a.BillingState, 
			        					a.BillingPostalCode, a.BillingCountry, a.BillingCity,
			        					a.OrganizationCode__c,a.DistributionChannel__c,a.Division__c FROM Account a WHERE a.Id =: this.accountId];
			        					
	        if (StringUtils.isEmpty(this.account.OrganizationCode__c)){
	        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid OrganizationCode.');
	        	return;
	        }
	        if (StringUtils.isEmpty(this.account.DistributionChannel__c)){
	        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid DistributionChannel.');
	        	return;
	        }
	        if (StringUtils.isEmpty(this.account.Division__c)){
	        	throw new ApplicationException('Selected Account: ' + this.account.Name + ' does not have a valid Division.');
	        	return;
	        }
	        	
	        /*
	        //Standard PriceBook     
	        this.priceBook = [SELECT p.Name, p.IsStandard, p.IsDeleted, p.IsActive, p.Id, p.Description 
	        				  from Pricebook2 p 
	        				  WHERE p.IsStandard=true LIMIT 1];
	        */
	        
	        try{
		        this.priceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: this.account.OrganizationCode__c 
		    						And DistributionChannel__c =: this.account.DistributionChannel__c And Division__c =: this.account.Division__c LIMIT 1];
		    	if (this.priceBook.IsActive == false){
		    		throw new ApplicationException('PriceBook ' + this.priceBook + ' is Inactive. Pricebook must be active inorder to create Quote.');
	        		return;
		    	}
	        }
	        catch(Exception e){
		    	this.priceBook = null;
		    }
		    if (this.priceBook == null){
	        	Pricebook2 customPricebook = new PriceBook2Builder()
	        			.name(this.account.OrganizationCode__c + this.account.DistributionChannel__c + this.account.Division__c)
	        			.organizationCode(this.account.OrganizationCode__c)
	        			.distributionChannel(this.account.DistributionChannel__c)
	        			.division(this.account.Division__c)
	        			.build(true);
	        			
	        	this.priceBook = [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: this.account.OrganizationCode__c 
		    						And DistributionChannel__c =: this.account.DistributionChannel__c And Division__c =: this.account.Division__c LIMIT 1];
		    }
	        
	        
	        if (this.extensionQuote != null && this.extensionQuote.Id != null)
	            updateGroupsInViewState();    
        
        }
	 	catch (Exception e) {
	 		ApexPages.addMessages(e);
	 		return;
	 	}
    }

	// update view state so that rerender sees updated rollups 
	private void updateQuoteInViewState() {
    	if (this.extensionQuote != null && this.extensionQuote.Id != null) {			
			this.extensionQuote = [Select q.Id, q.Opportunity.Pricebook2Id, q.Opportunity.Id, q.OpportunityId,  q.QuoteNumber, q.GrandTotal,
								   q.Opportunity.Account.Id, q.Opportunity.Account.Name,
								   q.Total_Discount_Amount__c,q.Tax,q.TotalPrice,q.Subtotal,q.Total_Gross_Margin_Amount__c,
								   q.Total_Gross_Margin_Percent__c,q.Version__c
	  							   From Quote q 
								   WHERE q.Id = :this.extensionQuote.Id];
    	}
	}
	
	public PageReference updateViewStateHeader() {
		updateQuoteInViewState();
		return null;
	}

    //Action Method
    public PageReference addItem()
    {
    	this.message = '';
        if (this.productId == null)
        {
            return null;
        }
        
        PricebookEntry pbe = [select UnitPrice from PricebookEntry
        					  where Pricebook2Id = :this.pricebook.Id
        					  and Product2Id = :this.productId];
        					  
        Quote_Line_Group__c quoteLineGroup = null;					  
        List<Quote_Line_Group__c> quoteLineGroups = [Select g.Id, g.Name, g.QuoteId__c, g.SetClassId__c, g.GroupType__c 
							From Quote_Line_Group__c g Where g.QuoteId__c =:this.extensionQuote.Id And g.SetClassId__c = :addingGroupClassId];
		if (quoteLineGroups.size() == 1)	
			quoteLineGroup = quoteLineGroups[0];
		//validation required for quoteLineGroup is null
							  
        System.debug('pbe for ad-hoc item: '+pbe+' quote id: '+this.extensionQuote.Id+' class id: '+addingGroupClassId);        
        QuoteLineItem newQuoteLine = new QuoteLineItem(
						QuoteId=this.extensionQuote.Id,
						PriceBookEntryId=pbe.Id,
						UnitPrice=pbe.UnitPrice,
						Quantity=1,
						Set_Class__c = addingGroupClassId,
						Quote_Line_Group__c = quoteLineGroup.Id);
						
		System.debug('Adding new item: '+newQuoteLine);
						
		insert newQuoteLine;

		this.newQuotationLineId = newQuoteLine.Id;
		
		updateQuoteInViewState();
        updateGroupsInViewState();
        
        return null;
    }
    
    public PageReference chooseSet()
    {
        this.message = '';
        if (this.setClassId == null)
        {
            return null;
        }
        //look for SetClass exist based on the user input.
        List<Set_Class__c> setClass = [Select s.Id From Set_Class__c s Where s.Id = :setClassId];
          
        boolean isNewQuote = false;
        if (this.extensionQuote == null || this.extensionQuote.id == null)
        {
        	isNewQuote = true;
        	
        	String nextOpportunityDocumentNumber = CustomSettingsTools.GetNextOpportunityNumber();
            //Create Opportunity
            Opportunity opportunity = new Opportunity(
                Name = nextOpportunityDocumentNumber,
                CloseDate = Date.today().addDays(30),
                Amount = 0.0,
                StageName = 'Qualification',
                AccountId = this.account.Id,
                Pricebook2Id = this.priceBook.Id
                );
                
             Database.insert(opportunity);
        
            //Create Quote
            String nextQuoteDocumentNumber = CustomSettingsTools.GetNextQuotationNumber();
            this.extensionQuote = new Quote(
                Name = nextQuoteDocumentNumber,
                OpportunityId = opportunity.Id,
                Status = 'Draft',
                Pricebook2Id = this.priceBook.Id,
                Version__c = 1
                );
            Database.insert(this.extensionQuote);
            
            //link together
            OpportunityTools.UpdateOpportunitySyncQuoteId(opportunity.Id, this.extensionQuote.Id);
        }
    
        
        System.debug('>>>>SetClassId<<<<<' + setClassId);
        System.debug('>>>>QuoteId<<<<<' + this.extensionQuote.Id);
        if (setClassId != null && this.extensionQuote.Id != null)
        {
            QuoteGroupLineItemsPopulator groupLinePopulator = new QuoteGroupLineItemsPopulator(this.extensionQuote.Id, setClassId);
            groupLinePopulator.quote = this.extensionQuote;
            groupLinePopulator.run();
            
	 		updateQuoteInViewState();           
            updateGroupsInViewState();
        }
        
        // do full page refresh for new Quotes to support refresh/back button
        // for users
        if (isNewQuote) {
        	PageReference pr = Page.Quotation;
        	pr.getParameters().put('id',this.extensionQuote.Id);
        	pr.setRedirect(true);
        	return pr;
        } else {
        	return null;
        }
    }
    
    public String itemIdToUpdate{get;set;}
    public String fieldToUpdate{get;set;}
    public String valueToUpdate{get;set;}
    
    public PageReference updateItem() {
    	System.debug('itemIdToUpdate: '+itemIdToUpdate+' fieldToUpdate: '+fieldToUpdate+' valueToUpdate: '+valueToUpdate);

    	Double actualValue = (valueToUpdate == null || valueToUpdate == '')? 
    		0 : Double.valueOf(valueToUpdate);    	
    	
    	QuoteLineItem item = getLineItemFromViewState(itemIdToUpdate);
    	if (fieldToUpdate == 'quantity') {
    		item.Quantity = actualValue.intValue();    		
    	} else if (fieldToUpdate == 'discountP') {
    		item.Discount = actualValue;
    	} else if (fieldToUpdate == 'discountA') {
    		Double percentageOfUnit = actualValue / item.UnitPrice * 100;
    		item.Discount = percentageOfUnit;
    	}
    	update item;
 		updateQuoteInViewState();
        updateGroupsInViewState();
    	return null;
    }
    
    public String itemIdToDelete{get;set;}
    
    public PageReference deleteItem(){
    	
    	QuoteLineItem item = getLineItemFromViewState(itemIdToDelete);
    	
    	Database.DeleteResult dr = database.delete(item);
    	//System.debug('Delete result: '+dr);
    	
    	updateQuoteInViewState();
        updateGroupsInViewState();
        
        // Cycle thru viewstate to determine if deleted item is still there - temporary
       
        for (ItemGroupWrapper grp : groups.values()) {
        
        		
       	if (grp.setClassId == item.Set_Class__c){
        		for(ItemWrapper iteminViewState : grp.items){
        			if (iteminViewState.item.Id==itemIdToDelete){
        				system.debug('>>>>>>>>>>>>>>>>Found item in view state - set class: ' + iteminViewState.item.Set_Class__c);
        				system.debug('>>>>>>>>>>>>>>>> Found item in view state - item id: ' + iteminViewState.item.Id);
        			}		
        		}
        	}
        }
        
    	return null;
    }
    
/*  This method is being commented out because it fails on division by zero when UnitPrice is zero. This pgm is no longer used. Unit Price can be zero.

    public PageReference fillItems() {
    	Double actualValue = (valueToUpdate == null || valueToUpdate == '')? 
    		0 : Double.valueOf(valueToUpdate);    	
    	
    	List<QuoteLineItem> selected = new List<QuoteLineItem>();
    	for (ItemGroupWrapper grp : groups.values()) { 
    		for (ItemWrapper item : grp.items) {
    			if (item.selected) {
    				selected.add(item.item);
			    	if (fieldToUpdate == 'quantity') {
			    		item.item.Quantity = actualValue.intValue();    		
			    	} else if (fieldToUpdate == 'discountP') {
			    		item.item.Discount = actualValue;
			    	} else if (fieldToUpdate == 'discountA') {
			    		Double percentageOfUnit = actualValue / item.item.UnitPrice * 100;
			    		item.item.Discount = percentageOfUnit;
			    	}
    	    	}
			}
		}
		update selected;
    	
 		updateQuoteInViewState();
        updateGroupsInViewState();
    	return null;
    }
 */         
}