public class CSynthesJP_TestClasses {
	static testMethod void testOpportunity() { 
    	Account newAccount = new Account();
    	newAccount.Name = 'Test Account';
    	insert newAccount;
	
    	Contact newContact = new Contact();
    	newContact.LastName = 'Test';
    	newContact.FirstName = 'Test';
    	newContact.Salutation = 'Test';
    	newContact.AccountId = newAccount.Id;
    	insert newContact;
    	
    	Dr_OR_Schedule__c newORSchedule1 = new Dr_OR_Schedule__c();
    	newORSchedule1.RecordTypeId = '012200000000fCj';
    	newORSchedule1.Synthes_Staff_Attending__c = 'Yes';
    	newORSchedule1.Hospital__c = newAccount.Id;
    	newORSchedule1.Dr__c = newContact.Id;
		newORSchedule1.Date_Time__c = Datetime.now();
		newORSchedule1.OR_Room__c = 'Test Location';
		newORSchedule1.Est_Duration__c = '1 hour';
		insert newORSchedule1;

    	Dr_OR_Schedule__c newORSchedule2 = new Dr_OR_Schedule__c();
    	newORSchedule2.RecordTypeId = '012200000000fCj';
    	newORSchedule2.Synthes_Staff_Attending__c = 'Yes';
    	newORSchedule2.Hospital__c = newAccount.Id;
    	newORSchedule2.Dr__c = newContact.Id;
		newORSchedule2.Date_Time__c = Datetime.now();
		newORSchedule2.OR_Room__c = 'Test Location';
		newORSchedule2.Est_Duration__c = '2 hours';
		insert newORSchedule2;

    	Dr_OR_Schedule__c newORSchedule3 = new Dr_OR_Schedule__c();
    	newORSchedule3.RecordTypeId = '012200000000fCj';
    	newORSchedule3.Synthes_Staff_Attending__c = 'Yes';
    	newORSchedule3.Hospital__c = newAccount.Id;
    	newORSchedule3.Dr__c = newContact.Id;
		newORSchedule3.Date_Time__c = Datetime.now();
		newORSchedule3.OR_Room__c = 'Test Location';
		newORSchedule3.Est_Duration__c = '3 hours';
		insert newORSchedule3;
		
    	Dr_OR_Schedule__c newORSchedule4 = new Dr_OR_Schedule__c();
    	newORSchedule4.RecordTypeId = '012200000000fCt';
    	newORSchedule4.Synthes_Staff_Attending__c = 'Yes';
    	newORSchedule4.Hospital__c = newAccount.Id;
    	newORSchedule4.Dr__c = newContact.Id;
		newORSchedule4.Date_Time__c = Datetime.now();
		newORSchedule4.OR_Room__c = 'Test Location';
		newORSchedule4.Est_Duration__c = '4 hours';
		insert newORSchedule4;					

    	Dr_OR_Schedule__c newORSchedule5 = new Dr_OR_Schedule__c();
    	newORSchedule5.RecordTypeId = '012200000000fCt';
    	newORSchedule5.Synthes_Staff_Attending__c = 'Yes';
    	newORSchedule5.Hospital__c = newAccount.Id;
    	newORSchedule5.Dr__c = newContact.Id;
		newORSchedule5.Date_Time__c = Datetime.now();
		newORSchedule5.OR_Room__c = 'Test Location';
		newORSchedule5.Est_Duration__c = '5 hours';
		insert newORSchedule5;

    	Dr_OR_Schedule__c newORSchedule6 = new Dr_OR_Schedule__c();
    	newORSchedule6.RecordTypeId = '012200000000fCt';
    	newORSchedule6.Synthes_Staff_Attending__c = 'Yes';
    	newORSchedule6.Hospital__c = newAccount.Id;
    	newORSchedule6.Dr__c = newContact.Id;
		newORSchedule6.Date_Time__c = Datetime.now();
		newORSchedule6.OR_Room__c = 'Test Location';
		newORSchedule6.Est_Duration__c = '6 hours';
		insert newORSchedule6;			
	}
}