@IsTest
public class EditableTableQuotePluginTest {

	@IsTest
    private static void testEditableTableQuoteUpdate() {
    
    	///////// TEST SET VALUE
    	QuoteLineItem testQuoteLine = TestingTools.getTestQuoteLineItem();    	
    	System.assertEquals(2, testQuoteLine.Quantity, 'Initial Quantity should be 2');
    	System.assertEquals(10, testQuoteLine.Discount, 'Initial Discount Percentage should be 10%');
    	Id groupId = testQuoteLine.Quote_Line_Group__c;
    	Id quoteId = testQuoteLine.QuoteId;
    	
    	EditableTableController.EditableTablePlugin plugin = new EditableTableQuotePlugin(); 
    	
    	QuoteLineItem updatedItem = (QuoteLineItem) plugin.setValue(testQuoteLine.Id, 'Quantity', '20');    	
    	System.assertEquals(20, updatedItem.Quantity, 'Quantity in db should now be updated');

    	Double discountFor20 = updatedItem.Discount_Amount__c;

    	updatedItem = (QuoteLineItem) plugin.setValue(testQuoteLine.Id, 'Discount', '50');    	
    	System.assertEquals(50, updatedItem.Discount, 'Quantity in db should now be updated');    	
    	System.assertEquals(discountFor20*5, updatedItem.Discount_Amount__c, 'discount amount in db should be 5x');

		try {
	    	plugin.setValue(testQuoteLine.Id, 'foo', '20');
	    	System.assert(false,'setValue(foo) should not succeed');
		} catch (EditableTableQuotePlugin.ValueException ve) {
			// this is the expected behaviour
		}   

		// TEST BULK UPDATE
		Map<Id, String> bulkValues = new Map<Id, String>();
		bulkValues.put(testQuoteLine.Id, '60'); 
    	List<QuoteLineItem> updatedItems = (List<QuoteLineItem>) plugin.setValues(bulkValues, 'Discount');    	
    	System.assertEquals(60, updatedItems[0].Discount, 'Quantity in db should now be updated');    	
    	System.assertEquals(discountFor20*6, updatedItems[0].Discount_Amount__c, 'discount amount in db should be 6x');
		
		
		/////// TEST GET GROUPS
		List<EditableTableGroup> groups = plugin.getGroups(testQuoteLine.QuoteId);
		system.assert(groups.size() > 0, 'No groups found');
		system.assertEquals(1, groups.size(), 'More than one group returned but expected only one for testQuoteLine');
		

		/////// TEST DELETING QUOTELINEITEMS
		// Add 2 more quotelineitems so that we have 3 altogether
		QuoteLineItem quoteLineItem1 = new QuoteLineItem(
						QuoteId=testQuoteLine.QuoteId,        // Same quote
						PriceBookEntryId=testQuoteLine.PriceBookEntryId,
						UnitPrice=30.00,
						Quantity= 4,
						Discount = 20.00,
						Set_Class__c = testQuoteLine.Set_Class__c,
						Set_Class_Item__c = testQuoteLine.Set_Class_Item__c,
						Quote_Line_Group__c = testQuoteLine.Quote_Line_Group__c,
						Tax_Rate__c = 40.00,
						Standard_Cost__c = 20.00,
						Landed_Cost__c = 60.00,
						Sequence_Number__c = 30,
						Price_Status__c = 'New'
						);
		insert quoteLineItem1;
		System.assert(quoteLineItem1.Id <> null,'An error occured while creating quoteLineItem1.');
		
		QuoteLineItem quoteLineItem2 = new QuoteLineItem(
						QuoteId=testQuoteLine.QuoteId,        // Same quote
						PriceBookEntryId=testQuoteLine.PriceBookEntryId,
						UnitPrice=50.00,
						Quantity= 6,
						Discount = 80.00,
						Set_Class__c = testQuoteLine.Set_Class__c,
						Set_Class_Item__c = testQuoteLine.Set_Class_Item__c,
						Quote_Line_Group__c = testQuoteLine.Quote_Line_Group__c,
						Tax_Rate__c = 80.00,
						Standard_Cost__c = 70.00,
						Landed_Cost__c = 80.00,
						Sequence_Number__c = 60,
						Price_Status__c = 'New'
						);
		insert quoteLineItem2;
		System.assert(quoteLineItem2.Id <> null,'An error occured while creating quoteLineItem2.');
		
		// Delete 2 out of the three quotelineitems - ie.send in a list of more than one to delete
		List<String> itemsToDelete = new List<String>();
		itemsToDelete.Add((String)quoteLineItem1.Id);
		itemsToDelete.Add((String)quoteLineItem2.Id);
		system.debug('>>>>>>> EditableTableQuotePluginTest - Items being sent for deletion : ' + itemsToDelete);
		String returnResult = plugin.deleteItems(itemsToDelete);
		System.assertEquals('{}', returnResult, 'Delete of two quotelineitems has failed but should have been successful'); 
		
		// The first QuoteLineitem created should still be there 
		QuoteLineItem oneLeft = [SELECT Id, Quantity from QuoteLineItem WHERE Id = :testQuoteLine.Id LIMIT 1]; 
		System.assert(oneLeft.Id <> null,'Quotelineitem not found. This one should not have been deleted');
		System.assertEquals(20, oneLeft.Quantity, 'Initial Quantity should be 20');
		
		// This one should not be there if the delete was successful
		for (List<QuoteLineItem> itemsShouldNotExist : [SELECT Id, Quantity from QuoteLineItem WHERE Id = :quoteLineItem2.Id]) {
			System.assertEquals(0, itemsShouldNotExist.size(), 'quoteLineItem2 should not exist any longer - it should have been deleted');
		}
		
		// Now delete just one quotelineitem
		itemsToDelete = new List<String>();
		itemsToDelete.Add((String)testQuoteLine.Id);
		returnResult = plugin.deleteItems(itemsToDelete);
		System.assertEquals('{}', returnResult, 'Delete of one quoteline item has failed but should have been successful'); 
		
		// This one should not be there if the delete was successful
		for (List<QuoteLineItem> itemsShouldNotExist : [SELECT Id, Quantity from QuoteLineItem WHERE Id = :testQuoteLine.Id]) {
			System.assertEquals(0, itemsShouldNotExist.size(), 'testQuoteLine should not exist any longer - it should have been deleted');
		}
		
		// Now force a delete to fail - this id is a quote id , not a valid quotelineitem id
		itemsToDelete = new List<String>();
		itemsToDelete.Add('0Q080000000ClrMCCC');
		returnResult = plugin.deleteItems(itemsToDelete);
		system.debug('>>>>>>>>>>>> ReturnResult from delete request : '+returnResult);
		System.assertNotEquals('{}', returnResult, 'Delete request should have failed'); 
		
		
		
		
		/////// TEST DELETING QUOTE_LINE_GROUPS
		// Add 1 more group and attach an item to it so that we have 2 altogether
		// This test will delete one group that has no items and one group that has 1 item. That will also test the quotelinegroupbeforedelete trigger.
		
		Boolean createInDatabase = true;
		
		String organizationCode = 'UT-AU10';
        String distributionChannel = '01';
        String division = '01'; 
        	        
        Pricebook2 customPricebook = null;
	    try{
	    	customPricebook =  [SELECT Id,Name,IsActive from Pricebook2 WHERE OrganizationCode__c =: organizationCode 
	    			And DistributionChannel__c =: distributionChannel LIMIT 1];
	    			
	    	if (customPricebook.IsActive == false){
	    		Pricebook2 updatePricebook = new Pricebook2();
	    		updatePricebook.Id = customPricebook.Id;
	    		updatePricebook.IsActive = true;
	    		update updatePricebook;
	    	}
	    }
	    catch(Exception e){
	    	customPricebook = null;
	    }
	    if (customPricebook == null){
        	customPricebook = new PriceBook2Builder()
        			.name(organizationCode + distributionChannel)
        			.organizationCode(organizationCode)
        			.distributionChannel(distributionChannel)
        			.build(true);
        	System.assert(customPricebook.Id <> null,'An error occured while creating Pricebook: ' + organizationCode + distributionChannel);
	    }
    	System.debug('CustomPriceBook>>>>:' + customPricebook);

	        
	    // Get quote created earlier in the test so that we can add another group to it
	    List<Quote> quotes = [SELECT Id From Quote WHERE Id = :quoteId];
	    System.assert(quotes.size() > 0, 'No quote found. Expected to find the one created earlier in the test');
	    System.assertEquals(1, quotes.size(), 'Expected 1 quote to be retrieved but more than one was found');
	    Quote quote = quotes[0];
	    
	    // Get Group created earlier
		List<Quote_Line_Group__c> qlGroups = [SELECT Id From Quote_Line_Group__c WHERE Id = :groupId];
		System.assert(qlGroups.size() > 0, 'An error occurred retrieving quote line group created earlier in the test');
	    
	    // Create another product
		Product2 product = new ProductBuilder()
        		.name('BENDING TMPLTE F/OCCIPIT PLATE MEDIAL LARGE')
        		.code('03.162.002')
        		.build(createInDatabase);
        System.assert(product.Id <> null,'An error occured while creating Product 03.162.002.');
        
         // Create another product
		Product2 bomProduct = new ProductBuilder()
        		.name('BOM Product')
        		.code('03.162.002BOM')
        		.build(createInDatabase);
        System.assert(bomProduct.Id <> null,'An error occured while creating Product 03.162.002BOM');
        
     	//Create PricebookEntry for product
        PricebookEntry pbStandardEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(Test.getStandardPricebookId()).build(createInDatabase);
        System.assert(pbStandardEntry.Id <> null,'An error occured while creating PricebookEntry for Standard PriceBook.');
        
         //Create PricebookEntry for Custom Pricebook
        PricebookEntry pbCustomEntry = new PricebookEntryBuilder()
        									.productId(product.Id)
        									.pricebookId(customPricebook.Id).build(createInDatabase);
        System.assert(pbCustomEntry.Id <> null,'An error occured while creating PricebookEntry.');
	        
	        
		 //Create Set Class
        Set_Class__c setClass = new SetClassBuilder()
        								.equipmentNumber('TEST SC202').productCode('03.162.002BOM').build(createInDatabase);
        System.assert(setClass.Id <> null,'An error occured while creating SetClass TEST SC202.');
        
        //Create SetClass Items
        Set_Class_Items__c setClassItem1 = new SetClassItemBuilder()
        								.header(setClass)
        								.product(product)
        								.quantity(4).build(createInDatabase);
        
        System.assert(setClassItem1.Id <> null,'An error occured while creating SetClassItem1.');
        
        Quote_Line_Group__c quoteLineGroup = new QuoteLineGroupBuilder()
	        		.name('TEST SC102')
	        		.quote(quote)
	        		.setClass(setClass)
	        		.groupType('Kit')
	        		.groupStatus('Validated')
	        		.build(createInDatabase);
	    System.assert(quoteLineGroup.Id <> null,'An error occured while creating QuoteLineGroup : TEST SC102.');
	    
	    QuoteLineItem quoteLineItem3 = new QuoteLineItem(
						QuoteId=quote.Id,        // Same quote
						PriceBookEntryId=pbCustomEntry.Id,
						UnitPrice=120.00,
						Quantity= 8,
						Discount = 20.00,
						Set_Class__c = SetClass.Id,
						Set_Class_Item__c = setClassItem1.Id,
						Quote_Line_Group__c = quoteLineGroup.Id,
						Tax_Rate__c = 40.00,
						Standard_Cost__c = 20.00,
						Landed_Cost__c = 22.00,
						Sequence_Number__c = 90,
						Price_Status__c = 'New'
						);
		insert quoteLineItem3;
		System.assert(quoteLineItem3.Id <> null,'An error occured while creating quoteLineItem3.');
		
	   
		// Build list of groups to delete
		List<String> groupsToDelete = new List<String>();
		groupsToDelete.Add((String)groupId);
		groupsToDelete.Add((String)quoteLineGroup.Id);
		system.debug('>>>>>>> Groups being sent for deletion : ' + groupsToDelete);
		String returnGroupResult = plugin.deleteGroups(groupsToDelete);
		system.debug('>>>>>>>>>>>>>>>>>>>>>> return Result - in plugin : ' + returnGroupResult);
		System.assertEquals('{}', returnGroupResult, 'Delete of two quote line groups has failed but should have been successful'); 
		
		
		///// Now test Delete Groups where there are no items for that group - this also tests the quotelinegroupbeforedelete trigger.
		Quote_Line_Group__c quoteLineGroup2 = new QuoteLineGroupBuilder()
	        		.name('TEST SC103')
	        		.quote(quote)
	        		.setClass(setClass)
	        		.groupType('Kit')
	        		.groupStatus('Validated')
	        		.build(createInDatabase);
	    System.assert(quoteLineGroup2.Id <> null,'An error occured while creating QuoteLineGroup : TEST SC103.');
		
		groupsToDelete = new List<String>();
		groupsToDelete.Add((String)quoteLineGroup2.Id);
		returnGroupResult = plugin.deleteGroups(groupsToDelete);
		System.assertEquals('{}', returnGroupResult, 'Delete of quote line group with no items attached has failed but should have been successful');
    
    	// Now force a delete to fail - this id is a quote id , not a valid quote line group id
		groupsToDelete = new List<String>();
		groupsToDelete.Add('0Q080000000ClrMCCC');
		returnResult = plugin.deleteGroups(groupsToDelete);
		system.debug('>>>>>>>>>>>> ReturnResult from delete group request : '+returnResult);
		System.assertNotEquals('{}', returnResult, 'Delete request for group should have failed'); 
		
    }
    
    
    //Cannot test this way since SAP endpoint are enabled, and SFDC doesn't allow invoke external web service endpoints through test method
	//need to impliment external webservice mock
    @IsTest
    private static void testEditableTableQuoteUpdate2() {
    
    	///////// TEST GET PRICING for items
    	QuoteLineItem testQuoteLine = TestingTools.getTestQuoteLineItem();    	
    	System.assertEquals(2, testQuoteLine.Quantity, 'Initial Quantity should be 2');
    	System.assertEquals(10, testQuoteLine.Discount, 'Initial Discount Percentage should be 10%');
    	Id quoteId = testQuoteLine.QuoteId;
    	
 		//Create UnitTest Prerequisite Data.
        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
           	
    	Test.startTest() ;
    	 // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        // Call the method that invokes a callout
        
    	EditableTableController.EditableTablePlugin plugin = new EditableTableQuotePlugin(); 
    	
    	List<String> itemsForPricing = new List<String>();
    	itemsForPricing.Add((Id)testQuoteLine.Id);
    	
    	Boolean isSuccessful = plugin.getItemPricing(quoteId, itemsForPricing);
    	    	
    	System.assertEquals(true, isSuccessful, 'Expected pricing to be successful but it failed');
    	Test.stopTest();
    }
    
     @IsTest
    private static void testEditableTableQuoteUpdate3() {
    
    	///////// TEST GET PRICING for group
    	QuoteLineItem testQuoteLine = TestingTools.getTestQuoteLineItem();    	
    	System.assertEquals(2, testQuoteLine.Quantity, 'Initial Quantity should be 2');
    	System.assertEquals(10, testQuoteLine.Discount, 'Initial Discount Percentage should be 10%');
    	Id groupId = testQuoteLine.Quote_Line_Group__c;
    	Id quoteId = testQuoteLine.QuoteId;
    	
    	//Create UnitTest Prerequisite Data.
        UnitTestPrerequisiteData.createQuoteRelatedPrerequisiteData();
        
    	Test.startTest() ;
    	 // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new WebMethodsPriceRequestSoapAPIv1MockImpl());
        
        // Call the method that invokes a callout
    	EditableTableController.EditableTablePlugin plugin = new EditableTableQuotePlugin(); 
    	
    	Boolean isSuccessful = plugin.getGroupPricing(quoteId, groupId);
    	    	
    	System.assertEquals(true, isSuccessful, 'Expected pricing to be successful but it failed');
    	Test.stopTest();
    }
    
}