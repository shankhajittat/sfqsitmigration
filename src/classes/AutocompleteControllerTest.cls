@IsTest
private class AutocompleteControllerTest {
	@IsTest
	public static void testAutocompleteController() {   
		
		AutocompleteController theAutocompleteController = new AutocompleteController();
		
		Map<String,String> context = new Map<String,String>();
		context.put('id', null);
		
		//Create an account
		Account vinnies = new AccountBuilder().name('St Vincents').build(true);
		
		theAutocompleteController.pluginType = 'AutocompleteAccountPlugin';
		
		string customCSS = theAutocompleteController.getCustomCSS();
		system.assert(customCSS <> null, 'customCSS should not return null');
		
		string jsRender = theAutocompleteController.getRenderJS();
		system.assert(jsRender <> null, 'jsRender should not return null');
		
		string getHandleSelectionJS = theAutocompleteController.getHandleSelectionJS();
		system.assert(getHandleSelectionJS <> null, 'getHandleSelectionJS should not return null');
		
		
		// Test where set account exists 
    	Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = vinnies.Id;
        Test.setFixedSearchResults(fixedSearchResults);
		
		//Find the above account using first two letters
		List<SObject> theList = AutocompleteController.doSearch('St', 'AutocompleteAccountPlugin', context);
		system.assert(theList.size() <> 0, 'An account should have been returned');
	}
}