public with sharing class educationMaterialsAddEventPublicCal {

	public List<Event> relatedEvents = new List<Event>();
	public Set<Id> emIds = new Set<Id>();
	public List<Event> eventsModified = new List<Event>();
	public List<Event> existingEvents = new List<Event>();
	public Map<Id,Education_Materials__c> emWithOwner = new Map<Id,Education_Materials__c>();
	// Sand
	//public ID publicCalId = '023R0000000Lbpu';
	
	public ID publicCalId = '02320000000aSK7';
	
	public ID eventRecTypeId = '012200000000Pdh';
	
	public void getOwners() {
		
		Map<Id,Education_Materials__c> emWithOwner2 = new Map<Id,Education_Materials__c> ([Select Owner.Name, OwnerId From Education_Materials__c Where Id in :emIds]);
		emWithOwner = emWithOwner2.clone();
	}
	
	public void getEducationMaterialIds(List<Education_Materials__c> educationEventsPassed) {
	
		for(Education_Materials__c thisEM:educationEventsPassed) {
			
			emIds.add(thisEM.id);
			
		}
	}

	public void updateEvents() {
		
		if(eventsModified.size() > 0 ) {
			
			upsert eventsModified;
		}
	}
	
	public void insertEvents() {
		
		if(eventsModified.size() > 0 ) {
			
			insert eventsModified;
		}	 
	} 
	
	public void deleteEvents() {
		
		if(eventsModified.size() > 0 ) { 
			
			delete eventsModified;
		}
	}
	
	public void getExistingEvents(){
		
		existingEvents = [Select Id,DurationInMinutes, ActivityDateTime ,WhatId from Event Where WhatId in :emIds];
		
	}
	
	public void processEducationEventDelete(List<Education_Materials__c> educationEventsPassed) {
		
		getEducationMaterialIds(educationEventsPassed);
		getExistingEvents();
		
		/// Now I would just delete all these
		
		eventsModified = existingEvents.deepClone();
		existingEvents.clear();
		deleteEvents();
		
	}
	
	public void processEducationEventInsert(List<Education_Materials__c> educationEventsPassed) {
		
		getEducationMaterialIds(educationEventsPassed);
		getOwners();
		
		for(Education_Materials__c thisEM:educationEventsPassed) {
			
			if(thisEM.Projected_O_Shipping_Date__c != null && thisEM.Proj_Received_Date_L__c != null) {
				
				Event newEvent = new Event();	
				newEvent.whatId = thisEM.Id;
				newEvent.Subject = thisEM.Name + ' - ' + emWithOwner.get(thisEM.Id).Owner.Name;
				Integer numberDaysDue = thisEM.Projected_O_Shipping_Date__c.daysBetween(thisEM.Proj_Received_Date_L__c);				
				newEvent.DurationInMinutes = ((numberDaysDue+1) * 24) *60;
				newEvent.OwnerId  = publicCalId;			
				Datetime myDate = datetime.newInstance(thisEM.Projected_O_Shipping_Date__c.Year(), thisEM.Projected_O_Shipping_Date__c.Month(), thisEM.Projected_O_Shipping_Date__c.Day());		
				newEvent.ActivityDateTime = myDate;	
				newEvent.RecordTypeId = eventRecTypeId;
		
				eventsModified.add(newEvent);
			}
		}
		
		insertEvents();
	}
	
	public void processEducationEventUpdate(List<Education_Materials__c> educationEventsPassed,List<Education_Materials__c> oldEducationEventsPassed) {
		
		getEducationMaterialIds(educationEventsPassed);
		getExistingEvents();
		getOwners();
		
		for (Integer m=0; m<(educationEventsPassed.size()); m++) {
		
			Boolean StartDateMoved = false;
			Boolean EndDateMoved = false; 
			Boolean isThisAnInsert = false; 
			
			if(educationEventsPassed[m].Projected_O_Shipping_Date__c != oldEducationEventsPassed[m].Projected_O_Shipping_Date__c){
				
				if(oldEducationEventsPassed[m].Projected_O_Shipping_Date__c == null) {
					
					isThisAnInsert = true;
					
				} else {
					
					StartDateMoved = true;
				
				}
			}
			
			if(educationEventsPassed[m].Proj_Received_Date_L__c != oldEducationEventsPassed[m].Proj_Received_Date_L__c){
				
				if(oldEducationEventsPassed[m].Proj_Received_Date_L__c == null) {
					
					isThisAnInsert = true;
					
				} else {
						
					EndDateMoved = true;
				}
			}
			
			if(isThisAnInsert) { 
			
				System.debug('This is an update of an existing');
				Event newEvent = new Event();	
				newEvent.whatId = educationEventsPassed[m].Id;
				newEvent.Subject = educationEventsPassed[m].Name + ' - ' + emWithOwner.get(educationEventsPassed[m].Id).Owner.Name;
				Integer numberDaysDue = educationEventsPassed[m].Projected_O_Shipping_Date__c.daysBetween(educationEventsPassed[m].Proj_Received_Date_L__c);				
				newEvent.DurationInMinutes = ((numberDaysDue+1) * 24) *60;
				newEvent.OwnerId  = publicCalId;			
				Datetime myDate = datetime.newInstance(educationEventsPassed[m].Projected_O_Shipping_Date__c.Year(), educationEventsPassed[m].Projected_O_Shipping_Date__c.Month(), educationEventsPassed[m].Projected_O_Shipping_Date__c.Day());		
				newEvent.ActivityDateTime = myDate;	
				newEvent.RecordTypeId = eventRecTypeId;
		
				eventsModified.add(newEvent);
				
			} else {
			
				
				if(StartDateMoved || EndDateMoved) {
				
					adjustEventDates(educationEventsPassed[m]);
				
				}	
					
			}
		}
	
		updateEvents();
	
	}

	public void adjustEventDates(Education_Materials__c thisEM) {
		
		for(Event curEvent:existingEvents) {
			
				if(curEvent.WhatId == thisEM.id) {
				
					Integer numberDaysDue = thisEM.Projected_O_Shipping_Date__c.daysBetween(thisEM.Proj_Received_Date_L__c);				
					curEvent.DurationInMinutes = ((numberDaysDue+1) * 24) *60;			
					Datetime myDate = datetime.newInstance(thisEM.Projected_O_Shipping_Date__c.Year(), thisEM.Projected_O_Shipping_Date__c.Month(), thisEM.Projected_O_Shipping_Date__c.Day());		
					curEvent.ActivityDateTime = myDate;	
					eventsModified.add(curEvent);	
				}
		}
	}
}