public class EditableTableQuoteControllerExtension {

	public class ValueException extends Exception{}

	public Quote extQuote{get;set;}
	public String selectTranType {get; set;} 
	public Id defaultTranId {get; set;}
    //public List<Quote_Transaction_Type__c> transactionTypes {get; set;}
    public Boolean isQuoteUpdatedWithApprovers {get; set;}
    public GlobalConstants.QuoteTypes quoteType {get; set;}
    public Boolean showForCBC {get; set;}
    public Boolean showForSP {get; set;}
    public Boolean showForSetBuy {get; set;}
    public Boolean showForConSetBuild {get; set;}
    public Boolean showForChangeSet {get; set;}
    public Boolean showForLoanSetRequest {get; set;}
    
    public transient String errorMessage;
    public String actionType {get;set;}
    public String actionFieldName {get;set;}
	// Additional fields added to this retrieve to support User Preference settings 
	public void refresh() {
		try{
            actionType = '';
            actionFieldName = '';
	    	extQuote = [select Id, QuoteNumber, Status, GrandTotal, ExpirationDate, Total_Net_Margin_Amount__c,  Total_Standard_Cost__c, Total_Net_Margin_Percent__c, 
	    		Total_Landed_Cost__c, Total_Gross_Margin_Percent__c, Total_Gross_Margin_Amount__c, Total_Extended_Unit_Price__c, Total_Discount_Percentage__c, 
	    		SystemModstamp, ShippingStreet, ShippingState, ShippingPostalCode, ShippingName, ShippingLongitude, ShippingLatitude, ShippingHandling, ShippingCountry, 
	    		ShippingCity, QuoteToStreet, QuoteToState, QuoteToPostalCode, QuoteToName, QuoteToLongitude, QuoteToLatitude, QuoteToCountry, 
	    		QuoteToCity, Phone, Name, LineItemCount, LastViewedDate, LastReferencedDate, LastModifiedDate, LastModifiedById, Fax, 
	    		Instruction_Comments__c, Instruction_Do_Not_Ship__c, Instruction_Other__c, Instruction_Ship_To_Address_OK__c, Instruction_Transfer__c, 
	    		Email, ERP_Document_Number__c, Document_Date__c, Discount, Description, Customer_Purchase_Order_Number__c, CreatedDate, CreatedById, ContactId, 
	    		BillingStreet, BillingState, BillingPostalCode, BillingName, BillingLongitude, BillingLatitude, BillingCountry, BillingCity, Pricebook2Id,
	    		AdditionalStreet, AdditionalState, AdditionalPostalCode, AdditionalName, AdditionalLongitude, AdditionalLatitude, AdditionalCountry, AdditionalCity, 
	    		Total_Discount_Amount__c, TotalPrice, Subtotal, Tax, TotalPrice_ExTax__c, Version__c, LastModifiedBy.Name,CreatedBy.Name,
	    		QuoteType__c, Address__c, Address__r.Name, 
	    		Consignment_Business_Case__c, Consignment_Business_Case__r.Agreement_From_Date__c,Consignment_Business_Case__r.Agreement_To_Date__c,
	    		Opportunity.Account.Name, Opportunity.OwnerId, Opportunity.Owner.Name, Opportunity.Account.Division__c, Opportunity.Account.OrganizationCode__c,
	    		SAP_Request_Response_Message__c,Opportunity.AccountId,Customer_Approval__c,Show_rebate_details_in_Document__c,Quote_Description__c,
	    		ActualBillOfMaterial__c,TotalPrice_ImpMaterialGroup__c,TotalPrice_InsMaterialGroup__c,TotalPrice_OtherMaterialGroup__c,
	    		TotalStdCost_ImpMaterialGroup__c,TotalStdCost_InsMaterialGroup__c,TotalStdCost_OtherMaterialGroup__c,DiscountedLineItemCount__c,
	    		DiscountedLineItemCountRatio__c,DiscountedLineItemCountText__c,InternalComments__c,Surgeon_Preference__c,Sales_Consultant__c,Opportunity.Id ,Business_Unit__c 
	    		from Quote where Id = :extQuote.Id];
		} catch (Exception e) {
			String readQuoteErrorMessage = 'An error occurred reading the quote. Contact support with the following error : \r\nError occured in refresh() method of EditableTableQuoteControllerExtension.cls.' + e.getMessage();
			addMessagesForVFPage(readQuoteErrorMessage); 
		}
	}
	
	public String getExtQuoteJS() {
		return JSON.serialize(extQuote);
	}
	
	 public EditableTableQuoteControllerExtension(ApexPages.StandardController stdController) {
    	extQuote = (Quote) stdController.getRecord();
    	refresh();
    	quoteType = (extQuote != null && extQuote.Id != null)? QuoteTools.determineQuoteType(extQuote.QuoteType__c) : GlobalConstants.QuoteTypes.None;
    	showForCBC = quoteType == GlobalConstants.QuoteTypes.CBC? true : false;
    	showForSetBuy = ((quoteType == GlobalConstants.QuoteTypes.CustomerQuote) || (quoteType == GlobalConstants.QuoteTypes.SetRequest)) ? true : false;
    	showForSP = quoteType == GlobalConstants.QuoteTypes.SurgeonPreference ? true : false;
    	showForConSetBuild = quoteType == GlobalConstants.QuoteTypes.SetRequest ? true : false;
    	showForChangeSet = quoteType == GlobalConstants.QuoteTypes.ChangeSet ? true : false;
    	showForLoanSetRequest = quoteType == GlobalConstants.QuoteTypes.LoanSetRequest ? true : false;
    }
    
	public Boolean getShowApprovalHistory() {
		if (showForSetBuy && (extQuote.Status == GlobalConstants.QS_STATUS_DRAFT || extQuote.Status == GlobalConstants.QS_STATUS_PRICE_CONFIRMED )) {
			return false;
		}
		return true;
	}
	
	// Getter to determine when the Save button is disabled - also used to determine whether Customer PO number is editable or not
	public Boolean getSaveButtonDisabledStatus() {
		//system.debug('>>>>>>>>>>>>>>>>>>>>savebuttondisabledstatus, status, customer approval : ' + extQuote.Status + extQuote.Customer_Approval__c);
		Boolean isDisabled = false;
		if (showForSetBuy) { 
			if (extQuote.Status == Globalconstants.QS_STATUS_COMPLETED_WON || extQuote.Status == Globalconstants.QS_STATUS_COMPLETED_LOST ||
				extQuote.Status == Globalconstants.QS_STATUS_SUBMITTED_FOR_PRICING) {
				isDisabled = true;
			}
		}
		if ((showForCBC || showForSP) && (extQuote.Status != GlobalConstants.QS_STATUS_DRAFT && extQuote.Status != GlobalConstants.QS_STATUS_PRICE_CONFIRMED)) {
			isDisabled = true;
		}
		return isDisabled;
	}
	
	// Getter to determine when the Revise button is disabled 
	public Boolean getReviseButtonDisabled() {
		Boolean isDisabled = true;
		if (showForSetBuy) { 
			if (((extQuote.Status == Globalconstants.QS_STATUS_COMPLETED || 
				extQuote.Status == Globalconstants.QS_STATUS_APPROVED))||(extQuote.Status == Globalconstants.QS_STATUS_REJECTED)) {
				isDisabled = false;
			}
		}
		else if((showForCBC || showForSP) && (extQuote.Status == Globalconstants.QS_STATUS_REJECTED))
		{
			isDisabled = false;
		}
		return isDisabled;
	}
	
	public void updateQuote() {
    	try {
    		Database.SaveResult r = Database.update(extQuote);
    	}
    	catch (Exception e) {
    		addMessagesForVFPage(e.getMessage());
	    }
    }

	// Quote is re-retrieved. This method is a way for the JS prgs to read the quote, via the VF page
    public PageReference updateViewStateHeader() {
    	refresh();
    	return null;
    }
    
    // Save button pressed
    public PageReference saveQuoteHeaderChanges() {
		updateQuote();
		refresh();
		return null;
    }
    
    // Save button pressed
    public PageReference saveInstructions() {
		updateQuote();
		refresh();
		return null;
    }
    
    //Get Prices button pressed
    public PageReference submitQuoteWithAllQuoteLinesForPricing(){
    	try {
    		updateQuote();
    		QuoteTools.submitQuoteWithAllQuoteLinesForPricing(extQuote.id);
			refresh();
		}
	 	catch (Exception e) {
	 		addMessagesForVFPage(e.getMessage()); 
	 	}
	 	return null;
    }
     
    // Select Approvers button pressed
	public PageReference selectApprovers() {
    	try {
    		if (extQuote.ExpirationDate == null) {
    			if (quoteType == GlobalConstants.QuoteTypes.CBC) {
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.QE_CBC_AGREEMENT_TO_DATE_REQUIRED));
    			} else if (quoteType == GlobalConstants.QuoteTypes.SurgeonPreference) {
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.QE_SP_VALID_TO_DATE_REQUIRED));
    			}
				return null;
    		}
    		
    		if (quoteType == GlobalConstants.QuoteTypes.CBC || quoteType == GlobalConstants.QuoteTypes.SurgeonPreference){
    			//before an SP or CBC uis submitted to approval the system should do a validation step to ensure there are no discountinued items on the agreement
    			Boolean isValidQuote = QuoteTools.validateQuote(extQuote.id);
    			if (isValidQuote == false){
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Quote Validation failed! One or more Item(s) in this Quote have failed to pass validation. Please check Processlog for more details.'));
    				return null;
    			}
    		}
    		updateQuote();
    		pagereference page= new PageReference('/apex/Approval_Picker?id=' + extQuote.id); 
    		page.setRedirect(true); 
    		return page; 
		}
	 	catch (Exception e) {
        	addMessagesForVFPage(e.getMessage()); 
	 		return null;
	 	}
    }
    
    // Revise button pressed
    public PageReference reviseQuote() {
    	try {
    		pagereference page= new PageReference('/apex/ReviseQuote?id=' + extQuote.id);
    		page.setRedirect(true); 
    		return page; 
		}
	 	catch (Exception e) {
        	addMessagesForVFPage(e.getMessage()); 
	 	}
	 	return null;
    }
    
    // Won button pressed (Called Process to ERP)
    public PageReference wonQuote() {
		try {
			if (StringUtils.isNullOrEmpty(extQuote.Customer_Purchase_Order_Number__c)) {
				addMessagesForVFPage('Customer Purchase Order number must be entered before sending this quote to ERP');
			} else {
				updateQuote();
	    		QuoteTools.sendCustomerQuoteToSAP(extQuote.id);
				refresh();
			}
		}
	 	catch (Exception e) {
			addMessagesForVFPage(e.getMessage()); 
	 	}
	 	return null;
    }
    
    // Lost button pressed
	public PageReference lostQuote() {
    	extQuote.Status = GlobalConstants.QS_STATUS_COMPLETED_LOST;
		updateQuote();
		refresh();
		return null;
    }
    
    // CBC Send to ERP button pressed
    public PageReference cbcSendToERP() {
		try {
			updateQuote();
    		QuoteTools.sendCBCToSAP(extQuote.id);
			refresh();
		}
	 	catch (Exception e) {
	 		addMessagesForVFPage(e.getMessage()); 
	 	}
	 	return null;
    }
    
    // SP Send to ERP button pressed
    public PageReference spSendToERP() {
		try {
			updateQuote();
    		QuoteTools.sendSurgeonPrefernceToSAP(extQuote.id);
			refresh(); 
		}
	 	catch (Exception e) {
	 		addMessagesForVFPage(e.getMessage());  
	 	}
	 	return null;
    }
    /**
    Consolidation of spSendToERP , cbcSendToERP & wonQuote into
    submitToERP
    */
     public PageReference submitToERP() {
		try {
			if(extQuote.QuoteType__c=='SurgeonPreference')
			{
    			QuoteTools.sendSurgeonPrefernceToSAP(extQuote.id);
			}
			else if(extQuote.QuoteType__c=='CBC')
			{
    			QuoteTools.sendCBCToSAP(extQuote.id);
			}
			else if(extQuote.QuoteType__c=='CustomerQuote')
			{
				if (StringUtils.isNullOrEmpty(extQuote.Customer_Purchase_Order_Number__c)) {
                    throw new ApplicationException(Label.ERR_MSG_1);
					//extQuote.Customer_Purchase_Order_Number__c.addError(Label.ERR_MSG_1);
                    //return null;
				} 
				else
				{
					updateQuote();
    				QuoteTools.sendCustomerQuoteToSAP(extQuote.id);
				}
			}
			refresh(); 
		}
	 	catch (Exception e) {
	 		addMessagesForVFPage(e.getMessage());  
	 	}
	 	return null;
    }
    
	// Copy button pressed
	public PageReference copyQuote() {
    	try {
    		pagereference page= new PageReference('/apex/CopyQuote?id=' + extQuote.id); 
    		page.setRedirect(true); 
    		return page; 
		}
	 	catch (Exception e) {
			addMessagesForVFPage(e.getMessage()); 
	 		return null;
	 	}
    }
    
    // My Settings button pressed
    public PageReference navigateToUserPreferences() {
    	try {
    		pagereference page= new PageReference('/apex/quoteUserPreference?Name=quoteeditor?id=' + extQuote.id); 
    		page.setRedirect(true); 
    		return page; 
		}
	 	catch (Exception e) {
			addMessagesForVFPage(e.getMessage());  
	 		return null;
	 	}
    }

	@RemoteAction
    public static Map<String, Object> addSetToSet(String quoteId, String setClassId, String groupId) {  	
    	Integer duplicateCount = 0;
    	
    	Id theQuoteId = quoteId;
    	Id theSetClassId = setClassId;
    	Id theGroupId = groupId;
    	QuoteGroupLineItemsPopulator groupLinePopulator = new QuoteGroupLineItemsPopulator(theQuoteId, theSetClassId, theGroupId);
    	Boolean isSuccessAddBOMtoBOM = groupLinePopulator.runAddBOM2BOM();
    	if (!isSuccessAddBOMtoBOM) {
    		throw new ValueException('Merging BOM with existing BOM failed');
        	return null;
    	}
    	
    	
    	/*if (duplicateCount > 0) {
    		messages.add(String.format(Label.QE_ADD_SET_DUPLICATES,
    			new List<String> {''+duplicateCount, 'BOM Name'}));
    	}*/
    	
        BuildGroupData buildGrpData = new BuildGroupData();
        return new Map<String, Object>
        	{'group' 	=> buildGrpData.getGroupAndItems(quoteId, theGroupId),
        	 'messages'	=> groupLinePopulator.messagesToTheUser};
    }
     
	@RemoteAction
    public static Map<String, Object> addSet(String quoteId, String setClassId) {
    	Id newGroupId;
    	
        QuoteGroupLineItemsPopulator groupLinePopulator = new QuoteGroupLineItemsPopulator(quoteId, setClassId);
        String newGroup = groupLinePopulator.run();
        if (newGroup == null || String.IsEmpty(newGroup)) {
        	throw new ValueException('Adding BOM failed');
        	return null;
        } else {
        	newGroupId = (Id)newGroup;
        }
        
        BuildGroupData buildGrpData = new BuildGroupData();
        EditableTableGroup grp = buildGrpData.getGroupAndItems(quoteId, newGroupId);
       	return new Map<String, Object>
        	{'group' 	=> grp,
        	 'messages'	=> groupLinePopulator.messagesToTheUser};
        
    }
    
    @RemoteAction
     public static List<Set_Class_Items__c> bomIndicator(String productCode, String lastSetClassItemId, Integer noOfBatches) {
    	List<Set_Class_Items__c> items = new List<Set_Class_Items__c>();
    	Integer noOfbatchesProcessed = 0;
    	system.debug('In bomIndicator : ');
    	system.debug(productCode + ', ' + lastSetClassItemId + ', ' + noOfBatches);
    	List<Set_Class__c> setClasses = [SELECT Id, Name  FROM Set_Class__c  WHERE Name = :productCode LIMIT 1];
    	if (!setClasses.isEmpty() ) {
		
	    	for (List<Set_Class_Items__c> theSetClassItems : [Select Id, Product__r.Name, Product__r.ProductCode, Quantity__c, Product__r.Description
					    										From Set_Class_Items__c 
					    										WHERE Set_Class__c = :setClasses[0].Id
					    										AND Id > :lastSetClassItemId
					    										ORDER BY Id]) { 
	    	
	    		items = theSetClassItems;
	    		noOfbatchesProcessed++;
	    		if (noOfbatchesProcessed >= noOfBatches) {
			    	break;
			    }
	    	}
    	}
    	system.debug('>>>>>>>>>>> returning these results : ' + items);
    	return items;
    }
    
	 public void addMessagesForVFPage(String pErrorMessage){
		errorMessage = pErrorMessage;
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage));
	} 
	public PageReference deleteQuote()
	{
		/*CRUD has not been checked as all users will not have delete permission so that delete link does not get visible 
		in Standard page(to avoid customizing the standard pages) and  quote owner can delete the record if status in Draft 
		which has been enforced in UI*/
		try
		{
			String retURL = ApexPages.currentPage().getParameters().get('retURL');
			PageReference retPage = null;
			if(retURL<> null && retURL <> '')
			{
				retPage = new PageReference(retURL);
			}
			else
			{
				retPage = new PageReference('/'+extQuote.Opportunity.AccountId);			
			}
			//Opportunity linkedDefaultOpportunity = extQuote.Opportunity;
			delete extQuote;
			//delete the linked default opportunity
			//delete linkedDefaultOpportunity;
			retPage.setRedirect(true);
			return retPage;		
		}
		catch(Exception ex)	
		{
			ApexPages.addMessages(ex);
			return null;
		}
	}
	public List<SelectOption> getAllDefaultBUSelectOptions()
    {
        List<SelectOption> retList = new List<SelectOption>();
        Set<String> allowedDefaultBUOptions = Default_BU_List__c.getAll().keySet();
        for(String buName : allowedDefaultBUOptions)
        {
            SelectOption selOption = new SelectOption(buName,buName);
            retList.add(selOption);
        }
        return retList;
    }
	// ImportCsv button Click Redirect action
	public PageReference importFromCsv() {
    	try {
    		pagereference page= new PageReference('/apex/QuoteCSVImport?id=' + extQuote.id); 
    		page.setRedirect(true); 
    		return page; 
		}
	 	catch (Exception e) {
			addMessagesForVFPage(e.getMessage()); 
	 		return null;
	 	}
    }
	
}