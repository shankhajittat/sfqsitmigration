public with sharing class ObjectFieldsUserPreferenceBuilder {

	private Id user;
	private String objectName;
	private String objectSection;
	
	public ObjectFieldsUserPreferenceBuilder setUser(Id user)
	{
		this.user = user;
		return this;
	}
	
	public ObjectFieldsUserPreferenceBuilder setObjectName(String objectName)
	{
		this.objectName = objectName;
		return this;
	}
	
	public ObjectFieldsUserPreferenceBuilder setObjectSection(ObjectFieldsUserPreference.ObjectFieldsUserPreference_ObjectSections objectSection)
	{
		this.objectSection = objectSection.name();
		return this;
	}
	
	public ObjectFieldsUserPreferenceBuilder setObjectSection(String objectSection)
	{
		this.objectSection = objectSection;
		return this;
	}
	
	
	public Object_Fields_User_Preference__c build(Boolean createInDatabase)
	{
		Object_Fields_User_Preference__c up = new Object_Fields_User_Preference__c(
			User__c = this.user,
			Object_Name__c = this.objectName,
			Object_Section__c = objectSection
		);
						
		if (createInDatabase)
			insert up;
		
		return up;
	}
	
	
}