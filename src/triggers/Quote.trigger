trigger Quote on Quote (after delete, 
						after insert, 
						after update, 
						before delete, 
						before insert, 
						before update) {
	
	if (trigger.isBefore && trigger.isDelete == false){
		//system.debug('Quote Trigger B4 Old=>' + trigger.old);
		QuotePreparatoryActions quotePreparatoryActions = new QuotePreparatoryActions(trigger.new, trigger.old);	
		quotePreparatoryActions.autoPopulate();
		//system.debug('Quote Trigger B4 New=>' + trigger.new);
 	}
 	
 	if (trigger.isInsert && trigger.isAfter){
 		//Update Name Field with Custom Prefix and QuoteNumber
 		List<Quote> quotesToUpdate = new List<Quote>();
 		List<Opportunity> opportunitiesToUpdate = new List<Opportunity>();
 		for(Quote q : trigger.new){
 			//if it is not revising
 			if (q.Revised_From_Quote__c == null){
				String quotePrefix = CustomSettingsTools.getQuotePrefix(q.QuoteType__c);
				String quoteName = '';
				if (StringUtils.isNullOrWhiteSpace(quotePrefix)){
					quoteName = q.QuoteNumber;
				}
				else{
					quoteName = quotePrefix + '-' + q.QuoteNumber;
				}
				Quote uQ = new Quote();
				uQ.Id = q.Id;
				uQ.Name = quoteName;
				quotesToUpdate.add(uQ);
				
				if (q.OpportunityId != null){
					Opportunity oU = new Opportunity();
					oU.Id = q.OpportunityId;
					oU.Name = 'OPP-' + q.QuoteNumber;
					opportunitiesToUpdate.add(oU);
				}
			}
 		}
		system.debug('Update Quotes From Trigger=>' + quotesToUpdate);
		if (quotesToUpdate.size() > 0){
			update quotesToUpdate;
		}
		system.debug('Update Opportunities From Trigger=>' + opportunitiesToUpdate);
		if (opportunitiesToUpdate.size() > 0){
			update opportunitiesToUpdate;
		}
		
		//Copy Quote details into Custom Quote Object.
 		QuoteShadowTools qst = new QuoteShadowTools();
 		qst.createShadowRecords(trigger.new);
 		//system.debug('Quote Trigger A4 New=>' + trigger.new);
 	}
 	system.debug('Quote Trigger trigger.isInsert=>' +  trigger.isInsert );
 	system.debug('Quote Trigger trigger.isAfter=>' + trigger.isAfter);
 	system.debug('Quote Trigger trigger.isBefore=>' + trigger.isBefore);
 	system.debug('Quote Trigger trigger.isUpdate=>' +  trigger.isUpdate );
 	system.debug('Quote Trigger Old=>' +  trigger.old );
 	system.debug('Quote Trigger New=>' + trigger.new);
 	
}