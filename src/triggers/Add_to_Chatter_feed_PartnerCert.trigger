trigger Add_to_Chatter_feed_PartnerCert on Partner_Certification__c (before insert) {
 List<Partner_Certification__c > partnerCerts = trigger.new;
    for(Partner_Certification__c partnerCert: partnerCerts){
        string contactSFDCId =  partnerCert.Contact__c;
        string certSFDCId =  partnerCert.NECertification__c;
        
        Contact C;   
        C = [SELECT Name FROM Contact WHERE Id = :contactSFDCId];       
        string cName = C.Name;
        
        NECertification__c cert;
        cert = [SELECT Name FROM NECertification__c WHERE Id = :certSFDCId];
        string certName = cert.Name;
        
        FeedItem fItem = new FeedItem();
        fItem .body = 'Congratulations to  ' + cName  + ' for passing Certification ' + certName ;
        fItem .parentid = Userinfo.getuserid();
        Database.insert(fItem ,false);
    }
}