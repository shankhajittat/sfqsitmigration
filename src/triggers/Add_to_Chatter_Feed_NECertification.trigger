trigger Add_to_Chatter_Feed_NECertification on NECertification__c (before insert) {
List<NECertification__c > NECerts = trigger.new;
    for(NECertification__c NECert : NECerts){
        FeedItem fItem = new FeedItem();
        fItem.body = 'New Certification is Created: ' + NECert.Name;
        fItem.parentid =  Userinfo.getuserid();
        Database.insert(fItem,false);
    }
}