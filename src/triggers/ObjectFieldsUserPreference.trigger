trigger ObjectFieldsUserPreference on Object_Fields_User_Preference__c (after delete,
																		after insert,
																		after update, 
																		before delete,
																		before insert,
																		before update) {

	
	if (trigger.isBefore && trigger.isDelete == false){
		//Set Name field as User (Id / Name ???) + Object Name + Object Section
		if (trigger.isBefore && trigger.isUpdate == false)
			{
				for(Object_Fields_User_Preference__c up : Trigger.new)
				{
					if (up.Object_Name__c != null && up.User__c != null && up.Object_Section__c != null)
					{
						up.Name = up.Object_Name__c + '-' + up.User__c + '-' + up.Object_Section__c;
					}
				}	
			}
		else if (trigger.isBefore && trigger.isUpdate == true)
			{
				for(Object_Fields_User_Preference__c up : Trigger.new)
				{
					if (up.Object_Name__c != Trigger.oldMap.get(up.Id).Object_Name__c
						|| up.User__c != Trigger.oldMap.get(up.Id).User__c 
						|| up.Object_Section__c != Trigger.oldMap.get(up.Id).Object_Section__c)
					{
						up.Name = up.Object_Name__c + '-' + up.User__c + '-' + up.Object_Section__c;
					}
				}	
			}		
	}
	

}