trigger TSynthesJP_ORScheduleEvent on Dr_OR_Schedule__c (after insert) {
	Event[] bulkEvent = new Event[0];
	
	for (Dr_OR_Schedule__c thisORSched : Trigger.new) {
		if (thisORSched.Synthes_Staff_Attending__c == 'Yes') {
			Integer duration = 0;
			if (thisORSched.Est_Duration__c == '1 hour') {
				duration = 60;
			} 
			else if (thisORSched.Est_Duration__c == '2 hours') {
				duration = 120;
			} 
			else if (thisORSched.Est_Duration__c == '3 hours') {
				duration = 180;
			} 
			else if (thisORSched.Est_Duration__c == '4 hours') {
				duration = 240;
			} 
			else if (thisORSched.Est_Duration__c == '5 hours') {
				duration = 300;
			} 
			else if (thisORSched.Est_Duration__c == '6 hours') {
				duration = 360;
			} 
			else if (thisORSched.Est_Duration__c == '7 hours') {
				duration = 420;
			} 
			else if (thisORSched.Est_Duration__c == '8+ hours') {
				duration = 480;
			} 
			
			if (thisORSched.RecordTypeId == '012200000000fCj') {
				Event thisEvent = new Event();
				thisEvent.RecordTypeId = '012200000000TJT'; //Japan Event
				thisEvent.Subject = '????'; //'Attend OR with Dr.';
				thisEvent.WhoId = thisORSched.Dr__c;
				thisEvent.WhatId = thisORSched.Hospital__c;
				thisEvent.ActivityDateTime = thisORSched.Date_Time__c;
				thisEvent.Location = thisORSched.OR_Room__c; 
				thisEvent.DurationInMinutes = duration;
				bulkEvent.add(thisEvent);
			} 
			if (thisORSched.RecordTypeId == '012200000000fCt') {
				Event thisEvent = new Event();
				thisEvent.RecordTypeId = '012200000000cCW'; //India Event
				thisEvent.Subject = 'Attend OR with Dr.';
				thisEvent.WhoId = thisORSched.Dr__c;
				thisEvent.WhatId = thisORSched.Hospital__c;
				thisEvent.ActivityDateTime = thisORSched.Date_Time__c;
				thisEvent.Location = thisORSched.OR_Room__c; 
				thisEvent.DurationInMinutes = duration;
				bulkEvent.add(thisEvent);
			} 
		}
	}
	
    try {
    	Database.SaveResult[] upsertResults = Database.Insert(bulkEvent, true);
	} catch(DmlException e) {
    	System.debug( 'DML Exception Caught In Bulk Upsert: ' + e );
	}
}