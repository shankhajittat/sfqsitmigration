trigger QuoteLineItemTrigger on QuoteLineItem (after delete,
												after insert,
												after update, 
												before delete,
												before insert,
												before update) {
	
	if (trigger.isAfter && trigger.isDelete == false){
		//system.debug('QuoteLine Trigger B4 Old=>' + trigger.old);
		QuoteLineGroupRollups rollupQuoteLines = new QuoteLineGroupRollups(trigger.new);
		boolean isUpdated = rollupQuoteLines.rollupQuoteLines();
		//system.debug('QuoteLine Trigger B4 New=>' + trigger.new);
	}

	if (trigger.isAfter && trigger.isDelete == true){
		//system.debug('QuoteLine Trigger A4 Old=>' + trigger.old);
		QuoteLineGroupRollups rollupQuoteLinesDelete = new QuoteLineGroupRollups(trigger.old);
		boolean isUpdated_Delete = rollupQuoteLinesDelete.rollupQuoteLines();
		//system.debug('QuoteLine Trigger A4 New=>' + trigger.new);
	}
	
	
	
}