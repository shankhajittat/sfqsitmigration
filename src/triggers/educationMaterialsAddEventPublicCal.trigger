trigger educationMaterialsAddEventPublicCal on Education_Materials__c (after delete, after insert, after update) {

	educationMaterialsAddEventPublicCal procEdMat = new educationMaterialsAddEventPublicCal();

	if(Trigger.isDelete) {
		
		procEdMat.processEducationEventDelete(Trigger.Old);	
	
	} 
	
	if(Trigger.isInsert) {
		
		procEdMat.processEducationEventInsert(Trigger.New);	
	}

	if(Trigger.isUpdate) {
		
		procEdMat.processEducationEventUpdate(Trigger.New, Trigger.Old);	
	}
}