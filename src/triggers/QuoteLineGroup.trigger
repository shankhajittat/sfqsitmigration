trigger QuoteLineGroup on Quote_Line_Group__c (	after delete, 
												after insert, 
												after update, 
												before delete, 
												before insert, 
												before update) {
	
	
	if (trigger.isBefore && trigger.isUpdate){
		//Update the Group totals to reflect if the user has updated NumberOfBOMsRequired
		for(Quote_Line_Group__c groupItem : trigger.new){
			double newNumberOfBOMsRequired = groupItem.NumberOfBOMsRequired__c;
			double oldNumberOfBOMsRequired = trigger.oldMap.get(groupItem.Id).NumberOfBOMsRequired__c;
			if (newNumberOfBOMsRequired != oldNumberOfBOMsRequired){
				
				oldNumberOfBOMsRequired = oldNumberOfBOMsRequired != null && oldNumberOfBOMsRequired > 0 
											? oldNumberOfBOMsRequired : 1;
											
				newNumberOfBOMsRequired = newNumberOfBOMsRequired != null && newNumberOfBOMsRequired > 0 
											? newNumberOfBOMsRequired : 1;
				
				
				double oldLineGroup_Total_Discount_Amount = trigger.oldMap.get(groupItem.Id).LineGroup_Total_Discount_Amount__c;
				double oldLineGroup_Total_Extended_UnitPrice = trigger.oldMap.get(groupItem.Id).LineGroup_Total_Extended_UnitPrice__c;
				double oldLineGroup_Total_GrossMargin_Amount = trigger.oldMap.get(groupItem.Id).LineGroup_Total_GrossMargin_Amount__c;
				double oldLineGroup_Total_NetMargin_Amount = trigger.oldMap.get(groupItem.Id).LineGroup_Total_NetMargin_Amount__c;
				double oldLineGroup_Total_Landed_Cost = trigger.oldMap.get(groupItem.Id).LineGroup_Total_Landed_Cost__c;
				double oldLineGroup_Total_Standard_Cost = trigger.oldMap.get(groupItem.Id).LineGroup_Total_Standard_Cost__c;
				double oldLineGroup_TotalPrice_ExTax = trigger.oldMap.get(groupItem.Id).LineGroup_TotalPrice_ExTax__c;
				double oldLineGroup_TotalPrice_IncTax = trigger.oldMap.get(groupItem.Id).LineGroup_TotalPrice_IncTax__c;
				double oldLineGroup_Total_Quantity = trigger.oldMap.get(groupItem.Id).LineGroup_Total_Quantity__c;
				double oldLineGroup_Total_Tax = trigger.oldMap.get(groupItem.Id).LineGroup_Total_Tax__c;
				double oldLineGroup_TotalPrice_ImpMaterialGroup = trigger.oldMap.get(groupItem.Id).LineGroup_TotalPrice_ImpMaterialGroup__c;
				double oldLineGroup_TotalPrice_InsMaterialGroup = trigger.oldMap.get(groupItem.Id).LineGroup_TotalPrice_InsMaterialGroup__c;
				double oldLineGroup_TotalPrice_OtherMaterialGroup = trigger.oldMap.get(groupItem.Id).LineGroup_TotalPrice_OtherMaterialGroup__c;
				
				double oldLineGroup_TotalStdCost_ImpMaterialGroup = trigger.oldMap.get(groupItem.Id).LineGroup_TotalStdCost_ImpMaterialGroup__c;
				double oldLineGroup_TotalStdCost_InsMaterialGroup = trigger.oldMap.get(groupItem.Id).LineGroup_TotalStdCost_InsMaterialGroup__c;
				double oldLineGroup_TotalStdCost_OtherMaterialGroup = trigger.oldMap.get(groupItem.Id).LineGroup_TotalStdCost_OtherMaterialGrou__c;
				
				
				groupItem.NumberOfBOMsRequired__c = newNumberOfBOMsRequired;
				groupItem.LineGroup_Total_Discount_Amount__c = oldLineGroup_Total_Discount_Amount != null ? 
																		((oldLineGroup_Total_Discount_Amount / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_Total_Extended_UnitPrice__c = oldLineGroup_Total_Extended_UnitPrice != null ? 
																		((oldLineGroup_Total_Extended_UnitPrice / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_Total_GrossMargin_Amount__c = oldLineGroup_Total_GrossMargin_Amount != null ? 
																		((oldLineGroup_Total_GrossMargin_Amount / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_Total_NetMargin_Amount__c = oldLineGroup_Total_NetMargin_Amount != null ? 
																	((oldLineGroup_Total_NetMargin_Amount / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_Total_Landed_Cost__c = oldLineGroup_Total_Landed_Cost != null ? 
																	((oldLineGroup_Total_Landed_Cost / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_Total_Standard_Cost__c = oldLineGroup_Total_Standard_Cost != null ? 
																	((oldLineGroup_Total_Standard_Cost / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalPrice_ExTax__c = oldLineGroup_TotalPrice_ExTax != null ? 
																((oldLineGroup_TotalPrice_ExTax / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalPrice_IncTax__c = oldLineGroup_TotalPrice_IncTax != null ? 
																((oldLineGroup_TotalPrice_IncTax /  oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_Total_Quantity__c = oldLineGroup_Total_Quantity != null ? 
																((oldLineGroup_Total_Quantity  / oldNumberOfBOMsRequired)  * newNumberOfBOMsRequired) : 0;
				groupItem.LineGroup_Total_Tax__c = oldLineGroup_Total_Tax != null ? 
														((oldLineGroup_Total_Tax / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalPrice_ImpMaterialGroup__c = oldLineGroup_TotalPrice_ImpMaterialGroup != null ? 
																			((oldLineGroup_TotalPrice_ImpMaterialGroup / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalPrice_InsMaterialGroup__c = oldLineGroup_TotalPrice_InsMaterialGroup != null ? 
																			((oldLineGroup_TotalPrice_InsMaterialGroup / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalPrice_OtherMaterialGroup__c = oldLineGroup_TotalPrice_OtherMaterialGroup  != null ? 
																			((oldLineGroup_TotalPrice_OtherMaterialGroup / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
																			
				groupItem.LineGroup_TotalStdCost_ImpMaterialGroup__c = oldLineGroup_TotalStdCost_ImpMaterialGroup != null ? 
																			((oldLineGroup_TotalStdCost_ImpMaterialGroup / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalStdCost_InsMaterialGroup__c = oldLineGroup_TotalStdCost_InsMaterialGroup != null ? 
																			((oldLineGroup_TotalStdCost_InsMaterialGroup / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
				groupItem.LineGroup_TotalStdCost_OtherMaterialGrou__c = oldLineGroup_TotalStdCost_OtherMaterialGroup  != null ? 
																			((oldLineGroup_TotalStdCost_OtherMaterialGroup / oldNumberOfBOMsRequired) * newNumberOfBOMsRequired) : 0.00;
																			
			}
		}
	}
	
	if (trigger.isBefore && trigger.isDelete == true){
		QuoteLineGroupBeforeDelete deleteGroups = new QuoteLineGroupBeforeDelete(trigger.old, trigger.oldMap);
		boolean isDeleted = deleteGroups.run();
	}										
													
	if (trigger.isAfter && trigger.isDelete == false){
		QuoteRollups rollupQuoteLineGroups = new QuoteRollups(trigger.new);
		boolean isUpdated = rollupQuoteLineGroups.rollupQuoteLineGroups();
	}

	if (trigger.isAfter && trigger.isDelete == true){
		QuoteRollups rollupQuoteLineGroups = new QuoteRollups(trigger.old);
		boolean isUpdated_Delete = rollupQuoteLineGroups.rollupQuoteLineGroups();
	}												

}