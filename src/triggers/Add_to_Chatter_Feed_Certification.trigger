trigger Add_to_Chatter_Feed_Certification on Certification__c (before insert) {
List<Certification__c> NECerts = trigger.new;
    for(Certification__c NECert : NECerts){
        FeedItem fItem = new FeedItem();
        fItem.body = 'New Certification is Created: ' + NECert.Name;
        fItem.parentid =  Userinfo.getuserid();
        Database.insert(fItem,false);
    }
}